/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_ProductReoort]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_ProductReoort] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- execute [SP_ProductReoort] 1,null,null,null,null,null,null,null,null,null,null,null
 -- execute SP_ProductReoort 1,null,null,null,null,null,null,null,null,null,null,0

Create PROCEDURE [dbo].[SP_ProductReoort]   
	(
	@Block int,
	@ProductId int=null,
	@StockFrom int=null,
	@StockTo int=null,
	@CatagoryId int=null,
	@SubCategoryId int=null,
	@BrandId int=null,
	@UOMId int=null,
	@SizeId int=null,
	@PriceFrom int=null,
	@PriceTo int=null,
	@Image bit = 0
	)
AS
BEGIN
If(@Block=1)
BEGIN
select p.Id,p.Name,p.Code,u.UOMName,b.BrandName ProductBrandName,c.ColorName ProductColorName,siz.SizeName ProductSizeName,ca.CatagoryName
from dbo.Products p
	 left JOIN dbo.Colors c on c.Id=p.ColorId
	 left JOIN dbo.SubCategories s on s.Id=p.SubCatagoryId
	 left JOIN dbo.Brands b on b.id=p.ProductBrandId
	 left join dbo.UOMs u on u.Id=p.UOMId
	 left join dbo.Sizes siz on siz.Id=p.SizeId
	 left join [dbo].[Categories] ca on ca.id=s.CatagoryId
where       (@ProductId is null or p.Id=@ProductId)
		and (@BrandId is null or p.ProductBrandId=@BrandId)
		and (@SubCategoryId is null or p.SubCatagoryId=@SubCategoryId)
		and (@CatagoryId is null or ca.Id=@CatagoryId)
		and (@UOMId is null or p.UOMId=@UOMId)
		and (@SizeId is null or p.SizeId=@SizeId) 
		and (@StockFrom is null or p.Quantity>=@StockFrom)
		and (@StockTo is null or p.Quantity<=@StockTo)
		and (@PriceFrom is null or p.SalsPrice >=@PriceFrom)
		and (@PriceTo is null or p.SalsPrice <=@PriceTo) 
		and ((@Image=1 and  p.Image IS NOT NULL)OR(@Image=0 and p.Image IS NULL))
order by p.[Name] asc
END

END



