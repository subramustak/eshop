/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_Stock]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_Stock] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --execute [AllSectionClassWise] 10,1
Create PROCEDURE [dbo].[SP_Stock]  
	(
	@ProductId int=null,
	@BranchId int=null
	)
AS
BEGIN
select st.*,P.Code +'~'+p.Name ProductName from dbo.Stocks st
INNER JOIN VM_ProductDetail P on p.Id=st.ProductId
where st.ProductId =ISNULL(@ProductId,st.ProductId) and st.BranchId =ISNULL(@BranchId,st.BranchId)
END

