/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_Transaction]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_Transaction] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --execute [SP_Transaction] 4
Create PROCEDURE [dbo].[SP_Transaction]   
	(
	@Block int,
	@AccountId int=null,
	
	@startdate date=null,
	@enddate date=null
	
	)
AS
BEGIN
Declare @Start varchar(max) 
, @End varchar(max) 
Set @Start= convert(varchar, @startdate, 103)
Set @End= convert(varchar, @enddate, 103)

If(@Block=1) -- Transaction
BEGIN
select a.Name as AccountName , t.*from dbo.TransectionHistories t
join AccountNames a on a.Id=t.AccountId
where t.AccountId=@AccountId
END
If(@Block=2) -- Transaction
BEGIN
select a.Name as AccountName , t.*from dbo.TransectionHistories t
join AccountNames a on a.Id=t.AccountId
where (@start is null or convert(varchar, [date], 103)>=@start) 
and (@end is null or convert(varchar, [date], 103) <=@end)and t.AccountId=ISNULL(@AccountId,t.AccountId)
END
If(@Block=3) -- Transaction Payment
BEGIN
select AccountId, a.Name as AccountName ,Sum(t.PaymentAmount) as Amount from dbo.TransectionHistories t
join AccountNames a on a.Id=t.AccountId
where (@start is null or convert(varchar, [date], 103)>=@start) 
and (@end is null or convert(varchar, [date], 103) <=@end)   and t.IsPayment=1 and t.AccountId=ISNULL(@AccountId,t.AccountId)
group by AccountId,a.Name
END
If(@Block=4) -- Transaction receive
BEGIN
select AccountId, a.Name as AccountName , Sum(t.ReciveAmount) as Amount from dbo.TransectionHistories t
join AccountNames a on a.Id=t.AccountId

where (@start is null or convert(varchar, [date], 103)>=@start) 
and (@end is null or convert(varchar, [date], 103) <=@end)  and t.IsRecive=1 and t.AccountId=ISNULL(@AccountId,t.AccountId)
group by AccountId,a.Name
END
If(@Block=5) -- Transaction Payment
BEGIN
select a.Name as AccountName , t.*from dbo.TransectionHistories t
join AccountNames a on a.Id=t.AccountId
where (@start is null or convert(varchar, [date], 103)>=@start) 
and (@end is null or convert(varchar, [date], 103) <=@end)   and t.AccountId=ISNULL(@AccountId,t.AccountId)
Order By t.Date desc
END
If(@Block=6) -- Transaction receive
BEGIN
select a.Name as AccountName , t.*from dbo.TransectionHistories t
join AccountNames a on a.Id=t.AccountId

where (@start is null or convert(varchar, [date], 103)>=@start) 
and (@end is null or convert(varchar, [date], 103) <=@end) and t.IsRecive=1  and t.IsRecive=1 and t.AccountId=ISNULL(@AccountId,t.AccountId)
END
END
