/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_PurchaseReport]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_PurchaseReport] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- execute [SP_PurchaseReport] 1,'00001',null,null,null,null,null,null
 
alter PROCEDURE [dbo].[SP_PurchaseReport]   
	(
	@Block int,
	@InvoiceNo varchar(15)=null,
	@SupplierId int=null,
	@BranchId int=null,
	@MaxPrice decimal=null,
	@MinPrice decimal=null,
	@startdate date=null,
	@enddate date=null
	)
AS
BEGIN
If(@Block=1)
BEGIN

select 
p.Id,p.[InvoiecNo],p.[TotalPaid] TotalPayment,s.Name,FORMAT(cast(p.Date as datetime),'dd/MM/yyyy', 'en-us') as Date
from [dbo].[Purchases] p
left join [dbo].[Suppliers] s on s.Id=p.SupplierId
where (@InvoiceNo is null or p.InvoiecNo=@InvoiceNo)
and (@SupplierId is null or p.SupplierId=@SupplierId)
and (@BranchId is null or p.BranchId=@BranchId)
and (@MinPrice is null or p.Total>=@MinPrice)
and (@MaxPrice is null or p.Total<=@MaxPrice)
and (@startdate is null or p.Date>=@startdate)
and (@enddate is null or p.Date<=@enddate) 
order by InvoiecNo

END

END



