/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_StockTransfarReport]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_StockTransfarReport] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- execute SP_StockTransfarReport 1,null,3,'03-26-2019','03-26-2019'

Create PROCEDURE [dbo].[SP_StockTransfarReport]   
	(
	@Block int,
	@FromBranchId int=null,
	@ToBranchId int=null,
	@from date=null,
	@to date=null
	)
AS
BEGIN
If(@Block=1)
BEGIN

select fb.[BranchName] FromBranch,tb.[BranchName] ToBranch,st.Quantity
,CASE WHEN IsApprove=1 THEN 'approved' WHEN IsApprove=0 THEN 'Panding'END Status
,RTRIM(pro.Code +' '+ pro.Name) as ProductName
,FORMAT(cast(p.Date as datetime),'dd/MM/yyyy', 'en-us') as Date
from dbo.StockTransfars st 
left join dbo.Branches fb on st.FromBranchId = fb.Id
left join dbo.Branches tb on st.ToBranchId = tb.Id
left join [dbo].[Products] pro on st.[ProductId]=pro.Id
	 
where  (@FromBranchId is null or st.FromBranchId =@FromBranchId)
and (@ToBranchId is null or st.ToBranchId=@ToBranchId)
and (@from is null or CONVERT(datetime,st.Date)>=@from) 
and (@to is null or CONVERT(datetime,st.Date) <=@to)
and st.Status='A'
END

END



