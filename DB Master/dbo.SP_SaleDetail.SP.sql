/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_SaleDetail]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_SaleDetail] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create PROCEDURE [dbo].[SP_SaleDetail]   
	(
	@Block int,
	@Id int=null,
	@SalesId int=null,
	@ProductId int=null,
	@UnitePrice int=null
	)
AS
BEGIN
If(@Block=1)
BEGIN
select sud.*,P.Name ProductName,P.Code ProductCode from
 dbo.SalesDetails sud 
JOIN dbo.Products p on p.Id=sud.ProductId
order by sud.Id desc
END
If(@Block=2)
BEGIN
select sud.*,P.Name ProductName,P.Code ProductCode from
 dbo.SalesDetails sud 
JOIN dbo.Products p on p.Id=sud.ProductId
where sud.Id=@Id
order by sud.Id desc
END
If(@Block=3)
BEGIN
select sud.*,P.Name ProductName,P.Code ProductCode,P.SalsPrice from
 dbo.SalesDetails sud 
JOIN dbo.Products p on p.Id=sud.ProductId

where sud.SalesId=@SalesId
order by sud.Id desc
END
END

