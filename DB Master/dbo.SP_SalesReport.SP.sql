/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_SalesReport]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_SalesReport] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --   exec SP_SalesReport 1,null,null,null,null,null
 --   exec SP_SalesReport 1,'000012',10,null,null,null
 --   exec SP_SalesReport 1,null,null,null,'2019-03-14T10:14:10.481Z','2019-03-24T10:14:10.481Z'

Create PROCEDURE [dbo].[SP_SalesReport]   
	(
	@Block int,
	@InvoiceNo varchar(15)=null,
	@CustomerId int=null,
	@BranchId int=null,
	@from date=null,
	@to date=null
	)
AS
BEGIN
If(@Block=1)
BEGIN
select s.InvoiecNo Invoice
, CASE 
	  WHEN S.PaymentType=1 Then 'Cash' 
	  WHEN S.PaymentType=2 Then 'Bank'  
	  WHEN S.PaymentType=3 Then 'Card' 
	  WHEN S.PaymentType=4 Then 'MFS' 
	  ELSE 'N/A'
  END PaymentType
,s.TotalPaid,FORMAT(cast(s.Date as datetime),'dd/MM/yyyy', 'en-us') as Date 
,c.[Name] CustomName,count(sd.id) as SalesQuantity,b.BranchName
from Sales s
inner join [dbo].[Customers] c on s.CustomerId=c.id
inner join [dbo].[SalesDetails] sd on s.Id=sd.SalesId
inner join [dbo].[Products] p on sd.ProductId=p.Id
inner join [dbo].[Branches] b on s.[BranchId]=b.Id

where (@InvoiceNo is null or s.InvoiecNo =@InvoiceNo)
and (@BranchId is null or s.BranchId =@BranchId)
and (@CustomerId is null or s.CustomerId=@CustomerId)
and (@from is null or s.date>=@from) 
--and (@to is null or s.date <=@to)
group by s.id,s.InvoiecNo,s.PaymentType,s.TotalPaid,s.[Date] ,c.[Name],b.BranchName
order by s.Id desc

END
END

