/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_GetProductbyBranchId]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_GetProductbyBranchId] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --  execute SP_GetProductbyBranchId 1,3,'eye'
Create PROCEDURE [dbo].[SP_GetProductbyBranchId]  
	(
	@BlockId int,
	@BranchId int,
	@Name varchar(max)
	)
AS
BEGIN
IF(@BlockId =1)
BEGIN
SELECT TOP 12 (RTRIM( P.Code) +' '+ P.Name ) AS [Text], CAST( p.Id AS varchar) AS [Value],s.Quantity,p.SalsPrice
		 FROM Products p INNER JOIN dbo.Stocks s on s.ProductId=p.Id
		 WHERE s.BranchId=@BranchId and  p.IsDeleted=0 and p.Status='A' and ( P.Code like '%'+@Name+'%' OR  P.Name like '%'+@Name+'%' )
END


END

