/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_Sales]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_Sales] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create PROCEDURE [dbo].[SP_Sales]   
	(
	@Block int,
	@Id int,
	@InvoiceNo varchar(max)=null,
	@SupplierId int=null,
	@startdate date=null,
	@enddate date=null
	
	)
AS
BEGIN
If(@Block=1)
BEGIN
select s.Id, s.InvoiecNo,s.Discount,s.Due,s.TotalPaid,s.Date,s.[Return],s.NetTotal,s.VAT,s.AddBy as [User], P.Name ProductName,P.Code ProductCode, sud.UnitePrice,sud.Quantity,sud.Amount,sud.NetAmount from dbo.Sales s
JOIN dbo.SalesDetails sud on sud.SalesId=s.Id
JOIN dbo.Products p on p.Id=sud.ProductId
where s.Id=@Id
order by s.Id desc
END
If(@Block=2)
BEGIN
select *from dbo.Sales 
where [Date] between @startdate and @enddate 
order by Id desc
END
If(@Block=3)
BEGIN
select S.*,C.Name CustomerName from dbo.Sales S
Left JOIN Customers c on c.Id=S.CustomerId
where Due >0
END
If(@Block=4)
BEGIN
select s.Id, s.InvoiecNo,s.Discount,s.Due,s.TotalPaid,s.Date,s.[Return],s.NetTotal,s.VAT,s.AddBy,s.CustomerId, C.Name CustomerName from dbo.Sales S
Left JOIN Customers c on c.Id=S.CustomerId
where s.Id=@Id
END
END

