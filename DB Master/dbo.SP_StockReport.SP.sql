/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_StockReport]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_StockReport] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- execute [SP_StockReport] 1,null,null,null,null,null

Create PROCEDURE [dbo].[SP_StockReport]   
	(
	@Block int,
	@ProductId INT=null,
	@BranchId int=null,
	@StockMin int=null,
	@StockMax int=null,
	@PriceMin int=null,
	@PriceMax int=null
	)
AS
BEGIN
If(@Block=1)
BEGIN
Select (p.Code+' '+ p.Name) ProductName,s.TotalPrice,s.UnitPrice,s.Quantity,CONVERT(DECIMAL(10,2),(s.UnitPrice*s.Quantity))as GrandTotal 
,p.SalsPrice,b.BranchName from dbo.Stocks s
INNER JOIN dbo.products p on p.Id=s.ProductId
INNER JOIN dbo.UOMs U on U.Id=p.UOMId
INNER JOIN dbo.Branches b on b.Id=s.BranchId
where s.ProductId=ISNULL(@ProductId,s.ProductId) 
and s.BranchId=ISNULL(@BranchId,s.BranchId)
and (@StockMin is null or s.Quantity>=@StockMin)
and (@StockMax is null or s.Quantity<=@StockMax)
and (@PriceMin is null or s.UnitPrice >=@PriceMin)
and (@PriceMax is null or s.UnitPrice <=@PriceMax)

END

END



