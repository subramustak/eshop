USE [eShop_DB]
truncate table [dbo].[AspNetPages]
GO
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(100,0,'Dashboard','Home','Index','fa fa-lg fa-fw fa-desktop',1,1,'Dashboard')
GO
-----------------------------SET UP
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(200,0,'SET UP',null,null,'fa fa-lg fa-fw fa-cogs',2,1,'Set Up')
GO
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2100,200,'Branch','MasterSetUp','Branch',null,1,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2200,200,'Color','MasterSetUp','Color',null,2,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2300,200,'Size','MasterSetUp','Size',null,3,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2400,200,'Brand','MasterSetUp','Brand',null,4,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2500,200,'Category','MasterSetUp','Category',null,5,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2600,200,'UOM','MasterSetUp','UOM',null,6,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2700,200,'SubCategory','MasterSetUp','SubCategory',null,7,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2800,200,'Customer','MasterSetUp','Customer',null,8,0,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2900,200,'Supplier','MasterSetUp','Supplier',null,9,1,'Set Up')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(2110,200,'Event/Discount','MasterSetUp','Event_Discount',null,10,0,'Set Up')
GO
---------------------------------------------Product Setup
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(300,0,'Product Setup',null,null,'fa fa-lg fa-fw fa fa-dropbox',3,1,'Product Setup')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(3100,300,'Product Add','Product','ProductAdd',null,1,0,'Product Setup')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(3200,300,'Product List','Product','Productlist',null,2,0,'Product Setup')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(3300,300,'Bulk Product Upload','Product','ProductSaveExcel',null,3,0,'Product Setup')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(3400,300,'Product Report','Product','ProductSearch',null,4,0,'Product Setup')
GO
-------------------------------------Purchease
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(400,0,'Purchease',null,null,'fa fa-lg fa-fw fa-shopping-cart',4,1,'Purchease')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(4100,400,'Stock In','Purchease','PurcheasePOS',null,1,0,'Purchease')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(4200,400,'Purchease List','Purchease','Purcheaselist',null,2,0,'Purchease')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(4300,400,'Purchease Return','Purchease','PurcheaseReturn',null,3,0,'Purchease')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(4400,400,'Purchease Return List','Purchease','PurcheaseReturnList',null,4,0,'Purchease')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(4500,400,'Purchease Report','Purchease','PurcheaseReport',null,5,0,'Purchease')
GO
---------------------------Sales
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(500,0,'Sales',null,null,'fa fa-lg fa-fw fa-shopping-bag',5,1,'Sales')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(5100,500,'Invoice List','Sales','SalesList',null,1,0,'Sales')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(5200,500,'SalesPOS','Sales','SalesPOS',null,2,0,'Sales')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(5300,500,'Sales Return','Sales','SalesReturn',null,3,0,'Sales')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(5400,500,'Sales Return List','Sales','SalesReturnList',null,4,0,'Sales')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(5500,500,'Salse Report','Sales','SalseReport',null,5,0,'Sales')
GO
----------------------------Stock
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(600,0,'Stock',null,null,'fa fa-lg fa-fw fa-line-chart',6,1,'Stock')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(6100,600,'Stock List','Stock','StockList',null,1,0,'Stock')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(6200,600,'Stock Report','Stock','StockReportAction',null,2,0,'Stock')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(6300,600,'Stock Transfer','Stock','StockTransfer',null,3,0,'Stock')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(6400,600,'Stock Transfer Report','Stock','StockTransferReport',null,4,0,'Stock')
GO
---------------Account
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(700,0,'Account',null,null,'fa fa-lg fa-fw fa-money',7,1,'Account')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(7100,700,'Account Reciveable','Accounting','AccountRecive',null,1,0,'Account')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(7200,700,'Account Payable','Accounting','AccountPayable',null,2,0,'Account')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(7300,700,'Account Create','Accounting','AccountCreate',null,3,0,'Account')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(7400,700,'Payment','Accounting','AccountPayment',null,4,0,'Account')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(7500,700,'Receipt','Accounting','AccountReceipt',null,5,0,'Account')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(7600,700,'Management','Accounting','AccountManagement',null,6,0,'Account')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(7700,700,'Account Recive Payment','Accounting','AccountRecivePayment',null,7,0,'Account')
GO
-----------------------------User Management
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(800,0,'User Management',null,null,'fa fa-lg fa-fw fa-users',8,1,'User Management')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(8100,800,'Roles List','Account','RolesList',null,1,0,'User Management')
GO
	INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(8200,800,'User Create','Account','UserCreate',null,2,0,'User Management')
GO
INSERT INTO [dbo].[AspNetPages]([PageID],[ParentID],[NameOption_En],[Controller],[Action],[IconClass],[Displayorder],[IsParent] ,[ModuleName])
     VALUES(900,0,'User Management','MasterSetUp','Organization','fa fa-lg fa-fw fa-gear',1,0,'MasterSetUp')
GO


