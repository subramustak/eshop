/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[VM_ProductDetail]'))
BEGIN
DROP PROCEDURE  VM_ProductDetail
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create VIEW VM_ProductDetail  AS
select p.*,brd.BrandName,c.ColorName,Sz.SizeName,U.UOMName from  dbo.Products P
LEFT JOIN dbo.Brands brd on brd.Id=p.ProductBrandId
LEFT JOIN dbo.Colors C on C.Id=p.ColorId
LEFT JOIN dbo.Sizes Sz on Sz.Id=p.SizeId
LEFT JOIN dbo.UOMs U on U.Id=p.UOMId




