GO
truncate table [AspNetPages]
GO
SET IDENTITY_INSERT [dbo].[AspNetPages] ON 
GO

 ----------------------------------------------------------------------------------------------------------------------------------------------------
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (2, 0, N'Set Up ', N'b', NULL, NULL, NULL, N'fa fa-graduation-cap', NULL, N'A', 1, 1)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (3, 0, N'Product Setup', N'b', NULL, NULL, NULL, N'fa fa-newspaper-o', NULL, N'A', 2, 1)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (4, 0, N'Purchease', N'b', NULL, NULL, NULL, N'fa fa-cog', NULL, N'A', 3, 1)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (5, 0, N'Sales', N'b', NULL, NULL, NULL, N'fa fa-user-plus', NULL, N'A', 5, 1)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (6, 0, N'Stock', N'b', NULL, NULL, NULL, N'fa fa-user', NULL, N'A', 4, 1)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (7, 0, N'Account', N'b', NULL, NULL, NULL, N'fa fa-user', NULL, N'A', 6, 1)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (8, 0, N'User Management', N'b', NULL, NULL, NULL, N'fa fa-user', NULL, N'A', 7, 1)
GO



---Set Up


INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (202, 2, N'Branch', N'b', N'MasterSetUp', N'Branch', NULL, N'fa fa-address-card-o', NULL, N'A', 1, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (203, 2, N'Color', N'b', N'MasterSetUp', N'Color',  NULL, N'fa fa-address-card-o', NULL, N'A', 2, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (204, 2, N'Size', N'b', N'MasterSetUp', N'Size',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (205, 2, N'Brand', N'b', N'MasterSetUp', N'Brand',  NULL, N'fa fa-address-card-o', NULL, N'A', 4, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (206, 2, N'Category', N'b', N'MasterSetUp', N'Category',  NULL, N'fa fa-address-card-o', NULL, N'A', 5, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (207, 2, N'SubCategory', N'b', N'MasterSetUp', N'SubCategory',  NULL, N'fa fa-address-card-o', NULL, N'A', 6, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (208, 2, N'Customer', N'b', N'MasterSetUp', N'Customer',  NULL, N'fa fa-address-card-o', NULL, N'A', 7, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (209, 2, N'Supplier', N'b', N'MasterSetUp', N'Supplier',  NULL, N'fa fa-address-card-o', NULL, N'A', 8, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (210, 2, N'Event_Discount', N'b', N'MasterSetUp', N'Event_Discount',  NULL, N'fa fa-address-card-o', NULL, N'A', 9, 0)
GO
---Product Setup
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (300, 3, N'ProductAdd', N'b', N'Product', N'ProductAdd',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (301, 3, N'Productlist', N'b', N'Product', N'Productlist',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
---Purchease
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (400, 4, N'Purcheaselist', N'b', N'Purchease', N'Purcheaselist',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (401, 4, N'PurcheasePOS', N'b', N'Purchease', N'PurcheasePOS',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
---Sales
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (500, 5, N'SalesList', N'b', N'Sales', N'Purcheaselist',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (501, 5, N'SalesPOS', N'b', N'Sales', N'PurcheasePOS',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
---Stock
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (600, 6, N'StockList', N'b', N'Stock', N'StockList',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO

---Account
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (700, 7, N'AccountCreate', N'b', N'Accounting', N'AccountCreate',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO																																														   
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (701, 7, N'AccountPayment', N'b', N'Accounting', N'AccountPayment',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO																																														   
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (702, 7, N'AccountReceipt', N'b', N'Accounting', N'AccountReceipt',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO																																														   
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (703, 7, N'AccountManagement', N'b', N'Accounting', N'AccountManagement',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
---User Management
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (800, 8, N'RolesList', N'b', N'Account', N'RolesList',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
INSERT [dbo].[AspNetPages] ([PageID], [ParentID], [NameOption_En], [NameOption_Bn], [Controller], [Action], [Area], [IconClass], [ActiveLi], [Status], [Displayorder], [IsParent]) VALUES (801, 8, N'UserCreate', N'b', N'Account', N'AccountManagement',  NULL, N'fa fa-address-card-o', NULL, N'A', 3, 0)
GO
---Account
SET IDENTITY_INSERT [dbo].[AspNetPages] OFF
GO
