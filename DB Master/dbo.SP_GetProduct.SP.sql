/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_GetProduct]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_GetProduct] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --execute [AllSectionClassWise] 10,1
Create PROCEDURE [dbo].[SP_GetProduct]  
	(
	@BlockId int,
	@Name varchar(max),
	@BranchId int=null
	)
AS
BEGIN
IF(@BlockId =1)
BEGIN
SELECT TOP 12 (RTRIM( P.Code) +' '+ P.Name ) AS [Text], CAST( p.Id AS varchar) AS [Value],p.Image
		 FROM Products p 
		 WHERE p.IsDeleted=0 and p.Status='A' and ( P.Code like '%'+@Name+'%' OR  P.Name like '%'+@Name+'%' )
END
IF(@BlockId =2)
BEGIN
SELECT TOP 12 (RTRIM( P.Code) +' '+ P.Name ) AS [Text], CAST( p.Id AS varchar) AS [Value],p.[Image],s.Quantity,p.SalsPrice
		 FROM Products p JOIN dbo.Stocks s on s.ProductId=p.Id
		 WHERE p.IsDeleted=0 and s.BranchId=@BranchId and p.Status='A' and ( P.Code like '%'+@Name+'%' OR  P.Name like '%'+@Name+'%' )
END
IF(@BlockId =3)
BEGIN
SELECT TOP 12 (RTRIM( P.Code) +' '+ P.Name ) AS [Text], CAST( p.Id AS varchar) AS [Value]
		 FROM Customers p 
		 WHERE p.IsDeleted=0 and p.Status='A' and ( P.Code like '%'+@Name+'%' OR  P.Name like '%'+@Name+'%' )
END

END

