/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_ProductReoort]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_ProductReoort] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- execute [SP_ProductReoort] 1,null,null,null,null,null,null,null,null,null,null
  -- execute [SP_ProductReoort] 1,null,null,null,null,null,null,null,1,null,null

Create PROCEDURE [dbo].[SP_ProductReoort]   
	(
	@Block int,
	@Name varchar(max)=null,
	@Code varchar(max)=null,
	@StockFrom int=null,
	@StockTo int=null,
	@CatagoryId int=null,
	@SubCategoryId int=null,
	@BrandId int=null,
	@UOMId int=null,
	@SizeId int=null,
	@PriceFrom int=null,
	@PriceTo int=null,
	@Image bit=false
	)
AS
BEGIN
If(@Block=1)
BEGIN
select p.Id,p.Name,p.Code,u.UOMName,b.BrandName ProductBrandName,c.ColorName,siz.SizeName
from dbo.Products p
left JOIN dbo.Colors c on c.Id=p.ColorId
left JOIN dbo.SubCategories s on s.Id=p.SubCatagoryId
left JOIN dbo.Brands b on b.id=p.ProductBrandId
left join dbo.UOMs u on u.Id=p.UOMId
left join dbo.Sizes siz on siz.Id=p.SizeId
where p.Name=ISNULL(@Name,p.Name) and p.Code=ISNULL(@Code,p.Code) -- (@code is null or code = @code) and s.CatagoryId=ISNULL(@CatagoryId,s.CatagoryId)
and ((@StockFrom is not null and @StockTo is not null) or p.MinimumStock between ISNULL(@StockFrom,p.MinimumStock) and ISNULL(@StockTo,p.MinimumStock)) 
and p.SubCatagoryId=ISNULL(@SubCategoryId,p.SubCatagoryId)
and p.UOMId=ISNULL(@UOMId,p.UOMId) and p.SizeId=ISNULL(@SizeId,p.SizeId) 
and ((@PriceFrom is not null and @PriceTo is not null) or p.SalsPrice between ISNULL(@PriceFrom,p.SalsPrice) and ISNULL(@PriceTo,p.SalsPrice)) 
--and p.[Image] is not null
order by p.Name asc
END

END



