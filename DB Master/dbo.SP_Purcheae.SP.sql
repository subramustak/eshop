/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_Purchease]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_Purchease] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --execute [AllSectionClassWise] 10,1
Create PROCEDURE [dbo].[SP_Purchease]   
	(
	@Block int,
	@Id int=null,
	@InvoiceNo varchar(max)=null,
	@SupplierId int=null,
	@startdate date=null,
	@enddate date=null
	
	)
AS
BEGIN
If(@Block=1)
BEGIN
select pur.*,P.Name ProductName,P.Code ProductCode, pud.UnitePrice,pud.Quantity,pud.Amount,s.Name SupplierName,s.PermanentAddress  SupplierAddress,s.Mobile SupllierContact, s.Email  SupllierEmail from dbo.Purchases pur
JOIN dbo.PurcheaseDetails pud on pud.PurchaseId=pur.Id
JOIN dbo.Products p on p.Id=pud.ProductId
JOIN dbo.Suppliers S on S.Id=pur.SupplierId
where pur.Id=@Id
order by pur.Id desc
END
If(@Block=2)
BEGIN
select *from dbo.Purchases 
where [Date] between @startdate and @enddate 
order by Id desc
END
If(@Block=3)
BEGIN
select p.*, s.Name SupplierName from dbo.Purchases p
JOIN Suppliers s on s.Id=p.SupplierId
where Due >0
END
If(@Block=4)
BEGIN
select pur.*,P.Name ProductName,P.Code ProductCode, pud.UnitePrice,pud.Quantity,pud.TotalAmount,s.Name SupplierName,s.PermanentAddress  SupplierAddress,s.Mobile SupllierContact, s.Email  SupllierEmail from dbo.PurchaseReturns pur
JOIN dbo.PurcheaseDetailReturns pud on pud.PurchaseReturnId=pur.Id
JOIN dbo.Products p on p.Id=pud.ProductId
JOIN dbo.Suppliers S on S.Id=pur.SupplierId
where pur.Id=@Id
order by pur.Id desc
END
END




