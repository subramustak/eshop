/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_ProductforBarcode]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_ProductforBarcode] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- execute [SP_ProductforBarcode] 1,',1011,1009,28'

Create PROCEDURE [dbo].[SP_ProductforBarcode]   
	(
	@Block int,
	@ProductIIDs varchar(max)=null
	)
AS
BEGIN
If(@Block=1)
BEGIN
SELECT *  INTO #ProductIID
FROM [eShopUser].[Func_SplitString](@ProductIIDs, ',')

select p.Id,p.Name,p.Code,p.Barcode
from dbo.Products p
where p.Id  in(select * from #ProductIID)

DROP TABLE #ProductIID
END

END



