/****** Object:  StoredProcedure [dbo].[SP_Stock]    Script Date: 7/22/2017 4:26:07 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SP_Product]'))
BEGIN
DROP PROCEDURE  [dbo].[SP_Product] 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --execute [dbo].[SP_Product]  1 ,  'and  p.CatagoryId=1' ,null ,null ,null ,null ,null ,null ,null 
Create PROCEDURE [dbo].[SP_Product]  
	(
	@Block int,
	@Where varchar(max)=null
	)
AS
BEGIN
DECLARE @SQL varchar(max)
if(@Block =1)
BEGIN

set @SQL ='select  p.*,cr.ColorName,s.SizeName,c.CatagoryName,sc.SubCatagoryName,u.UOMName,bd.BrandName from dbo.Products p
LEFT outer JOIN dbo.Colors cr on cr.Id=p.ColorId
LEFT JOIN dbo.Sizes s on s.Id=p.SizeId
LEFT JOIN dbo.Categories c on c.Id=p.CatagoryId
LEFT JOIN dbo.SubCategories sc on sc.Id=p.SubCatagoryId
LEFT JOIN dbo.UOMs u on u.Id=p.UOMId
LEFT JOIN dbo.Brands bd on bd.Id=p.ProductBrandId
where  1=1 '
IF(@Where <> ' ')
BEGIN
SET @SQL = @SQL+ @Where
END
SET @SQL = @SQL+ ' order by p.Id desc'
EXEC(@SQL)
print @SQL
END
if(@Block =2)
BEGIN
select st.*,cr.ColorName,s.SizeName,c.CatagoryName,sc.SubCatagoryName,u.UOMName,bd.BrandName from dbo.Stocks st
INNER JOIN Products P on p.Id=st.ProductId
LEFT JOIN dbo.Colors cr on cr.Id=p.ColorId
LEFT JOIN dbo.Sizes s on s.Id=p.SizeId
LEFT JOIN dbo.Categories c on c.Id=p.CatagoryId
LEFT JOIN dbo.SubCategories sc on sc.Id=p.SubCatagoryId
LEFT JOIN dbo.UOMs u on u.Id=p.UOMId
LEFT JOIN dbo.Brands bd on bd.Id=p.ProductBrandId
LEFT JOIN dbo.Events e on e.Id=p.EventId

order by p.Id desc
END
if(@Block =3)
BEGIN
select p.Name ProductName,p.Code ProductCode, pur.UnitePrice,pur.Quantity,pur.Discount,Pur.Amount as TotalAmount,format( pur.AddDate,'dd/MM/yyyy') as[Date] from dbo.PurcheaseDetails pur
JOIN dbo.Products p on p.Id=pur.ProductId
where pur.ProductId=@Where
END
if(@Block =4)
BEGIN
select p.Name ProductName,p.Code ProductCode, pur.UnitePrice,pur.Quantity,pur.Discount,Pur.NetAmount as TotalAmount,format( pur.AddDate,'dd/MM/yyyy') as[Date] from dbo.SalesDetails pur
JOIN dbo.Products p on p.Id=pur.ProductId
where pur.ProductId=@Where
END
if(@Block =5)
BEGIN
select p.Name ProductName,p.Code ProductCode, pur.UnitPrice as UnitePrice,pur.Quantity,cast( (pur.UnitPrice*pur.Quantity) as DECIMAL(10,2)) TotalAmount,
pur.Date ,
(select SUM(Quantity) from dbo.SalesDetails where ProductId=pur.ProductId group by ProductId) salesQuantity,
(select SUM(Quantity) from dbo.SalesDetailsReturns where ProductId=pur.ProductId group by ProductId) salesReturnQuantity,
(select SUM(Quantity) from dbo.PurcheaseDetails where ProductId=pur.ProductId group by ProductId) PurcheaseQuantity,
(select SUM(Quantity) from dbo.PurcheaseDetailReturns where ProductId=pur.ProductId group by ProductId) PurcheaseReturnQuantity,
p.SalsPrice
from dbo.Stocks pur 
JOIN dbo.Products p on p.Id=pur.ProductId
where pur.ProductId=@Where
END

END

