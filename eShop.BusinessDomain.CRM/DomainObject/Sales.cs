﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.DomainObject
{
   public class Sales:Entity
    {
        public string InvoiecNo { get; set; }
        public int CustomerId { get; set; }
        public int EmployeeId { get; set; }
        public int? BankId { get; set; }
        public int? BranchId { get; set; }
        public int PaymentType { get; set; }
        public decimal? Discount { get; set; }
        public decimal? TotalDiscount { get; set; }
        public decimal Due { get; set; }
        public decimal NetTotal { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal? Return { get; set; }
        public decimal? VAT { get; set; }
        public DateTime Date { get; set; }
        public string ChequeNo { get; set; }
        public IEnumerable<SalesDetails> SalesDetails { get; set; }
    }
}
