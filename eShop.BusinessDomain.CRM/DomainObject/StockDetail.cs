﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.DomainObject
{
   public class StockDetail : Entity
    {
        public Nullable<int> SalesId { get; set; }
        public Nullable<int> SalesReturnId { get; set; }
        public Nullable<int> PurcheaseId { get; set; }
        public Nullable<int> PurcheaseReturnId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> SupplierId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> BankId { get; set; }
        public Nullable<decimal> StockReplace { get; set; }
        public Nullable<decimal> TransReplace { get; set; }
        public Nullable<decimal> TotalReplace { get; set; }
        public Nullable<decimal> StockReturn { get; set; }
        public Nullable<decimal> TransReturn { get; set; }
        public Nullable<decimal> TotalReturn { get; set; }
        public Nullable<decimal> StockDiscount { get; set; }
        public Nullable<decimal> TransDiscount { get; set; }
        public Nullable<decimal> TotalDiscount { get; set; }
        public Nullable<decimal> StockQuantity { get; set; }
        public Nullable<decimal> TransQuantity { get; set; }
        public Nullable<decimal> TotalQuantity { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> StockPrice { get; set; }
        public Nullable<decimal> TransPrice { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<bool> StockStutes { get; set; }
        public string Date { get; set; }
    }
}
