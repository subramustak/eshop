﻿using eShop.BusinessDomain.Core;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace eShop.BusinessDomain.CRM.DomainObject
{
   public class Stocks:Entity
    {
        public Nullable<int> ProductId { get; set; }
        [NotMapped]
        public string ProductName { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<decimal> TotalPaid { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public DateTime Date { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<decimal> OpeningQuantity { get; set; }
        public decimal Quantity { get; set; }
    }
}
