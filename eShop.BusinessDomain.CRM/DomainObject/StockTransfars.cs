﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.DomainObject
{
  public  class StockTransfars:Entity
    {
        public int FromBranchId { get; set; }
        public int ToBranchId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string Date { get; set; }
        public bool IsApprove { get; set; }
    }
}
