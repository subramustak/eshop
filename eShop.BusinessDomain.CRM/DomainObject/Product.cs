﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.DomainObject
{
    public class Product: Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Barcode { get; set; }

        public int? MinimumStock { get; set; }
        public decimal? OtherCost { get; set; }
        public decimal? Discount { get; set; }
        public decimal? UnitePrice { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? OpeningQuantity { get; set; }
        public decimal SalsPrice { get; set; }
        public bool? IsPercentage { get; set; }
        public bool? IsFixed { get; set; }
        public int? ColorId { get; set; }
        public int? SizeId { get; set; }
        public int? MaterialId { get; set; }
        public int? SubCatagoryId { get; set; }
        public int? CatagoryId { get; set; }
        public int? UOMId { get; set; }
        public byte[] Image { get; set; }
        public int? ProductBrandId { get; set; }
        public int? EventId { get; set; }
        public int? VATPercent { get; set; }
    }
}
