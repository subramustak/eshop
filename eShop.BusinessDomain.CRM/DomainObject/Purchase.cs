using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace eShop.BusinessDomain.CRM.DomainObject
{
    public partial class Purchases: Entity
    {
        public string InvoiecNo { get; set; }
        public int SupplierId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> BankId { get; set; }
        public string ChequeNo { get; set; }
        public int PaymentType { get; set; } //1=Cash 2=Bank
        public Nullable<int> EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public decimal Due { get; set; }
        public decimal NetTotal { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal? Return { get; set; }
        public decimal? Discount { get; set; }
        [NotMapped]
        public string BranchName { get; set; }
        [NotMapped]
        public string EmployeeName { get; set; }
        [NotMapped]
        public string SupplierName { get; set; }
        public IEnumerable<PurcheaseDetails> PurcheaseDetails { get; set; }

    }
}
