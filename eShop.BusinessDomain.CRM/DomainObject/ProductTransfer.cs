﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eShop.BusinessDomain.CRM.DomainObject
{
   public class ProductBranchTransfer:Entity
    {
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public int ProductId { get; set; }
        public string Date { get; set; }
        public bool IsApprove { get; set; }
        public int FromBranchId { get; set; }
        public int ToBranchId { get; set; }
    }
}
