namespace eShop.BusinessDomain.CRM.DomainObject
{
    using eShop.BusinessDomain.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class SalesDetailsReturn :Entity
    {
        public int SalesId { get; set; }
        public int ProductId { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> UnitePrice { get; set; }
        public decimal Quantity { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public SalesReturn SalesReturns { get; set; }
    }
}
