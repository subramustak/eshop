﻿using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.DomainObject
{
   public class SalesReturn : Entity
    {
        public string InvoiecNo { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> BankId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> PaymentType { get; set; }
        public string DiscountRemarks { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> TotalDiscount { get; set; }
        public Nullable<decimal> Due { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> Return { get; set; }
        public Nullable<decimal> NetTotal { get; set; }
        public DateTime Date { get; set; }
        public string ChequeNo { get; set; }
        public IEnumerable<SalesDetailVM> SalesDetails { get; set; }
    }
}
