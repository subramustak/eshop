namespace eShop.BusinessDomain.CRM.DomainObject
{
    using eShop.BusinessDomain.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class SalesDetails : Entity
    {
       
        public int SalesId { get; set; }
        public int ProductId { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public decimal UnitePrice { get; set; }
        [NotMapped]
        public Nullable<decimal> SalsPrice { get; set; }
        public decimal Quantity { get; set; }
        public decimal Return { get; set; }
        public decimal Amount { get; set; }
        public decimal NetAmount { get; set; }
        public int? VAT { get; set; }
        public Sales Sales { get; set; }


    }
}
