﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.ViewModels
{
   public class SelseReportVM
    {
        public string InvoiceNo { get; set; }
        public int? CustomerId { get; set; }
        public int? BranchId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
