using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.Sales.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class PurchaseVM
    {
      public int Id { get; set; }
       
        public string InvoiecNo { get; set; }
        public int SupplierId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> BankId { get; set; }
        public string ChequeNo { get; set; }
        public int PaymentType { get; set; } //1=Cash 2=Bank
        public Nullable<int> EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public decimal Due { get; set; }
        public decimal NetTotal { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal? Return { get; set; }
        [NotMapped]
        public string BranchName { get; set; }
        [NotMapped]
        public string EmployeeName { get; set; }
        [NotMapped]
        public string SupplierName { get; set; }
        public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime? AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime? UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
      public List<PurcheaseDetailVM> PurcheaseDetails { get; set; }
        public string PresentAddress { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public decimal? Discount { get; set; }
    }
}
