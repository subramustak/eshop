using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class ProductVM
    {
      public int Id { get; set; }
      public string Name { get; set; }
      public string Code { get; set; }
      public int? MinimumStock { get; set; }
      public decimal? OtherCost { get; set; }
      public decimal? Discount { get; set; }
      public decimal? UnitePrice { get; set; }
      public decimal? Quantity { get; set; }
      public decimal? OpeningQuantity { get; set; }
      public decimal SalsPrice { get; set; }
        public bool? IsPercentage { get; set; } = false;
      public bool? IsFixed { get; set; } = false;
        public int? ColorId { get; set; } 
        public int? SizeId { get; set; } 
        public int? MaterialId { get; set; }
      public int? SubCatagoryId { get; set; }
      public int? UOMId { get; set; }
      public int? ProductBrandId { get; set; }
      public bool? IsArchived { get; set; }
      public bool? IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime? AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime? UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
        public string ProductBrandName { get; set; }
        public string UOMName { get; set; }
        public string SubCatagoryName { get; set; }
        public string CatagoryName { get; set; }
        public string MaterialName { get; set; }
        public string ProductSizeName { get; set; }
        public string ProductColorName { get; set; }
        public byte[] Image { get; set; }
        public string Barcode { get; set; }
        public bool select { get; set; } = true;
        public int? CatagoryId { get; set; }
        public int? EventId { get; set; }
        public int? VATPercent { get; set; }
    }
}
