using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class ProductSearchVM
    {
      public int? ProductId { get; set; }
      public int? StockFrom { get; set; }
      public int? StockTo { get; set; }
      public decimal? PriceFrom { get; set; }
      public decimal? PriceTo { get; set; }
        public int? SizeId { get; set; }
        public int? CatagoryId { get; set; }
        public int? SubCatagoryId { get; set; }
      public int? UOMId { get; set; }
      public int? BrandId { get; set; }
        public bool IsImage { get; set; }

    }
}
