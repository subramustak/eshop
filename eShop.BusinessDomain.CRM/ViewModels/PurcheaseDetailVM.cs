using System;
namespace eShop.BusinessDomain.Sales.ViewModels
{
    public class PurcheaseDetailVM
    {
      public int Id { get; set; }
        public int Purcheases_Id { get; set; }
        public int PurchaseId { get; set; }
        public int ProductId { get; set; }
        public decimal UnitePrice { get; set; }
        public decimal Sales { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }
        public decimal NetAmount { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
    }
}
