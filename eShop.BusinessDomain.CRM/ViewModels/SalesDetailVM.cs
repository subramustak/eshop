using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class SalesDetailVM
    {
      public int Id { get; set; }
      public int SalesId { get; set; }
      public int ProductId { get; set; }
      public int BranchId { get; set; }
      public decimal? Discount { get; set; }
      public decimal UnitePrice { get; set; }
      public string Date { get; set; }
      public decimal Quantity { get; set; }
      public decimal StockQuantity { get; set; }
      public decimal? Return { get; set; }
        public decimal Amount { get; set; }
        public decimal NetAmount { get; set; }
        public string ProductName { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal SalsPrice { get; set; }
        public int? DsicountAmount { get; set; }
        public bool? DsicountPercent { get; set; }
        public  int? DsicountCount { get; set; }
        public int? EventId { get; set; }
        public string EventName { get; set; }
        public string EVendDate { get; set; }
        public string EVstartDate { get; set; }
        public string Barcode { get; set; }

        public bool Checked { get; set; } = false;
        public int? VAT { get; set; }
        public int? VATPercent { get; set; }
    }
}
