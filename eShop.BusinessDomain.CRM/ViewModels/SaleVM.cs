using eShop.BusinessDomain.MasterSetUp.DomainObject;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using System;
using System.Collections.Generic;

namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class SaleVM
    {
      public int Id { get; set; }
        public string InvoiecNo { get; set; }
        public int CustomerId { get; set; }
        public int EmployeeId { get; set; }
        public int? BankId { get; set; }
        public int? BranchId { get; set; }
        public int PaymentType { get; set; }
        public decimal? Discount { get; set; }
        public decimal? TotalDiscount { get; set; }
        public decimal Due { get; set; }
        public decimal NetTotal { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal? Return { get; set; }
        public decimal? VAT { get; set; }
        public DateTime Date { get; set; }
        public string ChequeNo { get; set; }
        public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime? AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime? UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
      public Employee Employee { get; set; }
      public Customer Customer { get; set; }
      public IEnumerable<SalesDetailVM> SalesDetails { get; set; }
        public string BankName { get; set; }
        public string CustomerName { get; set; }
    }
}
