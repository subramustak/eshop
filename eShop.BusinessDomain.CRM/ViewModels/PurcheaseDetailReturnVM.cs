using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class PurcheaseDetailReturnVM
    {
      public int Id { get; set; }
      public int PurchaseId { get; set; }
      public int ProductId { get; set; }
      public decimal UnitePrice { get; set; }
      public decimal ReturnQuantity { get; set; }
      public decimal ReturnDiscount { get; set; }
      public decimal? ReturnTotalAmount { get; set; }
     
      public int purchaseReturns_Id { get; set; }
        public bool Checked { get; set; }
    }
}
