using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class SalesDetailsReturnVM
    {
      public int Id { get; set; }
      public int SalesId { get; set; }
      public int ProductId { get; set; }
      public decimal? Discount { get; set; }
      public decimal? UnitePrice { get; set; }
      public string Date { get; set; }
      public decimal Quantity { get; set; }
      public decimal? Amount { get; set; }
      public decimal? SalsPrice { get; set; }

        public int SalesReturns_Id { get; set; }
      public bool Checked { get; set; }
        public int? Return { get; set; }
        public string ProductName { get; set; }
    }
}
