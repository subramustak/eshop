using System;
using System.Collections.Generic;

namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class PurchaseReturnVM
    {
      public int Id { get; set; }
      public string InvoiecNo { get; set; }
      public int? SupplierId { get; set; }
      public int? BranchId { get; set; }
      public int? BankId { get; set; }
      public string ChequeNo { get; set; }
      public int PaymentType { get; set; }
      public int? EmployeeId { get; set; }
      public DateTime Date { get; set; }
      public decimal ReturnDue { get; set; }
      public decimal ReturnTotal { get; set; }
      public decimal ReturnTotalPayment { get; set; }
      public decimal Discount { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverNumber { get; set; }
        public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime? AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime? UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
        
        public List<PurcheaseDetailReturnVM> PurcheaseDetails { get; set; }
        public string SupplierName { get; set; }
    }
}
