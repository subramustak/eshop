using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.Sales.ViewModels;
using System;
using System.Collections.Generic;

namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class PurchaseReportVM
    {
        public string InvoiecNo { get; set; }
        public int? SupplierId { get; set; }
        public int? BranchId { get; set; }
        public decimal? MaxAmount { get; set; }
        public decimal? MinAmunt { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
