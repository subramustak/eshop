using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class StockVM
    {
      public int Id { get; set; }
      public int? ProductId { get; set; }
      public string ProductName { get; set; }
      public string ProductCode { get; set; }
      public string UOMName { get; set; }
        public int? BranchId { get; set; }
      public int? CustomerId { get; set; }
      public int? SupplierId { get; set; }
      public decimal? Discount { get; set; }
      public decimal? TotalPaid { get; set; }
        public decimal? TotalPrice { get; set; }
      public decimal? GrandTotal { get; set; }
      public DateTime Date { get; set; }
      public decimal? UnitPrice { get; set; }
       
        public decimal? OpeningQuantity { get; set; }
      public decimal Quantity { get; set; }
      public bool StockStutes { get; set; }
      public int? SalesId { get; set; }
      public int? MinimumStock { get; set; }
        public int? SalesReturnId { get; set; }
      public int? PurcheaseId { get; set; }
      public int? PurcheaseReturnId { get; set; }
      public int? EmployeeId { get; set; }
        public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime? AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime? UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
        public string ProductBrandName { get; set; }
        public string SubCatagoryName { get; set; }
        public string CatagoryName { get; set; }
        public string MaterialName { get; set; }
        public string ProductSizeName { get; set; }
        public string ProductColorName { get; set; }
        public string BranchName { get; set; }
        public decimal? SalsPrice { get; set; }
    }
}
