﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.ViewModels
{
   public class StockReportVM
    {
        public int? ProductId { get; set; }
        public int? BranchId { get; set; }
        public int? StockMin { get; set; }
        public int? StockMax { get; set; }
        public int? PriceMin { get; set; }
        public int? PriceMax { get; set; }
    }
}
