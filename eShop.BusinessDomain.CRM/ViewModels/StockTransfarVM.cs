﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class StockTransfarVM
    {
        public int FromBranchId { get; set; }
        public int ToBranchId { get; set; }
        public  List<ProductDetail> SalesDetails { get; set; }
    }
    public class ProductDetail
    {
        public int ProdutId { get; set; }// Value
        public int ProductName { get; set; }//Text
        public int Quantity { get; set; }
        public int SalsPrice { get; set; }
        public int StockQuantity { get; set; }
        public int? BranchId { get; set; }
    }
}
