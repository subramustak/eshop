﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace eShop.WebUI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string FullName { get; set; }
        public string MobileNo { get; set; }
        public  int? DatabaseId { get; set; }
        public byte[] Image { get; set; }
        public string Address { get; set; }
        public bool IsArchived { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        [StringLength(128)]
        public string AddBy { get; set; }

        public DateTime? AddDate { get; set; }

        [StringLength(128)]
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        [StringLength(100)]
        public string Remarks { get; set; }
        [StringLength(20)]
        public string Status { get; set; }

    }
    //public class ApplicationRole : IdentityRole
    //{
    //    public ApplicationRole() : base() { }
    //    public ApplicationRole(string name, int databaseId) : base(name)
    //    {
    //        this.DatabaseId = databaseId;
    //    }
    //    public virtual int? DatabaseId { get; set; }
    //}

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("eShopDB", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<eShop.BusinessDomain.CRM.ViewModels.StockVM> StockVMs { get; set; }
    }
}