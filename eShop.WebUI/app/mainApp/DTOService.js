﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('DTOService', DTOService);
    DTOService.$inject = ['$http'];
    function DTOService($http) {
        var service = {
        };
        service.GetData = function GetData(url) {
            $http({
                method: 'GET',
                url: baseUrl + url
            }).then(function successCallback(response) {
                return response.data.results;
            }, function errorCallback(response) {
                toastr["warning"](response.data.Message);
            });
        }
        service.warning = function warning(data) {
            $.smallBox({
                title: data,
                content: "<i class='fa fa-clock-o'></i> <i>2 </i>",
                color: "#296191",
                iconSmall: "fa fa-thumbs-up bounce animated",
                timeout: 4000
            });
        }
        service.denger =  function denger(data) {
            $.smallBox({
                title: data,
                content: "<i class='fa fa-clock-o'></i> <i>2 </i>",
                color: "#296191",
                iconSmall: "fa fa-thumbs-up bounce animated",
                timeout: 4000
            });
        }
        return service;
    }
})();