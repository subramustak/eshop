﻿using eshop.ApplicationService.CRM;
using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.Infrastructure.Data.UnitOfWork;
using Infrastructure.Crosscutting.Utility;
using Microsoft.Reporting.WinForms;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace eShop.WebUI.Controllers
{
    public class ProductController : Controller
    {
        StockService stockService;
        ProductService productService;
        UnitOfWork unitOfWork = new UnitOfWork();
        SaleService saleService;
        OrganizationService organizationService;
        public ProductController()
        {
            this.productService = new ProductService();
            this.saleService = new SaleService();
            this.organizationService = new OrganizationService();
            this.stockService = new StockService();
        }
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult ProductList()
        {
            return View();
        }
        public ActionResult ProductSearch()
        {
            return View();
        }
        public ActionResult ProductDiscount()
        {
            return View();
        }
        public ActionResult ProductSaveExcel()
        {
            return View();
        }
        public ActionResult ProductDetail(int Id)
        {
            ViewBag.Id = Id;
            return View();
        }
        public ActionResult DownloadSaveExcel(int CategoryId, int SubCategoryId, int BrandId, int UOMId, int SizeId)
        {
            MemoryStream memStream;
            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("ProductUpload");
                worksheet.Cells["A1"].Value = "Pro_Category_" + CategoryId;
                worksheet.Cells["A1"].Style.Locked = true;
                worksheet.Cells["B1"].Value = "Pro_SubCategory_" + SubCategoryId;
                worksheet.Cells["B1"].Style.Locked = true;
                worksheet.Cells["C1"].Value = "Pro_Brand_" + BrandId;
                worksheet.Cells["C1"].Style.Locked = true;

                worksheet.Cells["D1"].Value = "Pro_UOM_" + UOMId;
                worksheet.Cells["D1"].Style.Locked = true;

                worksheet.Cells["E1"].Value = "Pro_Size_" + SizeId;
                worksheet.Cells["E1"].Style.Locked = true;
                for (int i = 2; i < 20; i++)
                {
                    worksheet.Cells["A" + i].Value = unitOfWork.CategoryRepository.Get(CategoryId).CatagoryName;
                    worksheet.Cells["A" + i].Style.Locked = true;
                    worksheet.Cells["B" + i].Value = unitOfWork.SubCategoryRepository.Get(SubCategoryId).SubCatagoryName;
                    worksheet.Cells["B" + i].Style.Locked = true;
                    worksheet.Cells["C" + i].Value = unitOfWork.BrandRepository.Get(BrandId).BrandName;
                    worksheet.Cells["C" + i].Style.Locked = true;
                    worksheet.Cells["D" + i].Value = unitOfWork.UOMRepository.Get(UOMId).UOMName;
                    worksheet.Cells["D" + i].Style.Locked = true;
                    worksheet.Cells["E" + i].Value = unitOfWork.SizeRepository.Get(SizeId).SizeName;
                    worksheet.Cells["E" + i].Style.Locked = true;
                }
                worksheet.Cells["F1"].Value = "Code";
                worksheet.Cells["G1"].Value = "Name";

                worksheet.Column(1).Style.Locked = true;
                worksheet.Column(2).Style.Locked = true;
                worksheet.Column(3).Style.Locked = true;
                worksheet.Column(4).Style.Locked = true;
                worksheet.Column(5).Style.Locked = true;

                worksheet.Column(1).Width = 15;
                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 15;
                worksheet.Column(4).Width = 15;
                worksheet.Column(5).Width = 15;
                worksheet.Column(6).Width = 15;
                worksheet.Column(7).Width = 100;


                var rngTable = worksheet.Cells["A1:G1"];
                rngTable.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rngTable.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(253, 233, 217));
                rngTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                memStream = new MemoryStream(package.GetAsByteArray());
            }

            Random rand = new Random();
            string r = rand.Next(10000, 999999).ToString();

            return File(memStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Products" + r + ".xlsx");

        }
        public FileResult Barcode(int? ProId)
        {
            ReportViewer rptViewer = new ReportViewer();
            List<Barcodes> vs = new List<Barcodes>();
            List<ProductVM> products = new List<ProductVM>();
            BarcodeGenerator barcodeGenerator = new BarcodeGenerator();
            dynamic data = null;
            products = this.productService.GetAllProduct().ToList();

            if (ProId != null)
            {
                data = products.Where(e => e.Id.Equals(ProId)).ToArray();
                Barcodes barcodes = new Barcodes();
                barcodes.Barcode1 = barcodeGenerator.BarCode(data[0].Barcode);
                barcodes.ProName1 = data[0].Name;
                barcodes.ProCode1 = data[0].Code;
                vs.Add(barcodes);
            }
            else
            {

                data = products.ToArray();
                int a = 0;
                if (products.Count() ==3)
                {
                    Barcodes barcodes = new Barcodes();
                    barcodes.Barcode1 = barcodeGenerator.BarCode(data[a].Barcode);
                    barcodes.ProName1 = data[a].Name;
                    barcodes.ProCode1 = data[a].Code;
                    a++;
                    barcodes.Barcode2 = barcodeGenerator.BarCode(data[a].Barcode);
                    barcodes.ProName2 = data[a].Name;
                    barcodes.ProCode2 = data[a].Code;
                    a++;
                    barcodes.Barcode3 = barcodeGenerator.BarCode(data[a].Barcode);
                    barcodes.ProName3 = data[a].Name;
                    barcodes.ProCode3 = data[a].Code;
                    a++;
                    vs.Add(barcodes);
                }
               
                for (int i = 0; i < products.Count() / 3; i++)
                {
                    Barcodes barcodes = new Barcodes();
                    barcodes.Barcode1 = barcodeGenerator.BarCode(data[a].Barcode);
                    barcodes.ProName1 = data[a].Name;
                    barcodes.ProCode1 = data[a].Code;
                    a++;
                    barcodes.Barcode2 = barcodeGenerator.BarCode(data[a].Barcode);
                    barcodes.ProName2 = data[a].Name;
                    barcodes.ProCode2 = data[a].Code;
                    a++;
                    barcodes.Barcode3 = barcodeGenerator.BarCode(data[a].Barcode);
                    barcodes.ProName3 = data[a].Name;
                    barcodes.ProCode3 = data[a].Code;
                    a++;

                    //barcodes.Barcode4 = barcodeGenerator.BarCode(data[a].Id + " ~ " + data[a].Name);
                    //barcodes.ProName4 = data[a].Barcode;
                    //barcodes.ProCode4 = data[a].Code;
                    //a++;
                    vs.Add(barcodes);
                }
            }

            rptViewer.ProcessingMode = ProcessingMode.Local;

            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptBarcode.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", vs));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-excel", "Report.xls");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
        public ActionResult ProductAdd(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
        public ActionResult Purchease() => View();
        [HttpPost]
        public FileResult ProductReport(ProductSearchVM product)
        {
            ReportViewer rptViewer = new ReportViewer();
            var data = this.productService.ProdutReport(product);
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptProductReport.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
        }
        [HttpPost]
        public FileResult SelectedPrductforBarcode(string ProductIIDs)
        {

            ReportViewer rptViewer = new ReportViewer();
            List<Barcodes> vs = new List<Barcodes>();
            List<ProductVM> products = new List<ProductVM>();
            BarcodeGenerator barcodeGenerator = new BarcodeGenerator();
            dynamic data = null;
           // products = this.productService.GetAllProduct().ToList();
            products = productService.SelectedProdutBarcode(ProductIIDs);
            
                data = products.ToArray();
                int a = 0;
                for (int i = 0; i < products.Count() / 4; i++)
                {
                    Barcodes barcodes = new Barcodes();
                    barcodes.Barcode1 = barcodeGenerator.BarCode(data[a].Id + " ~ " + data[a].Name);
                    barcodes.ProName1 = data[a].Barcode;
                    barcodes.ProCode1 = data[a].Code;
                    a++;
                    barcodes.Barcode2 = barcodeGenerator.BarCode(data[a].Id + " ~ " + data[a].Name);
                    barcodes.ProName2 = data[a].Barcode;
                    barcodes.ProCode2 = data[a].Code;
                    a++;

                    barcodes.Barcode3 = barcodeGenerator.BarCode(data[a].Id + " ~ " + data[a].Name);
                    barcodes.ProName3 = data[a].Barcode;
                    barcodes.ProCode3 = data[a].Code;
                    a++;

                    barcodes.Barcode4 = barcodeGenerator.BarCode(data[a].Id + " ~ " + data[a].Name);
                    barcodes.ProName4 = data[a].Barcode;
                    barcodes.ProCode4 = data[a].Code;
                    a++;
                    vs.Add(barcodes);
                }
            

            rptViewer.ProcessingMode = ProcessingMode.Local;

            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptBarcode.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", vs));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-excel", "Report.xls");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
        public FileResult Invoice(int Id)
        {
            ReportViewer rptViewer = new ReportViewer();
            var data = saleService.GetProductDetail((int)Id);
            var org = this.organizationService.GetAllOrganization();
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoiceSalesPos.rdlc");
            //rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoiceSalesPos.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgResults", org));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
        [HttpPost]
        public FileResult StockReportCall(StockReportVM selseReportVM)
        {
            ReportViewer rptViewer = new ReportViewer();

            var data = stockService.GetStockReportData(selseReportVM);
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptStockReport.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
        }
    }
    //   rptInvoiceSales
    public class Barcodes
    {
        public byte[] Barcode1 { get; set; }
        public string ProName1 { get; set; }
        public string ProCode1 { get; set; }
        public byte[] Barcode2 { get; set; }
        public string ProName2 { get; set; }
        public string ProCode2 { get; set; }
        public byte[] Barcode3 { get; set; }
        public string ProName3 { get; set; }
        public string ProCode3 { get; set; }
        public byte[] Barcode4 { get; set; }
        public string ProName4 { get; set; }
        public string ProCode4 { get; set; }
    }
}