﻿using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.CRM.ViewModels;
using Microsoft.Reporting.WinForms;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eShop.WebUI.Controllers
{
    public class StockController : Controller
    {
        StockService stockService = new StockService();
        // GET: Stock
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult StockList()
        {
            return View();
        }
        public ActionResult StockTransfer()
        {
            return View();
        }
        public ActionResult StockReportAction()
        {
            return View();
        }
        public ActionResult StockTransferReport()
        {
            return View();
        }
        
        [HttpPost]
        public FileResult StockTransfarReportCall(StockTransfarReportVM stockTransfarReportVM)
        {
            ReportViewer rptViewer = new ReportViewer();

            var data = stockService.TransfarProductList(stockTransfarReportVM);
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptStockTransfarReport.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
        }
       
    }
}