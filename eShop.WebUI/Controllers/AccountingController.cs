﻿using eshop.ApplicationService.CRM;
using eshop.ApplicationService.MasterSetUp;
using eShop.ApplicationService.Account;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.Infrastructure.Data.UnitOfWork;
using Infrastructure.Crosscutting.Utility;
using Microsoft.Reporting.WinForms;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eShop.WebUI.Controllers
{
  
    public class AccountingController : Controller
    {
        TransectionHistoryService transectionHistoryService = new TransectionHistoryService();
        // GET: Accounting
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AccountCreate()
        {
            return View();
        }
        public ActionResult AccountPayment(int Id=0)
        {
            ViewBag.Id = Id;
            return View();
        }
        public ActionResult AccountReceipt(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
        public ActionResult AccountManagement()
        {
            return View();
        }
        public ActionResult AccountPayable()
        {
            return View();
        }
        public ActionResult AccountRecive()
        {
            return View();
        }
        public ActionResult CustomerReciveable()
        {
            return View();
        }
        public ActionResult AccountRecivePayment()
        {
            return View();
        }
        public ActionResult AccountTransaction(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
        //public FileResult Invoice(int Id)
        //{
        //    ReportViewer rptViewer = new ReportViewer();
        //    var data = saleService.GetProductDetail((int)Id);
        //    var org = this.organizationService.GetAllOrganization();
        //    rptViewer.ProcessingMode = ProcessingMode.Local;
        //    rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/eShop.Infrastructure.RDLC/rptInvoiceSalesPos.rdlc");
        //    //rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoiceSalesPos.rdlc");
        //    rptViewer.LocalReport.EnableExternalImages = true;
        //    rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
        //    rptViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgResults", org));
        //    rptViewer.LocalReport.Refresh();
        //    Warning[] warnings;
        //    string[] streamIds;
        //    string mimeType = string.Empty;
        //    string encoding = string.Empty;
        //    string extension = string.Empty;
        //    byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        //    return File(bytes, "application/pdf");
        //    //return File(bytes, "application/vnd.ms-word", "Report.doc");
        //}
        [HttpGet]
        public FileResult AccountMonthDaily(StockReportVM selseReportVM)
        {
            ReportViewer rptViewer = new ReportViewer();

            var data = transectionHistoryService.GetAllTransectionHistory();
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptStockReport.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
        }
    }
}