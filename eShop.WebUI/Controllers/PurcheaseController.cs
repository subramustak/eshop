﻿using eshop.ApplicationService.CRM;
using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.CRM.ViewModels;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eShop.WebUI.Controllers
{
    public class PurcheaseController : Controller
    {
        // GET: Purchease
        PurchaseService PurchaseService;
        PurchaseReturnService purchaseReturnService;
        OrganizationService organizationService;
        public PurcheaseController()
        {
            this.PurchaseService = new PurchaseService();
            this.organizationService = new OrganizationService();
            this.purchaseReturnService = new PurchaseReturnService();
        }
        public ActionResult Index()
        {
            return View();


        }
        public ActionResult PurcheasePos(int Id=0)
        {
            ViewBag.Id = Id;
            return View();
        }
        public ActionResult PurcheasePos2(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
        public ActionResult Purchease() => View();
        public ActionResult PurcheaseList() => View();
        public ActionResult PurcheaseReport() => View();
        [HttpPost]
        public FileResult PurcheaseReport(PurchaseReportVM purchase)
        {

            ReportViewer rptViewer = new ReportViewer();

            var data = this.PurchaseService.PerchesReport(purchase);

            rptViewer.ProcessingMode = ProcessingMode.Local;

            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptPurchaseReport.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
        }
        public ActionResult PurcheaseReturn() => View();
        public ActionResult PurcheaseReturnList() => View();
        public ActionResult PurcheaseReturnReport() => View();
        public FileResult Invoice(int Id)
        {
            ReportViewer rptViewer = new ReportViewer();
            var data = PurchaseService.GetProductDetail((int)Id);
            var org = this.organizationService.GetAllOrganization();
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoicePurchease.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgResults", org));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-excel", "Report.xls");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
        public FileResult InvoiceReturn(int Id)
        {
            ReportViewer rptViewer = new ReportViewer();
            var data = purchaseReturnService.GetProductDetail((int)Id);
            var org = this.organizationService.GetAllOrganization();
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoicePurcheaseReturn.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgResults", org));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-excel", "Report.xls");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
    }
}