﻿using Infrastructure.Crosscutting.CulturalHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eShop.WebUI.Controllers
{
    public class MasterSetUpController : MyBaseController
    {
        public ActionResult ChangeCurrentCulture(int id)
        {
            //  
            // Change the current culture for this user.  
            //  
            CultureHelper.CurrentCulture = id;
            //  
            // Cache the new current culture into the user HTTP session.   
            //  
            Session["CurrentCulture"] = id;
            //  
            // Redirect to the same page from where the request was made!   
            //  
            return Redirect(Request.UrlReferrer.ToString());
        }
        // GET: MasterSetUp
        public ActionResult Index() => View();
        public ActionResult Size() => View();
        public ActionResult Branch() => View();
        public ActionResult Bank() => View();
        public ActionResult Brand() => View();
        public ActionResult Category() => View();
        public ActionResult SubCategory() => View();
        public ActionResult Color() => View();
        public ActionResult UOM() => View();
        public ActionResult Employee() => View();
        public ActionResult Customer() => View();
        public ActionResult Supplier() => View();
        public ActionResult Setting() => View();
        public ActionResult Event_Discount() => View();
        public ActionResult Organization() => View();
    }
}