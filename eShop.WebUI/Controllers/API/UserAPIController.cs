﻿using eShop.BusinessDomain.Admin.DomainObject;
using eShop.BusinessDomain.Core;
using eShop.WebUI.Models;
using Infrastructure.Crosscutting.Utility;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Http;

namespace eShop.WebUI.Controllers.API
{
    public class UserAPIController : ApiController
    {
        ApplicationDbContext context = new ApplicationDbContext();

        [Route("Role/GetAllRoleUser/")]
        [HttpGet]
        public IHttpActionResult GetAllRoleUser()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = context.Roles.OrderBy(r => r.Name).ToList().Select(rr => new  { Value = rr.Id.ToString(), Text = rr.Name }).ToList();
                cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Role/AddRoleUser/")]
        [HttpPost]
        public IHttpActionResult AddRoleUser(CreateRoleBindingModel aspnetuser)
        {
            CommonResponse cr = new CommonResponse();
            AspNetUser oAspNetUser = new AspNetUser();
            try
            {
                context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole()
                {
                    Name = aspnetuser.Name
                });
                context.SaveChanges();
            }
            //catch (Exception ex)
            //{
            //    return BadRequest(ex.Message);
            //}
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            return Json(cr);
        }
        [Route("Role/EditRoleUser/")]
        [HttpPost]
        public IHttpActionResult EditRoleUser(CreateRoleBindingModel aspnetuser)
        {
            CommonResponse cr = new CommonResponse();
            AspNetUser oAspNetUser = new AspNetUser();
            try
            {
                IdentityRole identityRole = new IdentityRole();

                identityRole.Name = aspnetuser.Name;
                context.Entry(identityRole).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Role/DeleteRole/")]
        [HttpGet]
        public IHttpActionResult DeleteRole(CreateRoleBindingModel aspnetuser)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                var thisRole = context.Roles.Where(r => r.Name.Equals(aspnetuser.Name, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                context.Roles.Remove(thisRole);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
    
        [Route("Role/RoleAddToUser/")]
        [HttpPost]
        public IHttpActionResult RoleAddToUser(string UserName, string RoleName)
        {
        CommonResponse cr = new CommonResponse();
        AspNetUser oAspNetUser = new AspNetUser();
        try
        {
                ApplicationUser user = context.Users.Where(u => u.UserName.Equals(UserName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                var account = new AccountController();
                account.UserManager.AddToRole(user.Id, RoleName);

                cr.message = "Role created successfully !";
            }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
        return Json(cr);
    }
    }
}