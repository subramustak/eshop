﻿using eshop.ApplicationService.Admin;
using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace eShop.WebUI.Controllers
{
    //[AuthAttribute]
    [Authorize]
    public class HomeController : Controller
    {
        private AspNetRoleService aspNetPageService =new AspNetRoleService();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        [HttpGet]
        public ActionResult GetMenu()
        {
            try
            {
                var pages = aspNetPageService.GetByUserId(User.Identity.GetUserId()).ToList();
                return View(pages);
            }
            catch (Exception ex)
            {

                throw;
            }

            
        }

    }
}