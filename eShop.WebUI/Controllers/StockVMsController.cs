﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.WebUI.Models;

namespace eShop.WebUI.Controllers
{
    public class StockVMsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: StockVMs
        public ActionResult Index()
        {
            return View(db.StockVMs.ToList());
        }

        // GET: StockVMs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockVM stockVM = db.StockVMs.Find(id);
            if (stockVM == null)
            {
                return HttpNotFound();
            }
            return View(stockVM);
        }

        // GET: StockVMs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StockVMs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductId,ProductName,ProductCode,UOMName,BranchId,CustomerId,SupplierId,Discount,TotalPaid,TotalPrice,GrandTotal,Date,UnitPrice,OpeningQuantity,Quantity,StockStutes,SalesId,SalesReturnId,PurcheaseId,PurcheaseReturnId,EmployeeId,IsArchived,IsDeleted,AddBy,AddDate,UpdateBy,UpdateDate,Remarks,Status,ProductBrandName,SubCatagoryName,CatagoryName,MaterialName,ProductSizeName,ProductColorName,SalsPrice")] StockVM stockVM)
        {
            if (ModelState.IsValid)
            {
                db.StockVMs.Add(stockVM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(stockVM);
        }

        // GET: StockVMs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockVM stockVM = db.StockVMs.Find(id);
            if (stockVM == null)
            {
                return HttpNotFound();
            }
            return View(stockVM);
        }

        // POST: StockVMs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProductId,ProductName,ProductCode,UOMName,BranchId,CustomerId,SupplierId,Discount,TotalPaid,TotalPrice,GrandTotal,Date,UnitPrice,OpeningQuantity,Quantity,StockStutes,SalesId,SalesReturnId,PurcheaseId,PurcheaseReturnId,EmployeeId,IsArchived,IsDeleted,AddBy,AddDate,UpdateBy,UpdateDate,Remarks,Status,ProductBrandName,SubCatagoryName,CatagoryName,MaterialName,ProductSizeName,ProductColorName,SalsPrice")] StockVM stockVM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stockVM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(stockVM);
        }

        // GET: StockVMs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockVM stockVM = db.StockVMs.Find(id);
            if (stockVM == null)
            {
                return HttpNotFound();
            }
            return View(stockVM);
        }

        // POST: StockVMs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StockVM stockVM = db.StockVMs.Find(id);
            db.StockVMs.Remove(stockVM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
