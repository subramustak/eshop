﻿using eshop.ApplicationService.CRM;
using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.CRM.ViewModels;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace eShop.WebUI.Controllers
{
    public class SalesController : Controller
    {
        SaleService saleService;
        OrganizationService organizationService;
        public SalesController()
        {
            this.saleService = new SaleService();
            this.organizationService = new OrganizationService();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SalesPos(int Id = 0)
        {
            ViewBag.Id = Id;
            return View();
        }
        public ActionResult SalesReturn()
        {
            return View();
        }
        public ActionResult SalesReturnList()
        {
            return View();
        }
        public ActionResult SalesList()
        {
          
            return View();
        }
        public ActionResult Salesreport()
        {
            return View();
        }
        public FileResult Invoice(int Id)
        {
            ReportViewer rptViewer = new ReportViewer();
            var data = saleService.GetProductDetail((int)Id);
            var org = this.organizationService.GetAllOrganization();
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/eShop.Infrastructure.RDLC/rptInvoiceSalesPos.rdlc");
            //rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoiceSalesPos.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgResults", org));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
        public FileResult Invoice1(int Id)
        {
            ReportViewer rptViewer = new ReportViewer();
            var data = saleService.GetProductDetail((int)Id);
            var org = this.organizationService.GetAllOrganization();
            rptViewer.ProcessingMode = ProcessingMode.Local;
            //rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/eShop.Infrastructure.RDLC/rptInvoiceSalesPos.rdlc");
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoiceSalesPos.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgResults", org));
            rptViewer.LocalReport.Refresh();

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
        public FileResult Invoicereturn(int Id)
        {
            ReportViewer rptViewer = new ReportViewer();
            var data = saleService.GetProductDetail((int)Id);
            var org = this.organizationService.GetAllOrganization();
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptInvoiceSalesPos.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgResults", org));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
            //return File(bytes, "application/vnd.ms-word", "Report.doc");
        }
        public ActionResult SalseReport()
        {
            return View();
        }
        [HttpPost]
        public FileResult SalseReportCall(SelseReportVM selseReportVM)
        {
            ReportViewer rptViewer = new ReportViewer();

            var data = saleService.GetSalesReportData(selseReportVM);
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/rptSalseReport.rdlc");
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("SPResults", data));
            rptViewer.LocalReport.Refresh();
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            return File(bytes, "application/pdf");
        }
    }
}