﻿
using eShop.BusinessDomain.Account;
using eShop.BusinessDomain.Account.DomainObject;
using eShop.BusinessDomain.Admin.DomainObject;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using eShop.Infrastructure.Data.Context.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.Infrastructure.Data.UnitOfWork
{
    public class UnitOfWork
    {
        //public static string entityConnection()
        //{
        //    SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();
        //    sqlBuilder.DataSource = "XXX";
        //    sqlBuilder.InitialCatalog = "YYY";
        //    sqlBuilder.PersistSecurityInfo = true;
        //    sqlBuilder.IntegratedSecurity = true;
        //    sqlBuilder.MultipleActiveResultSets = true;

        //    return sqlBuilder.ToString();
        //}
        //private CRMContext cRMContext = new CRMContext(entityConnection());
        private CRMContext cRMContext = new CRMContext();
        #region MasterSetup
        private Repository<Branch> branchRepository;
        public Repository<Branch> BranchRepository
        {
            get
            {
                if (this.branchRepository == null)
                {
                    this.branchRepository = new Repository<Branch>(cRMContext);
                }
                return branchRepository;
            }
        }
        private Repository<Brand> brandRepository;
        public Repository<Brand> BrandRepository
        {
            get
            {
                if (this.brandRepository == null)
                {
                    this.brandRepository = new Repository<Brand>(cRMContext);
                }
                return brandRepository;
            }
        }
        private Repository<Color> colorRepository;
        public Repository<Color> ColorRepository
        {
            get
            {
                if (this.colorRepository == null)
                {
                    this.colorRepository = new Repository<Color>(cRMContext);
                }
                return colorRepository;
            }
        }
        private Repository<Bank> bankRepository;
        public Repository<Bank> BankRepository
        {
            get
            {
                if (this.bankRepository == null)
                {
                    this.bankRepository = new Repository<Bank>(cRMContext);
                }
                return bankRepository;
            }
        }
        private Repository<Category> categoryRepository;
        public Repository<Category> CategoryRepository
        {
            get
            {
                if (this.categoryRepository == null)
                {
                    this.categoryRepository = new Repository<Category>(cRMContext);
                }
                return categoryRepository;
            }
        }
        private Repository<SubCategory> subcategoryRepository;
        public Repository<SubCategory> SubCategoryRepository
        {
            get
            {
                if (this.subcategoryRepository == null)
                {
                    this.subcategoryRepository = new Repository<SubCategory>(cRMContext);
                }
                return subcategoryRepository;
            }
        }
        private Repository<Size> sizeRepository;
        public Repository<Size> SizeRepository
        {
            get
            {
                if (this.sizeRepository == null)
                {
                    this.sizeRepository = new Repository<Size>(cRMContext);
                }
                return sizeRepository;
            }
        }
        private Repository<UOM> uomRepository;
        public Repository<UOM> UOMRepository
        {
            get
            {
                if (this.uomRepository == null)
                {
                    this.uomRepository = new Repository<UOM>(cRMContext);
                }
                return uomRepository;
            }
        }
        private Repository<Customer> customerRepository;
        public Repository<Customer> CustomerRepository
        {
            get
            {
                if (this.customerRepository == null)
                {
                    this.customerRepository = new Repository<Customer>(cRMContext);
                }
                return customerRepository;
            }
        }
        private Repository<Supplier> supplierRepository;
        public Repository<Supplier> SupplierRepository
        {
            get
            {
                if (this.supplierRepository == null)
                {
                    this.supplierRepository = new Repository<Supplier>(cRMContext);
                }
                return supplierRepository;
            }
        }
        private Repository<Employee> employeeRepository;
        public Repository<Employee> EmployeeRepository
        {
            get
            {
                if (this.employeeRepository == null)
                {
                    this.employeeRepository = new Repository<Employee>(cRMContext);
                }
                return employeeRepository;
            }
        }
        private Repository<Setting> settingRepository;
        public Repository<Setting> SettingRepository
        {
            get
            {
                if (this.settingRepository == null)
                {
                    this.settingRepository = new Repository<Setting>(cRMContext);
                }
                return settingRepository;
            }
        }
        private Repository<Event> eventRepository;
        public Repository<Event> EventRepository
        {
            get
            {
                if (this.eventRepository == null)
                {
                    this.eventRepository = new Repository<Event>(cRMContext);
                }
                return eventRepository;
            }
        }
        private Repository<Organization> organizationRepository;
        public Repository<Organization> OrganizationRepository
        {
            get
            {
                if (this.organizationRepository == null)
                {
                    this.organizationRepository = new Repository<Organization>(cRMContext);
                }
                return organizationRepository;
            }
        }

        #endregion MasterSetup
        #region Admin
        private Repository<AspNetUser> aspnetuserRepository;
        public Repository<AspNetUser> AspNetUserRepository
        {
            get
            {
                if (this.aspnetuserRepository == null)
                {
                    this.aspnetuserRepository = new Repository<AspNetUser>(cRMContext);
                }
                return aspnetuserRepository;
            }
        }
        #endregion Admin
        #region CRM
        private Repository<Product> productRepository;
        public Repository<Product> ProductRepository
        {
            get
            {
                if (this.productRepository == null)
                {
                    this.productRepository = new Repository<Product>(cRMContext);
                }
                return productRepository;
            }
        }
        private Repository<Purchases> purchaseRepository;
        public Repository<Purchases> PurchaseRepository
        {
            get
            {
                if (this.purchaseRepository == null)
                {
                    this.purchaseRepository = new Repository<Purchases>(cRMContext);
                }
                return purchaseRepository;
            }
        }
        private Repository<PurcheaseDetails> purcheasedetailRepository;
        public Repository<PurcheaseDetails> PurcheaseDetailRepository
        {
            get
            {
                if (this.purcheasedetailRepository == null)
                {
                    this.purcheasedetailRepository = new Repository<PurcheaseDetails>(cRMContext);
                }
                return purcheasedetailRepository;
            }
        }

        private Repository<Sales> saleRepository;
        public Repository<Sales> SaleRepository
        {
            get
            {
                if (this.saleRepository == null)
                {
                    this.saleRepository = new Repository<Sales>(cRMContext);
                }
                return saleRepository;
            }
        }

        private Repository<SalesDetails> salesdetailRepository;
        public Repository<SalesDetails> SalesDetailRepository
        {
            get
            {
                if (this.salesdetailRepository == null)
                {
                    this.salesdetailRepository = new Repository<SalesDetails>(cRMContext);
                }
                return salesdetailRepository;
            }
        }
        private Repository<PurchaseReturn> purchasereturnRepository;
        public Repository<PurchaseReturn> PurchaseReturnRepository
        {
            get
            {
                if (this.purchasereturnRepository == null)
                {
                    this.purchasereturnRepository = new Repository<PurchaseReturn>(cRMContext);
                }
                return purchasereturnRepository;
            }
        }
        private Repository<PurcheaseDetailReturn> purcheasedetailreturnRepository;
        public Repository<PurcheaseDetailReturn> PurcheaseDetailReturnRepository
        {
            get
            {
                if (this.purcheasedetailreturnRepository == null)
                {
                    this.purcheasedetailreturnRepository = new Repository<PurcheaseDetailReturn>(cRMContext);
                }
                return purcheasedetailreturnRepository;
            }
        }
        private Repository<SalesReturn> salesreturnRepository;
        public Repository<SalesReturn> SalesReturnRepository
        {
            get
            {
                if (this.salesreturnRepository == null)
                {
                    this.salesreturnRepository = new Repository<SalesReturn>(cRMContext);
                }
                return salesreturnRepository;
            }
        }
        private Repository<SalesDetailsReturn> salesdetailsreturnRepository;
        public Repository<SalesDetailsReturn> SalesDetailsReturnRepository
        {
            get
            {
                if (this.salesdetailsreturnRepository == null)
                {
                    this.salesdetailsreturnRepository = new Repository<SalesDetailsReturn>(cRMContext);
                }
                return salesdetailsreturnRepository;
            }
        }

        private Repository<Stocks> stockRepository;
        public Repository<Stocks> StockRepository
        {
            get
            {
                if (this.stockRepository == null)
                {
                    this.stockRepository = new Repository<Stocks>(cRMContext);
                }
                return stockRepository;
            }
        }
        private Repository<StockDetail> stockdetailRepository;
        public Repository<StockDetail> StockdetailRepository
        {
            get
            {
                if (this.stockdetailRepository == null)
                {
                    this.stockdetailRepository = new Repository<StockDetail>(cRMContext);
                }
                return stockdetailRepository;
            }
        }
        private Repository<StockTransfars> stocktransfarRepository;
        public Repository<StockTransfars> StockTransfarRepository
        {
            get
            {
                if (this.stocktransfarRepository == null)
                {
                    this.stocktransfarRepository = new Repository<StockTransfars>(cRMContext);
                }
                return stocktransfarRepository;
            }
        }

        #endregion CRM
        #region Accounts
        private Repository<AccountName> accountNameRepository;
        public Repository<AccountName> AccountNameRepository
        {
            get
            {
                if (this.accountNameRepository == null)
                {
                    this.accountNameRepository = new Repository<AccountName>(cRMContext);
                }
                return accountNameRepository;
            }
        }
        private Repository<RootAccount> rootAccountRepository;
        public Repository<RootAccount> RootAccountRepository
        {
            get
            {
                if (this.rootAccountRepository == null)
                {
                    this.rootAccountRepository = new Repository<RootAccount>(cRMContext);
                }
                return rootAccountRepository;
            }
        }
        private Repository<TransectionHistory> transectionHistoryRepository;
        public Repository<TransectionHistory> TransectionHistoryRepository
        {
            get
            {
                if (this.transectionHistoryRepository == null)
                {
                    this.transectionHistoryRepository = new Repository<TransectionHistory>(cRMContext);
                }
                return transectionHistoryRepository;
            }
        }
        #endregion Accounts
        public void Save()
        {
            cRMContext.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    cRMContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
