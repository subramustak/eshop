﻿

using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Core.Helpers;
using eShop.BusinessDomain.CRM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace eShop.Infrastructure.Data.UnitOfWork
{
    public  class Repository<TEntity>
        where TEntity :Entity
       
    {
        protected DbContext Context { get; set; }

        public Repository(DbContext context)
        {
            this.Context = context;
        }

        public void Add(TEntity entity)
        {
            try
            {
                this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);
                this.Context.Set<TEntity>().Add(entity);
                this.Context.SaveChanges();
            }
            //catch (Exception ex)
            //{
            //    if (ex.InnerException.InnerException.Message.ToUpper().Contains("VIOLATION OF UNIQUE KEY"))
            //    {
            //        throw new Exception("Duplicate unique key");
            //    }
            //    else
            //    {
            //        throw new Exception("OTHER ERROR " + ex.Message);
            //    }
            //}
            catch (DbUpdateException ex)
            {
                throw new Exception(ex.InnerException.InnerException.Message);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        throw new Exception(error.PropertyName + " : " + error.ErrorMessage);
                        //Trace.TraceInformation("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual int Update(TEntity entity)
        {
            try
            {
                this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);
                if (!Context.Set<TEntity>().Any(x => x.Id == entity.Id))
                {
                    Context.Set<TEntity>().Attach(entity);
                    Context.Entry<TEntity>(entity).State = EntityState.Modified;
                }
                else
                {
                    var currentEntity = this.Context.Set<TEntity>().Where(x => x.Id.Equals(entity.Id)).FirstOrDefault();
                    Context.Entry<TEntity>(currentEntity).CurrentValues.SetValues(entity);
                }
                return Context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {

                throw new Exception(ex.InnerException.InnerException.Message);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        throw new Exception(error.PropertyName + " : " + error.ErrorMessage);
                        //Trace.TraceInformation("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage);
                    }
                }
            }
            return 0;
        }

        public void Archive(string id)
        {
            this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);

            var currentEntity = this.Context.Set<TEntity>().Where(x => x.Id.Equals(id)).FirstOrDefault();
            currentEntity.IsArchived = true;
            Context.Entry<TEntity>(currentEntity).CurrentValues.SetValues(currentEntity);
            Context.SaveChanges();
        }

        public void Delete(int Id)
        {
            this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);

            var currentEntity = this.Context.Set<TEntity>().Where(x => x.Id.Equals(Id)).FirstOrDefault();
            Context.Set<TEntity>().Remove(currentEntity);
            Context.SaveChanges();
        }
        public void DeleteRange(List<TEntity> Id)
        {
            this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);
            Context.Set<TEntity>().RemoveRange(Id);
            Context.SaveChanges();
        }
        public int Count()
        {
            return Count(x => true);
        }

        public int Count(Expression<Func<TEntity, bool>> expression)
        {
            return this.Context.Set<TEntity>().Count(expression);
        }

        public TEntity Get(long id)
        {
            return this.Context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return GetAll(x => true);
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> expression)
        {
            //this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);
            return this.Context.Set<TEntity>().Where(expression);
        }

        public async Task<ICollection<TEntity>> GetAllAsync()
        {
            return await GetAllAsync(x => true);
        }

        
        public async Task<ICollection<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await this.Context.Set<TEntity>().Where(expression).ToListAsync();
        }
        public virtual IEnumerable<TEntity> Get(
           Expression<Func<TEntity, bool>> filter = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           string includeProperties = "")
        {
            this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);

            IQueryable<TEntity> query = this.Context.Set<TEntity>().AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }
        public TEntity GetAny(Expression<Func<TEntity, bool>> expression)
        {
            this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);

            TEntity rValue = this.Context.Set<TEntity>().Where(expression).FirstOrDefault();
            if (rValue == null)
            {
                rValue = Activator.CreateInstance<TEntity>();
            }
            return rValue;
        }

        public int GetId(Expression<Func<TEntity, bool>> expression)
        {
            this.Context.Database.Log = logInfo => FileLogger.Log(logInfo);

            return this.Context.Set<TEntity>().First(expression).Id;
        }

        public string GetAutoNumber()
        {
            string id;
            try
            {
                var data = this.Context.Set<TEntity>().ToList();
                id = data.Max(r => Convert.ToInt32(r.Id) + 1).ToString();
            }
            catch (Exception)
            {
                id = "1";
            }
            return id;
        }

        /// <summary>
        /// Provides autonumber with prefix
        /// </summary>
        /// <param name="typePrefix">PO</param>
        /// <param name="yearPrefix">2013</param>
        /// <param name="monthPrefix">03</param>
        /// <returns></returns>
        public string GetAutoNumber(string typePrefix, string yearPrefix, string monthPrefix, int digits)
        {
            // PO-201305-00001
            string prefix = typePrefix + "-" + yearPrefix + monthPrefix + "-";
            int prefixLength = prefix.Length;

            string sequencePrefix = typePrefix + "-" + yearPrefix;
            int id;
            try
            {
                var data = this.Context.Set<TEntity>().Where(x => x.Id.ToString().StartsWith(sequencePrefix)).Select(x => x.Id).ToList();
                id = data.Max(r => Convert.ToInt32(r.ToString().Substring(prefixLength)) + 1);
            }
            catch (Exception)
            {
                id = 1;
            }
            return prefix + id.ToString().PadLeft(digits, '0');
        }

        public string GetAutoNumber(string typePrefix, string yearPrefix, int digits)
        {
            // PO-201305-00001
            string prefix = typePrefix + "-" + yearPrefix + "-";
            int prefixLength = prefix.Length;

            string sequencePrefix = typePrefix + "-" + yearPrefix;
            int id;
            try
            {
                var data = this.Context.Set<TEntity>().Where(x => x.Id.ToString().StartsWith(sequencePrefix)).Select(x => x.Id).ToList();
                id = data.Max(r => Convert.ToInt32(r.ToString().Substring(prefixLength)) + 1);
            }
            catch (Exception)
            {
                id = 1;
            }
            return prefix + id.ToString().PadLeft(digits, '0');
        }

        public string GetAutoNumber(string prefix, int digits)
        {
            int prefixLength = prefix.Length;

            int id;
            try
            {
                var data = this.Context.Set<TEntity>().Select(x => x.Id).ToList();
                id = data.Max(r => Convert.ToInt32(r.ToString().Substring(prefixLength)) + 1);
            }
            catch (Exception)
            {
                id = 1;
            }
            return prefix + id.ToString().PadLeft(digits, '0');
        }

        public string GetAutoNumber(string prefix)
        {
            int prefixLength = prefix.Length;

            int id;
            try
            {
                var data = this.Context.Set<TEntity>().Select(x => x.Id).ToList();
                id = data.Max(r => Convert.ToInt32(r.ToString().Substring(prefixLength)) + 1);
            }
            catch (Exception)
            {
                id = 1;
            }
            return prefix + id.ToString().PadLeft(5, '0');
        }
        #region All Common Response Method

        public CommonResponse getPageResponse(int pageno, int pagesize)
        {

            // Set.Local.Clear();
            CommonResponse cr = new CommonResponse();
            List<TEntity> result = new List<TEntity>();
            IEnumerable<TEntity> _res;
            if (pagesize != -1)
            {
                result = this.Context.Set<TEntity>().AsNoTracking().ToList().Skip(pagesize * (pageno - 1)).Take(pagesize).ToList();
                cr.results = result;
            }
            else
            {
                _res = this.Context.Set<TEntity>().AsNoTracking();
                cr.results = _res;


            }
            cr.httpStatusCode = HttpStatusCode.OK;
            //CommonResponse cr = new CommonResponse(result.Count() > 0 ? HttpStatusCode.OK : HttpStatusCode.NoContent);
            cr.pageno = pageno;
            cr.pagesize = pagesize;
            cr.totalcount = GetCount();
            cr.totalSum = 0;
            cr.Message = string.Empty;
            // cr.results = result;

            return cr;
        }
      

        public CommonResponse getResponseBySp(string SpName)
        {
            try
            {
                DataTable result = SqlHelper.ExecuteDataTable(ConStr, SpName, null);
                List<TEntity> lstData = ConvertDataTable<TEntity>(result);
                CommonResponse cr = new CommonResponse(lstData.Count > 0 ? HttpStatusCode.Accepted : HttpStatusCode.NoContent);
                cr.pageno = 0;
                cr.totalcount = 0;
                cr.totalSum = 0;
                cr.Message = string.Empty;
                cr.results = lstData;
                return cr;
            }
            catch (Exception ex)
            {

                CommonResponse cr = new CommonResponse(HttpStatusCode.BadRequest);
                cr.results = null;
                cr.HasError = true;
                cr.Message = ex.Message;
                return cr;
            }

        }
        public CommonResponse getExcuteSql(string SQL)
        {
            try
            {
                DataTable result = SqlHelper.ExecuteDataTable(ConStr, CommandType.Text, SQL);
                List<TEntity> lstData = ConvertDataTable<TEntity>(result);
                CommonResponse cr = new CommonResponse(lstData.Count > 0 ? HttpStatusCode.Accepted : HttpStatusCode.NoContent);
                cr.pageno = 0;
                cr.totalcount = 0;
                cr.totalSum = 0;
                cr.Message = string.Empty;
                cr.results = lstData;
                return cr;
            }
            catch (Exception ex)
            {

                CommonResponse cr = new CommonResponse(HttpStatusCode.BadRequest);
                cr.results = null;
                cr.HasError = true;
                cr.Message = ex.Message;
                return cr;
            }

        }
        public CommonResponse getDatasetResponseBySp(string SpName)
        {
            try
            {

                DataSet result = SqlHelper.ExecuteDataset(ConStr, SpName, null);
                CommonResponse cr = new CommonResponse(result.Tables.Count > 0 ? HttpStatusCode.OK : HttpStatusCode.NoContent);
                cr.results = result;
                cr.pageno = 0;
                cr.totalcount = 0;
                cr.totalSum = 0;
                cr.Message = string.Empty;
                cr.results = result;
                return cr;
            }
            catch (Exception ex)
            {

                CommonResponse cr = new CommonResponse(HttpStatusCode.BadRequest);
                cr.results = null;
                cr.HasError = true;
                cr.Message = ex.Message;
                return cr;
            }

        }
        public CommonResponse getDatasetResponseBySp(string SpName, params object[] paramValues)
        {
            try
            {

                DataSet result = SqlHelper.ExecuteDataset(ConStr, SpName, paramValues);
                CommonResponse cr = new CommonResponse(result.Tables.Count > 0 ? HttpStatusCode.OK : HttpStatusCode.NoContent);
                cr.results = result;
                cr.pageno = 0;
                cr.totalcount = 0;
                cr.totalSum = 0;
                cr.Message = string.Empty;
                cr.results = result;
                return cr;
            }
            catch (Exception ex)
            {

                CommonResponse cr = new CommonResponse(HttpStatusCode.BadRequest);
                cr.results = null;
                cr.HasError = true;
                cr.Message = ex.Message;
                return cr;
            }

        }

        public CommonResponse getResponseBySpWithParam(string SpName, params object[] parameterValues)
        {
            try
            {
                DataTable result = SqlHelper.ExecuteDataTable(ConStr, SpName, parameterValues);
                List<TEntity> lstData = ConvertDataTable<TEntity>(result);
                CommonResponse cr = new CommonResponse(lstData.Count > 0 ? HttpStatusCode.Accepted : HttpStatusCode.NoContent);
                cr.pageno = 0;
                cr.totalcount = 0;
                cr.totalSum = 0;
                cr.Message = string.Empty;
                cr.results = lstData;
                return cr;
            }
            catch (Exception ex)
            {

                CommonResponse cr = new CommonResponse(HttpStatusCode.BadRequest);
                cr.results = null;
                cr.HasError = true;
                cr.Message = ex.Message;
                return cr;
            }

        }


        #endregion

        #region Private Method
        public class FileLogger
        {
            public static void Log(string logInfo)
            {
               //var path= HostingEnvironment.ApplicationPhysicalPath;
               // File.AppendAllText( path+ @"\Logger.txt", logInfo);
            }
        }
        public string ConStr => ConfigurationManager.ConnectionStrings["eShop"].ToString();
        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        var aa = dr[column.ColumnName];
                        if (dr[column.ColumnName] == DBNull.Value)
                        {
                            if (column.DataType.Name == "Int32")
                            {
                                pro.SetValue(obj, 0, null);
                            }
                            else if (column.DataType.Name == "Decimal")
                            {
                                pro.SetValue(obj, Convert.ToDecimal(0.00), null);
                            }
                            else
                            {
                                pro.SetValue(obj, dr[column.ColumnName], null);
                            }
                        }
                        else
                        {
                            pro.SetValue(obj, dr[column.ColumnName], null);
                        }
                    }

                    else
                        continue;
                }
            }
            return obj;
        }
        public int GetCount()
        {
            return this.Context.Set<TEntity>().Count();
        }
        #endregion 
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
