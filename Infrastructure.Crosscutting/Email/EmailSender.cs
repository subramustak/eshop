﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Threading;
using System.Net.Mime;

namespace Infrastructure.Crosscutting.Email
{

    public class EmailSender
    {
        public static string Send(string to, string cc, string subject, string body, string[] attachments)
        {
            EmailSender sender = new EmailSender();
            MailMessage msg = sender.PrepareMessage(to, cc, subject, body, attachments);
            return sender.Send(msg);
        }

        public static string SendAsync(string to, string cc, string subject, string body)
        {
            EmailSender sender = new EmailSender();
            MailMessage msg = sender.PrepareMessage(to, cc, subject, body, null);
            try
            {
                Thread emailThread = new Thread(() => sender.Send(msg));
                emailThread.Start();
                return "OK";
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public static string SendAsync(string to, string cc, string subject, string body, string[] attachments)
        {
            EmailSender sender = new EmailSender();
            MailMessage msg = sender.PrepareMessage(to, cc, subject, body, attachments);
            try
            {
                Thread emailThread = new Thread(() => sender.Send(msg));
                emailThread.Start();
                return "OK";
            }
            catch (Exception ex)
            {
                return "ERROR";
            }

        }
        private string Send(MailMessage msg)
        {
            SmtpClient smtp = null;
            try
            {
                smtp = GetSmtpClient();
                smtp.Send(msg);
                return "OK";
            }
            catch (Exception)
            {
                return "ERROR";
            }
            finally
            {
                if (smtp != null)
                {
                    smtp.Dispose();
                }
            }
        }

        private MailMessage PrepareMessage(string to, string cc, string subject, string body, string[] attachments)
        {
            MailMessage msg = new MailMessage();

            try
            {
                msg.From = new MailAddress(new System.Configuration.AppSettingsReader().GetValue("EMAIL_FROM_ADDRESS", typeof(string)).ToString());
            }
            catch
            {
                throw new Exception("Incorrect configuration settings.");
            }

            if (to != "")
            {
                var splittedTo = to.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < splittedTo.Length; i++)
                {
                    try
                    {
                        MailAddress ccAddress = new MailAddress(splittedTo[i]);
                        msg.To.Add(ccAddress);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }

            }
            else
            {
                throw new Exception("No email address in to field");
            }

            if(msg.To.Count == 0)
            {
                throw new Exception("No valid email address in to field");
            }

            if (cc != "")
            {
                var splittedCC = cc.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < splittedCC.Length; i++)
                {
                    try
                    {
                        MailAddress ccAddress = new MailAddress(splittedCC[i]);
                        msg.CC.Add(ccAddress);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }

            if (attachments != null)
            {
                for (int i = 0; i < attachments.Length; i++)
                {
                    Attachment attached = new Attachment(attachments[i]);
                    msg.Attachments.Add(attached);
                }
            }
            msg.Subject = subject;
            msg.Body = body;
            msg.IsBodyHtml = true;
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body,  new ContentType("text/html"));
            msg.AlternateViews.Add(htmlView);
            return msg;
        }

        private SmtpClient GetSmtpClient()
        {
            SmtpClient smtp = new SmtpClient();
            try
            {
                string host = new AppSettingsReader().GetValue("EMAIL_SMTP_SERVER", typeof(string)).ToString();
                int port = Convert.ToInt16(new AppSettingsReader().GetValue("EMAIL_SMTP_PORT", typeof(string)).ToString());
                string username = new AppSettingsReader().GetValue("EMAIL_USERNAME", typeof(string)).ToString();
                string password = new AppSettingsReader().GetValue("EMAIL_PASSWORD", typeof(string)).ToString();
                bool isSSL = Convert.ToBoolean(new AppSettingsReader().GetValue("EMAIL_SECURED", typeof(string)).ToString());

                smtp = new SmtpClient(host, port);
                NetworkCredential cred = new NetworkCredential(username, password);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = cred;
                smtp.EnableSsl = isSSL;
            }
            catch
            {
                throw new Exception("Incorrect configuration settings.");
            }
            return smtp;
        }

    }
}