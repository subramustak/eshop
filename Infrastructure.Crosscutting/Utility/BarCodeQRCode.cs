﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using ZXing;
using ZXing.Common;
using ZXing.Rendering;

namespace Infrastructure.Crosscutting.Utility
{
    public class BarcodeGenerator
    {
       
        private string userid;

        public  byte[] BarCode(string data) {
            Image img = null;
            using (var ms = new MemoryStream())
            {
                var writer = new ZXing.BarcodeWriter() { Format = BarcodeFormat.CODE_128 };
                writer.Options.Height = 80;
                writer.Options.Width = 280;
                writer.Options.PureBarcode = true;
                img = writer.Write(data);
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
       

        public byte[] GetImageBytes(System.Drawing.Image image)
        {
            byte[] byteArray = new byte[0];
            Image img = null;
            
            return byteArray;

        }


    }

        public class ImageRenderer
        {

    }
}
