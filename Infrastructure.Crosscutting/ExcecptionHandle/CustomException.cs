﻿using System;
using System.Collections.Concurrent;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.IO;
using System.Text;
using System.Web.Hosting;

namespace Infrastructure.Crosscutting.ExcecptionHandle
{
   public class CustomException
    {

    }
    public class CustomEFInterceptor : IDbCommandInterceptor
    {
        static readonly ConcurrentDictionary<DbCommand, DateTime> m_StartTime = new ConcurrentDictionary<DbCommand, DateTime>();
        void IDbCommandInterceptor.NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            Log(command, interceptionContext);
        }
        void IDbCommandInterceptor.NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            OnStart(command);
        }
        void IDbCommandInterceptor.ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            Log(command, interceptionContext);
        }
        void IDbCommandInterceptor.ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            OnStart(command);
        }
        void IDbCommandInterceptor.ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            Log(command, interceptionContext);
        }
        void IDbCommandInterceptor.ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            OnStart(command);
        }
        private static void OnStart(DbCommand command)
        {
            m_StartTime.TryAdd(command, DateTime.Now);
        }
        public static void Log<T>(DbCommand command, DbCommandInterceptionContext<T> interceptionContext)
        {
            DateTime startTime;
            TimeSpan duration;
            m_StartTime.TryRemove(command, out startTime);
            if (startTime != default(DateTime))
            {
                duration = DateTime.Now - startTime;
            }
            else
            {
                duration = TimeSpan.Zero;
            }
            string message;
            var parameters = new StringBuilder();
            foreach (DbParameter param in command.Parameters)
            {
                parameters.AppendLine(param.ParameterName + " " + param.DbType + " = " + param.Value);
            }

            if (interceptionContext.Exception == null)
            {
                message = string.Format("\r\nDatabase call took {0} ms. \r\nCommand:\r\n{1}\r\n", duration.TotalMilliseconds.ToString(), parameters.ToString() + command.CommandText);
            }
            else
            {
                message = string.Format("\r\nEF Database call failed after {0} ms. \r\nCommand:\r\n{1}\r\nError:{2} ", duration.TotalMilliseconds.ToString(), parameters.ToString() + command.CommandText, interceptionContext.Exception);
            }
            var path = HostingEnvironment.ApplicationPhysicalPath;
            File.AppendAllText(path + @"\ErrorLogger.txt", message);
         
        }
    }
 
}
