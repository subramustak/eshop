namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bankid : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TransectionHistories", "BankId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TransectionHistories", "BankId", c => c.Int(nullable: false));
        }
    }
}
