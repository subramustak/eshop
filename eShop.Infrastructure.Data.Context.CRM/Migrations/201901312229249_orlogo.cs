namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orlogo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Organizations", "Logo", c => c.Binary());
            AddColumn("dbo.Organizations", "Header", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Organizations", "Header");
            DropColumn("dbo.Organizations", "Logo");
        }
    }
}
