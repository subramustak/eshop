namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class envt2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "EventId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "EventId", c => c.Int(nullable: false));
        }
    }
}
