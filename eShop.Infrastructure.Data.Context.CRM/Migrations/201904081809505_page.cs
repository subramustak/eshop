namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class page : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetPages", "ModuleName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetPages", "ModuleName");
        }
    }
}
