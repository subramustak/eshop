namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sales : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseReturns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiecNo = c.String(),
                        SupplierId = c.Int(),
                        BranchId = c.Int(),
                        BankId = c.Int(),
                        ChequeNo = c.String(),
                        PaymentType = c.Int(nullable: false),
                        EmployeeId = c.Int(),
                        Date = c.String(),
                        Due = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PurcheaseDetailReturns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        UnitePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAmount = c.Decimal(precision: 18, scale: 2),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                        purchaseReturns_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.PurchaseReturns", t => t.purchaseReturns_Id)
                .Index(t => t.ProductId)
                .Index(t => t.purchaseReturns_Id);
            
            CreateTable(
                "dbo.SalesDetailsReturns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        UnitePrice = c.Decimal(precision: 18, scale: 2),
                        Date = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAmount = c.Decimal(precision: 18, scale: 2),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                        SalesReturns_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SalesReturns", t => t.SalesReturns_Id)
                .Index(t => t.SalesReturns_Id);
            
            CreateTable(
                "dbo.SalesReturns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiecNo = c.String(),
                        CustomerId = c.Int(),
                        EmployeeId = c.Int(),
                        BankId = c.Int(),
                        BranchId = c.Int(),
                        PaymentType = c.Int(),
                        DiscountRemarks = c.String(),
                        Discount = c.Decimal(precision: 18, scale: 2),
                        TotalDiscount = c.Decimal(precision: 18, scale: 2),
                        Due = c.Decimal(precision: 18, scale: 2),
                        Total = c.Decimal(precision: 18, scale: 2),
                        Return = c.Decimal(precision: 18, scale: 2),
                        NetTotal = c.Decimal(precision: 18, scale: 2),
                        Date = c.String(),
                        ChequeNo = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Sales", "Return", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesDetailsReturns", "SalesReturns_Id", "dbo.SalesReturns");
            DropForeignKey("dbo.PurcheaseDetailReturns", "purchaseReturns_Id", "dbo.PurchaseReturns");
            DropForeignKey("dbo.PurcheaseDetailReturns", "ProductId", "dbo.Products");
            DropIndex("dbo.SalesDetailsReturns", new[] { "SalesReturns_Id" });
            DropIndex("dbo.PurcheaseDetailReturns", new[] { "purchaseReturns_Id" });
            DropIndex("dbo.PurcheaseDetailReturns", new[] { "ProductId" });
            DropColumn("dbo.Sales", "Return");
            DropTable("dbo.SalesReturns");
            DropTable("dbo.SalesDetailsReturns");
            DropTable("dbo.PurcheaseDetailReturns");
            DropTable("dbo.PurchaseReturns");
        }
    }
}
