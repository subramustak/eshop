namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cusID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "CustomerId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "CustomerId");
        }
    }
}
