namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class envt : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventName = c.String(),
                        DsicountAmount = c.Int(nullable: false),
                        DsicountPercent = c.Boolean(),
                        DsicountCount = c.Int(nullable: false),
                        startDate = c.String(),
                        endDate = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Products", "Barcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Barcode");
            DropTable("dbo.Events");
        }
    }
}
