namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "VAT", c => c.Int());
            AddColumn("dbo.TransectionHistories", "InvoiceNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TransectionHistories", "InvoiceNo");
            DropColumn("dbo.Products", "VAT");
        }
    }
}
