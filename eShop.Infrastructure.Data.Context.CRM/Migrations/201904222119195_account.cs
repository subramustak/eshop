namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class account : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RootAccounts", "DC", c => c.Boolean(nullable: false));
            AddColumn("dbo.RootAccounts", "IsDisplay", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RootAccounts", "IsDisplay");
            DropColumn("dbo.RootAccounts", "DC");
        }
    }
}
