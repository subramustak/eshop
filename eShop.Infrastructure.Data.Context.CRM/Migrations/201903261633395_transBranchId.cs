namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class transBranchId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransectionHistories", "BranchId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TransectionHistories", "BranchId");
        }
    }
}
