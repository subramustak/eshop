namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventC : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "startDate", c => c.String());
            AddColumn("dbo.Events", "endDate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "endDate");
            DropColumn("dbo.Events", "startDate");
        }
    }
}
