// <auto-generated />
namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AccountPay : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AccountPay));
        
        string IMigrationMetadata.Id
        {
            get { return "201901251819018_AccountPay"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
