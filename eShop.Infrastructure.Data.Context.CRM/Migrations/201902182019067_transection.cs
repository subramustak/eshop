namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class transection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountNames", "OpeningBalance", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.AccountNames", "ClossingBalance", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.TransectionHistories", "OpeningBalance", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.TransectionHistories", "ClossingBalance", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TransectionHistories", "ClossingBalance");
            DropColumn("dbo.TransectionHistories", "OpeningBalance");
            DropColumn("dbo.AccountNames", "ClossingBalance");
            DropColumn("dbo.AccountNames", "OpeningBalance");
        }
    }
}
