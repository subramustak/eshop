namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AccountNames", "PayNreciveId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AccountNames", "PayNreciveId", c => c.Int(nullable: false));
        }
    }
}
