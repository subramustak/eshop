namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settingupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "ReportSize", c => c.Int(nullable: false));
            AddColumn("dbo.Settings", "VATNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "VATNo");
            DropColumn("dbo.Settings", "ReportSize");
        }
    }
}
