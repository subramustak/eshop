namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Events : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Events", "DsicountPercent", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "DsicountPercent", c => c.Boolean(nullable: false));
        }
    }
}
