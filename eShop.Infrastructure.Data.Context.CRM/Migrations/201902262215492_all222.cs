namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class all222 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurcheaseDetailReturns", "purchaseReturns_Id", "dbo.PurchaseReturns");
            DropIndex("dbo.PurcheaseDetailReturns", new[] { "purchaseReturns_Id" });
            RenameColumn(table: "dbo.PurcheaseDetailReturns", name: "purchaseReturns_Id", newName: "PurchaseReturnId");
            AlterColumn("dbo.PurcheaseDetailReturns", "PurchaseReturnId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurcheaseDetailReturns", "PurchaseReturnId");
            AddForeignKey("dbo.PurcheaseDetailReturns", "PurchaseReturnId", "dbo.PurchaseReturns", "Id", cascadeDelete: true);
            DropColumn("dbo.PurcheaseDetailReturns", "PurchaseId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurcheaseDetailReturns", "PurchaseId", c => c.Int(nullable: false));
            DropForeignKey("dbo.PurcheaseDetailReturns", "PurchaseReturnId", "dbo.PurchaseReturns");
            DropIndex("dbo.PurcheaseDetailReturns", new[] { "PurchaseReturnId" });
            AlterColumn("dbo.PurcheaseDetailReturns", "PurchaseReturnId", c => c.Int());
            RenameColumn(table: "dbo.PurcheaseDetailReturns", name: "PurchaseReturnId", newName: "purchaseReturns_Id");
            CreateIndex("dbo.PurcheaseDetailReturns", "purchaseReturns_Id");
            AddForeignKey("dbo.PurcheaseDetailReturns", "purchaseReturns_Id", "dbo.PurchaseReturns", "Id");
        }
    }
}
