namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VVAadd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SalesDetails", "VAT", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SalesDetails", "VAT");
        }
    }
}
