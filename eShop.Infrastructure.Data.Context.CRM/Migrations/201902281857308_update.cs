namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PurcheaseDetailReturns", "Date");
            DropColumn("dbo.PurcheaseDetails", "Date");
            DropColumn("dbo.SalesDetails", "Date");
            DropColumn("dbo.SalesDetails", "Bonus");
            DropColumn("dbo.SalesDetails", "AssaignQuantity");
            DropColumn("dbo.SalesDetails", "WithOurDiscountPrice");
            DropColumn("dbo.SalesDetailsReturns", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalesDetailsReturns", "Date", c => c.String());
            AddColumn("dbo.SalesDetails", "WithOurDiscountPrice", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "AssaignQuantity", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "Bonus", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "Date", c => c.String());
            AddColumn("dbo.PurcheaseDetails", "Date", c => c.String());
            AddColumn("dbo.PurcheaseDetailReturns", "Date", c => c.String());
        }
    }
}
