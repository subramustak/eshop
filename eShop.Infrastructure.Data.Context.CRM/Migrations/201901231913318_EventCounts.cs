namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventCounts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "DsicountCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "DsicountCount");
        }
    }
}
