namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class set : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Organizations", "Currency", c => c.String());
            AddColumn("dbo.Organizations", "VAT", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Organizations", "VATNo", c => c.String());
            AddColumn("dbo.Organizations", "ReportSize", c => c.Int(nullable: false));
            AddColumn("dbo.Organizations", "CustomerId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Organizations", "CustomerId");
            DropColumn("dbo.Organizations", "ReportSize");
            DropColumn("dbo.Organizations", "VATNo");
            DropColumn("dbo.Organizations", "VAT");
            DropColumn("dbo.Organizations", "Currency");
        }
    }
}
