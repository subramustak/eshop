namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class receivername : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseReturns", "ReceiverName", c => c.String());
            AddColumn("dbo.PurchaseReturns", "ReceiverNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseReturns", "ReceiverNumber");
            DropColumn("dbo.PurchaseReturns", "ReceiverName");
        }
    }
}
