namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adddatab123 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetRoles", "DatabaseId", c => c.Int());
            AlterColumn("dbo.AspNetUsers", "DatabaseId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "DatabaseId", c => c.Int(nullable: false));
            AlterColumn("dbo.AspNetRoles", "DatabaseId", c => c.Int(nullable: false));
        }
    }
}
