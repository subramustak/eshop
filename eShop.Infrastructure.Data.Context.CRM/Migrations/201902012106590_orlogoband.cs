namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orlogoband : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "ProductBrandId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "ProductBrandId", c => c.String());
        }
    }
}
