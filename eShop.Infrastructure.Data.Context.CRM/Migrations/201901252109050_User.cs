namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class User : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetPages",
                c => new
                    {
                        PageID = c.Int(nullable: false, identity: true),
                        ParentID = c.Int(nullable: false),
                        NameOption_En = c.String(),
                        NameOption_Bn = c.String(),
                        Controller = c.String(),
                        Action = c.String(),
                        Area = c.String(),
                        IconClass = c.String(),
                        ActiveLi = c.String(),
                        Status = c.String(),
                        Displayorder = c.Int(nullable: false),
                        IsParent = c.Boolean(),
                    })
                .PrimaryKey(t => t.PageID);
            
            CreateTable(
                "dbo.AspNetPagesRoles",
                c => new
                    {
                        PageRoleId = c.Int(nullable: false, identity: true),
                        PageId = c.Int(nullable: false),
                        RoleId = c.String(maxLength: 128),
                        CanAdd = c.Boolean(nullable: false),
                        CanEdit = c.Boolean(nullable: false),
                        CanView = c.Boolean(nullable: false),
                        CanDelete = c.Boolean(nullable: false),
                        CanApprove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PageRoleId)
                .ForeignKey("dbo.AspNetPages", t => t.PageId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.PageId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetPagesRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetPagesRoles", "PageId", "dbo.AspNetPages");
            DropIndex("dbo.AspNetPagesRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetPagesRoles", new[] { "PageId" });
            DropTable("dbo.AspNetPagesRoles");
            DropTable("dbo.AspNetPages");
        }
    }
}
