namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "AccountPrefix", c => c.String());
            AddColumn("dbo.Settings", "AccountPreStart", c => c.String());
            AddColumn("dbo.Settings", "AccountPostfix", c => c.String());
            AddColumn("dbo.Settings", "VAT", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "VAT");
            DropColumn("dbo.Settings", "AccountPostfix");
            DropColumn("dbo.Settings", "AccountPreStart");
            DropColumn("dbo.Settings", "AccountPrefix");
        }
    }
}
