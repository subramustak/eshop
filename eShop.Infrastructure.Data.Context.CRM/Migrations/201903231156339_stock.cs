namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stock : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "VATPercent", c => c.Int());
            DropColumn("dbo.Products", "VAT");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "VAT", c => c.Int());
            DropColumn("dbo.Products", "VATPercent");
        }
    }
}
