namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            
           
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiecNo = c.String(),
                        LCNo = c.String(),
                        SupplierId = c.Int(),
                        BranchId = c.Int(),
                        BankId = c.Int(),
                        ChequeNo = c.String(),
                        PaymentType = c.Int(nullable: false),
                        EmployeeId = c.Int(),
                        Date = c.String(),
                        Due = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CouponName = c.String(),
                        CouponAmunt = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PurcheaseDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PurchaseId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        ProductName = c.String(),
                        ProductCode = c.String(),
                        UnitePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Slup = c.Decimal(precision: 18, scale: 2),
                        TotalPrice = c.Decimal(precision: 18, scale: 2),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                        Purcheases_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Purchases", t => t.Purcheases_Id)
                .Index(t => t.ProductId)
                .Index(t => t.Purcheases_Id);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurcheaseDetails", "Purcheases_Id", "dbo.Purchases");
            DropIndex("dbo.PurcheaseDetails", new[] { "Purcheases_Id" });
            DropIndex("dbo.PurcheaseDetails", new[] { "ProductId" });
            DropTable("dbo.PurcheaseDetails");
            DropTable("dbo.Purchases");
        }
    }
}
