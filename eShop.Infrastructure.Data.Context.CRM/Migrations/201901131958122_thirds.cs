namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class thirds : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StockDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesId = c.Int(),
                        SalesReturnId = c.Int(),
                        PurcheaseId = c.Int(),
                        PurcheaseReturnId = c.Int(),
                        ProductId = c.Int(),
                        SupplierId = c.Int(),
                        CustomerId = c.Int(),
                        BranchId = c.Int(),
                        EmployeeId = c.Int(),
                        BankId = c.Int(),
                        StockReplace = c.Decimal(precision: 18, scale: 2),
                        TransReplace = c.Decimal(precision: 18, scale: 2),
                        TotalReplace = c.Decimal(precision: 18, scale: 2),
                        StockReturn = c.Decimal(precision: 18, scale: 2),
                        TransReturn = c.Decimal(precision: 18, scale: 2),
                        TotalReturn = c.Decimal(precision: 18, scale: 2),
                        StockDiscount = c.Decimal(precision: 18, scale: 2),
                        TransDiscount = c.Decimal(precision: 18, scale: 2),
                        TotalDiscount = c.Decimal(precision: 18, scale: 2),
                        StockQuantity = c.Decimal(precision: 18, scale: 2),
                        TransQuantity = c.Decimal(precision: 18, scale: 2),
                        TotalQuantity = c.Decimal(precision: 18, scale: 2),
                        TotalPaid = c.Decimal(precision: 18, scale: 2),
                        StockPrice = c.Decimal(precision: 18, scale: 2),
                        TransPrice = c.Decimal(precision: 18, scale: 2),
                        TotalPrice = c.Decimal(precision: 18, scale: 2),
                        StockStutes = c.Boolean(),
                        Date = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StockDetails");
        }
    }
}
