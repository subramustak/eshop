namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class transDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransectionHistories", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TransectionHistories", "Date");
        }
    }
}
