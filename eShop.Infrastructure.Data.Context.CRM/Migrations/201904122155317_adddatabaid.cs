namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adddatabaid : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DatabaseDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CoCode = c.String(),
                        DatabaseName = c.String(),
                        DBName = c.String(),
                        DBSource = c.String(),
                        DBUserName = c.String(),
                        DBPassword = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetRoles", "DatabaseId", c => c.Int(nullable: true));
            AddColumn("dbo.AspNetUsers", "DatabaseId", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "DatabaseId");
            DropColumn("dbo.AspNetRoles", "DatabaseId");
            DropTable("dbo.DatabaseDetails");
        }
    }
}
