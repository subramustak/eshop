namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addnew : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Purchases", "NetTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Purchases", "Total", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Purchases", "TotalPaid", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Purchases", "Return", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.PurcheaseDetails", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Sales", "NetTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Sales", "Total", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "NetAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "SalsPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Purchases", "SupplierId", c => c.Int(nullable: false));
            AlterColumn("dbo.Purchases", "Discount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Sales", "CustomerId", c => c.Int(nullable: false));
            AlterColumn("dbo.Sales", "EmployeeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Sales", "PaymentType", c => c.Int(nullable: false));
            AlterColumn("dbo.Sales", "Due", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Sales", "TotalPaid", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.SalesDetails", "UnitePrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.SalesDetails", "Return", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Purchases", "LCNo");
            DropColumn("dbo.Purchases", "TotalPayment");
            DropColumn("dbo.Purchases", "CouponName");
            DropColumn("dbo.Purchases", "CouponAmunt");
            DropColumn("dbo.PurcheaseDetails", "ProductName");
            DropColumn("dbo.PurcheaseDetails", "ProductCode");
            DropColumn("dbo.PurcheaseDetails", "Slup");
            DropColumn("dbo.PurcheaseDetails", "TotalPrice");
            DropColumn("dbo.Sales", "DiscountRemarks");
            DropColumn("dbo.Sales", "GrandTotal");
            DropColumn("dbo.Sales", "PackUnitPrice");
            DropColumn("dbo.Sales", "PackQuantity");
            DropColumn("dbo.Sales", "EventName");
            DropColumn("dbo.Sales", "EventAamount");
            DropColumn("dbo.SalesDetails", "Replace");
            DropColumn("dbo.SalesDetails", "TotalAmount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalesDetails", "TotalAmount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "Replace", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Sales", "EventAamount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Sales", "EventName", c => c.String());
            AddColumn("dbo.Sales", "PackQuantity", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Sales", "PackUnitPrice", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Sales", "GrandTotal", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Sales", "DiscountRemarks", c => c.String());
            AddColumn("dbo.PurcheaseDetails", "TotalPrice", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.PurcheaseDetails", "Slup", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.PurcheaseDetails", "ProductCode", c => c.String());
            AddColumn("dbo.PurcheaseDetails", "ProductName", c => c.String());
            AddColumn("dbo.Purchases", "CouponAmunt", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Purchases", "CouponName", c => c.String());
            AddColumn("dbo.Purchases", "TotalPayment", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Purchases", "LCNo", c => c.String());
            AlterColumn("dbo.SalesDetails", "Return", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.SalesDetails", "UnitePrice", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Sales", "TotalPaid", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Sales", "Due", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Sales", "PaymentType", c => c.Int());
            AlterColumn("dbo.Sales", "EmployeeId", c => c.Int());
            AlterColumn("dbo.Sales", "CustomerId", c => c.Int());
            AlterColumn("dbo.Purchases", "Discount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Purchases", "SupplierId", c => c.Int());
            AlterColumn("dbo.Products", "SalsPrice", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.SalesDetails", "NetAmount");
            DropColumn("dbo.SalesDetails", "Amount");
            DropColumn("dbo.Sales", "Total");
            DropColumn("dbo.Sales", "NetTotal");
            DropColumn("dbo.PurcheaseDetails", "Amount");
            DropColumn("dbo.Purchases", "Return");
            DropColumn("dbo.Purchases", "TotalPaid");
            DropColumn("dbo.Purchases", "Total");
            DropColumn("dbo.Purchases", "NetTotal");
        }
    }
}
