namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _209 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PurchaseReturns", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Purchases", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Sales", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SalesReturns", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Stocks", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Stocks", "Date", c => c.String());
            AlterColumn("dbo.SalesReturns", "Date", c => c.String());
            AlterColumn("dbo.Sales", "Date", c => c.String());
            AlterColumn("dbo.Purchases", "Date", c => c.String());
            AlterColumn("dbo.PurchaseReturns", "Date", c => c.String());
        }
    }
}
