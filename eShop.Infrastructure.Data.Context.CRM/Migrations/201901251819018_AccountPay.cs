namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountPay : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountNames", "PayNreciveId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AccountNames", "PayNreciveId");
        }
    }
}
