namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orlogobands : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "ProductBrandId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "ProductBrandId", c => c.Int(nullable: false));
        }
    }
}
