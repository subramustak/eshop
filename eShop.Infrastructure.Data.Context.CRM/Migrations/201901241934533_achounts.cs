namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class achounts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        RootId = c.Int(nullable: false),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RootAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TransectionHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        PaymentAmount = c.Decimal(precision: 18, scale: 2),
                        ReciveAmount = c.Decimal(precision: 18, scale: 2),
                        PaymentType = c.Int(nullable: false),
                        IsPayment = c.Boolean(),
                        IsRecive = c.Boolean(),
                        BankId = c.Int(nullable: false),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Events");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventName = c.String(),
                        DsicountAmount = c.Int(nullable: false),
                        DsicountPercent = c.Boolean(),
                        DsicountCount = c.Int(nullable: false),
                        startDate = c.String(),
                        endDate = c.String(),
                        IsArchived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        AddBy = c.String(maxLength: 128),
                        AddDate = c.DateTime(),
                        UpdateBy = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        Remarks = c.String(maxLength: 100),
                        Status = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.TransectionHistories");
            DropTable("dbo.RootAccounts");
            DropTable("dbo.AccountNames");
        }
    }
}
