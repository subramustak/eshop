namespace eShop.Infrastructure.Data.Context.CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stocktrns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StockTransfars", "Date", c => c.String());
            AddColumn("dbo.StockTransfars", "IsApprove", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StockTransfars", "IsApprove");
            DropColumn("dbo.StockTransfars", "Date");
        }
    }
}
