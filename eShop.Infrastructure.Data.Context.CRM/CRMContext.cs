﻿using eShop.BusinessDomain.Account;
using eShop.BusinessDomain.Account.DomainObject;
using eShop.BusinessDomain.Admin.DomainObject;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using System.Data.Entity;

namespace eShop.Infrastructure.Data.Context.CRM
{
        public class CRMContext : DbContext
        {
        
            public CRMContext()
                : base("eShopDB")
            {
            //#if NO_Migration
            //              Database.SetInitializer<AdminContext>(new NoMigration());
            //#else
            //              Database.SetInitializer<CRMContext>(new MigrateDatabaseToLatestVersion<CRMContext, Configuration>());
            //#endif
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<AspNetPage> AspNetPage { get; set; }
        public DbSet<AspNetPagesRole> AspNetPagesRole { get; set; }
        public DbSet<AspNetRole> AspNetRole { get; set; }
        public DbSet<AspNetUser> AspNetUser { get; set; }

        public IDbSet<Branch> Branch { get; set; }
        public IDbSet<Bank> Bank { get; set; }
        public IDbSet<Brand> Brand { get; set; }
        public IDbSet<Category> Category { get; set; }
        public IDbSet<Color> Color { get; set; }
        public IDbSet<Size> Size { get; set; }
        public IDbSet<SubCategory> SubCategory { get; set; }
        public IDbSet<UOM> UOM { get; set; }
        public IDbSet<Event> Event { get; set; }
        public IDbSet<Product> Product { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Purchases> Purchases { get; set; }
        public DbSet<PurcheaseDetails> PurcheaseDetails { get; set; }
        public DbSet<Sales> Sales { get; set; }
        public DbSet<SalesDetails> SalesDetails { get; set; }
        public DbSet<Stocks> Stocks { get; set; }
        public DbSet<StockDetail> StockDetail { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<AccountName> AccountName { get; set; }
        public DbSet<RootAccount> RootAccount { get; set; }
        public DbSet<TransectionHistory> TransectionHistory { get; set; }
        public DbSet<Organization> Organization { get; set; }
        public DbSet<PurchaseReturn> PurchaseReturn { get; set; }
        public DbSet<PurcheaseDetailReturn> PurcheaseDetailReturns { get; set; }
        public DbSet<SalesReturn> SalesReturn { get; set; }
        public DbSet<SalesDetailsReturn> SalesDetailsReturns { get; set; }
        public DbSet<StockTransfars> StockTransfars { get; set; }
        public DbSet<DatabaseDetail> DatabaseDetails { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
