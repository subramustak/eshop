using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.CRM.ViewModels;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Collections.Generic;
using System.Transactions;
using System.Web.Http;

namespace eshop.ApplicationService.CRM
{
    public class PurchaseController : ApiController
    {
        public class mustak
        {
            public Purchases purchase { get; set; }
            public List<PurcheaseDetails> PurcheaseDetails { get; set; }
        }
        PurchaseService purchaseService;
        public PurchaseController()
        {
            this.purchaseService = new PurchaseService();
        }

        #region Purchase

        [Route("Purchase/AddPurchase/")]
        [HttpPost]
        public IHttpActionResult AddPurchase(Purchases purchase)
        {
            CommonResponse cr = new CommonResponse();
            Purchases oPurchase = new Purchases();
            try
            {
                using (var scope = new TransactionScope())
                {
                    if (purchase.Id == 0)
                    {
                        purchaseService.Add(purchase);
                        cr.results = purchase.Id;
                        cr.Message = Message.SAVED;
                    }
                    else
                    {
                        purchaseService.Update(purchase);
                        cr.results = this.purchaseService.Get(purchase.Id);
                        cr.Message = Message.UPDATED;
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/GetAllPurchase/")]
        [HttpGet]
        public IHttpActionResult GetAllPurchase()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = purchaseService.GetAllPurchase();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/GetPurchase/{Id}")]
        [HttpGet]
        public IHttpActionResult GetPurchase(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.purchaseService.Get(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/GetInvoice/{InvoiceNo}")]
        [HttpGet]
        public IHttpActionResult GetInvoice(string InvoiceNo)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.purchaseService.GetInvoice(InvoiceNo);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/GenerateInvoice/")]
        [HttpGet]
        public IHttpActionResult GenerateInvoice()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.purchaseService.GenerateInvoices();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/GetAllPurchaseDue/")]
        [HttpGet]
        public IHttpActionResult GetAllPurchaseDue()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.purchaseService.GetAllPurchaseDue();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/DeletePurchase/")]
        [HttpPost]
        public IHttpActionResult DeletePurchase(Purchases purchase)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                purchaseService.Delete(purchase);
                cr.Message = Message.DELETED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/StatusPurchase/")]
        [HttpPost]
        public IHttpActionResult StatusPurchase(Purchases purchase)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                this.purchaseService.ChangeStatus(purchase);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Purchase/PurchaseReport/")]
        [HttpPost]
        public IHttpActionResult PurchaseReport(PurchaseReportVM  purchase)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results=this.purchaseService.PerchesReport(purchase);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }

        #endregion Purchase
    }
}
