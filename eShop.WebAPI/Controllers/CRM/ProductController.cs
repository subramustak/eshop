using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.CRM.ViewModels;
using Infrastructure.Crosscutting.Utility;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;

namespace eshop.ApplicationService.CRM
{
    public class ProductController : ApiController
    {
        ProductService productService;
        public ProductController()
        {
            this.productService = new ProductService();
        }
        #region Product
        [Route("Product/AddProduct/")]
        [HttpPost]
        public IHttpActionResult AddProduct()
        {
            CommonResponse cr = new CommonResponse();
            Product oProduct = new Product();
            try
            {
                string currFilePath = string.Empty;
                string currFileExtension = string.Empty;
                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files["Image"] : null;

                string value = HttpContext.Current.Request.Form["Product"] ?? "";
                if (string.IsNullOrEmpty(value))
                    return BadRequest("Incorrect Format.");
                Product Product = JsonConvert.DeserializeObject<Product>(value);
                if (Product == null) throw new Exception("Product Not Found");
                if (file != null)
                {
                    string fileName = file.FileName;
                    string tempPath = System.IO.Path.GetTempPath();   //Get Temporary File Path
                    fileName = System.IO.Path.GetFileName(fileName); //Get File Name (not including path)
                    currFileExtension = System.IO.Path.GetExtension(fileName);   //Get File Extension
                    currFilePath = tempPath + fileName; //Get File Path after Uploading and Record to Former Declared Global Variable
                    file.SaveAs(currFilePath);  //Upload
                    Product.Image = Converters.ToByte(file);
                }
                if (Product.Id == 0)
                {
                    productService.Add(Product);
                    cr.Message = Message.SAVED;
                }
                else
                {
                    productService.Update(Product);
                    cr.Message = Message.UPDATED;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/AddProductExcel/")]
        [HttpPost]
        public IHttpActionResult AddProductExcel()
        {
            CommonResponse cr = new CommonResponse();
            Product oProduct = new Product();
            ProductVM productVM = new ProductVM();
            ProductVM productVMsave = new ProductVM();
            List<ProductVM> productVMLIst = new List<ProductVM>();

            try
            {

                string currFilePath = string.Empty;
                string currFileExtension = string.Empty;
                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files["file"] : null;

                string value = HttpContext.Current.Request.Form["Product"] ?? "";
                if (string.IsNullOrEmpty(value))
                    return BadRequest("Incorrect Format.");
                Product Product = JsonConvert.DeserializeObject<Product>(value);
                if (Product == null) throw new Exception("Product Not Found");
                if (file != null)
                {
                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var workSheet = package.Workbook.Worksheets["ProductUpload"];
                        var noOfRow = workSheet.Dimension.End.Row;
                        for (int i = 2; i <= noOfRow; i++)
                        {
                            productVM = new ProductVM();
                            string Category = workSheet.Cells[1, 1].Value != null ? workSheet.Cells[1, 1].Value.ToString() : string.Empty;
                            if (Category == null) throw new Exception("Category not Found");
                            productVM.CatagoryName = Category.Split('_')[1];
                            productVM.CatagoryId = Category == null ? 0 : Convert.ToInt32(Category.Split('_')[2]);

                            string SubCategory = workSheet.Cells[1, 2].Value != null ? workSheet.Cells[1, 2].Value.ToString() : string.Empty;
                            if (SubCategory == null) throw new Exception("SubCategory not Found");
                            productVM.SubCatagoryName = SubCategory.Split('_')[1];
                            productVM.SubCatagoryId = SubCategory == null ? 0 : Convert.ToInt32(SubCategory.Split('_')[2]);

                            string Brand = workSheet.Cells[1, 3].Value != null ? workSheet.Cells[1, 3].Value.ToString() : string.Empty;
                            if (Brand == null) throw new Exception("Brand not Found");
                            productVM.ProductBrandName = Brand.Split('_')[1];
                            productVM.ProductBrandId = Brand == null ? 0 : Convert.ToInt32(Brand.Split('_')[2]);

                            string UOM = workSheet.Cells[1, 4].Value != null ? workSheet.Cells[1, 4].Value.ToString() : string.Empty;
                            if (UOM == null) throw new Exception("UOM not Found");
                            productVM.UOMName = UOM.Split('_')[1];
                            productVM.UOMId = UOM == null ? 0 : Convert.ToInt32(UOM.Split('_')[2]);

                            string Size = workSheet.Cells[1, 5].Value != null ? workSheet.Cells[1, 5].Value.ToString() : string.Empty;
                            if (Size == null) throw new Exception("Size not Found");
                            productVM.ProductSizeName = Size.Split('_')[1];
                            productVM.SizeId = Size == null ? 0 : Convert.ToInt32(Size.Split('_')[2]);


                            string Name1 = workSheet.Cells[1, 6].Value != null ? workSheet.Cells[1, 6].Value.ToString() : string.Empty;
                            string Code = workSheet.Cells[1, 6].Value != null ? workSheet.Cells[1, 6].Value.ToString() : string.Empty;

                            productVM.Code = workSheet.Cells[i, 6].Value != null ? workSheet.Cells[i, 6].Value.ToString() : string.Empty;
                            if (string.IsNullOrEmpty(productVM.Code))
                            {

                            }
                            productVM.Name = workSheet.Cells[i, 7].Value != null ? workSheet.Cells[i, 7].Value.ToString() : string.Empty;
                            if (string.IsNullOrEmpty(productVM.Name) && string.IsNullOrEmpty(productVM.Code)) {
                                continue;
                            }

                            productVMLIst.Add(productVM);
                        }
                    }
                }
                cr.results = productVMLIst;
                cr.Message = productVMLIst.Count.ToString() + " Data Found";
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/AddProductbulk/")]
        [HttpPost]
        public IHttpActionResult AddProductbulk(List<ProductVM> productVMs)
        {
            CommonResponse cr = new CommonResponse();
            Product oProduct = new Product();
            try
            {
                if (productVMs.Count > 0)
                {
                    foreach (var item in productVMs)
                    {
                        oProduct = new Product();
                        oProduct.CatagoryId = item.CatagoryId;
                        oProduct.SubCatagoryId = item.SubCatagoryId;
                        oProduct.SizeId = item.SizeId;
                        oProduct.UOMId = item.UOMId;
                        oProduct.Code = item.Code;
                        oProduct.ColorId = item.ColorId;
                        oProduct.EventId = item.EventId;
                        oProduct.MaterialId = item.MaterialId;
                        oProduct.Name = item.Name;
                        oProduct.ProductBrandId = item.ProductBrandId;
                        productService.Add(oProduct);
                    }
                    cr.Message = Message.SAVED;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/GetAllProduct/")]
        [HttpGet]
        public IHttpActionResult GetAllProduct()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = productService.GetAllProduct();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/GetProductPurchease/{Id}")]
        [HttpGet]
        public IHttpActionResult GetProductPurchease(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = productService.ProdutPurcheas(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/GetProductSales/{Id}")]
        [HttpGet]
        public IHttpActionResult GetProductSales(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = productService.ProdutSales(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/GetProductStock/{Id}")]
        [HttpGet]
        public IHttpActionResult GetProductStock(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = productService.ProdutStock(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/GetAllProductFilter/")]
        [HttpPost]
        public IHttpActionResult GetAllProductFilter(ProductVM product)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = productService.GetAllProductByFilter(product);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/GetProduct/{Id}")]
        [HttpGet]
        public IHttpActionResult GetProduct(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.productService.Get(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }

        [Route("Product/GetProducts/{Name}")]
        [HttpGet]
        public IHttpActionResult GetProducts(string Name)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.productService.GetProductName(Name);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/DeleteProduct/")]
        [HttpPost]
        public IHttpActionResult DeleteProduct(Product product)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                productService.Delete(product);
                cr.Message = Message.DELETED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/StatusProduct/")]
        [HttpPost]
        public IHttpActionResult StatusProduct(Product product)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                this.productService.ChangeStatus(product);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/ScarchProduct/{ScarchString}")]
        [HttpGet]
        public IHttpActionResult ScarchProduct(string ScarchString)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = productService.ScarchProduct(ScarchString);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Product/GetPurcheasDetail/{ProductId}/{Qty}")]
        [HttpGet]
        public IHttpActionResult GetPurcheasDetail(int ProductId, decimal Qty)
        {
            CommonResponse res = new CommonResponse();
            SalesDetailVM salesDetails = new SalesDetailVM();
            var productData = productService.Get(ProductId);
            salesDetails.ProductName = productData.Code + "~" + productData.Name;
            salesDetails.ProductId = ProductId;
            salesDetails.Quantity = Qty;
            salesDetails.SalsPrice = productData.SalsPrice;
            salesDetails.Discount = (productData.IsPercentage == true ? (salesDetails.TotalPrice * productData.Discount) / 100 : productData.Discount) ?? 00;
            salesDetails.TotalPrice = (Qty * salesDetails.SalsPrice);
            salesDetails.UnitePrice = salesDetails.SalsPrice;
            res.results = salesDetails;
            res.Message = res.results != null ? "Data Found " : Message.NOTFOUND;
            return Json(res);
        }
        [Route("Product/SearchProduct/")]
        [HttpPost]
        public IHttpActionResult SearchProduct(ProductSearchVM product)
        {
            CommonResponse cr = new CommonResponse();
            Product oProduct = new Product();
            try
            {
                var data = productService.ProdutReport(product);
                cr.results = data;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion Product
    }
}
