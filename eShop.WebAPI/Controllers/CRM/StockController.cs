using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.CRM.ViewModels;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;

namespace eshop.ApplicationService.CRM
{
    public class StockController : ApiController
    {
        StockService stockService;

        public StockController()
        {
            this.stockService = new StockService();
        }
        #region Stock
        [Route("Stock/AddStock/")]
        [HttpPost]
        public IHttpActionResult AddStock(Stocks stock)
        {
            CommonResponse cr = new CommonResponse();
            Stocks oStock = new Stocks();
            try
            {
                if (stock.Id == 0)
                {
                    stockService.Add(stock);
                    cr.Message = Message.SAVED;
                }
                else
                {
                    stockService.Update(stock);
                    cr.Message = Message.UPDATED;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/GetAllStock/{BranchId}")]
        [HttpGet]
        public IHttpActionResult GetAllStock(int BranchId)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = stockService.GetAllStock(BranchId);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/GetStock/")]
        [HttpGet]
        public IHttpActionResult GetStock(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.stockService.Get(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/DeleteStock/")]
        [HttpGet]
        public IHttpActionResult DeleteStock(Stocks stock)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                stockService.Delete(stock);
                cr.Message = Message.DELETED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/StatusStock/")]
        [HttpPost]
        public IHttpActionResult StatusStock(Stocks stock)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                this.stockService.ChangeStatus(stock);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/GetProducts/{Name}/{BranchId}")]
        [HttpGet]
        public IHttpActionResult GetProducts(string Name, int BranchId)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.stockService.GetProductName(Name, BranchId);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);             }
            return Json(cr);
        }
        [Route("Stock/GetStockByProduct/{Id}/{BranchId}")]
        [HttpGet]
        public IHttpActionResult GetStockByProduct(string Id,int BranchId)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.stockService.GetStockByProduct(Id,BranchId);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/GetStockByProductBarCode/{BarCode}/{BranchId}")]
        [HttpGet]
        public IHttpActionResult GetStockByProductBarCode(string BarCode, int BranchId)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.stockService.GetStockByProductByBarCode(BarCode, BranchId);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion Stock
        #region Stock Report
        [Route("Stock/StockReport/")]
        [HttpPost]
        public IHttpActionResult StockReport(StockReportVM stockReportVM)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                CommonResponse res = new CommonResponse();
                var repotDate = stockService.GetStockReportData(stockReportVM);
                res.results = repotDate;
                res.Message = res.results != null ? "Data Found " : Message.NOTFOUND;
                return Json(res);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            //return Json(cr);
        }
        #endregion
        #region StockTransfer
        [Route("Stock/GetAllActiveBranches/")]
        [HttpGet]
        public IHttpActionResult GetAllActiveBranches()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.stockService.getAllActiveBranches();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/GetProductsbyBranchId/{Name}/{BranchId}")]
        [HttpGet]
        public IHttpActionResult GetProductsbyBranchId(string Name, int BranchId)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.stockService.GetProductsbyBranchId(Name, BranchId);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/TransfarProduct/")]
        [HttpPost]
        public IHttpActionResult TransfarProduct(StockTransfarVM stockTransfarVM)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                int roweffect = this.stockService.TransfarProducts(stockTransfarVM);
                cr.Message = roweffect > 0 ? "Transfar Successfull" : "Transfar Fail..";
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Stock/TransfarProductList/")]
        [HttpPost]
        public IHttpActionResult TransfarProductList(StockTransfarReportVM stockTransfarReportVM)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                CommonResponse res = new CommonResponse();
                var repotDate = stockService.TransfarProductList(stockTransfarReportVM);
                res.results = repotDate;
                res.Message = res.results != null ? "Data Found " : Message.NOTFOUND;
                return Json(res);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}