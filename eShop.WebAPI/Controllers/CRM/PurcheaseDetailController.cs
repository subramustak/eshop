using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class PurcheaseDetailController : ApiController
   {
    PurcheaseDetailService purcheasedetailService;
    public PurcheaseDetailController()
     {
       this.purcheasedetailService = new PurcheaseDetailService();
     }
    #region PurcheaseDetail
   [Route("PurcheaseDetail/AddPurcheaseDetail/")]
   [HttpPost]
   public IHttpActionResult AddPurcheaseDetail(PurcheaseDetails purcheasedetail)
   {
    CommonResponse cr = new CommonResponse();
     PurcheaseDetails oPurcheaseDetail = new PurcheaseDetails();
     try
      {
      if (purcheasedetail.Id == 0)
      {
       purcheasedetailService.Add(purcheasedetail);
        cr.Message = Message.SAVED;
      }
     else
     {
      purcheasedetailService.Update(purcheasedetail);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("PurcheaseDetail/GetAllPurcheaseDetail/")]
   [HttpGet]
   public IHttpActionResult GetAllPurcheaseDetail()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = purcheasedetailService.GetAllPurcheaseDetail();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("PurcheaseDetail/GetPurcheaseDetail/")]
   [HttpGet]
   public IHttpActionResult GetPurcheaseDetail(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.purcheasedetailService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurcheaseDetail/DeletePurcheaseDetail/")]
   [HttpGet]
   public IHttpActionResult DeletePurcheaseDetail(PurcheaseDetails purcheasedetail)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       purcheasedetailService.Delete(purcheasedetail);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurcheaseDetail/StatusPurcheaseDetail/")]
   [HttpPost]
   public IHttpActionResult StatusPurcheaseDetail(PurcheaseDetails purcheasedetail)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.purcheasedetailService.ChangeStatus(purcheasedetail);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion PurcheaseDetail
   }
 }
