using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.CRM.ViewModels;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Web.Http;

namespace eshop.ApplicationService.CRM
{
    public class SaleController : ApiController
   {
    SaleService saleService;
    StockService stockService;
        public SaleController()
     {
       this.saleService = new SaleService();
       this.stockService = new StockService();
        }
    #region Sale
   [Route("Sale/AddSale/")]
   [HttpPost]
   public IHttpActionResult AddSale(SaleVM sale)
   {
    CommonResponse cr = new CommonResponse();
     Sales oSale = new Sales();
     try
      {
      if (sale.Id == 0)
      {
       saleService.Add(sale);
                    cr.results = sale.Id;
                    cr.Message = Message.SAVED;
      }
     else
     {
      saleService.Update(sale);
     cr.Message =  Message.UPDATED;
                    cr.results = this.saleService.Get(sale.Id);
                }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Sale/GetAllSale/")]
   [HttpGet]
   public IHttpActionResult GetAllSale()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = saleService.GetAllSale();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Sale/GetSale/{Id}")]
   [HttpGet]
   public IHttpActionResult GetSale(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.saleService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
  [Route("Sale/GetSaleDetail/{Id}")]
  [HttpGet]
  public IHttpActionResult GetSaleDetail(int Id)
  {
      CommonResponse cr = new CommonResponse();
      try
      {
          cr.results = this.saleService.GetSaleDetail(Id);
          cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
      }
      catch (Exception ex)
      {
          return BadRequest(ex.Message);
      }
      return Json(cr);
  }
        [Route("Sale/GetInvoice/{InvoiceName}")]
        [HttpGet]
        public IHttpActionResult GetInvoice(string InvoiceName)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.saleService.GetInvoice(InvoiceName);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Sale/GetSalesDue/")]
   [HttpGet]
   public IHttpActionResult GetSalesDue()
   {
       CommonResponse cr = new CommonResponse();
       try
       {
           cr.results = this.saleService.GetSalesDue();
           cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
       catch (Exception ex)
       {
           return BadRequest(ex.Message);
       }
       return Json(cr);
   }
        [Route("Sale/GetSalesDueCustomerWise/")]
        [HttpGet]
        public IHttpActionResult GetSalesDueCustomerWise()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.saleService.GetSalesDueCustomerWise();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Sale/DeleteSale/")]
   [HttpPost]
   public IHttpActionResult DeleteSale(Sales sale)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       saleService.Delete(sale);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Sale/StatusSale/")]
   [HttpPost]
   public IHttpActionResult StatusSale(Sales sale)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.saleService.ChangeStatus(sale);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
        [Route("Sale/GenerateInvoice/")]
        [HttpGet]
        public IHttpActionResult GenerateInvoice()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
             //cr.results=   this.saleService.GenerateInvoice();
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Sales/GetSalesDetail/{BranchId}/{ProductId}/{Qty}")]
        [HttpGet]
        public IHttpActionResult GetSalesDetail(int BranchId, int ProductId, decimal Qty)
        {
            CommonResponse res = new CommonResponse();
            SalesDetailVM _SalesDetails = new SalesDetailVM();
            _SalesDetails = saleService.GetStockDetailByProduct(BranchId, ProductId, Qty);
            res.results = _SalesDetails;
            res.Message = res.results != null ? "Data Found " : Message.NOTFOUND;
            return Json(res);
        }
        [Route("Sale/SalseReport/")]
        [HttpPost]
        public IHttpActionResult SalseReport(SelseReportVM selseReportVM)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                CommonResponse res = new CommonResponse();
                var repotDate =saleService.GetSalesReportData(selseReportVM);
                res.results = repotDate;
                res.Message = res.results != null ? "Data Found " : Message.NOTFOUND;
                return Json(res);
            }
            catch (Exception ex)
            {
               throw new Exception(ex.Message);
            }
            //return Json(cr);
        }
        
        #endregion Sale
    }
 }
