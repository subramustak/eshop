using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class SalesDetailController : ApiController
   {
    SalesDetailService salesdetailService;
    public SalesDetailController()
     {
       this.salesdetailService = new SalesDetailService();
     }
    #region SalesDetail
   [Route("SalesDetail/AddSalesDetail/")]
   [HttpPost]
   public IHttpActionResult AddSalesDetail(SalesDetails salesdetail)
   {
    CommonResponse cr = new CommonResponse();
     SalesDetails oSalesDetail = new SalesDetails();
     try
      {
      if (salesdetail.Id == 0)
      {
       salesdetailService.Add(salesdetail);
        cr.Message = Message.SAVED;
      }
     else
     {
      salesdetailService.Update(salesdetail);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("SalesDetail/GetAllSalesDetail/")]
   [HttpGet]
   public IHttpActionResult GetAllSalesDetail()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = salesdetailService.GetAllSalesDetail();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("SalesDetail/GetSalesDetail/")]
   [HttpGet]
   public IHttpActionResult GetSalesDetail(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.salesdetailService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SalesDetail/DeleteSalesDetail/")]
   [HttpGet]
   public IHttpActionResult DeleteSalesDetail(SalesDetails salesdetail)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       salesdetailService.Delete(salesdetail);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SalesDetail/StatusSalesDetail/")]
   [HttpPost]
   public IHttpActionResult StatusSalesDetail(SalesDetails salesdetail)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.salesdetailService.ChangeStatus(salesdetail);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion SalesDetail
   }
 }
