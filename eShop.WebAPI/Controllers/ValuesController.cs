﻿using eshop.ApplicationService.MasterSetUp;
using eShop.ApplicationService.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eShop.WebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        BranchService branchService = new BranchService();
        // GET api/values
        public IEnumerable<string> Get()
        {
            var aa = branchService.GetAllItems();
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
