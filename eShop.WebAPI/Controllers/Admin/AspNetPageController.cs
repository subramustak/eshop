using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Admin
 {
  public class AspNetPageController : ApiController
   {
    AspNetPageService aspnetpageService;
    public AspNetPageController()
     {
       this.aspnetpageService = new AspNetPageService();
     }
    #region AspNetPage
   [Route("AspNetPage/AddAspNetPage/")]
   [HttpPost]
   public IHttpActionResult AddAspNetPage(AspNetPage aspnetpage)
   {
    CommonResponse cr = new CommonResponse();
     AspNetPage oAspNetPage = new AspNetPage();
     try
      {
      if (aspnetpage.PageID == 0)
      {
       aspnetpageService.Add(aspnetpage);
        cr.Message = Message.SAVED;
      }
     else
     {
      aspnetpageService.Update(aspnetpage);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AspNetPage/GetAllAspNetPage/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetPage()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetpageService.GetAllAspNetPage();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
        [Route("AspNetPage/GetAllAspNetPageMain/")]
        [HttpGet]
        public IHttpActionResult GetAllAspNetPageMain()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = aspnetpageService.GetAllAspNetPageMain();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetPage/GetAspNetPage/")]
   [HttpGet]
   public IHttpActionResult GetAspNetPage(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetpageService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
        [Route("AspNetPage/GetAspNetPageByModule/{Id}")]
        [HttpGet]
        public IHttpActionResult GetAspNetPageByModule(string Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.aspnetpageService.GetAllAspNetPagesRole(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetPage/DeleteAspNetPage/")]
   [HttpPost]
   public IHttpActionResult DeleteAspNetPage(AspNetPage aspnetpage)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetpageService.Delete(aspnetpage);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetPage/StatusAspNetPage/")]
   [HttpPost]
   public IHttpActionResult StatusAspNetPage(AspNetPage aspnetpage)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.aspnetpageService.ChangeStatus(aspnetpage);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AspNetPage
   }
 }
