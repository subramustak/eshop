using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
using System.Web;
using Newtonsoft.Json;

namespace eshop.ApplicationService.Admin
 {
  public class AspNetUserController : ApiController
   {
    AspNetUserService aspnetuserService;
    public AspNetUserController()
     {
       this.aspnetuserService = new AspNetUserService();
     }
        #region AspNetUser
        [Route("AspNetUser/AddAspNetUser/")]
        [HttpPost]
        public IHttpActionResult AddAspNetUser(AspNetUser aspnetuser)
        {
            CommonResponse cr = new CommonResponse();
            AspNetUser oAspNetUser = new AspNetUser();
            try
            {
                string currFilePath = string.Empty;
                string currFileExtension = string.Empty;
                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files["img"] : null;

                string value = HttpContext.Current.Request.Form["Employee"] ?? "";
                if (string.IsNullOrEmpty(value))
                    return BadRequest("Incorrect Format.");
                AspNetUser aspNetUsers = JsonConvert.DeserializeObject<AspNetUser>(value);
                if (aspNetUsers == null) throw new Exception("User Not Found");
                if (file != null)
                {
                    string fileName = file.FileName;
                    string tempPath = System.IO.Path.GetTempPath();   //Get Temporary File Path
                    fileName = System.IO.Path.GetFileName(fileName); //Get File Name (not including path)
                    currFileExtension = System.IO.Path.GetExtension(fileName);   //Get File Extension
                    currFilePath = tempPath + fileName; //Get File Path after Uploading and Record to Former Declared Global Variable
                    file.SaveAs(currFilePath);  //Upload
                    aspNetUsers.Image = Converters.ToByte(file);
                }
                if (aspnetuser.Id == 0)
                {
                    aspnetuserService.Add(aspnetuser);
                    cr.Message = Message.SAVED;
                }
                else
                {
                    aspnetuserService.Update(oAspNetUser);
                    cr.Message = Message.UPDATED;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetUser/GetAllAspNetUser/")]
        [HttpGet]
        public IHttpActionResult GetAllAspNetUser()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = aspnetuserService.GetAllAspNetUser();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetUser/GetDashboardata/")]
        [HttpGet]
        public IHttpActionResult GetDashboardata()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = aspnetuserService.GetDashboardata();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetUser/GetAspNetUser/")]
        [HttpGet]
        public IHttpActionResult GetAspNetUser(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.aspnetuserService.Get(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetUser/DeleteAspNetUser/")]
        [HttpGet]
        public IHttpActionResult DeleteAspNetUser(AspNetUser aspnetuser)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                aspnetuserService.Delete(aspnetuser);
                cr.Message = Message.DELETED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetUser/StatusAspNetUser/")]
        [HttpPost]
        public IHttpActionResult StatusAspNetUser(AspNetUser aspnetuser)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                this.aspnetuserService.ChangeStatus(aspnetuser);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion AspNetUser
    }
}
