using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace eshop.ApplicationService.Admin
 {
  public class AspNetRoleController : ApiController
   {
    AspNetRoleService aspnetroleService;
    public AspNetRoleController()
     {
       this.aspnetroleService = new AspNetRoleService();
     }
    #region AspNetRole
   [Route("AspNetRole/AddAspNetRole/")]
   [HttpPost]
   public IHttpActionResult AddAspNetRole(AspNetRole aspnetrole)
   {
    CommonResponse cr = new CommonResponse();
     AspNetRole oAspNetRole = new AspNetRole();
     try
      {
      if (string.IsNullOrEmpty(aspnetrole.Id))
      {
       aspnetroleService.Add(aspnetrole);
        cr.Message = Message.SAVED;
      }
     else
     {
      aspnetroleService.Update(aspnetrole);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AspNetRole/GetAllAspNetRole/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetRole()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetroleService.GetAllAspNetRole();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("AspNetRole/GetAspNetRole/")]
   [HttpGet]
   public IHttpActionResult GetAspNetRole(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetroleService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetRole/DeleteAspNetRole/")]
   [HttpGet]
   public IHttpActionResult DeleteAspNetRole(AspNetRole aspnetrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetroleService.Delete(aspnetrole);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
        [Route("AspNetRole/GetRolePermission/{roleid}")]
        [HttpGet]
        public IHttpActionResult GetRolePermission(string roleid)
        {
         var data=   aspnetroleService.GetAllAspNetRoleByRole(roleid);
            return Json(data);
        }
        [Route("AspNetRole/AddRolePermission/{roleid}")]
        [HttpPost]
        public IHttpActionResult AddRolePermission([FromBody] List<string> permissions, string roleid)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                aspnetroleService.AddAspPageRole(permissions, roleid);
                cr.Message = Message.SAVED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion AspNetRole
    }
 }
