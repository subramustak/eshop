using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
using System.Collections.Generic;

namespace eshop.ApplicationService.Admin
 {
  public class AspNetPagesRoleController : ApiController
   {
    AspNetPagesRoleService aspnetpagesroleService;
    public AspNetPagesRoleController()
     {
       this.aspnetpagesroleService = new AspNetPagesRoleService();
     }
    #region AspNetPagesRole
   [Route("AspNetPagesRole/AddAspNetPagesRole/")]
   [HttpPost]
   public IHttpActionResult AddAspNetPagesRole(AspNetPagesRole aspnetpagesrole)
   {
    CommonResponse cr = new CommonResponse();
     AspNetPagesRole oAspNetPagesRole = new AspNetPagesRole();
     try
      {
      if ( aspnetpagesrole.PageRoleId == 0)
      {
       aspnetpagesroleService.Add(aspnetpagesrole);
        cr.Message = Message.SAVED;
      }
     else
     {
      aspnetpagesroleService.Update(aspnetpagesrole);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
        [Route("AspNetPagesRole/AddAspNetPagesRoles/")]
        [HttpPost]
        public IHttpActionResult AddAspNetPagesRoles(List<AspNetPagesRole> aspnetpagesrole)
        {
            CommonResponse cr = new CommonResponse();
            AspNetPagesRole oAspNetPagesRole = new AspNetPagesRole();
            try
            {
               
                    aspnetpagesroleService.Adds(aspnetpagesrole);
                    cr.Message = Message.SAVED;
               
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AspNetPagesRole/GetAllAspNetPagesRole/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetPagesRole()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetpagesroleService.GetAllAspNetPagesRole();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("AspNetPagesRole/GetAspNetPagesRole/")]
   [HttpGet]
   public IHttpActionResult GetAspNetPagesRole(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetpagesroleService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetPagesRole/DeleteAspNetPagesRole/")]
   [HttpGet]
   public IHttpActionResult DeleteAspNetPagesRole(AspNetPagesRole aspnetpagesrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetpagesroleService.Delete(aspnetpagesrole);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetPagesRole/StatusAspNetPagesRole/")]
   [HttpPost]
   public IHttpActionResult StatusAspNetPagesRole(AspNetPagesRole aspnetpagesrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.aspnetpagesroleService.ChangeStatus(aspnetpagesrole);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AspNetPagesRole
   }
 }
