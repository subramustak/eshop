using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSetUp
{
    public class BankController : ApiController
   {
    BankService bankService;
    public BankController()
     {
       this.bankService = new BankService();
     }
    #region Bank
   [Route("Bank/AddBank/")]
   [HttpPost]
   public IHttpActionResult AddBank(Bank bank)
   {
    CommonResponse cr = new CommonResponse();
     Bank oBank = new Bank();
     try
      {
      if (bank.Id == 0)
      {
       bankService.Add(bank);
        cr.Message = Message.SAVED;
      }
     else
     {
      bankService.Update(bank);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Bank/GetAllBank/")]
   [HttpGet]
   public IHttpActionResult GetAllBank()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = bankService.GetAllBank();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Bank/GetBank/")]
   [HttpGet]
   public IHttpActionResult GetBank(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.bankService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Bank/DeleteBank/")]
   [HttpPost]
   public IHttpActionResult DeleteBank(Bank bank)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       bankService.Delete(bank);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Bank/StatusBank/")]
   [HttpPost]
   public IHttpActionResult StatusBank(Bank bank)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.bankService.ChangeStatus(bank);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Bank
   }
 }
