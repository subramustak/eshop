using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
 
namespace eshop.ApplicationService.MasterSetUp
 {
  public class OrganizationController : ApiController
   {
    OrganizationService organizationService;
    public OrganizationController()
     {
       this.organizationService = new OrganizationService();
     }
    #region Organization
   [Route("Organization/AddOrganization/")]
   [HttpPost]
   public IHttpActionResult AddOrganization()
   {
    CommonResponse cr = new CommonResponse();
     Organization oOrganization = new Organization();
     try
      {
                string currFilePath = string.Empty;
                string currFileExtension = string.Empty;
                var logo = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files["logo"] : null;
                var header = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files["header"] : null;

                string value = HttpContext.Current.Request.Form["organization"] ?? "";
                if (string.IsNullOrEmpty(value))
                    return BadRequest("Incorrect Format.");
                Organization organization = JsonConvert.DeserializeObject<Organization>(value);
                if (organization == null) throw new Exception("Product Not Found");
                if (logo != null)
                {
                    organization.Logo = Converters.ToByte(logo);
                }
                if (header != null)
                {
                    organization.Header = Converters.ToByte(header);
                }
                if (organization.Id == 0)
      {
       organizationService.Add(organization);
        cr.Message = Message.SAVED;
      }
     else
     {
      organizationService.Update(organization);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Organization/GetAllOrganization/")]
   [HttpGet]
   public IHttpActionResult GetAllOrganization()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = organizationService.GetAllOrganization();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
        [Route("Organization/GetName/")]
        [HttpGet]
        public IHttpActionResult GetName()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = organizationService.GetAllOrganization().FirstOrDefault().Name;
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Organization/GetOrganization/")]
   [HttpGet]
   public IHttpActionResult GetOrganization(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.organizationService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Organization/DeleteOrganization/")]
   [HttpGet]
   public IHttpActionResult DeleteOrganization(Organization organization)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       organizationService.Delete(organization);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Organization/StatusOrganization/")]
   [HttpPost]
   public IHttpActionResult StatusOrganization(Organization organization)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.organizationService.ChangeStatus(organization);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Organization
   }
 }
