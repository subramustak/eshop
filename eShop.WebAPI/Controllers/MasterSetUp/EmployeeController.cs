using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSetUp
{
    public class EmployeeController : ApiController
    {
        EmployeeService employeeService;
        public EmployeeController()
        {
            this.employeeService = new EmployeeService();
        }
        #region Employee
        [Route("Employee/AddEmployee/")]
        [HttpPost]
        public IHttpActionResult AddEmployee(Employee employee)
        {
            CommonResponse cr = new CommonResponse();
            Employee oEmployee = new Employee();
            try
            {
                employee.JoiningDate = DateTime.Now;
                employee.ProbationPeriodEndDate = DateTime.Now;
                employee.ConfirmationDate = DateTime.Now;
                if (employee.Id == 0)
                {

                    employeeService.Add(employee);
                    cr.Message = Message.SAVED;
                }
                else
                {
                    employeeService.Update(employee);
                    cr.Message = Message.UPDATED;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Employee/GetAllEmployee/")]
        [HttpGet]
        public IHttpActionResult GetAllEmployee()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = employeeService.GetAllEmployee();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Employee/GetEmployee/{Id}")]
        [HttpGet]
        public IHttpActionResult GetEmployee(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.employeeService.Get(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Employee/DeleteEmployee/")]
        [HttpPost]
        public IHttpActionResult DeleteEmployee(Employee employee)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                employeeService.Delete(employee);
                cr.Message = Message.DELETED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Employee/StatusEmployee/")]
        [HttpPost]
        public IHttpActionResult StatusEmployee(Employee employee)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                this.employeeService.ChangeStatus(employee);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion Employee
    }
}
