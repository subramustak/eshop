using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.MasterSetUp
 {
  public class SupplierController : ApiController
   {
    SupplierService SupplierService;
    public SupplierController()
     {
       this.SupplierService = new SupplierService();
     }
    #region Supplier
   [Route("Supplier/AddSupplier/")]
   [HttpPost]
   public IHttpActionResult AddSupplier(Supplier Supplier)
   {
    CommonResponse cr = new CommonResponse();
     Supplier oSupplier = new Supplier();
     try
      {
      if (Supplier.Id == 0)
      {
       SupplierService.Add(Supplier);
        cr.Message = Message.SAVED;
      }
     else
     {
      SupplierService.Update(Supplier);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Supplier/GetAllSupplier/")]
   [HttpGet]
   public IHttpActionResult GetAllSupplier()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = SupplierService.GetAllSupplier();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Supplier/GetSupplier/")]
   [HttpGet]
   public IHttpActionResult GetSupplier(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.SupplierService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Supplier/DeleteSupplier/")]
   [HttpPost]
   public IHttpActionResult DeleteSupplier(Supplier Supplier)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       SupplierService.Delete(Supplier);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Supplier/StatusSupplier/")]
   [HttpPost]
   public IHttpActionResult StatusSupplier(Supplier Supplier)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.SupplierService.ChangeStatus(Supplier);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Supplier
   }
 }
