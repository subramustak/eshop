using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSetUp
{
    public class CustomerController : ApiController
    {
        CustomerService customerService;
        public CustomerController()
        {
            this.customerService = new CustomerService();
        }
        #region Customer
        [Route("Customer/AddCustomer/")]
        [HttpPost]
        public IHttpActionResult AddCustomer(Customer customer)
        {
            CommonResponse cr = new CommonResponse();
            Customer oCustomer = new Customer();
            try
            {
                if (customer.Id == 0)
                {
                    customerService.Add(customer);
                    cr.Message = Message.SAVED;
                }
                else
                {
                   //var existingCustormer= customerService.Get(customer.Id);
                   // existingCustormer.
                    customerService.Update(customer);
                    cr.Message = Message.UPDATED;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Customer/GetAllCustomer/")]
        [HttpGet]
        public IHttpActionResult GetAllCustomer()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = customerService.GetAllCustomer();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Customer/GetCustomer/{Id}")]
        [HttpGet]
        public IHttpActionResult GetCustomer(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.customerService.Get(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Customer/GetCustomerByName/{Name}")]
        [HttpGet]
        public IHttpActionResult GetCustomerByName(string name)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.customerService.GetCustomer(name);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Customer/DeleteCustomer/")]
        [HttpPost]
        public IHttpActionResult DeleteCustomer(Customer customer)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                customerService.Delete(customer);
                cr.Message = Message.DELETED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("Customer/StatusCustomer/")]
        [HttpPost]
        public IHttpActionResult StatusCustomer(Customer customer)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                this.customerService.ChangeStatus(customer);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion Customer
    }
}
