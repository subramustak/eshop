using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSetUp
{
    public class BrandController : ApiController
   {
    BrandService brandService;
    public BrandController()
     {
       this.brandService = new BrandService();
     }
    #region Brand
   [Route("Brand/AddBrand/")]
   [HttpPost]
   public IHttpActionResult AddBrand(Brand brand)
   {
    CommonResponse cr = new CommonResponse();
     Brand oBrand = new Brand();
     try
      {
      if (brand.Id == 0)
      {
       brandService.Add(brand);
        cr.Message = Message.SAVED;
      }
     else
     {
      brandService.Update(brand);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Brand/GetAllBrand/")]
   [HttpGet]
   public async Task<IHttpActionResult> GetAllBrand()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = await brandService.GetAllBrand();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Brand/GetBrand/")]
   [HttpGet]
   public IHttpActionResult GetBrand(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.brandService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Brand/DeleteBrand/")]
   [HttpPost]
   public IHttpActionResult DeleteBrand(Brand brand)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       brandService.Delete(brand);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Brand/StatusBrand/")]
   [HttpPost]
   public IHttpActionResult StatusBrand(Brand brand)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.brandService.ChangeStatus(brand);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Brand
   }
 }
