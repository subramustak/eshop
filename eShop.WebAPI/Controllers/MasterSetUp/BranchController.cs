﻿using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Web.Http;

namespace eShop.WebAPI.Controllers.MasterSetUp
{
    public class BranchController : ApiController
    {
        BranchService BranchService;
        public BranchController()
        {
            BranchService = new BranchService();
        }
        #region Branch
        [Route("Branch/AddBranch/")]
        [HttpPost]
        public IHttpActionResult AddBranch(Branch branch)
        {
            CommonResponse cr = new CommonResponse();
            Branch oBranch = new Branch();
            var res = false;
            try
            {
                if (branch.Id == 0)
                {
                     BranchService.Add(branch);
                    cr.Message =  Message.SAVED;
                }
                else
                {
                     BranchService.Update(branch);
                    cr.Message = Message.UPDATED ;
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }

        [Route("Branch/GetAllBranchs/")]
        [HttpGet]
        public IHttpActionResult GetAllBranchs()
        {
            CommonResponse res = new CommonResponse();
            try
            {
                res.results = BranchService.GetAllItems();
                res.Message = res.results != null ? "Data Found " : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(res);
        }
        [Route("Branch/GetBranchs/{Id}")]
        [HttpGet]
        public IHttpActionResult GetBranchs(int Id)
        {
            CommonResponse res = new CommonResponse();
            try
            {
            res.results = BranchService.Get(Id);
            res.Message = res.results != null ? "Data Found " + res.results : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(res);
        }
        [Route("Branch/DeleteBranch/")]
        [HttpPost]
        public IHttpActionResult DeleteBranch(Branch branch)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                BranchService.Delete(branch);
                cr.Message = Message.DELETED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
           
        }
        [Route("Branch/StatusBranch/")]
        [HttpPost]
        public IHttpActionResult StatusBranch(Branch Branch)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                BranchService.ChangeStatus(Branch);
                cr.Message = Message.UPDATED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion Branch
    }
}
