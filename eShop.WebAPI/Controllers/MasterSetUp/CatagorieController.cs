using eshop.ApplicationService.MastarSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MastarSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.MastarSetUp
 {
  public class CatagorieController : ApiController
   {
    CatagorieService catagorieService;
    public CatagorieController()
     {
       this.catagorieService = new CatagorieService();
     }
    #region Catagorie
   [Route("Catagorie/AddCatagorie/")]
   [HttpPost]
   public IHttpActionResult AddCatagorie(Catagorie catagorie)
   {
    CommonResponse cr = new CommonResponse();
     Catagorie oCatagorie = new Catagorie();
     try
      {
      if (catagorie.Id == 0)
      {
       catagorieService.Add(catagorie);
        cr.message = Message.SAVED;
      }
     else
     {
      catagorieService.Update(oCatagorie);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Branch/GetAllCatagorie/")]
   [HttpGet]
   public IHttpActionResult GetAllCatagorie()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = catagorieService.GetAllCatagorie();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Catagorie/GetCatagorie/")]
   [HttpGet]
   public IHttpActionResult GetCatagorie(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.catagorieService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Catagorie/DeleteCatagorie/")]
   [HttpGet]
   public IHttpActionResult DeleteCatagorie(Catagorie catagorie)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       catagorieService.Delete(catagorie);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Catagorie/StatusCatagorie/")]
   [HttpPost]
   public IHttpActionResult StatusCatagorie(Catagorie catagorie)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.catagorieService.ChangeStatus(catagorie);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Catagorie
   }
 }
