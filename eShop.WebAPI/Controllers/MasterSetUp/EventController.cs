using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.MasterSetUp
 {
  public class EventController : ApiController
   {
    EventService eventService;
    public EventController()
     {
       this.eventService = new EventService();
     }
    #region Event
   [Route("Event/AddEvent/")]
   [HttpPost]
   public IHttpActionResult AddEvent(Event _event)
   {
    CommonResponse cr = new CommonResponse();
     Event oEvent = new Event();
     try
      {
      if (_event.Id == 0)
      {
       eventService.Add(_event);
        cr.Message = Message.SAVED;
      }
     else
     {
      eventService.Update(_event);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Event/GetAllEvent/")]
   [HttpGet]
   public IHttpActionResult GetAllEvent()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = eventService.GetAllEvent();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Event/GetEvent/")]
   [HttpGet]
   public IHttpActionResult GetEvent(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.eventService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Event/DeleteEvent/")]
   [HttpPost]
   public IHttpActionResult DeleteEvent(Event _event)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       eventService.Delete(_event);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Event/StatusEvent/")]
   [HttpPost]
   public IHttpActionResult StatusEvent(Event _event)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.eventService.ChangeStatus(_event);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Event
   }
 }
