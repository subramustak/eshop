using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSteUp
{
    public class SizeController : ApiController
   {
    SizeService sizeService;
    public SizeController()
     {
       this.sizeService = new SizeService();
     }
    #region Size
   [Route("Size/AddSize/")]
   [HttpPost]
   public IHttpActionResult AddSize(Size size)
   {
    CommonResponse cr = new CommonResponse();
     try
      {
      if (size.Id == 0)
      {
       sizeService.Add(size);
        cr.Message = Message.SAVED;
      }
     else
     {
      sizeService.Update(size);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Size/GetAllSize/")]
   [HttpGet]
   public async Task<IHttpActionResult> GetAllSize()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = await sizeService.GetAllSize();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Size/GetSize/")]
   [HttpGet]
   public IHttpActionResult GetSize(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.sizeService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Size/DeleteSize/")]
   [HttpPost]
   public IHttpActionResult DeleteSize(Size size)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       sizeService.Delete(size);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Size/StatusSize/")]
   [HttpPost]
   public IHttpActionResult StatusSize(Size size)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.sizeService.ChangeStatus(size);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Size
   }
 }
