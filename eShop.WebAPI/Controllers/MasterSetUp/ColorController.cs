using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSetUp
{
    public class ColorController : ApiController
   {
    ColorService colorService;
    public ColorController()
     {
       this.colorService = new ColorService();
     }
    #region Color
   [Route("Color/AddColor/")]
   [HttpPost]
   public IHttpActionResult AddColor(Color color)
   {
    CommonResponse cr = new CommonResponse();
     Color oColor = new Color();
     try
      {
      if (color.Id == 0)
      {
       colorService.Add(color);
        cr.Message = Message.SAVED;
      }
     else
     {
      colorService.Update(color);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Color/GetAllColor/")]
   [HttpGet]
   public async Task<IHttpActionResult> GetAllColor()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = await colorService.GetAllColor();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Color/GetColor/")]
   [HttpGet]
   public IHttpActionResult GetColor(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.colorService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Color/DeleteColor/")]
   [HttpPost]
   public IHttpActionResult DeleteColor(Color color)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       colorService.Delete(color);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Color/StatusColor/")]
   [HttpPost]
   public IHttpActionResult StatusColor(Color color)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.colorService.ChangeStatus(color);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Color
   }
 }
