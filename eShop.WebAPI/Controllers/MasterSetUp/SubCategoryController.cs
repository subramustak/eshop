using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
 
namespace eshop.ApplicationService.MasterSetUp
 {
  public class SubCategoryController : ApiController
   {
    SubCategoryService subcategoryService;
    public SubCategoryController()
     {
       this.subcategoryService = new SubCategoryService();
     }
    #region SubCategory
   [Route("SubCategory/AddSubCategory/")]
   [HttpPost]
   public IHttpActionResult AddSubCategory(SubCategory subcategory)
   {
    CommonResponse cr = new CommonResponse();
     SubCategory oSubCategory = new SubCategory();
     try
      {
      if (subcategory.Id == 0)
      {
       subcategoryService.Add(subcategory);
        cr.Message = Message.SAVED;
      }
     else
     {
      subcategoryService.Update(subcategory);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("SubCategory/GetAllSubCategory/")]
   [HttpGet]
    public async Task<IHttpActionResult> GetAllSubCategory()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results =await subcategoryService.GetAllSubCategory();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
        [Route("SubCategory/GetAllSubByCategory/{Id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllSubByCategory(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = await subcategoryService.GetAllSubbYCategory( Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("SubCategory/GetSubCategory/")]
   [HttpGet]
   public IHttpActionResult GetSubCategory(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.subcategoryService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SubCategory/DeleteSubCategory/")]
   [HttpPost]
   public IHttpActionResult DeleteSubCategory(SubCategory subcategory)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       subcategoryService.Delete(subcategory);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SubCategory/StatusSubCategory/")]
   [HttpPost]
   public IHttpActionResult StatusSubCategory(SubCategory subcategory)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.subcategoryService.ChangeStatus(subcategory);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion SubCategory
   }
 }
