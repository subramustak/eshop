using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSteUp
{
    public class UOMController : ApiController
   {
    UOMService uomService;
    public UOMController()
     {
       this.uomService = new UOMService();
     }
    #region UOM
   [Route("UOM/AddUOM/")]
   [HttpPost]
   public IHttpActionResult AddUOM(UOM uom)
   {
    CommonResponse cr = new CommonResponse();
     UOM oUOM = new UOM();
     try
      {
      if (uom.Id == 0)
      {
       uomService.Add(uom);
        cr.Message = Message.SAVED;
      }
     else
     {
      uomService.Update(uom);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("UOM/GetAllUOM/")]
   [HttpGet]
   public async Task<IHttpActionResult> GetAllUOM()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results =await uomService.GetAllUOM();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("UOM/GetUOM/")]
   [HttpGet]
   public IHttpActionResult GetUOM(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.uomService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("UOM/DeleteUOM/")]
   [HttpPost]
   public IHttpActionResult DeleteUOM(UOM uom)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       uomService.Delete(uom);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("UOM/StatusUOM/")]
   [HttpPost]
   public IHttpActionResult StatusUOM(UOM uom)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.uomService.ChangeStatus(uom);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
      
        #endregion UOM
    }
 }
