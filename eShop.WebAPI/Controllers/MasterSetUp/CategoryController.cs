using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace eshop.ApplicationService.MasterSetUp
{
    public class CategoryController : ApiController
   {
    CategoryService categoryService;
    public CategoryController()
     {
       this.categoryService = new CategoryService();
     }
    #region Category
   [Route("Category/AddCategory/")]
   [HttpPost]
   public IHttpActionResult AddCategory(Category category)
   {
    CommonResponse cr = new CommonResponse();
     Category oCategory = new Category();
     try
      {
      if (category.Id == 0)
      {
       categoryService.Add(category);
        cr.Message = Message.SAVED;
      }
     else
     {
      categoryService.Update(category);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Category/GetAllCategory/")]
   [HttpGet]
   public async Task<IHttpActionResult> GetAllCategory()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = await categoryService.GetAllCategory();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Category/GetCategory/")]
   [HttpGet]
   public IHttpActionResult GetCategory(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.categoryService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Category/DeleteCategory/")]
   [HttpPost]
   public IHttpActionResult DeleteCategory(Category category)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       categoryService.Delete(category);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Category/StatusCategory/")]
   [HttpPost]
   public IHttpActionResult StatusCategory(Category category)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.categoryService.ChangeStatus(category);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Category
   }
 }
