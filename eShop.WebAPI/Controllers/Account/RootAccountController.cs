using eshop.ApplicationService.MasterSetUp;
using eShop.ApplicationService.Account;
using eShop.BusinessDomain.Account.DomainObject;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
 
namespace eshop.ApplicationService.Account
{
  public class RootAccountController : ApiController
   {
    RootAccountService RootAccountService;
    public RootAccountController()
     {
       this.RootAccountService = new RootAccountService();
     }
    #region RootAccount
   [Route("RootAccount/AddRootAccount/")]
   [HttpPost]
   public IHttpActionResult AddRootAccount(RootAccount RootAccount)
   {
    CommonResponse cr = new CommonResponse();
     RootAccount oRootAccount = new RootAccount();
     try
      {
      if (RootAccount.Id == 0)
      {
       RootAccountService.Add(RootAccount);
        cr.Message = Message.SAVED;
      }
     else
     {
      RootAccountService.Update(RootAccount);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("RootAccount/GetAllRootAccount/")]
   [HttpGet]
   public IHttpActionResult GetAllRootAccount()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results =  RootAccountService.GetAllRootAccount();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("RootAccount/GetRootAccount/")]
   [HttpGet]
   public IHttpActionResult GetRootAccount(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.RootAccountService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("RootAccount/DeleteRootAccount/")]
   [HttpPost]
   public IHttpActionResult DeleteRootAccount(RootAccount RootAccount)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       RootAccountService.Delete(RootAccount);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("RootAccount/StatusRootAccount/")]
   [HttpPost]
   public IHttpActionResult StatusRootAccount(RootAccount RootAccount)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.RootAccountService.ChangeStatus(RootAccount);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion RootAccount
   }
 }
