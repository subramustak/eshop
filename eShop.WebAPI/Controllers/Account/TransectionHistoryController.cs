using eshop.ApplicationService.MasterSetUp;
using eShop.ApplicationService.Account;
using eShop.BusinessDomain.Account.DomainObject;
using eShop.BusinessDomain.Account.ViewModel;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
 
namespace eshop.ApplicationService.Account
{
  public class TransectionHistoryController : ApiController
   {
    TransectionHistoryService TransectionHistoryService;
    public TransectionHistoryController()
     {
       this.TransectionHistoryService = new TransectionHistoryService();
     }
    #region TransectionHistory
   [Route("TransectionHistory/AddTransectionHistoryP/")]
   [HttpPost]
   public IHttpActionResult AddTransectionHistoryP(TransectionHistory TransectionHistory)
   {
    CommonResponse cr = new CommonResponse();
     TransectionHistory oTransectionHistory = new TransectionHistory();
     try
      {
                TransectionHistory.IsPayment = true;
                TransectionHistory.Id = 0;
                if (TransectionHistory.Id == 0)
      {
       TransectionHistoryService.AddP(TransectionHistory);
        cr.Message = Message.SAVED;
      }
     else
     {
      TransectionHistoryService.Update(TransectionHistory);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
        [Route("TransectionHistory/AddTransectionHistoryr/")]
        [HttpPost]
        public IHttpActionResult AddTransectionHistoryr(TransectionHistory TransectionHistory)
        {
            CommonResponse cr = new CommonResponse();
            TransectionHistory oTransectionHistory = new TransectionHistory();
            try
            {
                TransectionHistory.IsRecive = true;
                TransectionHistory.Id = 0;
                if (TransectionHistory.Id == 0)
                {
                    TransectionHistoryService.AddR(TransectionHistory);
                    cr.Message = Message.SAVED;
                }
                else
                {
                    TransectionHistoryService.Update(TransectionHistory);
                    cr.Message = Message.UPDATED;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("TransectionHistory/GetAllTransectionHistory/")]
   [HttpGet]
   public  IHttpActionResult GetAllTransectionHistoryP()
    {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results =  TransectionHistoryService.GetAllTransectionHistory();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("TransectionHistory/GetTransectionHistory/")]
   [HttpGet]
   public IHttpActionResult GetTransectionHistory(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.TransectionHistoryService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
        [Route("TransectionHistory/GenereteInvoice/")]
        [HttpGet]
        public IHttpActionResult GenereteInvoice()
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = this.TransectionHistoryService.GenerateInvoice();
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("TransectionHistory/TransactionPayemnt/")]
        [HttpPost]
        public IHttpActionResult TransactionPayemnt(Search search)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = TransectionHistoryService.TransactionPayemnt(search.AccountId, search.startDate, search.endDate);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("TransectionHistory/TransactionReceipt/")]
        [HttpPost]
        public IHttpActionResult TransactionReceipt(Search search)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = TransectionHistoryService.TransactionReceipt(search.AccountId, search.startDate, search.endDate);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("TransectionHistory/TransactionPayemntDetail/")]
        [HttpPost]
        public IHttpActionResult TransactionPayemntDetail(Search search)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = TransectionHistoryService.TransactionPayemntDetail(search.AccountId, search.startDate, search.endDate);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("TransectionHistory/TransactionReceiptDetail/")]
        [HttpPost]
        public IHttpActionResult TransactionReceiptDetail(Search search)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = TransectionHistoryService.TransactionReceiptDetail(search.AccountId, search.startDate, search.endDate);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        public class Search {
            public int? AccountId { get; set; }
            public DateTime? startDate { get; set; }
            public DateTime? endDate { get; set; }
        }
        [Route("TransectionHistory/DeleteTransectionHistory/")]
   [HttpPost]
   public IHttpActionResult DeleteTransectionHistory(TransectionHistory TransectionHistory)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       TransectionHistoryService.Delete(TransectionHistory);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("TransectionHistory/StatusTransectionHistory/")]
   [HttpPost]
   public IHttpActionResult StatusTransectionHistory(TransectionHistory TransectionHistory)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.TransectionHistoryService.ChangeStatus(TransectionHistory);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
        #endregion TransectionHistory
        #region Customer Receiveable
        [Route("TransectionHistory/CusotmerWiseDuePay/")]
        [HttpPost]
        public IHttpActionResult CusotmerWiseDuePay(CusotmerWiseDuePayVM cusotmerWiseDuePay)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                TransectionHistoryService.CusotmerWiseDuePay(cusotmerWiseDuePay);
                cr.Message = Message.SAVED;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        #endregion
    }
}
