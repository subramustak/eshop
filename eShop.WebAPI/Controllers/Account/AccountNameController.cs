using eshop.ApplicationService.MasterSetUp;
using eShop.ApplicationService.Account;
using eShop.BusinessDomain.Account.DomainObject;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
 
namespace eshop.ApplicationService.Account
{
  public class AccountNameController : ApiController
   {
    AccountNameService AccountNameService;
    public AccountNameController()
     {
       this.AccountNameService = new AccountNameService();
     }
    #region AccountName
   [Route("AccountName/AddAccountName/")]
   [HttpPost]
   public IHttpActionResult AddAccountName(AccountName AccountName)
   {
    CommonResponse cr = new CommonResponse();
     AccountName oAccountName = new AccountName();
     try
      {
      if (AccountName.Id == 0)
      {
       AccountNameService.Add(AccountName);
        cr.Message = Message.SAVED;
      }
     else
     {
      AccountNameService.Update(AccountName);
     cr.Message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AccountName/GetAllAccountName/")]
   [HttpGet]
   public IHttpActionResult GetAllAccountName()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results =  AccountNameService.GetAllAccountName();
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
        [Route("AccountName/GetAllAccountNameById/{Id}")]
        [HttpGet]
        public IHttpActionResult GetAllAccountNameById(int Id)
        {
            CommonResponse cr = new CommonResponse();
            try
            {
                cr.results = AccountNameService.GetAllAccountNameById(Id);
                cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Json(cr);
        }
        [Route("AccountName/GetAccountName/{Id}")]
   [HttpGet]
   public IHttpActionResult GetAccountName(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.AccountNameService.Get(Id);
       cr.Message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AccountName/DeleteAccountName/")]
   [HttpPost]
   public IHttpActionResult DeleteAccountName(AccountName AccountName)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       AccountNameService.Delete(AccountName);
        cr.Message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AccountName/StatusAccountName/")]
   [HttpPost]
   public IHttpActionResult StatusAccountName(AccountName AccountName)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.AccountNameService.ChangeStatus(AccountName);
        cr.Message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AccountName
   }
 }
