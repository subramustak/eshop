using System;
namespace eShop.BusinessDomain.MasterSetUp.ViewModels
{
    public class SubCategoryVM
    {
      public int Id { get; set; }
      public string SubCatagoryName { get; set; }
      public string SubCatagoryCode { get; set; }
        public string CatagoryName { get; set; }
        public bool IsArchived { get; set; }
        public bool IsDeleted { get; set; }
        public string AddBy { get; set; }
        public DateTime? AddDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; } = "A";
        public int CatagoryId { get; set; } = 0;
    }
}
