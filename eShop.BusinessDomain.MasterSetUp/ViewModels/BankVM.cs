using System;
namespace eShop.BusinessDomain.MasterSetUp.ViewModels
{
    public class BankVM
    {
      public int Id { get; set; }
      public string BankName { get; set; }
      public string BranchName { get; set; }
      public string Code { get; set; }
      public string AccountNumber { get; set; }
      public string Address { get; set; }
      public string AddressBangla { get; set; }
      public string ContactNumber { get; set; }
      public string Email { get; set; }
      public string Fax { get; set; }
        public bool IsArchived { get; set; }
        public bool IsDeleted { get; set; }
        public string AddBy { get; set; }
        public DateTime? AddDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; } = "A";
    }
}
