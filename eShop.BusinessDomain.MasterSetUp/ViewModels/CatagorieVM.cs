using System;
namespace eShop.BusinessDomain.MasterSetUp.ViewModels
{
    public class CatagorieVM
    {
      public int Id { get; set; }
      public string CatagoryName { get; set; }
      public string CatagoryCode { get; set; }
      public string CatagoryParent { get; set; }
    }
}
