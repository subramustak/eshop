﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.ViewModels
{
  public  class BranchVM
    {
        public int Id { get; set; }
        public string BranchName { get; set; }
        public string BranchNameBangla { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string AddressBangla { get; set; }
        public string ContactNumber { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Fax { get; set; }
        [StringLength(100)]
        public string SS_Lang { get; set; }
        [StringLength(100)]
        public string SS_Latu { get; set; }
        public bool IsArchived { get; set; }
        public bool IsDeleted { get; set; }
        public string AddBy { get; set; }
        public DateTime? AddDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; } = "A";
    }
}
