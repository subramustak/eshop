using System;
namespace eShop.BusinessDomain.MasterSetUp.ViewModels
{
    public class ColorVM
    {
      public int Id { get; set; }
      public string ColorName { get; set; }
      public string HexadecimalCode { get; set; }
      public string ColorCode { get; set; }
        public bool IsArchived { get; set; }
        public bool IsDeleted { get; set; }
        public string AddBy { get; set; }
        public DateTime? AddDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; } = "A";
    }
}
