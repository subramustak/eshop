﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
    public class Group: Entity
    {
        
        public string GroupLabel { get; set; }
        public string GroupCode { get; set; }
    }
}
