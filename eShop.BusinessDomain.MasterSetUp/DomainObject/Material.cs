﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
   public class Material:Entity
    {
       
        public string MaterialName { get; set; }
        public string MaterialCode { get; set; }
        
    }
}
