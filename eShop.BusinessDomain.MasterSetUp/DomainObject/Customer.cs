﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
   public class Customer: Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public int DivisionId { get; set; }
        public string Mobile { get; set; }
        public string PermanentAddress { get; set; }
        public string PresentAddress { get; set; }
        public string PABX { get; set; }
        public string Email { get; set; }
        public string FAX { get; set; }

    }
}
