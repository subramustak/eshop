﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
    [Table("Banks")]
    public class Bank : Entity
    {
       
        [Required]
        public string BankName { get; set; }
        public string BranchName{ get; set; }
        public string Code { get; set; }
        public string AccountNumber { get; set; }
        public string Address { get; set; }
        public string AddressBangla { get; set; }
        public string ContactNumber { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Fax { get; set; }
      
       
    }
}
