﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
    public class Branch : Entity
    {
        [Required]
        public string BranchName { get; set; }
        public string BranchNameBangla { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
        public string AddressBangla { get; set; }
        public string ContactNumber { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Fax { get; set; }
        [StringLength(100)]
        public string SS_Lang { get; set; }
        [StringLength(100)]
        public string SS_Latu { get; set; }
    }
}
