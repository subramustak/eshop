﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject{
    public class UOM:Entity
    {
       
        public string UOMName { get; set; }
        public string UOMCode { get; set; }
    }
}
