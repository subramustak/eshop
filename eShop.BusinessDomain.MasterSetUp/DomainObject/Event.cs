﻿using eShop.BusinessDomain.Core;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject{
    public class Event:Entity
    {
        public string EventName { get; set; }
        public int DsicountAmount { get; set; }
        public bool? DsicountPercent { get; set; }
        public int DsicountCount { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
