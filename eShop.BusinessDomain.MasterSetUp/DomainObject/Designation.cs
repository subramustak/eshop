﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
    public class Designation: Entity
    {
        public string DesignationName { get; set; }
        public string DesignationCode { get; set; }
    }
}
