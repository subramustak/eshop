﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
    public class Color:Entity
    {
        public string ColorName { get; set; }
        public string HexadecimalCode { get; set; }
        public string ColorCode { get; set; }
    }
}
