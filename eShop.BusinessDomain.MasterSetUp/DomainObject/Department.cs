﻿using eShop.BusinessDomain.Core;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
    public class Department : Entity
    {
        public string DepartmentName { get; set; }
        public string DepartmentCode { get; set; }
    }
}
