﻿using eShop.BusinessDomain.Core;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject{
    public class Setting:Entity
    {
        public string PurcheasePrefix { get; set; }
        public string PurcheaseStart { get; set; }
        public string PurcheasePostfix { get; set; }
        public string SalesPrefix { get; set; }
        public string SalesPreStart { get; set; }
        public string SalesPostfix { get; set; }
       
        public string AccountPrefix { get; set; }
        public string AccountPreStart { get; set; }
        public string AccountPostfix { get; set; }
        public string Currency { get; set; }
        public decimal? VAT { get; set; }
        public string VATNo { get; set; }
        public int ReportSize { get; set; }
        public int? CustomerId { get; set; }
        public byte[] LogoImage { get; set; }
        public byte[] HeaderImage { get; set; }
    }
}
