﻿using eShop.BusinessDomain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.MasterSetUp.DomainObject
{
    public class Category:Entity
    {
      
        public string CatagoryName { get; set; }
        public string CatagoryCode { get; set; }
        public string CatagoryParent { get; set; }

    }
}
