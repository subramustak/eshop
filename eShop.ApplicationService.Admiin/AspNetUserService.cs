using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;

namespace eshop.ApplicationService.Admin
{
 public class AspNetUserService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        CommonRepository commonRepository = new CommonRepository();
        public ICollection<AspNetUserVM> GetAllAspNetUser()
   {
            var data = commonRepository.GetBySpWithParam("GetAllUserInfo", new object[] { 1 });
            var AspNetUsers = Converters.ConvertDataTable<AspNetUserVM>(data);
        
         return AspNetUsers;
    }

        public dynamic GetDashboardata()
        {
            var data = commonRepository.getDatasetResponseBySp("GetDashboardata").results;
            return data;
        }
        public AspNetUserVM Get(long Id)
    {
     AspNetUserVM aspnetuserVM = new AspNetUserVM();
    var aspnetuserItems = unitOfWork.AspNetUserRepository.Get(Id);
aspnetuserVM.Email = aspnetuserItems.Email ;
aspnetuserVM.EmailConfirmed = aspnetuserItems.EmailConfirmed ;
aspnetuserVM.PasswordHash = aspnetuserItems.PasswordHash ;
aspnetuserVM.SecurityStamp = aspnetuserItems.SecurityStamp ;
aspnetuserVM.PhoneNumber = aspnetuserItems.PhoneNumber ;
aspnetuserVM.PhoneNumberConfirmed = aspnetuserItems.PhoneNumberConfirmed ;
aspnetuserVM.TwoFactorEnabled = aspnetuserItems.TwoFactorEnabled ;
aspnetuserVM.LockoutEnabled = aspnetuserItems.LockoutEnabled ;
aspnetuserVM.AccessFailedCount = aspnetuserItems.AccessFailedCount ;
aspnetuserVM.UserName = aspnetuserItems.UserName ;
    return aspnetuserVM;
    }
    
    public void Add(AspNetUser aspnetuser) {
      unitOfWork.AspNetUserRepository.Add(entity: aspnetuser);
     unitOfWork.Save();
    }
    
    public void Update(AspNetUser aspnetuser) {
      unitOfWork.AspNetUserRepository.Update(entity: aspnetuser);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(AspNetUser aspnetuser) {
      unitOfWork.AspNetUserRepository.Update(entity: aspnetuser);
     unitOfWork.Save();
    }
    
    public void Delete(AspNetUser aspnetuser) {
      unitOfWork.Save();
    }
    
    }
}
