using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
using eShop.Infrastructure.Data.Context.CRM;
using System.Data.Entity;

namespace eshop.ApplicationService.Admin
{
 public class AspNetPageService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        CRMContext cRMContext = new CRMContext();
        public ICollection<AspNetPageVM> GetAllAspNetPage()
   {
    var aspnetpageItems = (from rItem in cRMContext.AspNetPage.OrderBy(r => r.PageID)
                           join lItemSize in cRMContext.AspNetPage.OrderBy(r => r.PageID) on rItem.ParentID equals lItemSize.PageID into ps
                           from lItemSize in ps.DefaultIfEmpty()
                           select new AspNetPageVM
                                 {
      PageID = rItem.PageID,
      ParentID = rItem.ParentID,
      ParantName = lItemSize.Controller,
      NameOption_En = rItem.NameOption_En,
      NameOption_Bn = rItem.NameOption_Bn,
      Controller = rItem.Controller,
      Action = rItem.Action,
      Area = rItem.Area,
      IconClass = rItem.IconClass,
      ActiveLi = rItem.ActiveLi,
      Status = rItem.Status,
      Displayorder = rItem.Displayorder,
      IsParent = rItem.IsParent,
                                    }).ToList();
    return aspnetpageItems;
    }
        public ICollection<AspNetPagesRoleVM> GetAllAspNetPagesRole(string moduleName)
        {
            var aa = Convert.ToInt32(moduleName);
            var aspnetpagesroleItems = (from rItem in cRMContext.AspNetPage.OrderBy(r => r.PageID).Where(e=>e.ModuleName==moduleName)
                                        join lItem in cRMContext.AspNetPagesRole.OrderBy(r => r.PageRoleId) on rItem.PageID equals lItem.PageId into ps
                                        from lItem in ps.DefaultIfEmpty()
                                        select new AspNetPagesRoleVM
                                        {
                                            PageRoleId = (int?)lItem.PageRoleId ?? 0,
                                            NameOption_En = rItem.NameOption_En ?? "",
                                            PageId = (int?)lItem.PageId ?? 0,
                                            RoleId = lItem.RoleId ,
                                            CanAdd = (bool?)lItem.CanAdd ?? false,
                                            CanEdit = (bool?)lItem.CanEdit ?? false,
                                            CanView = (bool?)lItem.CanView ?? false,
                                            CanDelete = (bool?)lItem.CanDelete ?? false,
                                            CanApprove = (bool?)lItem.CanApprove ?? false,
                                        }).ToList();
            return aspnetpagesroleItems;
        }
        public ICollection<AspNetPageVM> GetAllAspNetPageMain()
        {
            var aspnetpageItems = (from rItem in cRMContext.AspNetPage.Where(e=>e.PageID==0).OrderBy(r => r.PageID)
                                   select new AspNetPageVM
                                   {
                                       PageID = rItem.PageID,
                                       ParentID = rItem.ParentID,
                                       NameOption_En = rItem.NameOption_En,
                                       NameOption_Bn = rItem.NameOption_Bn,
                                       Controller = rItem.Controller,
                                       Action = rItem.Action,
                                       Area = rItem.Area,
                                       IconClass = rItem.IconClass,
                                       ActiveLi = rItem.ActiveLi,
                                       Status = rItem.Status,
                                       Displayorder = rItem.Displayorder,
                                       IsParent = rItem.IsParent,
                                   }).ToList();
            return aspnetpageItems;
        }


        public AspNetPageVM Get(long Id)
    {
     AspNetPageVM aspnetpageVM = new AspNetPageVM();
    var aspnetpageItems = cRMContext.AspNetPage.Find(Id);
aspnetpageVM.PageID = aspnetpageItems.PageID ;
aspnetpageVM.ParentID = aspnetpageItems.ParentID ;
aspnetpageVM.NameOption_En = aspnetpageItems.NameOption_En ;
aspnetpageVM.NameOption_Bn = aspnetpageItems.NameOption_Bn ;
aspnetpageVM.Controller = aspnetpageItems.Controller ;
aspnetpageVM.Action = aspnetpageItems.Action ;
aspnetpageVM.Area = aspnetpageItems.Area ;
aspnetpageVM.IconClass = aspnetpageItems.IconClass ;
aspnetpageVM.ActiveLi = aspnetpageItems.ActiveLi ;
aspnetpageVM.Status = aspnetpageItems.Status ;
aspnetpageVM.Displayorder = aspnetpageItems.Displayorder ;
aspnetpageVM.IsParent = aspnetpageItems.IsParent ;
    return aspnetpageVM;
    }
    
    public void Add(AspNetPage aspnetpage) {
            aspnetpage.PageID = cRMContext.AspNetPage.Where(e=>e.ParentID==aspnetpage.ParentID).FirstOrDefault().PageID+100;
     cRMContext.AspNetPage.Add(entity: aspnetpage);
            cRMContext.SaveChanges();
    }
    
    public void Update(AspNetPage aspnetpage) {
     
            cRMContext.Entry(aspnetpage).State = EntityState.Modified;
            cRMContext.SaveChanges();
        }
    
    public void ChangeStatus(AspNetPage aspnetpage) {
            cRMContext.Entry(aspnetpage).State = EntityState.Modified;
            cRMContext.SaveChanges();
        }
    
    public void Delete(AspNetPage aspnetpage) {
 
            var data = cRMContext.AspNetPage.Where(e => e.PageID == aspnetpage.PageID).First();
            cRMContext.AspNetPage.Remove(data);
            cRMContext.SaveChanges();
        }
    
    }
}
