using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
using eShop.Infrastructure.Data.Context.CRM;
using System.Data.Entity;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Core.Helpers;
using System.Data.SqlClient;
using System.Data;

namespace eshop.ApplicationService.Admin
{
 public class AspNetRoleService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        CRMContext cRMContext = new CRMContext();
        AspNetPagesRoleService aspNetPagesRoleService = new AspNetPagesRoleService();
        AspNetPageService aspNetPageService = new AspNetPageService();
        CommonRepository commonRepository = new CommonRepository();
        public ICollection<AspNetRoleVM> GetAllAspNetRole()
   {
            var aspnetroleItems = (from rItem in cRMContext.AspNetRole.ToList()
                                  select new AspNetRoleVM
                                 {
      Id = rItem.Id,
      Name = rItem.Name,
                                    }).ToList();
    return aspnetroleItems;
    }
    
    
    public AspNetRoleVM Get(long Id)
    {
     AspNetRoleVM aspnetroleVM = new AspNetRoleVM();
    var aspnetroleItems = cRMContext.AspNetRole.Find(Id);
    aspnetroleVM.Id = aspnetroleItems.Id ;
    aspnetroleVM.Name = aspnetroleItems.Name ;
    return aspnetroleVM;
    }
        public List<AspNetPage> GetByUserId(string UserId)
        {
            List<AspNetPage> listrole = new List<AspNetPage>();
            DataTable dt = commonRepository.GetBySpWithParam("GetPageBuUserId", new object[] { UserId });

            listrole = CommonRepository.ConvertDataTable<AspNetPage>(dt);
            
            return listrole;
        }
        public ICollection<MenuSelectVM> GetAllAspNetRoleByRole(string roleid)
        {
            // Point 1 Get All permisson by RoleId
            var roleperssion =aspNetPagesRoleService.GetAllAspNetPagesRole().Where(e => e.RoleId == roleid).ToList();
            var AspNetPages = aspNetPageService.GetAllAspNetPage();
            List<MenuSelectVM> menu = new List<MenuSelectVM>();
            foreach (var node in AspNetPages.OrderBy(e => e.Displayorder)) // Point 2 Looping and Order by DisplayOrder
            {
                MenuSelectVM obj = new MenuSelectVM();
                State state = new State();
                obj.state = state;
                obj.id = node.PageID.ToString();
                obj.parent = node.ParentID == 0 ? "#" : node.ParentID.ToString();
                obj.text = node.NameOption_En;
                obj.state.selected = roleperssion.FirstOrDefault(e => e.PageId == node.PageID) != null ? true : false;
                obj.state.disabled = false;
                obj.state.opened = true;
                menu.Add(obj);
            }
            return menu;
        }
        public void AddAspPageRole(List<string> permissions, string roleid)
        {
            string DeleteQuery = "DELETE AspNetPagesRoles WHERE RoleId = @roleid";
            SqlParameter[] commandParameters = new SqlParameter[1];
            commandParameters[0] = new SqlParameter("@roleid", roleid);
            commonRepository.ExecuteSQL(DeleteQuery, commandParameters);
            var lstPages = aspNetPageService.GetAllAspNetPage().ToList();
            List<AspNetPagesRole> listPermssion = new List<AspNetPagesRole>();
            for (int i = 0; i < permissions.Count(); i++) // Point 2 Looping and Add in List Object For Save
            {
                AspNetPagesRole Obj = new AspNetPagesRole();
                Obj.RoleId = roleid;
                Obj.PageId = Convert.ToInt32(permissions[i]);
                Obj.CanAdd = true;
                Obj.CanApprove = true;
                Obj.CanDelete = true;
                Obj.CanEdit = true;
                Obj.CanView = true;
                listPermssion.Add(Obj);

            }
        cRMContext.AspNetPagesRole.AddRange(listPermssion);
            cRMContext.SaveChanges();
        }
        public void Add(AspNetRole aspnetrole) {

            aspnetrole.Id = Guid.NewGuid().ToString();
            cRMContext.AspNetRole.Add(entity: aspnetrole);
            cRMContext.SaveChanges();
    }
    
    public void Update(AspNetRole aspnetrole) {
   
            cRMContext.Entry(aspnetrole).State = EntityState.Modified;
            cRMContext.SaveChanges();
        }
        public void Delete(AspNetRole aspnetrole) {
           var data= cRMContext.AspNetRole.Where(e => e.Id == aspnetrole.Id).First();
            cRMContext.AspNetRole.Remove(data);
            cRMContext.SaveChanges();
        }
    
    }
}
