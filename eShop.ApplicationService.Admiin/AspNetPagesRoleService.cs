using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
using eShop.Infrastructure.Data.Context.CRM;
using System.Data.Entity;

namespace eshop.ApplicationService.Admin
{
 public class AspNetPagesRoleService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        CRMContext cRMContext = new CRMContext();
        public ICollection<AspNetPagesRoleVM> GetAllAspNetPagesRole()
   {
    var aspnetpagesroleItems = (from rItem in cRMContext.AspNetPagesRole.OrderBy(r => r.PageRoleId)
                                  select new AspNetPagesRoleVM
                                 {
      PageRoleId = rItem.PageRoleId,
      PageId = rItem.PageId,
      RoleId = rItem.RoleId,
      CanAdd = rItem.CanAdd,
      CanEdit = rItem.CanEdit,
      CanView = rItem.CanView,
      CanDelete = rItem.CanDelete,
      CanApprove = rItem.CanApprove,
                                    }).ToList();
    return aspnetpagesroleItems;
    }
    
    
    public AspNetPagesRoleVM Get(long Id)
    {
     AspNetPagesRoleVM aspnetpagesroleVM = new AspNetPagesRoleVM();
    var aspnetpagesroleItems = cRMContext.AspNetPagesRole.Find(Id);
aspnetpagesroleVM.PageRoleId = aspnetpagesroleItems.PageRoleId ;
aspnetpagesroleVM.PageId = aspnetpagesroleItems.PageId ;
aspnetpagesroleVM.RoleId = aspnetpagesroleItems.RoleId ;
aspnetpagesroleVM.CanAdd = aspnetpagesroleItems.CanAdd ;
aspnetpagesroleVM.CanEdit = aspnetpagesroleItems.CanEdit ;
aspnetpagesroleVM.CanView = aspnetpagesroleItems.CanView ;
aspnetpagesroleVM.CanDelete = aspnetpagesroleItems.CanDelete ;
aspnetpagesroleVM.CanApprove = aspnetpagesroleItems.CanApprove ;
    return aspnetpagesroleVM;
    }
    
    public void Add(AspNetPagesRole aspnetpagesrole) {
            cRMContext.AspNetPagesRole.Add(entity: aspnetpagesrole);
            cRMContext.SaveChanges();
    }
        public void Adds(List<AspNetPagesRole> aspnetpagesroles)
        {
            foreach (var item in aspnetpagesroles)
            {
                if (cRMContext.AspNetPagesRole.Find(item.PageId) == null)
                {
                    cRMContext.AspNetPagesRole.Add(entity: item);
                }
                else {
                    cRMContext.Entry(item).State = EntityState.Modified;
                }
            }
            cRMContext.SaveChanges();
        }
        public void Update(AspNetPagesRole aspnetpagesrole) {
    
            cRMContext.Entry(aspnetpagesrole).State = EntityState.Modified;
            cRMContext.SaveChanges();
        }
    
    public void ChangeStatus(AspNetPagesRole aspnetpagesrole) {
            cRMContext.Entry(aspnetpagesrole).State = EntityState.Modified;
            cRMContext.SaveChanges();
        }
    
    public void Delete(AspNetPagesRole aspnetpagesrole) {
            var data = cRMContext.AspNetPagesRole.Where(e => e.PageId == aspnetpagesrole.PageId).First();
            cRMContext.AspNetPagesRole.Remove(data);
            cRMContext.SaveChanges();
        }
    
    }
}
