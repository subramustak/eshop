using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using System.Threading.Tasks;

namespace eshop.ApplicationService.MasterSteUp
{
    public class SizeService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public async Task<ICollection<SizeVM>> GetAllSize()
   {
    var sizeItems = (from rItem in await unitOfWork.SizeRepository.GetAllAsync()
                     orderby rItem.Id
                     select new SizeVM
                                 {
      Id = rItem.Id,
      SizeName = rItem.SizeName,
      SizeCode = rItem.SizeCode,
                         Status = rItem.Status,
                     }).ToList();
    return sizeItems;
    }
    
    
    public SizeVM Get(long Id)
    {
     SizeVM sizeVM = new SizeVM();
    var sizeItems = unitOfWork.SizeRepository.Get(Id);
sizeVM.Id = sizeItems.Id ;
sizeVM.SizeName = sizeItems.SizeName ;
sizeVM.SizeCode = sizeItems.SizeCode ;
    return sizeVM;
    }
    
    public void Add(Size size) {
      size.AddDate = DateTime.Now;
       size.AddBy = HttpContext.Current.User.Identity.Name;
      size.Status = "A";
      unitOfWork.SizeRepository.Add(entity: size);
     unitOfWork.Save();
    }
    
    public void Update(Size size) {
     size.UpdateDate = DateTime.Now;
      size.UpdateBy = HttpContext.Current.User.Identity.Name;
      size.Status = "A";
      unitOfWork.SizeRepository.Update(entity: size);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Size size) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.SizeId == size.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Size used in Product");
            size.UpdateDate = DateTime.Now;
       size.UpdateBy = HttpContext.Current.User.Identity.Name;
       size.Status = size.Status== "A" ? "D" : "A";
      unitOfWork.SizeRepository.Update(entity: size);
     unitOfWork.Save();
    }
    
    public void Delete(Size size) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.SizeId == size.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Size used in Product");
            unitOfWork.SizeRepository.Delete(Id: size.Id);
      unitOfWork.Save();
    }
    
    }
}
