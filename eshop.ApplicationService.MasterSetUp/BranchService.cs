﻿
using eShop.BusinessDomain.Core.Helpers;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.Infrastructure.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace eshop.ApplicationService.MasterSetUp
{
   public class BranchService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        CommonRepository commonRepository = new CommonRepository();
        public ICollection<BranchVM> GetAllItems()
        {
            var branchItems = (from rItem in unitOfWork.BranchRepository.GetAll().OrderBy(r => r.Id)
                                 select new BranchVM
                                 {
                                     Id=rItem.Id,
                                     Code = rItem.Code,
                                     Address = rItem.Address,
                                     BranchName = rItem.BranchName,
                                     BranchNameBangla = rItem.BranchNameBangla,
                                     ContactNumber = rItem.ContactNumber,
                                     Email = rItem.Email,
                                     Fax = rItem.Fax,
                                     SS_Lang = rItem.SS_Lang,
                                     SS_Latu = rItem.SS_Latu,
                                     Status = rItem.Status,
                                 }).ToList();
            return branchItems;
        }

        public BranchVM Get(long Id)
        {
            BranchVM branchVM = new BranchVM();
            var branchItems = unitOfWork.BranchRepository.Get(Id);
            branchVM.Id = branchItems.Id;
            branchVM.Code = branchItems.Code;
            branchVM.BranchName = branchItems.BranchName;
            branchVM.BranchNameBangla = branchItems.BranchNameBangla;
            branchVM.ContactNumber = branchItems.ContactNumber;
            branchVM.Email = branchItems.Email;
            branchVM.Address = branchItems.Address;
            branchVM.Fax = branchItems.Fax;
            branchVM.SS_Lang = branchItems.SS_Lang;
            branchVM.SS_Latu = branchItems.SS_Latu;
            return branchVM;
        }

        public void Add(Branch branch) {
            branch.AddDate = DateTime.Now;
            branch.AddBy = HttpContext.Current.User.Identity.Name;
            branch.Status = "A";
            unitOfWork.BranchRepository.Add(entity: branch);
            unitOfWork.Save();
        }
        public void Update(Branch branch)
        {
            branch.UpdateDate = DateTime.Now;
            branch.UpdateBy = HttpContext.Current.User.Identity.Name;
            branch.Status = "A";
            unitOfWork.BranchRepository.Update(entity: branch);
            unitOfWork.Save();
        }
        public void ChangeStatus(Branch branch)
        {
            //var product = unitOfWork.ProductRepository.GetAll(e => e.ProductBrandId == branch.Id && e.IsDeleted == false).Any();
            //if (product) throw new Exception("This Item used in Product");
            branch.UpdateDate= DateTime.Now;
            branch.UpdateBy = HttpContext.Current.User.Identity.Name;
            branch.Status = branch.Status== "A" ? "D" : "A";
            unitOfWork.BranchRepository.Update(entity: branch);
            unitOfWork.Save();
        }
        public void Delete(Branch branch)
        {
            if (commonRepository.IsExist("dbo.Stocks", "BranchId=" + branch.Id + "and Quantity > 0"))
            {
                throw new Exception("Branch Already use in Stock ");
            }
            //var product = unitOfWork.ProductRepository.GetAll(e => e. == branch.Id && e.IsDeleted == false).Any();
            //if (product) throw new Exception("This Item used in Product");
            branch.UpdateDate = DateTime.Now;
            branch.UpdateBy = HttpContext.Current.User.Identity.Name;
            branch.Status = "A";
            unitOfWork.BranchRepository.Delete(Id: branch.Id);
            unitOfWork.Save();
        }


    }
}
