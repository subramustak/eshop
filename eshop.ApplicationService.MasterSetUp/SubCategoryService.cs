using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using System.Threading.Tasks;

namespace eshop.ApplicationService.MasterSetUp
{
    public class SubCategoryService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        public async Task<ICollection<SubCategoryVM>> GetAllSubCategory()
        {
            var subcategoryItems = (from rItem in await unitOfWork.SubCategoryRepository.GetAllAsync()
                                    join lItem in await unitOfWork.CategoryRepository.GetAllAsync() on
                                    rItem.CatagoryId equals lItem.Id
                                    orderby rItem.Id
                                    select new SubCategoryVM
                                    {
                                        Id = rItem.Id,
                                        SubCatagoryName = rItem.SubCatagoryName,
                                        SubCatagoryCode = rItem.SubCatagoryCode,
                                        CatagoryName=lItem.CatagoryName,
                                        CatagoryId= rItem.CatagoryId,
                                        Status = rItem.Status,
                                    }).ToList();
            return subcategoryItems;
        }

        public async Task<ICollection<SubCategoryVM>> GetAllSubbYCategory(int Id)
        {
            var subcategoryItems = (from rItem in await unitOfWork.SubCategoryRepository.GetAllAsync()
                                   where rItem.CatagoryId == Id
                                    orderby rItem.Id
                                    select new SubCategoryVM
                                    {
                                        Id = rItem.Id,
                                        SubCatagoryName = rItem.SubCatagoryName,
                                        SubCatagoryCode = rItem.SubCatagoryCode,
                                      
                                    }).ToList();
            return subcategoryItems;
        }
        public SubCategoryVM Get(long Id)
        {
            SubCategoryVM subcategoryVM = new SubCategoryVM();
            var subcategoryItems = unitOfWork.SubCategoryRepository.Get(Id);
            subcategoryVM.Id = subcategoryItems.Id;
            subcategoryVM.SubCatagoryName = subcategoryItems.SubCatagoryName;
            subcategoryVM.SubCatagoryCode = subcategoryItems.SubCatagoryCode;
            return subcategoryVM;
        }
       
        public void Add(SubCategory subcategory)
        {
            subcategory.AddDate = DateTime.Now;
            subcategory.AddBy = HttpContext.Current.User.Identity.Name;
            subcategory.Status = "A";
            unitOfWork.SubCategoryRepository.Add(entity: subcategory);
            unitOfWork.Save();
        }

        public void Update(SubCategory subcategory)
        {
            subcategory.UpdateDate = DateTime.Now;
            subcategory.UpdateBy = HttpContext.Current.User.Identity.Name;
            subcategory.Status = "A";
            unitOfWork.SubCategoryRepository.Update(entity: subcategory);
            unitOfWork.Save();
        }

        public void ChangeStatus(SubCategory subcategory)
        {
            subcategory.UpdateDate = DateTime.Now;
            subcategory.UpdateBy = HttpContext.Current.User.Identity.Name;
            subcategory.Status = subcategory.Status == "A" ? "D" : "A";
            unitOfWork.SubCategoryRepository.Update(entity: subcategory);
            unitOfWork.Save();
        }

        public void Delete(SubCategory subcategory)
        {
            unitOfWork.SubCategoryRepository.Delete(Id: subcategory.Id);
            unitOfWork.Save();
        }

    }
}
