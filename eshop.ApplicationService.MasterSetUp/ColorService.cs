using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using System.Threading.Tasks;

namespace eshop.ApplicationService.MasterSetUp
{
 public class ColorService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public async Task<ICollection<ColorVM>> GetAllColor()
   {
    var colorItems = (from rItem in await unitOfWork.ColorRepository.GetAllAsync()
                      orderby rItem.Id
                                  select new ColorVM
                                 {
      Id = rItem.Id,
      ColorName = rItem.ColorName,
      HexadecimalCode = rItem.HexadecimalCode,
      ColorCode = rItem.ColorCode,
                                      Status = rItem.Status,
                                  }).ToList();
    return  colorItems;
    }
    
    
    public ColorVM Get(long Id)
    {
            ColorVM colorVM = new ColorVM();
          //  ColorVM colorVM = new ColorVM();
    var colorItems = unitOfWork.ColorRepository.Get(Id);
colorVM.Id = colorItems.Id ;
colorVM.ColorName = colorItems.ColorName ;
colorVM.HexadecimalCode = colorItems.HexadecimalCode ;
colorVM.ColorCode = colorItems.ColorCode ;
    return colorVM;
    }
    
    public void Add(Color color) {
      color.AddDate = DateTime.Now;
       color.AddBy = HttpContext.Current.User.Identity.Name;
      color.Status = "A";
      unitOfWork.ColorRepository.Add(entity: color);
     unitOfWork.Save();
    }
    
    public void Update(Color color) {
     color.UpdateDate = DateTime.Now;
      color.UpdateBy = HttpContext.Current.User.Identity.Name;
      color.Status = "A";
      unitOfWork.ColorRepository.Update(entity: color);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Color color) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.ProductBrandId == color.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Color used in Product");
            color.UpdateDate = DateTime.Now;
       color.UpdateBy = HttpContext.Current.User.Identity.Name;
       color.Status = color.Status== "A" ? "D" : "A";
      unitOfWork.ColorRepository.Update(entity: color);
     unitOfWork.Save();
    }
    
    public void Delete(Color color) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.ProductBrandId == color.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Color used in Product");
            unitOfWork.ColorRepository.Delete(Id: color.Id);
      unitOfWork.Save();
    }
    
    }
}
