using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using System.Threading.Tasks;

namespace eshop.ApplicationService.MasterSetUp
{
 public class BrandService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
    public async Task<ICollection<BrandVM>> GetAllBrand()
   {
    var brandItems = (from rItem in await unitOfWork.BrandRepository.GetAllAsync()
                      orderby rItem.Id
                                  select new BrandVM
                                 {
      Id = rItem.Id,
      BrandName = rItem.BrandName,
      BrandCode = rItem.BrandCode,
                                      Status = rItem.Status,
                                  }).ToList();
    return  brandItems;
    }
    
    
    public BrandVM Get(long Id)
    {
            BrandVM brandVM = new BrandVM();
    var brandItems = unitOfWork.BrandRepository.Get(Id);
brandVM.Id = brandItems.Id ;
brandVM.BrandName = brandItems.BrandName ;
brandVM.BrandCode = brandItems.BrandCode ;
    return brandVM;
    }
    
    public void Add(Brand brand) {
      brand.AddDate = DateTime.Now;
       brand.AddBy = HttpContext.Current.User.Identity.Name;
      brand.Status = "A";
      unitOfWork.BrandRepository.Add(entity: brand);
     unitOfWork.Save();
    }
    
    public void Update(Brand brand) {
     brand.UpdateDate = DateTime.Now;
      brand.UpdateBy = HttpContext.Current.User.Identity.Name;
      brand.Status = "A";
      unitOfWork.BrandRepository.Update(entity: brand);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Brand brand) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.ProductBrandId == brand.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Brand used in Product");
            brand.UpdateDate = DateTime.Now;
       brand.UpdateBy = HttpContext.Current.User.Identity.Name;
       brand.Status = brand.Status== "A" ? "D" : "A";
      unitOfWork.BrandRepository.Update(entity: brand);
     unitOfWork.Save();
    }
    
    public void Delete(Brand brand) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.ProductBrandId == brand.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Brand used in Product");
            unitOfWork.BrandRepository.Delete(Id: brand.Id);
      unitOfWork.Save();
    }
    
    }
}
