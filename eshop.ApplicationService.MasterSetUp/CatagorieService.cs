using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MastarSetUp.ViewModels;
using eShop.BusinessDomain.MastarSetUp.DomainObject;
using eShop.BusinessDomain.MasterSetUp.ViewModels;

namespace eshop.ApplicationService.MastarSetUp
{
 public class CatagorieService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<CatagorieVM> GetAllCatagorie()
   {
    var catagorieItems = (from rItem in unitOfWork.CatagorieRepository.GetAll().OrderBy(r => r.Id)
                                  select new CatagorieVM
                                 {
      Id = rItem.Id,
      CatagoryName = rItem.CatagoryName,
      CatagoryCode = rItem.CatagoryCode,
      CatagoryParent = rItem.CatagoryParent,
                                    }).ToList();
    return catagorieItems;
    }
    
    
    public CatagorieVM Get(long Id)
    {
     CatagorieVM catagorieVM = new CatagorieVM()
    var catagorieItems = unitOfWork.CatagorieRepository.Get(Id);
catagorieVM.Id = catagorieItems.Id ;
catagorieVM.CatagoryName = catagorieItems.CatagoryName ;
catagorieVM.CatagoryCode = catagorieItems.CatagoryCode ;
catagorieVM.CatagoryParent = catagorieItems.CatagoryParent ;
    return catagorieVM;
    }
    
    public void Add(Catagorie catagorie) {
      catagorie.AddDate = DateTime.Now;
       catagorie.AddBy = HttpContext.Current.User.Identity.Name;
      catagorie.Status = "A";
      unitOfWork.CatagorieRepository.Add(entity: catagorie);
     unitOfWork.Save();
    }
    
    public void Update(Catagorie catagorie) {
     catagorie.UpdateDate = DateTime.Now;
      catagorie.UpdateBy = HttpContext.Current.User.Identity.Name;
      catagorie.Status = "A";
      unitOfWork.CatagorieRepository.Update(entity: catagorie);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Catagorie catagorie) {
      catagorie.UpdateDate = DateTime.Now;
       catagorie.UpdateBy = HttpContext.Current.User.Identity.Name;
       catagorie.Status = catagorie.Status== "A" ? "D" : "A";
      unitOfWork.CatagorieRepository.Update(entity: catagorie);
     unitOfWork.Save();
    }
    
    public void Delete(Catagorie catagorie) {
      unitOfWork.CatagorieRepository.Delete(Id: catagorie.Id);
      unitOfWork.Save();
    }
    
    }
}
