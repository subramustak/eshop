using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
 
namespace eshop.ApplicationService.MasterSetUp
{
 public class EventService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<EventVM> GetAllEvent()
   {
    var eventItems = (from rItem in unitOfWork.EventRepository.GetAll().OrderBy(r => r.Id)
                                  select new EventVM
                                 {
      Id = rItem.Id,
      EventName = rItem.EventName,
      DsicountAmount = rItem.DsicountAmount,
      DsicountPercent = rItem.DsicountPercent,
                                      endDate = rItem.endDate,
                                      startDate = rItem.startDate,
                                      DsicountCount = rItem.DsicountCount,
                                      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return eventItems;
    }
    
    
    public EventVM Get(long Id)
    {
     EventVM eventVM = new EventVM();
    var eventItems = unitOfWork.EventRepository.Get(Id);
eventVM.Id = eventItems.Id ;
eventVM.EventName = eventItems.EventName ;
eventVM.DsicountAmount = eventItems.DsicountAmount ;
eventVM.DsicountPercent = eventItems.DsicountPercent ;
eventVM.IsArchived = eventItems.IsArchived ;
eventVM.IsDeleted = eventItems.IsDeleted ;
eventVM.AddBy = eventItems.AddBy ;
eventVM.AddDate = eventItems.AddDate ;
eventVM.UpdateBy = eventItems.UpdateBy ;
eventVM.UpdateDate = eventItems.UpdateDate ;
eventVM.Remarks = eventItems.Remarks ;
eventVM.Status = eventItems.Status ;
    return eventVM;
    }
    
    public void Add(Event _event) {
            _event.AddDate = DateTime.Now;
            _event.AddBy = HttpContext.Current.User.Identity.Name;
            _event.Status = "A";
      unitOfWork.EventRepository.Add(entity: _event);
     unitOfWork.Save();
    }
    
    public void Update(Event _event) {
            _event.UpdateDate = DateTime.Now;
        _event.UpdateBy = HttpContext.Current.User.Identity.Name;
            _event.Status = "A";
      unitOfWork.EventRepository.Update(entity: _event);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Event _event) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.EventId == _event.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Event used in Product");
            _event.UpdateDate = DateTime.Now;
            _event.UpdateBy = HttpContext.Current.User.Identity.Name;
            _event.Status = _event.Status== "A" ? "D" : "A";
      unitOfWork.EventRepository.Update(entity: _event);
     unitOfWork.Save();
    }
    
    public void Delete(Event _event) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.EventId == _event.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Event used in Product");
            unitOfWork.EventRepository.Delete(Id: _event.Id);
      unitOfWork.Save();
    }
    
    }
}
