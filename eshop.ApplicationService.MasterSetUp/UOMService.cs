using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using System.Threading.Tasks;
using eShop.BusinessDomain.Core.Helpers;
using Infrastructure.Crosscutting.Utility;
using eShop.BusinessDomain.CRM.ViewModels;
using System.Data;

namespace eshop.ApplicationService.MasterSteUp
{
    public class UOMService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        List<UOMVM> uOMVMs = new List<UOMVM>();
        UOMVM uOMVM = new UOMVM();
      
   public async Task<ICollection<UOMVM>> GetAllUOM()
   {
            try
            {

            
            uOMVMs = (from rItem in await unitOfWork.UOMRepository.GetAllAsync()
                      orderby rItem.Id
                      where rItem.IsDeleted==false
                                  select new UOMVM
                                 {
      Id = rItem.Id,
      UOMName = rItem.UOMName,
      UOMCode = rItem.UOMCode,
                                      Status = rItem.Status,
                                  }).ToList();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return uOMVMs;
    }
    
    
    public UOMVM Get(long Id)
    {
    var uomItems = unitOfWork.UOMRepository.Get(Id);
            uOMVM.Id = uomItems.Id ;
            uOMVM.UOMName = uomItems.UOMName ;
            uOMVM.UOMCode = uomItems.UOMCode ;
    return uOMVM;
    }
    
    public void Add(UOM uom) {
            var AllUOM = unitOfWork.UOMRepository.GetAll(e => e.UOMName == uom.UOMName);
            if (AllUOM.Any()) throw new Exception("UOM Name Already Exist");
      uom.AddDate = DateTime.Now;
       uom.AddBy = HttpContext.Current.User.Identity.Name;
      uom.Status = "A";
      unitOfWork.UOMRepository.Add(entity: uom);
     unitOfWork.Save();
    }
    
    public void Update(UOM uom) {
            var AllUOM = unitOfWork.UOMRepository.GetAll(e => e.UOMName == uom.UOMName && e.Id != uom.Id);
            if (AllUOM.Any()) throw new Exception("UOM Name Already Exist");
            uom.UpdateDate = DateTime.Now;
            uom.UpdateBy = HttpContext.Current.User.Identity.Name;
             uom.Status = "A";
      unitOfWork.UOMRepository.Update(entity: uom);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(UOM uom) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.UOMId == uom.Id && e.IsDeleted==false).Any();
            if (product) throw new Exception("This UOM used in Product");

      uom.UpdateDate = DateTime.Now;
       uom.UpdateBy = HttpContext.Current.User.Identity.Name;
       uom.Status = uom.Status== "A" ? "D" : "A";
      unitOfWork.UOMRepository.Update(entity: uom);
     unitOfWork.Save();
    }
    
    public void Delete(UOM uom) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.UOMId == uom.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This UOM used in Product");
            unitOfWork.UOMRepository.Delete(Id: uom.Id);
      unitOfWork.Save();
    }

       
    }
}
