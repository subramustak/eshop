using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
 
namespace eshop.ApplicationService.MasterSetUp
{
 public class BankService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<BankVM> GetAllBank()
   {
    var bankItems = (from rItem in unitOfWork.BankRepository.GetAll().OrderBy(r => r.Id)
                                  select new BankVM
                                 {
      Id = rItem.Id,
      BankName = rItem.BankName,
      BranchName = rItem.BranchName,
      Code = rItem.Code,
      AccountNumber = rItem.AccountNumber,
      Address = rItem.Address,
      AddressBangla = rItem.AddressBangla,
      ContactNumber = rItem.ContactNumber,
      Email = rItem.Email,
      Fax = rItem.Fax,
                                      Status = rItem.Status,
                                  }).ToList();
    return bankItems;
    }
    
    
    public BankVM Get(long Id)
    {
            BankVM bankVM = new BankVM();
    var bankItems = unitOfWork.BankRepository.Get(Id);
bankVM.Id = bankItems.Id ;
bankVM.BankName = bankItems.BankName ;
bankVM.BranchName = bankItems.BranchName ;
bankVM.Code = bankItems.Code ;
bankVM.AccountNumber = bankItems.AccountNumber ;
bankVM.Address = bankItems.Address ;
bankVM.AddressBangla = bankItems.AddressBangla ;
bankVM.ContactNumber = bankItems.ContactNumber ;
bankVM.Email = bankItems.Email ;
bankVM.Fax = bankItems.Fax ;
    return bankVM;
    }
    
    public void Add(Bank bank) {
      bank.AddDate = DateTime.Now;
       bank.AddBy = HttpContext.Current.User.Identity.Name;
      bank.Status = "A";
      unitOfWork.BankRepository.Add(entity: bank);
     unitOfWork.Save();
    }
    
    public void Update(Bank bank) {
     bank.UpdateDate = DateTime.Now;
      bank.UpdateBy = HttpContext.Current.User.Identity.Name;
      bank.Status = "A";
      unitOfWork.BankRepository.Update(entity: bank);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Bank bank) {
            var product = unitOfWork.SaleRepository.GetAll(e => e.BankId == bank.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Bank used in Sale");
            var product1 = unitOfWork.PurchaseRepository.GetAll(e => e.BankId == bank.Id && e.IsDeleted == false).Any();
            if (product1) throw new Exception("This Bank used in Purchase");
            bank.UpdateDate = DateTime.Now;
       bank.UpdateBy = HttpContext.Current.User.Identity.Name;
       bank.Status = bank.Status== "A" ? "D" : "A";
      unitOfWork.BankRepository.Update(entity: bank);
     unitOfWork.Save();
    }
    
    public void Delete(Bank bank) {
            var product = unitOfWork.SaleRepository.GetAll(e => e.BankId == bank.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Bank used in Sale");
            var product1 = unitOfWork.PurchaseRepository.GetAll(e => e.BankId == bank.Id && e.IsDeleted == false).Any();
            if (product1) throw new Exception("This Bank used in Purchase");
            unitOfWork.BankRepository.Delete(Id: bank.Id);
      unitOfWork.Save();
    }
    
    }
}
