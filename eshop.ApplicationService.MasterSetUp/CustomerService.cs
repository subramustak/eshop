using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using eShop.BusinessDomain.Account.DomainObject;
using eShop.ApplicationService.Account;
using Infrastructure.Crosscutting.Utility;

namespace eshop.ApplicationService.MasterSetUp
{
    public class CustomerService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        AccountNameService accountNameService = new AccountNameService();
        CommonRepository commonRepository = new CommonRepository();
        public ICollection<CustomerVM> GetAllCustomer()

        {
            var customerItems = (from rItem in unitOfWork.CustomerRepository.GetAll().OrderBy(r => r.Id)
                                 select new CustomerVM
                                 {
                                     Id = rItem.Id,
                                     Code = rItem.Code,
                                     Name = rItem.Name,
                                     CountryId = rItem.CountryId,

                                     Mobile = rItem.Mobile,
                                     PermanentAddress = rItem.PermanentAddress,
                                     PresentAddress = rItem.PresentAddress,
                                     PABX = rItem.PABX,
                                     Email = rItem.Email,
                                     FAX = rItem.FAX,
                                     Status = rItem.Status,
                                 }).ToList();
            return customerItems;
        }


        public CustomerVM Get(long Id)
        {
            CustomerVM customerVM = new CustomerVM();
            var customerItems = unitOfWork.CustomerRepository.Get(Id);
            customerVM.Id = customerItems.Id;
            customerVM.Code = customerItems.Code;
            customerVM.Name = customerItems.Name;
            customerVM.CountryId = customerItems.CountryId;
            customerVM.Mobile = customerItems.Mobile;
            customerVM.PermanentAddress = customerItems.PermanentAddress;
            customerVM.PresentAddress = customerItems.PresentAddress;
            customerVM.PABX = customerItems.PABX;
            customerVM.Email = customerItems.Email;
            customerVM.FAX = customerItems.FAX;
            return customerVM;
        }

        public void Add(Customer customer)
        {
            customer.AddDate = DateTime.Now;
            customer.AddBy = HttpContext.Current.User.Identity.Name;
            customer.Status = "A";
            unitOfWork.CustomerRepository.Add(entity: customer);
            AccountName accountName = new AccountName();
            accountName.Name = customer.Name;
            accountName.RootId = 1;
            accountName.PayNreciveId = customer.Id;
            accountNameService.Add(accountName);
            unitOfWork.Save();
        }

        public void Update(Customer customer)
        {
            Customer existingCustomer = unitOfWork.CustomerRepository.GetAll(e => e.Id == customer.Id).FirstOrDefault();
            
            existingCustomer.Code = customer.Code;
            existingCustomer.CountryId = customer.CountryId;
            existingCustomer.DivisionId = customer.DivisionId;
            existingCustomer.Email = customer.Email;
            existingCustomer.FAX = customer.FAX;
            existingCustomer.Mobile = customer.Mobile;
            existingCustomer.Name = customer.Name;
            existingCustomer.PABX = customer.PABX;
            existingCustomer.PermanentAddress = customer.PermanentAddress;
            existingCustomer.PresentAddress = customer.PresentAddress;
            existingCustomer.UpdateDate = DateTime.Now;
            existingCustomer.UpdateBy = HttpContext.Current.User.Identity.Name;
            existingCustomer.Status = "A";
            unitOfWork.CustomerRepository.Update(entity: existingCustomer);
            AccountName accountName = unitOfWork.AccountNameRepository.GetAll(e=>e.PayNreciveId==customer.Id).FirstOrDefault();
            accountName.Name = customer.Name;
            accountName.RootId = 1;
            accountName.PayNreciveId = customer.Id;
            accountName.UpdateDate = DateTime.Now;
            accountName.UpdateBy = HttpContext.Current.User.Identity.Name;
            accountNameService.UpdateByRoot(accountName);
            unitOfWork.Save();
        }
        public dynamic GetCustomer(string Name)
        {
            var product = unitOfWork.CustomerRepository.GetAll(e => e.Name.ToLower().StartsWith(Name.ToLower()) || e.Code.ToLower().StartsWith(Name.ToLower())).ToList();
            var data = (from p in product
                        select new { Text = p.Code + " " + p.Name, Value = p.Id }).ToList();
            return data;
        }
        public void ChangeStatus(Customer customer)
        {
            Customer customers = new Customer();
            var product = unitOfWork.SaleRepository.GetAll(e => e.CustomerId == customer.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Customer used in Sale");
            customers= unitOfWork.CustomerRepository.Get(customer.Id);
            customers.UpdateDate = DateTime.Now;
            customers.UpdateBy = HttpContext.Current.User.Identity.Name;
            customers.Status = customer.Status == "A" ? "D" : "A";
            unitOfWork.CustomerRepository.Update(entity: customers);
            unitOfWork.Save();
        }

        public void Delete(Customer customer)
        {
            var product = unitOfWork.SaleRepository.GetAll(e => e.CustomerId == customer.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Customer used in Sale");
            unitOfWork.CustomerRepository.Delete(Id: customer.Id);
            unitOfWork.Save();
        }

    }
}
