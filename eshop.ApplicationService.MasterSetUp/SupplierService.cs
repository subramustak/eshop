using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using eShop.BusinessDomain.Account.DomainObject;

namespace eshop.ApplicationService.MasterSetUp
{
    public class SupplierService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        public ICollection<SupplierVM> GetAllSupplier()
        {
            var SupplierItems = (from rItem in unitOfWork.SupplierRepository.GetAll().OrderBy(r => r.Id)
                                 select new SupplierVM
                                 {
                                     Id = rItem.Id,
                                     Code = rItem.Code,
                                     Name = rItem.Name,
                                     CountryId = rItem.CountryId,

                                     Mobile = rItem.Mobile,
                                     PermanentAddress = rItem.PermanentAddress,
                                     PresentAddress = rItem.PresentAddress,
                                     PABX = rItem.PABX,
                                     Email = rItem.Email,
                                     FAX = rItem.FAX,
                                     Status = rItem.Status,
                                 }).ToList();
            return SupplierItems;
        }


        public SupplierVM Get(long Id)
        {
            SupplierVM SupplierVM = new SupplierVM();
            var SupplierItems = unitOfWork.SupplierRepository.Get(Id);
            SupplierVM.Id = SupplierItems.Id;
            SupplierVM.Code = SupplierItems.Code;
            SupplierVM.Name = SupplierItems.Name;
            SupplierVM.CountryId = SupplierItems.CountryId;
            SupplierVM.Mobile = SupplierItems.Mobile;
            SupplierVM.PermanentAddress = SupplierItems.PermanentAddress;
            SupplierVM.PresentAddress = SupplierItems.PresentAddress;
            SupplierVM.PABX = SupplierItems.PABX;
            SupplierVM.Email = SupplierItems.Email;
            SupplierVM.FAX = SupplierItems.FAX;
            return SupplierVM;
        }

        public void Add(Supplier Supplier)
        {
            Supplier.AddDate = DateTime.Now;
            Supplier.AddBy = HttpContext.Current.User.Identity.Name;
            Supplier.Status = "A";
            unitOfWork.SupplierRepository.Add(entity: Supplier);
            AccountName accountName = new AccountName();
            accountName.PayNreciveId = Supplier.Id; ;
            accountName.Name = Supplier.Name;
            accountName.RootId = 2;
            accountName.AddDate = DateTime.Now;
            accountName.AddBy = HttpContext.Current.User.Identity.Name;
            accountName.Status = "A";
            unitOfWork.AccountNameRepository.Add(accountName);
            unitOfWork.Save();
        }

        public void Update(Supplier Supplier)
        {
            Supplier.UpdateDate = DateTime.Now;
            Supplier.UpdateBy = HttpContext.Current.User.Identity.Name;
            Supplier.Status = "A";
            unitOfWork.SupplierRepository.Update(entity: Supplier);
            unitOfWork.Save();
        }

        public void ChangeStatus(Supplier Supplier)
        {
            var product = unitOfWork.PurchaseRepository.GetAll(e => e.SupplierId == Supplier.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Supplier used in Purchase");
            Supplier.UpdateDate = DateTime.Now;
            Supplier.UpdateBy = HttpContext.Current.User.Identity.Name;
            Supplier.Status = Supplier.Status == "A" ? "D" : "A";
            unitOfWork.SupplierRepository.Update(entity: Supplier);
            unitOfWork.Save();
        }

        public void Delete(Supplier Supplier)
        {
            var product = unitOfWork.PurchaseRepository.GetAll(e => e.SupplierId == Supplier.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Supplier used in Purchase");
            unitOfWork.SupplierRepository.Delete(Id: Supplier.Id);
            unitOfWork.Save();
        }

    }
}
