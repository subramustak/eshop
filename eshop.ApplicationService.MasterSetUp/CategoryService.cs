using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using System.Threading.Tasks;

namespace eshop.ApplicationService.MasterSetUp
{
 public class CategoryService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        public async Task<ICollection<CategoryVM>> GetAllCategory()
   {
    var categoryItems = (from rItem in await unitOfWork.CategoryRepository.GetAllAsync()
                         orderby rItem.Id
                         select new CategoryVM
                                 {
      Id = rItem.Id,
      CatagoryName = rItem.CatagoryName,
      CatagoryCode = rItem.CatagoryCode,
      CatagoryParent = rItem.CatagoryParent,
                             Status = rItem.Status,
                         }).ToList();
    return categoryItems;
    }
    
    
    public CategoryVM Get(long Id)
    {
            CategoryVM categoryVM = new CategoryVM();
    var categoryItems = unitOfWork.CategoryRepository.Get(Id);
categoryVM.Id = categoryItems.Id ;
categoryVM.CatagoryName = categoryItems.CatagoryName ;
categoryVM.CatagoryCode = categoryItems.CatagoryCode ;
categoryVM.CatagoryParent = categoryItems.CatagoryParent ;
    return categoryVM;
    }
    
    public void Add(Category category) {
      category.AddDate = DateTime.Now;
       category.AddBy = HttpContext.Current.User.Identity.Name;
      category.Status = "A";
      unitOfWork.CategoryRepository.Add(entity: category);
     unitOfWork.Save();
    }
    
    public void Update(Category category) {
     category.UpdateDate = DateTime.Now;
      category.UpdateBy = HttpContext.Current.User.Identity.Name;
      category.Status = "A";
      unitOfWork.CategoryRepository.Update(entity: category);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Category category) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.ProductBrandId == category.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Category used in Product");
            category.UpdateDate = DateTime.Now;
       category.UpdateBy = HttpContext.Current.User.Identity.Name;
       category.Status = category.Status== "A" ? "D" : "A";
      unitOfWork.CategoryRepository.Update(entity: category);
     unitOfWork.Save();
    }
    
    public void Delete(Category category) {
            var product = unitOfWork.ProductRepository.GetAll(e => e.ProductBrandId == category.Id && e.IsDeleted == false).Any();
            if (product) throw new Exception("This Category used in Product");
            unitOfWork.CategoryRepository.Delete(Id: category.Id);
      unitOfWork.Save();
    }
    
    }
}
