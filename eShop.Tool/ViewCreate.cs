﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace eShop.Tool
{
    internal class ViewCreate
    {
        private readonly string _tableName;
        private readonly List<TableSchema> _tableSchema;
        private readonly TableSchema _tablePk;

        //Shahed vai created the following folder path
        private static string strOutPutPath;

        public ViewCreate(string tableName, List<TableSchema> tableSchema, string path)
        {
            _tableSchema = tableSchema;
            var firstOrDefault = tableSchema.FirstOrDefault(p => p.IsIdentity.ToLower() == "true");
            if (firstOrDefault != null)
                _tablePk = firstOrDefault;
           
            _tableName = tableName;
            strOutPutPath = path + tableName;
            if (!Directory.Exists(strOutPutPath))  // if it doesn't exist, create
                Directory.CreateDirectory(strOutPutPath);
        }

        public void WriteModel()
        {
            using (var writer = new StreamWriter(strOutPutPath + "\\" + _tableName + "VM.cs"))
            {

                writer.WriteLine("@{ ");
                writer.WriteLine("   ViewBag.Title = \"Branch\";");
                writer.WriteLine(" }");
                writer.WriteLine("<div class=\"widget-body\" ng-controller=\"" + _tableName + "Crtl\"> ");
                writer.WriteLine(" <!-- widget grid -->");
                writer.WriteLine("  <section id=\"widget-grid\" >");
                writer.WriteLine("   <!-- row -->");
                writer.WriteLine("    <div class=\"row\">");
                writer.WriteLine("     <!-- NEW WIDGET START -->");
                writer.WriteLine("     <article class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">");
                writer.WriteLine("      <!-- Widget ID (each widget will need unique ID)-->");
                writer.WriteLine("      <div class=\"jarviswidget jarviswidget-Branch-darken\" id=\"wid-id-1\" data-widget-editbutton=\"false\">");
                writer.WriteLine("        <header>");
                writer.WriteLine("         <span class=\"widget-icon\"> <i class=\"fa fa-table\"></i> </span>");
                writer.WriteLine("          <h2>" + _tableName + " List</h2>");
                writer.WriteLine("        </header>");
                writer.WriteLine("        <!-- widget div-->");
                writer.WriteLine("            <div>");
                writer.WriteLine("          <!-- widget edit box -->");
                writer.WriteLine("          <div class=\"jarviswidget-editbox\">");
                writer.WriteLine("          <!-- This area used as dropdown edit box -->");
                writer.WriteLine("          </div>");
                writer.WriteLine("          <!-- end widget edit box -->");
                writer.WriteLine("           <!-- widget content -->");
                writer.WriteLine("           <div class=\"widget-body no-padding\">");
                writer.WriteLine("             <div class=\"row\">");
                writer.WriteLine("                  <div class=\"col-md-4 col-sm-5 col-xs-6\">");
                writer.WriteLine("                      <div class=\"form - Product\">");
                writer.WriteLine("                          <div class=\"col - lg - 3 col - md - 5 col - sm - 4 col - xs - 8\" style=\"padding - left:0px\">");
                writer.WriteLine("                              <select class=\"form - control\" ng-model=\"pageSize\" ng-options=\"P.value as P.text for P in pageSizeArray\"></select>") ;
                writer.WriteLine("                          </div>");
                writer.WriteLine("                          <div class=\"col - md - 7  col - sm - 8  col - xs - 4\">");
                writer.WriteLine("                          </div>");
                writer.WriteLine("                        </div>");
                writer.WriteLine("                      </div>");
                writer.WriteLine("                      <div class=\"col - md - 6 col - sm - 3 col - xs - 1\">");
                writer.WriteLine("                       <div id=\"sample_3_filter\" class=\"pull - right\">");
                writer.WriteLine("                          <input ng-model=\"q\" id=\"search\" class=\"form - control\" placeholder=\"Filter text\">");
                writer.WriteLine("                        </div>");
                writer.WriteLine("                       </div>");
                writer.WriteLine("                      <div class=\"col - md - 2 col - sm - 4 col - xs - 5\">");
                writer.WriteLine("                      <button class=\"btn btn - success\" data-toggle=\"modal\" data-target=\"#addmodal\">");
                writer.WriteLine("                      <i class=\"fa fa - plus\"></i> <span class=\"hidden - mobile\">Add New Row</span>");
                writer.WriteLine("                      </button>");
                writer.WriteLine("                      </div>");
                writer.WriteLine("                     </div>");
                writer.WriteLine("                      <div class=\"table - responsive\">");
                writer.WriteLine("                      <table class=\"table table - bordered table - striped\">");
                writer.WriteLine("                       <thead>");
                writer.WriteLine("                           <tr>");
                for (int i = 0; i < _tableSchema.Count - 8; i++)
                {
                    if ("ntext".Equals(_tableSchema[i].DbTypeName))
                    {
                        writer.WriteLine("      <th width=\"10%\">" + _tableSchema[i].ColumnName + "</th> ");
                        continue;
                    }
                    writer.WriteLine("      <th width=\"10%\">" + _tableSchema[i].ColumnName + "</th> ");
                }
                writer.WriteLine("                          </tr>");
                writer.WriteLine("                       </thead>");
                writer.WriteLine("                       <tbody>");
                writer.WriteLine(" <tr ng-show=\""+_tableSchema+"s.length <= 0\"><td colspan=\""+ (_tableSchema.Count - 8).ToString() + "\" style=\"text - align:center; \"><i class=\"fa fa-refresh\"></i> Not Data Found!!</td></tr>");
                writer.WriteLine(" <tr ng-repeat=\"item in "+_tableSchema+"s\">");
                for (int i = 0; i < _tableSchema.Count - 8; i++)
                {
                    if ("ntext".Equals(_tableSchema[i].DbTypeName))
                    {
                        writer.WriteLine("  <td>{{item." + _tableSchema[i].ColumnName + "}}</td>");
                        continue;
                    }
                    writer.WriteLine("  <td>{{item." + _tableSchema[i].ColumnName + "}}</td>");
                }
                writer.WriteLine(" < td >");
                writer.WriteLine(" < a ng - show = \"item.Status=='A'\" ng - click = \"StatusBranch(item)\" href = \"javascript:;\" class=\"btn btn-xs btn-icon-only btn-primary\">");
                writer.WriteLine("  Active");
                writer.WriteLine("  </a>");
                writer.WriteLine(" < a ng-show=\"item.Status=='D'\" ng-click=\"StatusBranch(item)\" href=\"javascript:;\" class=\"btn btn-xs btn-icon-only btn-success\">");
                writer.WriteLine("   DeActive");
                writer.WriteLine(" </a>");
                writer.WriteLine(" </td>");
                writer.WriteLine("  < td>");
                writer.WriteLine("  < a href = \"javascript:;\" class=\"btn btn-icon-only green\" ng-click=\"edit(item)\" data-target=\"#addmodal\" data-toggle=\"modal\">");
                writer.WriteLine("  <i class=\"fa fa-edit\"></i>");
                writer.WriteLine("  </a>");
                writer.WriteLine(" < a ng-click=\"deleteBranch(item)\" href=\"javascript:;\" class=\"btn btn-icon-only red\">");
                writer.WriteLine("  <i class=\"fa fa-trash\"></i>");
                writer.WriteLine("  </a>");
                writer.WriteLine("  </td>");
                writer.WriteLine("  </tr>");
                writer.WriteLine("  </ tbody >");
                writer.WriteLine("  </ table >");
                writer.WriteLine("   </ div >");
                writer.WriteLine("   </ div >");
                writer.WriteLine("  < !--end widget content -->");
                writer.WriteLine("  </ div >");
                writer.WriteLine("  < !--end widget div -->");
                writer.WriteLine("  </ div >");
                writer.WriteLine("  < !--end widget-- >");
                writer.WriteLine("  </ article >");
                writer.WriteLine("  < !--WIDGET END-- >");
                writer.WriteLine("   </ div >");
                writer.WriteLine("  < !--end row-- >");
                writer.WriteLine("   </ section >");
                writer.WriteLine("  < !--end widget grid -->");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine(" ");
                writer.WriteLine("namespace eShop.BusinessDomain.MasterSetUp.ViewModels");
                writer.WriteLine("{");
                writer.WriteLine("    public class " + _tableName+"VM");
                writer.WriteLine("    {");
                for (int i = 0; i < _tableSchema.Count -8; i++)
                {
                    if ("ntext".Equals(_tableSchema[i].DbTypeName))
                    {
                        writer.WriteLine("      public string " + _tableSchema[i].ColumnName + " { get; set; }");
                        continue;
                    }
                    writer.WriteLine("      public " + _tableSchema[i].DataTypeName + " " + _tableSchema[i].ColumnName + " { get; set; }");
                }
                writer.WriteLine("    }");
                writer.WriteLine("}");
            }
        }
    }
}