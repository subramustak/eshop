﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.Tool
{
    class Program
    {
        public static string connectionString;
        private static string TableName { get; set; }
        private static string ModuleName { get; set; }
        private static List<TableSchema> _tableSchema;
        private static int surpress=0;

        //Shahed vai created the following folder path
        private static string currentPath = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory())) + "\\OutPut\\";

        static void Main(string[] args)
        {
            //connectionString = "Data Source=ATOMWS2012;Initial Catalog=FXTFPlusDev;User ID=devfxtf;Password=dev123456;Persist Security Info=false;";
            //connectionString = "Data Source= DESKTOP-7B9RGQ0;Initial Catalog=eShopDB;MultipleActiveResultSets=true;Integrated Security=True";
            connectionString = "Data Source=70.32.28.3;Initial Catalog=eShop_DB;User ID=eShopUser;Password=ZAQ!2wsx;MultipleActiveResultSets=true;";

            while (true)
            {
                Console.WriteLine("Insert table name (type \'q\' to exit): ");
                TableName = Console.ReadLine();

                if ("q".Equals(TableName))
                {
                    break;
                }
                Console.WriteLine("Insert Module Name: ");
                ModuleName = Console.ReadLine();
                _tableSchema = GetTables();
                if (null == _tableSchema)
                {
                    Console.WriteLine("Invalid table name");
                    TableName = "";
                    continue;
                }
                TableName = TableName.Substring(0, TableName.Length - 1);
                ModelCreate modelCreate = new ModelCreate(TableName, _tableSchema, ModuleName, surpress, currentPath);
                modelCreate.WriteModel();
                UnitWorkCreate unitWorkCreate = new UnitWorkCreate(TableName, _tableSchema, currentPath);
                unitWorkCreate.WriteUnitWork();
                ServiceCreate serviceCreate=new ServiceCreate(TableName, _tableSchema, ModuleName, surpress, currentPath);
                serviceCreate.WriteService();

                ControllerCreate controllerCreate=new ControllerCreate(TableName, _tableSchema, ModuleName, surpress, currentPath);
                controllerCreate.WriteController();

                //RepositoryCreate repositoryCreate = new RepositoryCreate(TableName, _tableSchema, currentPath);
                //repositoryCreate.WriteRepository();
                //InterfaceBLCreate interfaceBLCreate = new InterfaceBLCreate(TableName, _tableSchema, currentPath);
                //interfaceBLCreate.WriteInterfaceBL();
                //BLCreate blCreate = new BLCreate(TableName, _tableSchema, currentPath);
                //blCreate.WriteBL();
                //SqlCreate sqlCreate = new SqlCreate(TableName, _tableSchema, currentPath);
                //sqlCreate.WriteSql();
                TableName = "";
            }

        }

        public static List<TableSchema> GetTables()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                try
                {
                    var sql = "select * from " + TableName + " WHERE 1 = 0";
                    connection.Open();
                    var cmd = new SqlCommand(sql, connection);
                    var reader = cmd.ExecuteReader();

                    var schemaTable = reader.GetSchemaTable();

                    if (schemaTable != null)
                        return (from DataRow row in schemaTable.Rows
                                select new TableSchema
                                {
                                    ColumnName = row["ColumnName"].ToString(),
                                    ColumnSize = row["ColumnSize"].ToString(),
                                    DataTypeName = ConvertToType(row["DataTypeName"].ToString()),
                                    DbTypeName = row["DataTypeName"].ToString(),
                                    IsIdentity = row["IsIdentity"].ToString()
                                }).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return new List<TableSchema>();
        }

        private static string ConvertToType(string sqlDataType)
        {
            switch (sqlDataType)
            {
                case "bigint":
                    return "long";
                case "nvarchar":
                    return "string";
                case "datetime":
                    return "DateTime";
                case "bit":
                    return "bool";
                case "int":
                    return "int";
                case "tinyint":
                    return "tinyint";
                case "decimal":
                    return "decimal";
            }
            return "";
        }
    }
}
