﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace eShop.Tool
{
    internal class ModelCreate
    {
        private readonly string _tableName;
        private readonly List<TableSchema> _tableSchema;
        private readonly TableSchema _tablePk;
        private readonly int _surpress;
        private static string _ModuleName { get; set; }

        //Shahed vai created the following folder path
        private static string strOutPutPath;

        public ModelCreate(string tableName, List<TableSchema> tableSchema, string ModuleName, int surpress,  string path)
        {
            _tableSchema = tableSchema;
            var firstOrDefault = tableSchema.FirstOrDefault(p => p.IsIdentity.ToLower() == "true");
            if (firstOrDefault != null)
                _tablePk = firstOrDefault;
            _ModuleName = ModuleName;
            _tableName = tableName;
            _surpress = surpress;
            strOutPutPath = path + tableName;
            if (!Directory.Exists(strOutPutPath))  // if it doesn't exist, create
                Directory.CreateDirectory(strOutPutPath);
        }

        public void WriteModel()
        {
            using (var writer = new StreamWriter(strOutPutPath + "\\" + _tableName + "VM.cs"))
            {

                writer.WriteLine("using System;");
                writer.WriteLine("namespace eShop.BusinessDomain."+ _ModuleName + ".ViewModels");
                writer.WriteLine("{");
                writer.WriteLine("    public class " + _tableName+"VM");
                writer.WriteLine("    {");
                for (int i = 0; i < _tableSchema.Count- _surpress; i++)
                {
                    if ("ntext".Equals(_tableSchema[i].DbTypeName))
                    {
                        writer.WriteLine("      public string " + _tableSchema[i].ColumnName + " { get; set; }");
                        continue;
                    }
                    writer.WriteLine("      public " + _tableSchema[i].DataTypeName + " " + _tableSchema[i].ColumnName + " { get; set; }");
                }
                writer.WriteLine("    }");
                writer.WriteLine("}");
            }
        }
    }
}