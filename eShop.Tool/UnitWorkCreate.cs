﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace eShop.Tool
{
    internal class UnitWorkCreate
    {
        private readonly string _tableName;
        private readonly List<TableSchema> _tableSchema;
        private readonly TableSchema _tablePk;

        //Shahed vai created the following folder path
        private static string strOutPutPath;

        public UnitWorkCreate(string tableName, List<TableSchema> tableSchema, string path)
        {
            _tableSchema = tableSchema;
            var firstOrDefault = tableSchema.FirstOrDefault(p => p.IsIdentity.ToLower() == "true");
            if (firstOrDefault != null)
                _tablePk = firstOrDefault;
           
            _tableName = tableName;
            strOutPutPath = path + tableName;
            if (!Directory.Exists(strOutPutPath))  // if it doesn't exist, create
                Directory.CreateDirectory(strOutPutPath);
        }

        public void WriteUnitWork()
        {
            using (var writer = new StreamWriter(strOutPutPath + "\\UnitWork" + _tableName + ".cs"))
            {
                
                writer.WriteLine("       private Repository<"+ _tableName + "> "+ _tableName.ToLower() + "Repository;");
                writer.WriteLine("       public Repository<" + _tableName + "> "+ _tableName + "Repository" );
                writer.WriteLine("       {");
                writer.WriteLine("        get");
                writer.WriteLine("         {");
                writer.WriteLine("          if (this." + _tableName.ToLower() + "Repository == null)");
                writer.WriteLine("          {");
                writer.WriteLine("           this."+ _tableName.ToLower() + "Repository = new Repository<" + _tableName + ">(cRMContext);");
                writer.WriteLine("          }");
                writer.WriteLine("       return " + _tableName.ToLower() + "Repository;");
                writer.WriteLine("       }");
                writer.WriteLine("      }");
            }
        }
    }
}