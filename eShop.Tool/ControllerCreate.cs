﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace eShop.Tool
{
    internal class ControllerCreate
    {
        private readonly string _tableName;
        private readonly List<TableSchema> _tableSchema;
        private readonly TableSchema _tablePk;
           private readonly int _surpress;
        private static string _ModuleName { get; set; }

        //Shahed vai created the following folder path
        private static string strOutPutPath;

        public ControllerCreate(string tableName, List<TableSchema> tableSchema, string ModuleName,int surpress,  string path)
        {
            _tableSchema = tableSchema;
            var firstOrDefault = tableSchema.FirstOrDefault(p => p.IsIdentity.ToLower() == "true");
            if (firstOrDefault != null)
                _tablePk = firstOrDefault;

            _ModuleName = ModuleName;
            _tableName = tableName;
            _surpress = surpress;
            strOutPutPath = path + tableName;
            if (!Directory.Exists(strOutPutPath))  // if it doesn't exist, create
                Directory.CreateDirectory(strOutPutPath);
        }

        public void WriteController()
        {
            using (var writer = new StreamWriter(strOutPutPath + "\\" + _tableName + "Controller.cs"))
            {

                writer.WriteLine("using eshop.ApplicationService."+_ModuleName+";");
                writer.WriteLine("using eShop.BusinessDomain.Core;");
                writer.WriteLine("using eShop.BusinessDomain." + _ModuleName + ".DomainObject;");
                writer.WriteLine("using Infrastructure.Crosscutting.Utility;");
                writer.WriteLine("using System;");
                writer.WriteLine("using System.Net;");
                writer.WriteLine("using System.Web.Http;");
                writer.WriteLine(" ");
                writer.WriteLine("namespace eshop.ApplicationService."+ _ModuleName);
                writer.WriteLine(" {");
                writer.WriteLine("  public class " + _tableName+ "Controller : ApiController");
                writer.WriteLine("   {");
                writer.WriteLine("    " + _tableName + "Service " + _tableName.ToLower() + "Service;");
                writer.WriteLine("    public " + _tableName + "Controller()");
                writer.WriteLine("     {");
                writer.WriteLine("       this." + _tableName.ToLower() + "Service = new " + _tableName + "Service();");
                writer.WriteLine("     }");
                writer.WriteLine("    #region " + _tableName + "");
                //add
                writer.WriteLine("   [Route(\"" + _tableName + "/Add" + _tableName + "/\")]");
                writer.WriteLine("   [HttpPost]");
                writer.WriteLine("   public IHttpActionResult Add" + _tableName + "(" + _tableName + " " + _tableName.ToLower() + ")");
                writer.WriteLine("   {");
                writer.WriteLine("    CommonResponse cr = new CommonResponse();");
                writer.WriteLine("     " + _tableName + " o" + _tableName + " = new " + _tableName + "();");
                writer.WriteLine("     try");
                writer.WriteLine("      {");
                writer.WriteLine("      if (" + _tableName.ToLower() + ".Id == 0)");
                writer.WriteLine("      {");
                writer.WriteLine("       " + _tableName.ToLower() + "Service.Add(" + _tableName.ToLower() + ");");
                writer.WriteLine("        cr.message = Message.SAVED;" );
                writer.WriteLine("      }");
                writer.WriteLine("     else");
                writer.WriteLine("     {");
                writer.WriteLine("      " + _tableName.ToLower() + "Service.Update(" + _tableName.ToLower() + ");");
                writer.WriteLine("     cr.message =  Message.UPDATED; ");
                writer.WriteLine("     }");
                writer.WriteLine("    }");
                writer.WriteLine("    catch (Exception ex)");
                writer.WriteLine("    {");
                writer.WriteLine("      return BadRequest(ex.Message);");
                writer.WriteLine("    }");
                writer.WriteLine("     return Json(cr);");
                writer.WriteLine("   }");
                //getall
                writer.WriteLine("   [Route(\"" + _tableName + "/GetAll" + _tableName + "/\")]");
                writer.WriteLine("   [HttpGet]");
                writer.WriteLine("   public IHttpActionResult GetAll" + _tableName + "()");
                writer.WriteLine("   {");
                writer.WriteLine("    CommonResponse cr = new CommonResponse();");
                writer.WriteLine("     try");
                writer.WriteLine("      {");
                writer.WriteLine("       cr.results = " + _tableName.ToLower() + "Service.GetAll" + _tableName + "();");
                writer.WriteLine("       cr.message = cr.results != null ? \"Data Found\" : Message.NOTFOUND;");
                writer.WriteLine("       }");
                writer.WriteLine("     catch (Exception ex)");
                writer.WriteLine("      {");
                writer.WriteLine("       return BadRequest(ex.Message);");
                writer.WriteLine("      }");
                writer.WriteLine("      return Json(cr);");
                writer.WriteLine("    }");
                //get
                writer.WriteLine("   [Route(\"" + _tableName + "/Get" + _tableName + "/\")]");
                writer.WriteLine("   [HttpGet]");
                writer.WriteLine("   public IHttpActionResult Get" + _tableName + "(int Id)");
                writer.WriteLine("   {");
                writer.WriteLine("   CommonResponse cr = new CommonResponse();");
                writer.WriteLine("   try");
                writer.WriteLine("    {");
                writer.WriteLine("      cr.results = this." + _tableName.ToLower() + "Service.Get(Id);");
                writer.WriteLine("       cr.message = cr.results != null ? \"Data Found\" : Message.NOTFOUND;");
                writer.WriteLine("     }");
                writer.WriteLine("    catch (Exception ex)");
                writer.WriteLine("    {");
                writer.WriteLine("     return BadRequest(ex.Message);");
                writer.WriteLine("    }");
                writer.WriteLine("    return Json(cr);");
                writer.WriteLine("  }");

                //Delete
                writer.WriteLine("   [Route(\"" + _tableName + "/Delete" + _tableName + "/\")]");
                writer.WriteLine("   [HttpGet]");
                writer.WriteLine("   public IHttpActionResult Delete" + _tableName + "(" + _tableName + " " + _tableName.ToLower() + ")");
                writer.WriteLine("   {");
                writer.WriteLine("   CommonResponse cr = new CommonResponse();");
                writer.WriteLine("   try");
                writer.WriteLine("    {");
                writer.WriteLine("       " + _tableName.ToLower() + "Service.Delete(" + _tableName.ToLower() + ");");
                writer.WriteLine("        cr.message = Message.DELETED;");
                writer.WriteLine("     }");
                writer.WriteLine("    catch (Exception ex)");
                writer.WriteLine("    {");
                writer.WriteLine("     return BadRequest(ex.Message);");
                writer.WriteLine("    }");
                writer.WriteLine("    return Json(cr);");
                writer.WriteLine("  }");

                //Status
                writer.WriteLine("   [Route(\"" + _tableName + "/Status" + _tableName + "/\")]");
                writer.WriteLine("   [HttpPost]");
                writer.WriteLine("   public IHttpActionResult Status" + _tableName + "(" + _tableName + " " + _tableName.ToLower() + ")");
                writer.WriteLine("   {");
                writer.WriteLine("   CommonResponse cr = new CommonResponse();");
                writer.WriteLine("   try");
                writer.WriteLine("    {");
                writer.WriteLine("       this." + _tableName.ToLower() + "Service.ChangeStatus(" + _tableName.ToLower() + ");");
                writer.WriteLine("        cr.message = Message.UPDATED;");
                writer.WriteLine("     }");
                writer.WriteLine("    catch (Exception ex)");
                writer.WriteLine("    {");
                writer.WriteLine("      return BadRequest(ex.Message);");
                writer.WriteLine("     }");
                writer.WriteLine("    return Json(cr);");
                writer.WriteLine("   }");


                writer.WriteLine("    #endregion " + _tableName + "");
                writer.WriteLine("   }");
                writer.WriteLine(" }");
            }
        }
    }
}