﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace eShop.Tool
{
    internal class ServiceCreate
    {
        private readonly string _tableName;
        private readonly List<TableSchema> _tableSchema;
        private readonly TableSchema _tablePk;
        private readonly int _surpress;
        private static string _ModuleName { get; set; }

        //Shahed vai created the following folder path
        private static string strOutPutPath;

        public ServiceCreate(string tableName, List<TableSchema> tableSchema, string ModuleName,int surpress, string path)
        {
            _tableSchema = tableSchema;
            var firstOrDefault = tableSchema.FirstOrDefault(p => p.IsIdentity.ToLower() == "true");
            if (firstOrDefault != null)
                _tablePk = firstOrDefault;

            _ModuleName = ModuleName;
            _tableName = tableName;
            _surpress = surpress;

            strOutPutPath = path + tableName;
            if (!Directory.Exists(strOutPutPath))  // if it doesn't exist, create
                Directory.CreateDirectory(strOutPutPath);
        }

        public void WriteService()
        {
            using (var writer = new StreamWriter(strOutPutPath + "\\" + _tableName + "Service.cs"))
            {

                writer.WriteLine("using System;");
                writer.WriteLine("using eShop.Infrastructure.Data.UnitOfWork;");
                writer.WriteLine("using System.Collections.Generic;");
                writer.WriteLine("using System.Linq;");
                writer.WriteLine("using System.Web;");
                writer.WriteLine("using eShop.BusinessDomain." + _ModuleName+".ViewModels;");
                writer.WriteLine("using eShop.BusinessDomain." + _ModuleName + ".DomainObject;");
                writer.WriteLine(" ");
                writer.WriteLine("namespace eshop.ApplicationService."+ _ModuleName);
                writer.WriteLine("{");
                writer.WriteLine(" public class " + _tableName+"Service");
                writer.WriteLine("  {");
                writer.WriteLine("   UnitOfWork unitOfWork = new UnitOfWork();");
                writer.WriteLine("   public ICollection<"+ _tableName + "VM> GetAll"+_tableName+"()");
                writer.WriteLine("   {");
                writer.WriteLine("    var "+ _tableName.ToLower() + "Items = (from rItem in unitOfWork."+_tableName+"Repository.GetAll().OrderBy(r => r.Id)");
                writer.WriteLine("                                  select new "+ _tableName + "VM");
                writer.WriteLine("                                 {");
                for (int i = 0; i < _tableSchema.Count - _surpress; i++)
                {
                    if ("ntext".Equals(_tableSchema[i].DbTypeName))
                    {
                 writer.WriteLine("                                  " + _tableSchema[i].ColumnName + " = rItem." + _tableSchema[i].ColumnName + ",");
                        continue;
                    }
                    writer.WriteLine("      " + _tableSchema[i].ColumnName + " = rItem." + _tableSchema[i].ColumnName + ",");
                }
                writer.WriteLine("                                    }).ToList();");
                writer.WriteLine("    return " + _tableName.ToLower() + "Items;");
                writer.WriteLine("    }");
                writer.WriteLine("    ");
                //finish GetALL
                writer.WriteLine("    ");
                writer.WriteLine("    public " + _tableName + "VM Get(long Id)");
                writer.WriteLine("    {");
                writer.WriteLine("     "+ _tableName + "VM " + _tableName.ToLower() + "VM = new "  + _tableName + "VM();");
                writer.WriteLine("    var " + _tableName.ToLower() + "Items = unitOfWork." + _tableName + "Repository.Get(Id);");
                for (int i = 0; i < _tableSchema.Count - _surpress; i++)
                {
                    if ("ntext".Equals(_tableSchema[i].DbTypeName))
                    {
                        writer.WriteLine("" + _tableName.ToLower() + "VM." + _tableSchema[i].ColumnName + " = " + _tableName.ToLower() + "Items." + _tableSchema[i].ColumnName + " ;");
                        continue;
                    }
                    writer.WriteLine("" + _tableName.ToLower() + "VM." + _tableSchema[i].ColumnName + " = " + _tableName.ToLower() + "Items." + _tableSchema[i].ColumnName + " ;");
                }
                writer.WriteLine("    return " + _tableName.ToLower() + "VM;");
                writer.WriteLine("    }");
                //finish Get
                writer.WriteLine("    ");
                writer.WriteLine("    public void Add(" + _tableName + " " + _tableName.ToLower() + ") {");
                writer.WriteLine("      " + _tableName.ToLower() + ".AddDate = DateTime.Now;");
                writer.WriteLine("       " + _tableName.ToLower() + ".AddBy = HttpContext.Current.User.Identity.Name;");
                writer.WriteLine("      " + _tableName.ToLower() + ".Status = \"A\";");
                writer.WriteLine("      unitOfWork." + _tableName + "Repository.Add(entity: " + _tableName.ToLower() + ");");
                writer.WriteLine("     unitOfWork.Save();");
                writer.WriteLine("    }");
                //add
                writer.WriteLine("    ");
                writer.WriteLine("    public void Update(" + _tableName + " " + _tableName.ToLower() + ") {");
                writer.WriteLine("     " + _tableName.ToLower() + ".UpdateDate = DateTime.Now;");
                writer.WriteLine("      " + _tableName.ToLower() + ".UpdateBy = HttpContext.Current.User.Identity.Name;");
                writer.WriteLine("      " + _tableName.ToLower() + ".Status = \"A\";");
                writer.WriteLine("      unitOfWork." + _tableName + "Repository.Update(entity: " + _tableName.ToLower() + ");");
                writer.WriteLine("     unitOfWork.Save();");
                writer.WriteLine("    }");
                //update

                writer.WriteLine("    ");
                writer.WriteLine("    public void ChangeStatus(" + _tableName + " " + _tableName.ToLower() + ") {");
                writer.WriteLine("      " + _tableName.ToLower() + ".UpdateDate = DateTime.Now;");
                writer.WriteLine("       " + _tableName.ToLower() + ".UpdateBy = HttpContext.Current.User.Identity.Name;");
                writer.WriteLine("       " + _tableName.ToLower() + ".Status = " + _tableName.ToLower() + ".Status== \"A\" ? \"D\" : \"A\";");
                writer.WriteLine("      unitOfWork." + _tableName + "Repository.Update(entity: " + _tableName.ToLower() + ");");
                writer.WriteLine("     unitOfWork.Save();");
                writer.WriteLine("    }");
                writer.WriteLine("    ");
                writer.WriteLine("    public void Delete(" + _tableName + " " + _tableName.ToLower() + ") {");
                writer.WriteLine("      unitOfWork." + _tableName + "Repository.Delete(Id: " + _tableName.ToLower() + ".Id);");
                writer.WriteLine("      unitOfWork.Save();");
                writer.WriteLine("    }");

                writer.WriteLine("    ");
                writer.WriteLine("    }");
                writer.WriteLine("}");
            }
        }
    }
}