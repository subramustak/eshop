using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
 
namespace eshop.ApplicationService.MasterSetUp
{
 public class CustomerService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<CustomerVM> GetAllCustomer()
   {
    var customerItems = (from rItem in unitOfWork.CustomerRepository.GetAll().OrderBy(r => r.Id)
                                  select new CustomerVM
                                 {
      Id = rItem.Id,
      Code = rItem.Code,
      Name = rItem.Name,
      CountryId = rItem.CountryId,
      DivisionId = rItem.DivisionId,
      DistrictId = rItem.DistrictId,
      Mobile = rItem.Mobile,
      PermanentAddress = rItem.PermanentAddress,
      PresentAddress = rItem.PresentAddress,
      PABX = rItem.PABX,
      Email = rItem.Email,
      FAX = rItem.FAX,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return customerItems;
    }
    
    
    public CustomerVM Get(long Id)
    {
     CustomerVM customerVM = new CustomerVM();
    var customerItems = unitOfWork.CustomerRepository.Get(Id);
customerVM.Id = customerItems.Id ;
customerVM.Code = customerItems.Code ;
customerVM.Name = customerItems.Name ;
customerVM.CountryId = customerItems.CountryId ;
customerVM.DivisionId = customerItems.DivisionId ;
customerVM.DistrictId = customerItems.DistrictId ;
customerVM.Mobile = customerItems.Mobile ;
customerVM.PermanentAddress = customerItems.PermanentAddress ;
customerVM.PresentAddress = customerItems.PresentAddress ;
customerVM.PABX = customerItems.PABX ;
customerVM.Email = customerItems.Email ;
customerVM.FAX = customerItems.FAX ;
customerVM.IsArchived = customerItems.IsArchived ;
customerVM.IsDeleted = customerItems.IsDeleted ;
customerVM.AddBy = customerItems.AddBy ;
customerVM.AddDate = customerItems.AddDate ;
customerVM.UpdateBy = customerItems.UpdateBy ;
customerVM.UpdateDate = customerItems.UpdateDate ;
customerVM.Remarks = customerItems.Remarks ;
customerVM.Status = customerItems.Status ;
    return customerVM;
    }
    
    public void Add(Customer customer) {
      customer.AddDate = DateTime.Now;
       customer.AddBy = HttpContext.Current.User.Identity.Name;
      customer.Status = "A";
      unitOfWork.CustomerRepository.Add(entity: customer);
     unitOfWork.Save();
    }
    
    public void Update(Customer customer) {
     customer.UpdateDate = DateTime.Now;
      customer.UpdateBy = HttpContext.Current.User.Identity.Name;
      customer.Status = "A";
      unitOfWork.CustomerRepository.Update(entity: customer);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Customer customer) {
      customer.UpdateDate = DateTime.Now;
       customer.UpdateBy = HttpContext.Current.User.Identity.Name;
       customer.Status = customer.Status== "A" ? "D" : "A";
      unitOfWork.CustomerRepository.Update(entity: customer);
     unitOfWork.Save();
    }
    
    public void Delete(Customer customer) {
      unitOfWork.CustomerRepository.Delete(Id: customer.Id);
      unitOfWork.Save();
    }
    
    }
}
