using System;
namespace eShop.BusinessDomain.MasterSetUp.ViewModels
{
    public class CustomerVM
    {
      public int Id { get; set; }
      public string Code { get; set; }
      public string Name { get; set; }
      public int CountryId { get; set; }
      public int DivisionId { get; set; }
      public int DistrictId { get; set; }
      public string Mobile { get; set; }
      public string PermanentAddress { get; set; }
      public string PresentAddress { get; set; }
      public string PABX { get; set; }
      public string Email { get; set; }
      public string FAX { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
    }
}
