       private Repository<Customer> customerRepository;
       public Repository<Customer> CustomerRepository
       {
        get
         {
          if (this.customerRepository == null)
          {
           this.customerRepository = new Repository<Customer>(cRMContext);
          }
       return customerRepository;
       }
      }
