using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class SalesDetailsReturnController : ApiController
   {
    SalesDetailsReturnService salesdetailsreturnService;
    public SalesDetailsReturnController()
     {
       this.salesdetailsreturnService = new SalesDetailsReturnService();
     }
    #region SalesDetailsReturn
   [Route("SalesDetailsReturn/AddSalesDetailsReturn/")]
   [HttpPost]
   public IHttpActionResult AddSalesDetailsReturn(SalesDetailsReturn salesdetailsreturn)
   {
    CommonResponse cr = new CommonResponse();
     SalesDetailsReturn oSalesDetailsReturn = new SalesDetailsReturn();
     try
      {
      if (salesdetailsreturn.Id == 0)
      {
       salesdetailsreturnService.Add(salesdetailsreturn);
        cr.message = Message.SAVED;
      }
     else
     {
      salesdetailsreturnService.Update(salesdetailsreturn);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("SalesDetailsReturn/GetAllSalesDetailsReturn/")]
   [HttpGet]
   public IHttpActionResult GetAllSalesDetailsReturn()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = salesdetailsreturnService.GetAllSalesDetailsReturn();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("SalesDetailsReturn/GetSalesDetailsReturn/")]
   [HttpGet]
   public IHttpActionResult GetSalesDetailsReturn(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.salesdetailsreturnService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SalesDetailsReturn/DeleteSalesDetailsReturn/")]
   [HttpGet]
   public IHttpActionResult DeleteSalesDetailsReturn(SalesDetailsReturn salesdetailsreturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       salesdetailsreturnService.Delete(salesdetailsreturn);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SalesDetailsReturn/StatusSalesDetailsReturn/")]
   [HttpPost]
   public IHttpActionResult StatusSalesDetailsReturn(SalesDetailsReturn salesdetailsreturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.salesdetailsreturnService.ChangeStatus(salesdetailsreturn);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion SalesDetailsReturn
   }
 }
