       private Repository<SalesDetailsReturn> salesdetailsreturnRepository;
       public Repository<SalesDetailsReturn> SalesDetailsReturnRepository
       {
        get
         {
          if (this.salesdetailsreturnRepository == null)
          {
           this.salesdetailsreturnRepository = new Repository<SalesDetailsReturn>(cRMContext);
          }
       return salesdetailsreturnRepository;
       }
      }
