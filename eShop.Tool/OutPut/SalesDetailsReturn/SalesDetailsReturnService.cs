using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class SalesDetailsReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<SalesDetailsReturnVM> GetAllSalesDetailsReturn()
   {
    var salesdetailsreturnItems = (from rItem in unitOfWork.SalesDetailsReturnRepository.GetAll().OrderBy(r => r.Id)
                                  select new SalesDetailsReturnVM
                                 {
      Id = rItem.Id,
      SalesId = rItem.SalesId,
      ProductId = rItem.ProductId,
      Discount = rItem.Discount,
      UnitePrice = rItem.UnitePrice,
      Date = rItem.Date,
      Quantity = rItem.Quantity,
      TotalAmount = rItem.TotalAmount,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
      SalesReturns_Id = rItem.SalesReturns_Id,
                                    }).ToList();
    return salesdetailsreturnItems;
    }
    
    
    public SalesDetailsReturnVM Get(long Id)
    {
     SalesDetailsReturnVM salesdetailsreturnVM = new SalesDetailsReturnVM();
    var salesdetailsreturnItems = unitOfWork.SalesDetailsReturnRepository.Get(Id);
salesdetailsreturnVM.Id = salesdetailsreturnItems.Id ;
salesdetailsreturnVM.SalesId = salesdetailsreturnItems.SalesId ;
salesdetailsreturnVM.ProductId = salesdetailsreturnItems.ProductId ;
salesdetailsreturnVM.Discount = salesdetailsreturnItems.Discount ;
salesdetailsreturnVM.UnitePrice = salesdetailsreturnItems.UnitePrice ;
salesdetailsreturnVM.Date = salesdetailsreturnItems.Date ;
salesdetailsreturnVM.Quantity = salesdetailsreturnItems.Quantity ;
salesdetailsreturnVM.TotalAmount = salesdetailsreturnItems.TotalAmount ;
salesdetailsreturnVM.IsArchived = salesdetailsreturnItems.IsArchived ;
salesdetailsreturnVM.IsDeleted = salesdetailsreturnItems.IsDeleted ;
salesdetailsreturnVM.AddBy = salesdetailsreturnItems.AddBy ;
salesdetailsreturnVM.AddDate = salesdetailsreturnItems.AddDate ;
salesdetailsreturnVM.UpdateBy = salesdetailsreturnItems.UpdateBy ;
salesdetailsreturnVM.UpdateDate = salesdetailsreturnItems.UpdateDate ;
salesdetailsreturnVM.Remarks = salesdetailsreturnItems.Remarks ;
salesdetailsreturnVM.Status = salesdetailsreturnItems.Status ;
salesdetailsreturnVM.SalesReturns_Id = salesdetailsreturnItems.SalesReturns_Id ;
    return salesdetailsreturnVM;
    }
    
    public void Add(SalesDetailsReturn salesdetailsreturn) {
      salesdetailsreturn.AddDate = DateTime.Now;
       salesdetailsreturn.AddBy = HttpContext.Current.User.Identity.Name;
      salesdetailsreturn.Status = "A";
      unitOfWork.SalesDetailsReturnRepository.Add(entity: salesdetailsreturn);
     unitOfWork.Save();
    }
    
    public void Update(SalesDetailsReturn salesdetailsreturn) {
     salesdetailsreturn.UpdateDate = DateTime.Now;
      salesdetailsreturn.UpdateBy = HttpContext.Current.User.Identity.Name;
      salesdetailsreturn.Status = "A";
      unitOfWork.SalesDetailsReturnRepository.Update(entity: salesdetailsreturn);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(SalesDetailsReturn salesdetailsreturn) {
      salesdetailsreturn.UpdateDate = DateTime.Now;
       salesdetailsreturn.UpdateBy = HttpContext.Current.User.Identity.Name;
       salesdetailsreturn.Status = salesdetailsreturn.Status== "A" ? "D" : "A";
      unitOfWork.SalesDetailsReturnRepository.Update(entity: salesdetailsreturn);
     unitOfWork.Save();
    }
    
    public void Delete(SalesDetailsReturn salesdetailsreturn) {
      unitOfWork.SalesDetailsReturnRepository.Delete(Id: salesdetailsreturn.Id);
      unitOfWork.Save();
    }
    
    }
}
