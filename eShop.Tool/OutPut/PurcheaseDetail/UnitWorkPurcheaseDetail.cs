       private Repository<PurcheaseDetail> purcheasedetailRepository;
       public Repository<PurcheaseDetail> PurcheaseDetailRepository
       {
        get
         {
          if (this.purcheasedetailRepository == null)
          {
           this.purcheasedetailRepository = new Repository<PurcheaseDetail>(cRMContext);
          }
       return purcheasedetailRepository;
       }
      }
