using eshop.ApplicationService.Sales;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Sales.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Sales
 {
  public class PurcheaseDetailController : ApiController
   {
    PurcheaseDetailService purcheasedetailService;
    public PurcheaseDetailController()
     {
       this.purcheasedetailService = new PurcheaseDetailService();
     }
    #region PurcheaseDetail
   [Route("PurcheaseDetail/AddPurcheaseDetail/")]
   [HttpPost]
   public IHttpActionResult AddPurcheaseDetail(PurcheaseDetail purcheasedetail)
   {
    CommonResponse cr = new CommonResponse();
     PurcheaseDetail oPurcheaseDetail = new PurcheaseDetail();
     try
      {
      if (purcheasedetail.Id == 0)
      {
       purcheasedetailService.Add(purcheasedetail);
        cr.message = Message.SAVED;
      }
     else
     {
      purcheasedetailService.Update(purcheasedetail);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("PurcheaseDetail/GetAllPurcheaseDetail/")]
   [HttpGet]
   public IHttpActionResult GetAllPurcheaseDetail()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = purcheasedetailService.GetAllPurcheaseDetail();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("PurcheaseDetail/GetPurcheaseDetail/")]
   [HttpGet]
   public IHttpActionResult GetPurcheaseDetail(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.purcheasedetailService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurcheaseDetail/DeletePurcheaseDetail/")]
   [HttpGet]
   public IHttpActionResult DeletePurcheaseDetail(PurcheaseDetail purcheasedetail)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       purcheasedetailService.Delete(purcheasedetail);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurcheaseDetail/StatusPurcheaseDetail/")]
   [HttpPost]
   public IHttpActionResult StatusPurcheaseDetail(PurcheaseDetail purcheasedetail)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.purcheasedetailService.ChangeStatus(purcheasedetail);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion PurcheaseDetail
   }
 }
