using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Sales.ViewModels;
using eShop.BusinessDomain.Sales.DomainObject;
 
namespace eshop.ApplicationService.Sales
{
 public class PurcheaseDetailService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<PurcheaseDetailVM> GetAllPurcheaseDetail()
   {
    var purcheasedetailItems = (from rItem in unitOfWork.PurcheaseDetailRepository.GetAll().OrderBy(r => r.Id)
                                  select new PurcheaseDetailVM
                                 {
      Id = rItem.Id,
      PurchaseId = rItem.PurchaseId,
      ProductId = rItem.ProductId,
      ProductName = rItem.ProductName,
      ProductCode = rItem.ProductCode,
      UnitePrice = rItem.UnitePrice,
      Date = rItem.Date,
      Quantity = rItem.Quantity,
      Discount = rItem.Discount,
      Slup = rItem.Slup,
      TotalPrice = rItem.TotalPrice,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
      Purcheases_Id = rItem.Purcheases_Id,
                                    }).ToList();
    return purcheasedetailItems;
    }
    
    
    public PurcheaseDetailVM Get(long Id)
    {
     PurcheaseDetailVM purcheasedetailVM = new PurcheaseDetailVM();
    var purcheasedetailItems = unitOfWork.PurcheaseDetailRepository.Get(Id);
purcheasedetailVM.Id = purcheasedetailItems.Id ;
purcheasedetailVM.PurchaseId = purcheasedetailItems.PurchaseId ;
purcheasedetailVM.ProductId = purcheasedetailItems.ProductId ;
purcheasedetailVM.ProductName = purcheasedetailItems.ProductName ;
purcheasedetailVM.ProductCode = purcheasedetailItems.ProductCode ;
purcheasedetailVM.UnitePrice = purcheasedetailItems.UnitePrice ;
purcheasedetailVM.Date = purcheasedetailItems.Date ;
purcheasedetailVM.Quantity = purcheasedetailItems.Quantity ;
purcheasedetailVM.Discount = purcheasedetailItems.Discount ;
purcheasedetailVM.Slup = purcheasedetailItems.Slup ;
purcheasedetailVM.TotalPrice = purcheasedetailItems.TotalPrice ;
purcheasedetailVM.IsArchived = purcheasedetailItems.IsArchived ;
purcheasedetailVM.IsDeleted = purcheasedetailItems.IsDeleted ;
purcheasedetailVM.AddBy = purcheasedetailItems.AddBy ;
purcheasedetailVM.AddDate = purcheasedetailItems.AddDate ;
purcheasedetailVM.UpdateBy = purcheasedetailItems.UpdateBy ;
purcheasedetailVM.UpdateDate = purcheasedetailItems.UpdateDate ;
purcheasedetailVM.Remarks = purcheasedetailItems.Remarks ;
purcheasedetailVM.Status = purcheasedetailItems.Status ;
purcheasedetailVM.Purcheases_Id = purcheasedetailItems.Purcheases_Id ;
    return purcheasedetailVM;
    }
    
    public void Add(PurcheaseDetail purcheasedetail) {
      purcheasedetail.AddDate = DateTime.Now;
       purcheasedetail.AddBy = HttpContext.Current.User.Identity.Name;
      purcheasedetail.Status = "A";
      unitOfWork.PurcheaseDetailRepository.Add(entity: purcheasedetail);
     unitOfWork.Save();
    }
    
    public void Update(PurcheaseDetail purcheasedetail) {
     purcheasedetail.UpdateDate = DateTime.Now;
      purcheasedetail.UpdateBy = HttpContext.Current.User.Identity.Name;
      purcheasedetail.Status = "A";
      unitOfWork.PurcheaseDetailRepository.Update(entity: purcheasedetail);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(PurcheaseDetail purcheasedetail) {
      purcheasedetail.UpdateDate = DateTime.Now;
       purcheasedetail.UpdateBy = HttpContext.Current.User.Identity.Name;
       purcheasedetail.Status = purcheasedetail.Status== "A" ? "D" : "A";
      unitOfWork.PurcheaseDetailRepository.Update(entity: purcheasedetail);
     unitOfWork.Save();
    }
    
    public void Delete(PurcheaseDetail purcheasedetail) {
      unitOfWork.PurcheaseDetailRepository.Delete(Id: purcheasedetail.Id);
      unitOfWork.Save();
    }
    
    }
}
