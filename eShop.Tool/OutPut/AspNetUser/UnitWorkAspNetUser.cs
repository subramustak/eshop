       private Repository<AspNetUser> aspnetuserRepository;
       public Repository<AspNetUser> AspNetUserRepository
       {
        get
         {
          if (this.aspnetuserRepository == null)
          {
           this.aspnetuserRepository = new Repository<AspNetUser>(cRMContext);
          }
       return aspnetuserRepository;
       }
      }
