using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
 
namespace eshop.ApplicationService.Admin
{
 public class AspNetUserService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<AspNetUserVM> GetAllAspNetUser()
   {
    var aspnetuserItems = (from rItem in unitOfWork.AspNetUserRepository.GetAll().OrderBy(r => r.Id)
                                  select new AspNetUserVM
                                 {
      Id = rItem.Id,
      Email = rItem.Email,
      EmailConfirmed = rItem.EmailConfirmed,
      PasswordHash = rItem.PasswordHash,
      SecurityStamp = rItem.SecurityStamp,
      PhoneNumber = rItem.PhoneNumber,
      PhoneNumberConfirmed = rItem.PhoneNumberConfirmed,
      TwoFactorEnabled = rItem.TwoFactorEnabled,
      LockoutEndDateUtc = rItem.LockoutEndDateUtc,
      LockoutEnabled = rItem.LockoutEnabled,
      AccessFailedCount = rItem.AccessFailedCount,
      UserName = rItem.UserName,
      Image = rItem.Image,
      Address = rItem.Address,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
      FullName = rItem.FullName,
      MobileNo = rItem.MobileNo,
                                    }).ToList();
    return aspnetuserItems;
    }
    
    
    public AspNetUserVM Get(long Id)
    {
     AspNetUserVM aspnetuserVM = new AspNetUserVM();
    var aspnetuserItems = unitOfWork.AspNetUserRepository.Get(Id);
aspnetuserVM.Id = aspnetuserItems.Id ;
aspnetuserVM.Email = aspnetuserItems.Email ;
aspnetuserVM.EmailConfirmed = aspnetuserItems.EmailConfirmed ;
aspnetuserVM.PasswordHash = aspnetuserItems.PasswordHash ;
aspnetuserVM.SecurityStamp = aspnetuserItems.SecurityStamp ;
aspnetuserVM.PhoneNumber = aspnetuserItems.PhoneNumber ;
aspnetuserVM.PhoneNumberConfirmed = aspnetuserItems.PhoneNumberConfirmed ;
aspnetuserVM.TwoFactorEnabled = aspnetuserItems.TwoFactorEnabled ;
aspnetuserVM.LockoutEndDateUtc = aspnetuserItems.LockoutEndDateUtc ;
aspnetuserVM.LockoutEnabled = aspnetuserItems.LockoutEnabled ;
aspnetuserVM.AccessFailedCount = aspnetuserItems.AccessFailedCount ;
aspnetuserVM.UserName = aspnetuserItems.UserName ;
aspnetuserVM.Image = aspnetuserItems.Image ;
aspnetuserVM.Address = aspnetuserItems.Address ;
aspnetuserVM.IsArchived = aspnetuserItems.IsArchived ;
aspnetuserVM.IsDeleted = aspnetuserItems.IsDeleted ;
aspnetuserVM.AddBy = aspnetuserItems.AddBy ;
aspnetuserVM.AddDate = aspnetuserItems.AddDate ;
aspnetuserVM.UpdateBy = aspnetuserItems.UpdateBy ;
aspnetuserVM.UpdateDate = aspnetuserItems.UpdateDate ;
aspnetuserVM.Remarks = aspnetuserItems.Remarks ;
aspnetuserVM.Status = aspnetuserItems.Status ;
aspnetuserVM.FullName = aspnetuserItems.FullName ;
aspnetuserVM.MobileNo = aspnetuserItems.MobileNo ;
    return aspnetuserVM;
    }
    
    public void Add(AspNetUser aspnetuser) {
      aspnetuser.AddDate = DateTime.Now;
       aspnetuser.AddBy = HttpContext.Current.User.Identity.Name;
      aspnetuser.Status = "A";
      unitOfWork.AspNetUserRepository.Add(entity: aspnetuser);
     unitOfWork.Save();
    }
    
    public void Update(AspNetUser aspnetuser) {
     aspnetuser.UpdateDate = DateTime.Now;
      aspnetuser.UpdateBy = HttpContext.Current.User.Identity.Name;
      aspnetuser.Status = "A";
      unitOfWork.AspNetUserRepository.Update(entity: aspnetuser);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(AspNetUser aspnetuser) {
      aspnetuser.UpdateDate = DateTime.Now;
       aspnetuser.UpdateBy = HttpContext.Current.User.Identity.Name;
       aspnetuser.Status = aspnetuser.Status== "A" ? "D" : "A";
      unitOfWork.AspNetUserRepository.Update(entity: aspnetuser);
     unitOfWork.Save();
    }
    
    public void Delete(AspNetUser aspnetuser) {
      unitOfWork.AspNetUserRepository.Delete(Id: aspnetuser.Id);
      unitOfWork.Save();
    }
    
    }
}
