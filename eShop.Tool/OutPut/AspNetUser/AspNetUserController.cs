using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Admin
 {
  public class AspNetUserController : ApiController
   {
    AspNetUserService aspnetuserService;
    public AspNetUserController()
     {
       this.aspnetuserService = new AspNetUserService();
     }
    #region AspNetUser
   [Route("AspNetUser/AddAspNetUser/")]
   [HttpPost]
   public IHttpActionResult AddAspNetUser(AspNetUser aspnetuser)
   {
    CommonResponse cr = new CommonResponse();
     AspNetUser oAspNetUser = new AspNetUser();
     try
      {
      if (aspnetuser.Id == 0)
      {
       aspnetuserService.Add(aspnetuser);
        cr.message = Message.SAVED;
      }
     else
     {
      aspnetuserService.Update(aspnetuser);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AspNetUser/GetAllAspNetUser/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetUser()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetuserService.GetAllAspNetUser();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("AspNetUser/GetAspNetUser/")]
   [HttpGet]
   public IHttpActionResult GetAspNetUser(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetuserService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetUser/DeleteAspNetUser/")]
   [HttpGet]
   public IHttpActionResult DeleteAspNetUser(AspNetUser aspnetuser)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetuserService.Delete(aspnetuser);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetUser/StatusAspNetUser/")]
   [HttpPost]
   public IHttpActionResult StatusAspNetUser(AspNetUser aspnetuser)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.aspnetuserService.ChangeStatus(aspnetuser);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AspNetUser
   }
 }
