using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Master.ViewModels;
using eShop.BusinessDomain.Master.DomainObject;
 
namespace eshop.ApplicationService.Master
{
 public class SettingService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<SettingVM> GetAllSetting()
   {
    var settingItems = (from rItem in unitOfWork.SettingRepository.GetAll().OrderBy(r => r.Id)
                                  select new SettingVM
                                 {
      Id = rItem.Id,
      PurcheasePrefix = rItem.PurcheasePrefix,
      PurcheasePostfix = rItem.PurcheasePostfix,
      SalesPrefix = rItem.SalesPrefix,
      SalesPostfix = rItem.SalesPostfix,
      Currency = rItem.Currency,
      LogoImage = rItem.LogoImage,
      HeaderImage = rItem.HeaderImage,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
      PurcheaseStart = rItem.PurcheaseStart,
      SalesPreStart = rItem.SalesPreStart,
      AccountPrefix = rItem.AccountPrefix,
      AccountPreStart = rItem.AccountPreStart,
      AccountPostfix = rItem.AccountPostfix,
      VAT = rItem.VAT,
      CustomerId = rItem.CustomerId,
                                    }).ToList();
    return settingItems;
    }
    
    
    public SettingVM Get(long Id)
    {
     SettingVM settingVM = new SettingVM();
    var settingItems = unitOfWork.SettingRepository.Get(Id);
settingVM.Id = settingItems.Id ;
settingVM.PurcheasePrefix = settingItems.PurcheasePrefix ;
settingVM.PurcheasePostfix = settingItems.PurcheasePostfix ;
settingVM.SalesPrefix = settingItems.SalesPrefix ;
settingVM.SalesPostfix = settingItems.SalesPostfix ;
settingVM.Currency = settingItems.Currency ;
settingVM.LogoImage = settingItems.LogoImage ;
settingVM.HeaderImage = settingItems.HeaderImage ;
settingVM.IsArchived = settingItems.IsArchived ;
settingVM.IsDeleted = settingItems.IsDeleted ;
settingVM.AddBy = settingItems.AddBy ;
settingVM.AddDate = settingItems.AddDate ;
settingVM.UpdateBy = settingItems.UpdateBy ;
settingVM.UpdateDate = settingItems.UpdateDate ;
settingVM.Remarks = settingItems.Remarks ;
settingVM.Status = settingItems.Status ;
settingVM.PurcheaseStart = settingItems.PurcheaseStart ;
settingVM.SalesPreStart = settingItems.SalesPreStart ;
settingVM.AccountPrefix = settingItems.AccountPrefix ;
settingVM.AccountPreStart = settingItems.AccountPreStart ;
settingVM.AccountPostfix = settingItems.AccountPostfix ;
settingVM.VAT = settingItems.VAT ;
settingVM.CustomerId = settingItems.CustomerId ;
    return settingVM;
    }
    
    public void Add(Setting setting) {
      setting.AddDate = DateTime.Now;
       setting.AddBy = HttpContext.Current.User.Identity.Name;
      setting.Status = "A";
      unitOfWork.SettingRepository.Add(entity: setting);
     unitOfWork.Save();
    }
    
    public void Update(Setting setting) {
     setting.UpdateDate = DateTime.Now;
      setting.UpdateBy = HttpContext.Current.User.Identity.Name;
      setting.Status = "A";
      unitOfWork.SettingRepository.Update(entity: setting);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Setting setting) {
      setting.UpdateDate = DateTime.Now;
       setting.UpdateBy = HttpContext.Current.User.Identity.Name;
       setting.Status = setting.Status== "A" ? "D" : "A";
      unitOfWork.SettingRepository.Update(entity: setting);
     unitOfWork.Save();
    }
    
    public void Delete(Setting setting) {
      unitOfWork.SettingRepository.Delete(Id: setting.Id);
      unitOfWork.Save();
    }
    
    }
}
