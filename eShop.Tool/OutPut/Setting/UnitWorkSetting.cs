       private Repository<Setting> settingRepository;
       public Repository<Setting> SettingRepository
       {
        get
         {
          if (this.settingRepository == null)
          {
           this.settingRepository = new Repository<Setting>(cRMContext);
          }
       return settingRepository;
       }
      }
