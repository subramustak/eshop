using System;
namespace eShop.BusinessDomain.Master.ViewModels
{
    public class SettingVM
    {
      public int Id { get; set; }
      public string PurcheasePrefix { get; set; }
      public string PurcheasePostfix { get; set; }
      public string SalesPrefix { get; set; }
      public string SalesPostfix { get; set; }
      public string Currency { get; set; }
      public  LogoImage { get; set; }
      public  HeaderImage { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
      public string PurcheaseStart { get; set; }
      public string SalesPreStart { get; set; }
      public string AccountPrefix { get; set; }
      public string AccountPreStart { get; set; }
      public string AccountPostfix { get; set; }
      public decimal VAT { get; set; }
      public int CustomerId { get; set; }
    }
}
