using eshop.ApplicationService.Master;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Master.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Master
 {
  public class SettingController : ApiController
   {
    SettingService settingService;
    public SettingController()
     {
       this.settingService = new SettingService();
     }
    #region Setting
   [Route("Setting/AddSetting/")]
   [HttpPost]
   public IHttpActionResult AddSetting(Setting setting)
   {
    CommonResponse cr = new CommonResponse();
     Setting oSetting = new Setting();
     try
      {
      if (setting.Id == 0)
      {
       settingService.Add(setting);
        cr.message = Message.SAVED;
      }
     else
     {
      settingService.Update(setting);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Setting/GetAllSetting/")]
   [HttpGet]
   public IHttpActionResult GetAllSetting()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = settingService.GetAllSetting();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Setting/GetSetting/")]
   [HttpGet]
   public IHttpActionResult GetSetting(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.settingService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Setting/DeleteSetting/")]
   [HttpGet]
   public IHttpActionResult DeleteSetting(Setting setting)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       settingService.Delete(setting);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Setting/StatusSetting/")]
   [HttpPost]
   public IHttpActionResult StatusSetting(Setting setting)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.settingService.ChangeStatus(setting);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Setting
   }
 }
