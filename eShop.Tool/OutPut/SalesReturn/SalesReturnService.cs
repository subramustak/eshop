using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class SalesReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<SalesReturnVM> GetAllSalesReturn()
   {
    var salesreturnItems = (from rItem in unitOfWork.SalesReturnRepository.GetAll().OrderBy(r => r.Id)
                                  select new SalesReturnVM
                                 {
      Id = rItem.Id,
      InvoiecNo = rItem.InvoiecNo,
      CustomerId = rItem.CustomerId,
      EmployeeId = rItem.EmployeeId,
      BankId = rItem.BankId,
      BranchId = rItem.BranchId,
      PaymentType = rItem.PaymentType,
      DiscountRemarks = rItem.DiscountRemarks,
      Discount = rItem.Discount,
      TotalDiscount = rItem.TotalDiscount,
      Due = rItem.Due,
      Total = rItem.Total,
      Return = rItem.Return,
      NetTotal = rItem.NetTotal,
      Date = rItem.Date,
      ChequeNo = rItem.ChequeNo,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return salesreturnItems;
    }
    
    
    public SalesReturnVM Get(long Id)
    {
     SalesReturnVM salesreturnVM = new SalesReturnVM();
    var salesreturnItems = unitOfWork.SalesReturnRepository.Get(Id);
salesreturnVM.Id = salesreturnItems.Id ;
salesreturnVM.InvoiecNo = salesreturnItems.InvoiecNo ;
salesreturnVM.CustomerId = salesreturnItems.CustomerId ;
salesreturnVM.EmployeeId = salesreturnItems.EmployeeId ;
salesreturnVM.BankId = salesreturnItems.BankId ;
salesreturnVM.BranchId = salesreturnItems.BranchId ;
salesreturnVM.PaymentType = salesreturnItems.PaymentType ;
salesreturnVM.DiscountRemarks = salesreturnItems.DiscountRemarks ;
salesreturnVM.Discount = salesreturnItems.Discount ;
salesreturnVM.TotalDiscount = salesreturnItems.TotalDiscount ;
salesreturnVM.Due = salesreturnItems.Due ;
salesreturnVM.Total = salesreturnItems.Total ;
salesreturnVM.Return = salesreturnItems.Return ;
salesreturnVM.NetTotal = salesreturnItems.NetTotal ;
salesreturnVM.Date = salesreturnItems.Date ;
salesreturnVM.ChequeNo = salesreturnItems.ChequeNo ;
salesreturnVM.IsArchived = salesreturnItems.IsArchived ;
salesreturnVM.IsDeleted = salesreturnItems.IsDeleted ;
salesreturnVM.AddBy = salesreturnItems.AddBy ;
salesreturnVM.AddDate = salesreturnItems.AddDate ;
salesreturnVM.UpdateBy = salesreturnItems.UpdateBy ;
salesreturnVM.UpdateDate = salesreturnItems.UpdateDate ;
salesreturnVM.Remarks = salesreturnItems.Remarks ;
salesreturnVM.Status = salesreturnItems.Status ;
    return salesreturnVM;
    }
    
    public void Add(SalesReturn salesreturn) {
      salesreturn.AddDate = DateTime.Now;
       salesreturn.AddBy = HttpContext.Current.User.Identity.Name;
      salesreturn.Status = "A";
      unitOfWork.SalesReturnRepository.Add(entity: salesreturn);
     unitOfWork.Save();
    }
    
    public void Update(SalesReturn salesreturn) {
     salesreturn.UpdateDate = DateTime.Now;
      salesreturn.UpdateBy = HttpContext.Current.User.Identity.Name;
      salesreturn.Status = "A";
      unitOfWork.SalesReturnRepository.Update(entity: salesreturn);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(SalesReturn salesreturn) {
      salesreturn.UpdateDate = DateTime.Now;
       salesreturn.UpdateBy = HttpContext.Current.User.Identity.Name;
       salesreturn.Status = salesreturn.Status== "A" ? "D" : "A";
      unitOfWork.SalesReturnRepository.Update(entity: salesreturn);
     unitOfWork.Save();
    }
    
    public void Delete(SalesReturn salesreturn) {
      unitOfWork.SalesReturnRepository.Delete(Id: salesreturn.Id);
      unitOfWork.Save();
    }
    
    }
}
