using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class SalesReturnController : ApiController
   {
    SalesReturnService salesreturnService;
    public SalesReturnController()
     {
       this.salesreturnService = new SalesReturnService();
     }
    #region SalesReturn
   [Route("SalesReturn/AddSalesReturn/")]
   [HttpPost]
   public IHttpActionResult AddSalesReturn(SalesReturn salesreturn)
   {
    CommonResponse cr = new CommonResponse();
     SalesReturn oSalesReturn = new SalesReturn();
     try
      {
      if (salesreturn.Id == 0)
      {
       salesreturnService.Add(salesreturn);
        cr.message = Message.SAVED;
      }
     else
     {
      salesreturnService.Update(salesreturn);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("SalesReturn/GetAllSalesReturn/")]
   [HttpGet]
   public IHttpActionResult GetAllSalesReturn()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = salesreturnService.GetAllSalesReturn();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("SalesReturn/GetSalesReturn/")]
   [HttpGet]
   public IHttpActionResult GetSalesReturn(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.salesreturnService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SalesReturn/DeleteSalesReturn/")]
   [HttpGet]
   public IHttpActionResult DeleteSalesReturn(SalesReturn salesreturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       salesreturnService.Delete(salesreturn);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("SalesReturn/StatusSalesReturn/")]
   [HttpPost]
   public IHttpActionResult StatusSalesReturn(SalesReturn salesreturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.salesreturnService.ChangeStatus(salesreturn);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion SalesReturn
   }
 }
