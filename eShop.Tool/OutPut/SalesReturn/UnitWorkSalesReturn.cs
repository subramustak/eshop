       private Repository<SalesReturn> salesreturnRepository;
       public Repository<SalesReturn> SalesReturnRepository
       {
        get
         {
          if (this.salesreturnRepository == null)
          {
           this.salesreturnRepository = new Repository<SalesReturn>(cRMContext);
          }
       return salesreturnRepository;
       }
      }
