       private Repository<SalesDetail> salesdetailRepository;
       public Repository<SalesDetail> SalesDetailRepository
       {
        get
         {
          if (this.salesdetailRepository == null)
          {
           this.salesdetailRepository = new Repository<SalesDetail>(cRMContext);
          }
       return salesdetailRepository;
       }
      }
