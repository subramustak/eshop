using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class SalesDetailVM
    {
      public int Id { get; set; }
      public int SalesId { get; set; }
      public int ProductId { get; set; }
      public decimal Discount { get; set; }
      public decimal UnitePrice { get; set; }
      public string Date { get; set; }
      public decimal Bonus { get; set; }
      public decimal AssaignQuantity { get; set; }
      public decimal Quantity { get; set; }
      public decimal Return { get; set; }
      public decimal Replace { get; set; }
      public decimal WithOurDiscountPrice { get; set; }
      public decimal TotalAmount { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
    }
}
