using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class SalesDetailService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<SalesDetailVM> GetAllSalesDetail()
   {
    var salesdetailItems = (from rItem in unitOfWork.SalesDetailRepository.GetAll().OrderBy(r => r.Id)
                                  select new SalesDetailVM
                                 {
      Id = rItem.Id,
      SalesId = rItem.SalesId,
      ProductId = rItem.ProductId,
      Discount = rItem.Discount,
      UnitePrice = rItem.UnitePrice,
      Date = rItem.Date,
      Bonus = rItem.Bonus,
      AssaignQuantity = rItem.AssaignQuantity,
      Quantity = rItem.Quantity,
      Return = rItem.Return,
      Replace = rItem.Replace,
      WithOurDiscountPrice = rItem.WithOurDiscountPrice,
      TotalAmount = rItem.TotalAmount,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return salesdetailItems;
    }
    
    
    public SalesDetailVM Get(long Id)
    {
     SalesDetailVM salesdetailVM = new SalesDetailVM();
    var salesdetailItems = unitOfWork.SalesDetailRepository.Get(Id);
salesdetailVM.Id = salesdetailItems.Id ;
salesdetailVM.SalesId = salesdetailItems.SalesId ;
salesdetailVM.ProductId = salesdetailItems.ProductId ;
salesdetailVM.Discount = salesdetailItems.Discount ;
salesdetailVM.UnitePrice = salesdetailItems.UnitePrice ;
salesdetailVM.Date = salesdetailItems.Date ;
salesdetailVM.Bonus = salesdetailItems.Bonus ;
salesdetailVM.AssaignQuantity = salesdetailItems.AssaignQuantity ;
salesdetailVM.Quantity = salesdetailItems.Quantity ;
salesdetailVM.Return = salesdetailItems.Return ;
salesdetailVM.Replace = salesdetailItems.Replace ;
salesdetailVM.WithOurDiscountPrice = salesdetailItems.WithOurDiscountPrice ;
salesdetailVM.TotalAmount = salesdetailItems.TotalAmount ;
salesdetailVM.IsArchived = salesdetailItems.IsArchived ;
salesdetailVM.IsDeleted = salesdetailItems.IsDeleted ;
salesdetailVM.AddBy = salesdetailItems.AddBy ;
salesdetailVM.AddDate = salesdetailItems.AddDate ;
salesdetailVM.UpdateBy = salesdetailItems.UpdateBy ;
salesdetailVM.UpdateDate = salesdetailItems.UpdateDate ;
salesdetailVM.Remarks = salesdetailItems.Remarks ;
salesdetailVM.Status = salesdetailItems.Status ;
    return salesdetailVM;
    }
    
    public void Add(SalesDetail salesdetail) {
      salesdetail.AddDate = DateTime.Now;
       salesdetail.AddBy = HttpContext.Current.User.Identity.Name;
      salesdetail.Status = "A";
      unitOfWork.SalesDetailRepository.Add(entity: salesdetail);
     unitOfWork.Save();
    }
    
    public void Update(SalesDetail salesdetail) {
     salesdetail.UpdateDate = DateTime.Now;
      salesdetail.UpdateBy = HttpContext.Current.User.Identity.Name;
      salesdetail.Status = "A";
      unitOfWork.SalesDetailRepository.Update(entity: salesdetail);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(SalesDetail salesdetail) {
      salesdetail.UpdateDate = DateTime.Now;
       salesdetail.UpdateBy = HttpContext.Current.User.Identity.Name;
       salesdetail.Status = salesdetail.Status== "A" ? "D" : "A";
      unitOfWork.SalesDetailRepository.Update(entity: salesdetail);
     unitOfWork.Save();
    }
    
    public void Delete(SalesDetail salesdetail) {
      unitOfWork.SalesDetailRepository.Delete(Id: salesdetail.Id);
      unitOfWork.Save();
    }
    
    }
}
