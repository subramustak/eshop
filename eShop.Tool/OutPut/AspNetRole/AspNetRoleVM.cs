using System;
namespace eShop.BusinessDomain.Admin.ViewModels
{
    public class AspNetRoleVM
    {
      public string Id { get; set; }
      public string Name { get; set; }
    }
}
