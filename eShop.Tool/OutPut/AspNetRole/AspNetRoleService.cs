using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
 
namespace eshop.ApplicationService.Admin
{
 public class AspNetRoleService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<AspNetRoleVM> GetAllAspNetRole()
   {
    var aspnetroleItems = (from rItem in unitOfWork.AspNetRoleRepository.GetAll().OrderBy(r => r.Id)
                                  select new AspNetRoleVM
                                 {
      Id = rItem.Id,
      Name = rItem.Name,
                                    }).ToList();
    return aspnetroleItems;
    }
    
    
    public AspNetRoleVM Get(long Id)
    {
     AspNetRoleVM aspnetroleVM = new AspNetRoleVM();
    var aspnetroleItems = unitOfWork.AspNetRoleRepository.Get(Id);
aspnetroleVM.Id = aspnetroleItems.Id ;
aspnetroleVM.Name = aspnetroleItems.Name ;
    return aspnetroleVM;
    }
    
    public void Add(AspNetRole aspnetrole) {
      aspnetrole.AddDate = DateTime.Now;
       aspnetrole.AddBy = HttpContext.Current.User.Identity.Name;
      aspnetrole.Status = "A";
      unitOfWork.AspNetRoleRepository.Add(entity: aspnetrole);
     unitOfWork.Save();
    }
    
    public void Update(AspNetRole aspnetrole) {
     aspnetrole.UpdateDate = DateTime.Now;
      aspnetrole.UpdateBy = HttpContext.Current.User.Identity.Name;
      aspnetrole.Status = "A";
      unitOfWork.AspNetRoleRepository.Update(entity: aspnetrole);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(AspNetRole aspnetrole) {
      aspnetrole.UpdateDate = DateTime.Now;
       aspnetrole.UpdateBy = HttpContext.Current.User.Identity.Name;
       aspnetrole.Status = aspnetrole.Status== "A" ? "D" : "A";
      unitOfWork.AspNetRoleRepository.Update(entity: aspnetrole);
     unitOfWork.Save();
    }
    
    public void Delete(AspNetRole aspnetrole) {
      unitOfWork.AspNetRoleRepository.Delete(Id: aspnetrole.Id);
      unitOfWork.Save();
    }
    
    }
}
