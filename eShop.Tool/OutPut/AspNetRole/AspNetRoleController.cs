using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Admin
 {
  public class AspNetRoleController : ApiController
   {
    AspNetRoleService aspnetroleService;
    public AspNetRoleController()
     {
       this.aspnetroleService = new AspNetRoleService();
     }
    #region AspNetRole
   [Route("AspNetRole/AddAspNetRole/")]
   [HttpPost]
   public IHttpActionResult AddAspNetRole(AspNetRole aspnetrole)
   {
    CommonResponse cr = new CommonResponse();
     AspNetRole oAspNetRole = new AspNetRole();
     try
      {
      if (aspnetrole.Id == 0)
      {
       aspnetroleService.Add(aspnetrole);
        cr.message = Message.SAVED;
      }
     else
     {
      aspnetroleService.Update(aspnetrole);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AspNetRole/GetAllAspNetRole/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetRole()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetroleService.GetAllAspNetRole();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("AspNetRole/GetAspNetRole/")]
   [HttpGet]
   public IHttpActionResult GetAspNetRole(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetroleService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetRole/DeleteAspNetRole/")]
   [HttpGet]
   public IHttpActionResult DeleteAspNetRole(AspNetRole aspnetrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetroleService.Delete(aspnetrole);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetRole/StatusAspNetRole/")]
   [HttpPost]
   public IHttpActionResult StatusAspNetRole(AspNetRole aspnetrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.aspnetroleService.ChangeStatus(aspnetrole);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AspNetRole
   }
 }
