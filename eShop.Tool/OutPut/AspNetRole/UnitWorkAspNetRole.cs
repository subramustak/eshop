       private Repository<AspNetRole> aspnetroleRepository;
       public Repository<AspNetRole> AspNetRoleRepository
       {
        get
         {
          if (this.aspnetroleRepository == null)
          {
           this.aspnetroleRepository = new Repository<AspNetRole>(cRMContext);
          }
       return aspnetroleRepository;
       }
      }
