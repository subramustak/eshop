       private Repository<Organization> organizationRepository;
       public Repository<Organization> OrganizationRepository
       {
        get
         {
          if (this.organizationRepository == null)
          {
           this.organizationRepository = new Repository<Organization>(cRMContext);
          }
       return organizationRepository;
       }
      }
