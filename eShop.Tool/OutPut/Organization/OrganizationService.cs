using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
 
namespace eshop.ApplicationService.MasterSetUp
{
 public class OrganizationService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<OrganizationVM> GetAllOrganization()
   {
    var organizationItems = (from rItem in unitOfWork.OrganizationRepository.GetAll().OrderBy(r => r.Id)
                                  select new OrganizationVM
                                 {
      Id = rItem.Id,
      Code = rItem.Code,
      Name = rItem.Name,
      CountryId = rItem.CountryId,
      DivisionId = rItem.DivisionId,
      DistrictId = rItem.DistrictId,
      Mobile = rItem.Mobile,
      PermanentAddress = rItem.PermanentAddress,
      PresentAddress = rItem.PresentAddress,
      PABX = rItem.PABX,
      Email = rItem.Email,
      FAX = rItem.FAX,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return organizationItems;
    }
    
    
    public OrganizationVM Get(long Id)
    {
     OrganizationVM organizationVM = new OrganizationVM();
    var organizationItems = unitOfWork.OrganizationRepository.Get(Id);
organizationVM.Id = organizationItems.Id ;
organizationVM.Code = organizationItems.Code ;
organizationVM.Name = organizationItems.Name ;
organizationVM.CountryId = organizationItems.CountryId ;
organizationVM.DivisionId = organizationItems.DivisionId ;
organizationVM.DistrictId = organizationItems.DistrictId ;
organizationVM.Mobile = organizationItems.Mobile ;
organizationVM.PermanentAddress = organizationItems.PermanentAddress ;
organizationVM.PresentAddress = organizationItems.PresentAddress ;
organizationVM.PABX = organizationItems.PABX ;
organizationVM.Email = organizationItems.Email ;
organizationVM.FAX = organizationItems.FAX ;
organizationVM.IsArchived = organizationItems.IsArchived ;
organizationVM.IsDeleted = organizationItems.IsDeleted ;
organizationVM.AddBy = organizationItems.AddBy ;
organizationVM.AddDate = organizationItems.AddDate ;
organizationVM.UpdateBy = organizationItems.UpdateBy ;
organizationVM.UpdateDate = organizationItems.UpdateDate ;
organizationVM.Remarks = organizationItems.Remarks ;
organizationVM.Status = organizationItems.Status ;
    return organizationVM;
    }
    
    public void Add(Organization organization) {
      organization.AddDate = DateTime.Now;
       organization.AddBy = HttpContext.Current.User.Identity.Name;
      organization.Status = "A";
      unitOfWork.OrganizationRepository.Add(entity: organization);
     unitOfWork.Save();
    }
    
    public void Update(Organization organization) {
     organization.UpdateDate = DateTime.Now;
      organization.UpdateBy = HttpContext.Current.User.Identity.Name;
      organization.Status = "A";
      unitOfWork.OrganizationRepository.Update(entity: organization);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Organization organization) {
      organization.UpdateDate = DateTime.Now;
       organization.UpdateBy = HttpContext.Current.User.Identity.Name;
       organization.Status = organization.Status== "A" ? "D" : "A";
      unitOfWork.OrganizationRepository.Update(entity: organization);
     unitOfWork.Save();
    }
    
    public void Delete(Organization organization) {
      unitOfWork.OrganizationRepository.Delete(Id: organization.Id);
      unitOfWork.Save();
    }
    
    }
}
