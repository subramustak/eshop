using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.MasterSetUp
 {
  public class OrganizationController : ApiController
   {
    OrganizationService organizationService;
    public OrganizationController()
     {
       this.organizationService = new OrganizationService();
     }
    #region Organization
   [Route("Organization/AddOrganization/")]
   [HttpPost]
   public IHttpActionResult AddOrganization(Organization organization)
   {
    CommonResponse cr = new CommonResponse();
     Organization oOrganization = new Organization();
     try
      {
      if (organization.Id == 0)
      {
       organizationService.Add(organization);
        cr.message = Message.SAVED;
      }
     else
     {
      organizationService.Update(organization);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Organization/GetAllOrganization/")]
   [HttpGet]
   public IHttpActionResult GetAllOrganization()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = organizationService.GetAllOrganization();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Organization/GetOrganization/")]
   [HttpGet]
   public IHttpActionResult GetOrganization(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.organizationService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Organization/DeleteOrganization/")]
   [HttpGet]
   public IHttpActionResult DeleteOrganization(Organization organization)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       organizationService.Delete(organization);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Organization/StatusOrganization/")]
   [HttpPost]
   public IHttpActionResult StatusOrganization(Organization organization)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.organizationService.ChangeStatus(organization);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Organization
   }
 }
