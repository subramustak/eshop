using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class ProductService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<ProductVM> GetAllProduct()
   {
    var productItems = (from rItem in unitOfWork.ProductRepository.GetAll().OrderBy(r => r.Id)
                                  select new ProductVM
                                 {
      Id = rItem.Id,
      Name = rItem.Name,
      Code = rItem.Code,
      MinimumStock = rItem.MinimumStock,
      OtherCost = rItem.OtherCost,
      Discount = rItem.Discount,
      UnitePrice = rItem.UnitePrice,
      Quantity = rItem.Quantity,
      OpeningQuantity = rItem.OpeningQuantity,
      SalsPrice = rItem.SalsPrice,
      IsPercentage = rItem.IsPercentage,
      IsFixed = rItem.IsFixed,
      ColorId = rItem.ColorId,
      SizeId = rItem.SizeId,
      MaterialId = rItem.MaterialId,
      SubCatagoryId = rItem.SubCatagoryId,
      UOMId = rItem.UOMId,
      ProductBrandId = rItem.ProductBrandId,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return productItems;
    }
    
    
    public ProductVM Get(long Id)
    {
     ProductVM productVM = new ProductVM();
    var productItems = unitOfWork.ProductRepository.Get(Id);
productVM.Id = productItems.Id ;
productVM.Name = productItems.Name ;
productVM.Code = productItems.Code ;
productVM.MinimumStock = productItems.MinimumStock ;
productVM.OtherCost = productItems.OtherCost ;
productVM.Discount = productItems.Discount ;
productVM.UnitePrice = productItems.UnitePrice ;
productVM.Quantity = productItems.Quantity ;
productVM.OpeningQuantity = productItems.OpeningQuantity ;
productVM.SalsPrice = productItems.SalsPrice ;
productVM.IsPercentage = productItems.IsPercentage ;
productVM.IsFixed = productItems.IsFixed ;
productVM.ColorId = productItems.ColorId ;
productVM.SizeId = productItems.SizeId ;
productVM.MaterialId = productItems.MaterialId ;
productVM.SubCatagoryId = productItems.SubCatagoryId ;
productVM.UOMId = productItems.UOMId ;
productVM.ProductBrandId = productItems.ProductBrandId ;
productVM.IsArchived = productItems.IsArchived ;
productVM.IsDeleted = productItems.IsDeleted ;
productVM.AddBy = productItems.AddBy ;
productVM.AddDate = productItems.AddDate ;
productVM.UpdateBy = productItems.UpdateBy ;
productVM.UpdateDate = productItems.UpdateDate ;
productVM.Remarks = productItems.Remarks ;
productVM.Status = productItems.Status ;
    return productVM;
    }
    
    public void Add(Product product) {
      product.AddDate = DateTime.Now;
       product.AddBy = HttpContext.Current.User.Identity.Name;
      product.Status = "A";
      unitOfWork.ProductRepository.Add(entity: product);
     unitOfWork.Save();
    }
    
    public void Update(Product product) {
     product.UpdateDate = DateTime.Now;
      product.UpdateBy = HttpContext.Current.User.Identity.Name;
      product.Status = "A";
      unitOfWork.ProductRepository.Update(entity: product);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Product product) {
      product.UpdateDate = DateTime.Now;
       product.UpdateBy = HttpContext.Current.User.Identity.Name;
       product.Status = product.Status== "A" ? "D" : "A";
      unitOfWork.ProductRepository.Update(entity: product);
     unitOfWork.Save();
    }
    
    public void Delete(Product product) {
      unitOfWork.ProductRepository.Delete(Id: product.Id);
      unitOfWork.Save();
    }
    
    }
}
