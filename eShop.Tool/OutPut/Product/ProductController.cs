using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class ProductController : ApiController
   {
    ProductService productService;
    public ProductController()
     {
       this.productService = new ProductService();
     }
    #region Product
   [Route("Product/AddProduct/")]
   [HttpPost]
   public IHttpActionResult AddProduct(Product product)
   {
    CommonResponse cr = new CommonResponse();
     Product oProduct = new Product();
     try
      {
      if (product.Id == 0)
      {
       productService.Add(product);
        cr.message = Message.SAVED;
      }
     else
     {
      productService.Update(oProduct);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Branch/GetAllProduct/")]
   [HttpGet]
   public IHttpActionResult GetAllProduct()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = productService.GetAllProduct();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Product/GetProduct/")]
   [HttpGet]
   public IHttpActionResult GetProduct(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.productService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Product/DeleteProduct/")]
   [HttpGet]
   public IHttpActionResult DeleteProduct(Product product)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       productService.Delete(product);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Product/StatusProduct/")]
   [HttpPost]
   public IHttpActionResult StatusProduct(Product product)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.productService.ChangeStatus(product);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Product
   }
 }
