using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class ProductVM
    {
      public int Id { get; set; }
      public string Name { get; set; }
      public string Code { get; set; }
      public int MinimumStock { get; set; }
      public decimal OtherCost { get; set; }
      public decimal Discount { get; set; }
      public decimal UnitePrice { get; set; }
      public decimal Quantity { get; set; }
      public decimal OpeningQuantity { get; set; }
      public decimal SalsPrice { get; set; }
      public bool IsPercentage { get; set; }
      public bool IsFixed { get; set; }
      public int ColorId { get; set; }
      public int SizeId { get; set; }
      public int MaterialId { get; set; }
      public int SubCatagoryId { get; set; }
      public int UOMId { get; set; }
      public string ProductBrandId { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
    }
}
