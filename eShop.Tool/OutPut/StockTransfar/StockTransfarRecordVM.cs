using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class StockTransfarVM
    {
      public int Id { get; set; }
      public int FromBranchId { get; set; }
      public int ToBranchId { get; set; }
      public int ProductId { get; set; }
      public int Quantity { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
      public string Date { get; set; }
      public bool IsApprove { get; set; }
    }
}
