using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class StockTransfarController : ApiController
   {
    StockTransfarService stocktransfarService;
    public StockTransfarController()
     {
       this.stocktransfarService = new StockTransfarService();
     }
    #region StockTransfar
   [Route("StockTransfar/AddStockTransfar/")]
   [HttpPost]
   public IHttpActionResult AddStockTransfar(StockTransfar stocktransfar)
   {
    CommonResponse cr = new CommonResponse();
     StockTransfar oStockTransfar = new StockTransfar();
     try
      {
      if (stocktransfar.Id == 0)
      {
       stocktransfarService.Add(stocktransfar);
        cr.message = Message.SAVED;
      }
     else
     {
      stocktransfarService.Update(stocktransfar);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("StockTransfar/GetAllStockTransfar/")]
   [HttpGet]
   public IHttpActionResult GetAllStockTransfar()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = stocktransfarService.GetAllStockTransfar();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("StockTransfar/GetStockTransfar/")]
   [HttpGet]
   public IHttpActionResult GetStockTransfar(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.stocktransfarService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("StockTransfar/DeleteStockTransfar/")]
   [HttpGet]
   public IHttpActionResult DeleteStockTransfar(StockTransfar stocktransfar)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       stocktransfarService.Delete(stocktransfar);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("StockTransfar/StatusStockTransfar/")]
   [HttpPost]
   public IHttpActionResult StatusStockTransfar(StockTransfar stocktransfar)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.stocktransfarService.ChangeStatus(stocktransfar);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion StockTransfar
   }
 }
