using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class StockTransfarService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<StockTransfarRecordVM> GetAllStockTransfar()
   {
    var stocktransfarItems = (from rItem in unitOfWork.StockTransfarRepository.GetAll().OrderBy(r => r.Id)
                                  select new StockTransfarRecordVM
                                 {
      Id = rItem.Id,
      FromBranchId = rItem.FromBranchId,
      ToBranchId = rItem.ToBranchId,
      ProductId = rItem.ProductId,
      Quantity = rItem.Quantity,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
      Date = rItem.Date,
      IsApprove = rItem.IsApprove,
                                    }).ToList();
    return stocktransfarItems;
    }
    
    
    public StockTransfarRecordVM Get(long Id)
    {
     StockTransfarRecordVM StockTransfarRecordVM = new StockTransfarRecordVM();
    var stocktransfarItems = unitOfWork.StockTransfarRepository.Get(Id);
StockTransfarRecordVM.Id = stocktransfarItems.Id ;
StockTransfarRecordVM.FromBranchId = stocktransfarItems.FromBranchId ;
StockTransfarRecordVM.ToBranchId = stocktransfarItems.ToBranchId ;
StockTransfarRecordVM.ProductId = stocktransfarItems.ProductId ;
StockTransfarRecordVM.Quantity = stocktransfarItems.Quantity ;
StockTransfarRecordVM.IsArchived = stocktransfarItems.IsArchived ;
StockTransfarRecordVM.IsDeleted = stocktransfarItems.IsDeleted ;
StockTransfarRecordVM.AddBy = stocktransfarItems.AddBy ;
StockTransfarRecordVM.AddDate = stocktransfarItems.AddDate ;
StockTransfarRecordVM.UpdateBy = stocktransfarItems.UpdateBy ;
StockTransfarRecordVM.UpdateDate = stocktransfarItems.UpdateDate ;
StockTransfarRecordVM.Remarks = stocktransfarItems.Remarks ;
StockTransfarRecordVM.Status = stocktransfarItems.Status ;
StockTransfarRecordVM.Date = stocktransfarItems.Date ;
StockTransfarRecordVM.IsApprove = stocktransfarItems.IsApprove ;
    return StockTransfarRecordVM;
    }
    
    public void Add(StockTransfars stocktransfar) {
      stocktransfar.AddDate = DateTime.Now;
       stocktransfar.AddBy = HttpContext.Current.User.Identity.Name;
      stocktransfar.Status = "A";
      unitOfWork.StockTransfarRepository.Add(entity: stocktransfar);
     unitOfWork.Save();
    }
    
    public void Update(StockTransfars stocktransfar) {
     stocktransfar.UpdateDate = DateTime.Now;
      stocktransfar.UpdateBy = HttpContext.Current.User.Identity.Name;
      stocktransfar.Status = "A";
      unitOfWork.StockTransfarRepository.Update(entity: stocktransfar);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(StockTransfars stocktransfar) {
      stocktransfar.UpdateDate = DateTime.Now;
       stocktransfar.UpdateBy = HttpContext.Current.User.Identity.Name;
       stocktransfar.Status = stocktransfar.Status== "A" ? "D" : "A";
      unitOfWork.StockTransfarRepository.Update(entity: stocktransfar);
     unitOfWork.Save();
    }
    
    public void Delete(StockTransfars stocktransfar) {
      unitOfWork.StockTransfarRepository.Delete(Id: stocktransfar.Id);
      unitOfWork.Save();
    }
    
    }
}
