       private Repository<AspNetUserRole> aspnetuserroleRepository;
       public Repository<AspNetUserRole> AspNetUserRoleRepository
       {
        get
         {
          if (this.aspnetuserroleRepository == null)
          {
           this.aspnetuserroleRepository = new Repository<AspNetUserRole>(cRMContext);
          }
       return aspnetuserroleRepository;
       }
      }
