using System;
namespace eShop.BusinessDomain.Admin.ViewModels
{
    public class AspNetUserRoleVM
    {
      public string UserId { get; set; }
      public string RoleId { get; set; }
    }
}
