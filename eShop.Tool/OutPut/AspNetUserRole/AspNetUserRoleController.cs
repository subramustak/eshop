using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Admin
 {
  public class AspNetUserRoleController : ApiController
   {
    AspNetUserRoleService aspnetuserroleService;
    public AspNetUserRoleController()
     {
       this.aspnetuserroleService = new AspNetUserRoleService();
     }
    #region AspNetUserRole
   [Route("AspNetUserRole/AddAspNetUserRole/")]
   [HttpPost]
   public IHttpActionResult AddAspNetUserRole(AspNetUserRole aspnetuserrole)
   {
    CommonResponse cr = new CommonResponse();
     AspNetUserRole oAspNetUserRole = new AspNetUserRole();
     try
      {
      if (aspnetuserrole.Id == 0)
      {
       aspnetuserroleService.Add(aspnetuserrole);
        cr.message = Message.SAVED;
      }
     else
     {
      aspnetuserroleService.Update(aspnetuserrole);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AspNetUserRole/GetAllAspNetUserRole/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetUserRole()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetuserroleService.GetAllAspNetUserRole();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("AspNetUserRole/GetAspNetUserRole/")]
   [HttpGet]
   public IHttpActionResult GetAspNetUserRole(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetuserroleService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetUserRole/DeleteAspNetUserRole/")]
   [HttpGet]
   public IHttpActionResult DeleteAspNetUserRole(AspNetUserRole aspnetuserrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetuserroleService.Delete(aspnetuserrole);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetUserRole/StatusAspNetUserRole/")]
   [HttpPost]
   public IHttpActionResult StatusAspNetUserRole(AspNetUserRole aspnetuserrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.aspnetuserroleService.ChangeStatus(aspnetuserrole);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AspNetUserRole
   }
 }
