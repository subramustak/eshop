using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
 
namespace eshop.ApplicationService.Admin
{
 public class AspNetUserRoleService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<AspNetUserRoleVM> GetAllAspNetUserRole()
   {
    var aspnetuserroleItems = (from rItem in unitOfWork.AspNetUserRoleRepository.GetAll().OrderBy(r => r.Id)
                                  select new AspNetUserRoleVM
                                 {
      UserId = rItem.UserId,
      RoleId = rItem.RoleId,
                                    }).ToList();
    return aspnetuserroleItems;
    }
    
    
    public AspNetUserRoleVM Get(long Id)
    {
     AspNetUserRoleVM aspnetuserroleVM = new AspNetUserRoleVM();
    var aspnetuserroleItems = unitOfWork.AspNetUserRoleRepository.Get(Id);
aspnetuserroleVM.UserId = aspnetuserroleItems.UserId ;
aspnetuserroleVM.RoleId = aspnetuserroleItems.RoleId ;
    return aspnetuserroleVM;
    }
    
    public void Add(AspNetUserRole aspnetuserrole) {
      aspnetuserrole.AddDate = DateTime.Now;
       aspnetuserrole.AddBy = HttpContext.Current.User.Identity.Name;
      aspnetuserrole.Status = "A";
      unitOfWork.AspNetUserRoleRepository.Add(entity: aspnetuserrole);
     unitOfWork.Save();
    }
    
    public void Update(AspNetUserRole aspnetuserrole) {
     aspnetuserrole.UpdateDate = DateTime.Now;
      aspnetuserrole.UpdateBy = HttpContext.Current.User.Identity.Name;
      aspnetuserrole.Status = "A";
      unitOfWork.AspNetUserRoleRepository.Update(entity: aspnetuserrole);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(AspNetUserRole aspnetuserrole) {
      aspnetuserrole.UpdateDate = DateTime.Now;
       aspnetuserrole.UpdateBy = HttpContext.Current.User.Identity.Name;
       aspnetuserrole.Status = aspnetuserrole.Status== "A" ? "D" : "A";
      unitOfWork.AspNetUserRoleRepository.Update(entity: aspnetuserrole);
     unitOfWork.Save();
    }
    
    public void Delete(AspNetUserRole aspnetuserrole) {
      unitOfWork.AspNetUserRoleRepository.Delete(Id: aspnetuserrole.Id);
      unitOfWork.Save();
    }
    
    }
}
