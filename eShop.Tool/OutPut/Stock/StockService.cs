using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class StockService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<StockVM> GetAllStock()
   {
    var stockItems = (from rItem in unitOfWork.StockRepository.GetAll().OrderBy(r => r.Id)
                                  select new StockVM
                                 {
      Id = rItem.Id,
      ProductId = rItem.ProductId,
      BranchId = rItem.BranchId,
      TotalPaid = rItem.TotalPaid,
      TotalPrice = rItem.TotalPrice,
      GrandTotal = rItem.GrandTotal,
      Date = rItem.Date,
      UnitPrice = rItem.UnitPrice,
      OpeningQuantity = rItem.OpeningQuantity,
      Quantity = rItem.Quantity,
      StockStutes = rItem.StockStutes,
      SalesId = rItem.SalesId,
      SalesReturnId = rItem.SalesReturnId,
      PurcheaseId = rItem.PurcheaseId,
      PurcheaseReturnId = rItem.PurcheaseReturnId,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return stockItems;
    }
    
    
    public StockVM Get(long Id)
    {
     StockVM stockVM = new StockVM();
    var stockItems = unitOfWork.StockRepository.Get(Id);
stockVM.Id = stockItems.Id ;
stockVM.ProductId = stockItems.ProductId ;
stockVM.BranchId = stockItems.BranchId ;
stockVM.TotalPaid = stockItems.TotalPaid ;
stockVM.TotalPrice = stockItems.TotalPrice ;
stockVM.GrandTotal = stockItems.GrandTotal ;
stockVM.Date = stockItems.Date ;
stockVM.UnitPrice = stockItems.UnitPrice ;
stockVM.OpeningQuantity = stockItems.OpeningQuantity ;
stockVM.Quantity = stockItems.Quantity ;
stockVM.StockStutes = stockItems.StockStutes ;
stockVM.SalesId = stockItems.SalesId ;
stockVM.SalesReturnId = stockItems.SalesReturnId ;
stockVM.PurcheaseId = stockItems.PurcheaseId ;
stockVM.PurcheaseReturnId = stockItems.PurcheaseReturnId ;
stockVM.IsArchived = stockItems.IsArchived ;
stockVM.IsDeleted = stockItems.IsDeleted ;
stockVM.AddBy = stockItems.AddBy ;
stockVM.AddDate = stockItems.AddDate ;
stockVM.UpdateBy = stockItems.UpdateBy ;
stockVM.UpdateDate = stockItems.UpdateDate ;
stockVM.Remarks = stockItems.Remarks ;
stockVM.Status = stockItems.Status ;
    return stockVM;
    }
    
    public void Add(Stock stock) {
      stock.AddDate = DateTime.Now;
       stock.AddBy = HttpContext.Current.User.Identity.Name;
      stock.Status = "A";
      unitOfWork.StockRepository.Add(entity: stock);
     unitOfWork.Save();
    }
    
    public void Update(Stock stock) {
     stock.UpdateDate = DateTime.Now;
      stock.UpdateBy = HttpContext.Current.User.Identity.Name;
      stock.Status = "A";
      unitOfWork.StockRepository.Update(entity: stock);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Stock stock) {
      stock.UpdateDate = DateTime.Now;
       stock.UpdateBy = HttpContext.Current.User.Identity.Name;
       stock.Status = stock.Status== "A" ? "D" : "A";
      unitOfWork.StockRepository.Update(entity: stock);
     unitOfWork.Save();
    }
    
    public void Delete(Stock stock) {
      unitOfWork.StockRepository.Delete(Id: stock.Id);
      unitOfWork.Save();
    }
    
    }
}
