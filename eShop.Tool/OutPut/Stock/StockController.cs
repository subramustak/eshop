using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class StockController : ApiController
   {
    StockService stockService;
    public StockController()
     {
       this.stockService = new StockService();
     }
    #region Stock
   [Route("Stock/AddStock/")]
   [HttpPost]
   public IHttpActionResult AddStock(Stock stock)
   {
    CommonResponse cr = new CommonResponse();
     Stock oStock = new Stock();
     try
      {
      if (stock.Id == 0)
      {
       stockService.Add(stock);
        cr.message = Message.SAVED;
      }
     else
     {
      stockService.Update(stock);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Stock/GetAllStock/")]
   [HttpGet]
   public IHttpActionResult GetAllStock()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = stockService.GetAllStock();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Stock/GetStock/")]
   [HttpGet]
   public IHttpActionResult GetStock(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.stockService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Stock/DeleteStock/")]
   [HttpGet]
   public IHttpActionResult DeleteStock(Stock stock)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       stockService.Delete(stock);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Stock/StatusStock/")]
   [HttpPost]
   public IHttpActionResult StatusStock(Stock stock)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.stockService.ChangeStatus(stock);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Stock
   }
 }
