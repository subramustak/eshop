using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class StockVM
    {
      public int Id { get; set; }
      public int ProductId { get; set; }
      public int BranchId { get; set; }
      public decimal TotalPaid { get; set; }
      public decimal TotalPrice { get; set; }
      public decimal GrandTotal { get; set; }
      public string Date { get; set; }
      public decimal UnitPrice { get; set; }
      public decimal OpeningQuantity { get; set; }
      public decimal Quantity { get; set; }
      public bool StockStutes { get; set; }
      public int SalesId { get; set; }
      public int SalesReturnId { get; set; }
      public int PurcheaseId { get; set; }
      public int PurcheaseReturnId { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
    }
}
