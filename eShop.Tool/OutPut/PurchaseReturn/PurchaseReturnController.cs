using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class PurchaseReturnController : ApiController
   {
    PurchaseReturnService purchasereturnService;
    public PurchaseReturnController()
     {
       this.purchasereturnService = new PurchaseReturnService();
     }
    #region PurchaseReturn
   [Route("PurchaseReturn/AddPurchaseReturn/")]
   [HttpPost]
   public IHttpActionResult AddPurchaseReturn(PurchaseReturn purchasereturn)
   {
    CommonResponse cr = new CommonResponse();
     PurchaseReturn oPurchaseReturn = new PurchaseReturn();
     try
      {
      if (purchasereturn.Id == 0)
      {
       purchasereturnService.Add(purchasereturn);
        cr.message = Message.SAVED;
      }
     else
     {
      purchasereturnService.Update(purchasereturn);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("PurchaseReturn/GetAllPurchaseReturn/")]
   [HttpGet]
   public IHttpActionResult GetAllPurchaseReturn()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = purchasereturnService.GetAllPurchaseReturn();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("PurchaseReturn/GetPurchaseReturn/")]
   [HttpGet]
   public IHttpActionResult GetPurchaseReturn(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.purchasereturnService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurchaseReturn/DeletePurchaseReturn/")]
   [HttpGet]
   public IHttpActionResult DeletePurchaseReturn(PurchaseReturn purchasereturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       purchasereturnService.Delete(purchasereturn);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurchaseReturn/StatusPurchaseReturn/")]
   [HttpPost]
   public IHttpActionResult StatusPurchaseReturn(PurchaseReturn purchasereturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.purchasereturnService.ChangeStatus(purchasereturn);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion PurchaseReturn
   }
 }
