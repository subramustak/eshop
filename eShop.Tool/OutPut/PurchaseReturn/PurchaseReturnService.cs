using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class PurchaseReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<PurchaseReturnVM> GetAllPurchaseReturn()
   {
    var purchasereturnItems = (from rItem in unitOfWork.PurchaseReturnRepository.GetAll().OrderBy(r => r.Id)
                                  select new PurchaseReturnVM
                                 {
      Id = rItem.Id,
      InvoiecNo = rItem.InvoiecNo,
      SupplierId = rItem.SupplierId,
      BranchId = rItem.BranchId,
      BankId = rItem.BankId,
      ChequeNo = rItem.ChequeNo,
      PaymentType = rItem.PaymentType,
      EmployeeId = rItem.EmployeeId,
      Date = rItem.Date,
      Due = rItem.Due,
      Total = rItem.Total,
      NetTotal = rItem.NetTotal,
      Discount = rItem.Discount,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return purchasereturnItems;
    }
    
    
    public PurchaseReturnVM Get(long Id)
    {
     PurchaseReturnVM purchasereturnVM = new PurchaseReturnVM();
    var purchasereturnItems = unitOfWork.PurchaseReturnRepository.Get(Id);
purchasereturnVM.Id = purchasereturnItems.Id ;
purchasereturnVM.InvoiecNo = purchasereturnItems.InvoiecNo ;
purchasereturnVM.SupplierId = purchasereturnItems.SupplierId ;
purchasereturnVM.BranchId = purchasereturnItems.BranchId ;
purchasereturnVM.BankId = purchasereturnItems.BankId ;
purchasereturnVM.ChequeNo = purchasereturnItems.ChequeNo ;
purchasereturnVM.PaymentType = purchasereturnItems.PaymentType ;
purchasereturnVM.EmployeeId = purchasereturnItems.EmployeeId ;
purchasereturnVM.Date = purchasereturnItems.Date ;
purchasereturnVM.Due = purchasereturnItems.Due ;
purchasereturnVM.Total = purchasereturnItems.Total ;
purchasereturnVM.NetTotal = purchasereturnItems.NetTotal ;
purchasereturnVM.Discount = purchasereturnItems.Discount ;
purchasereturnVM.IsArchived = purchasereturnItems.IsArchived ;
purchasereturnVM.IsDeleted = purchasereturnItems.IsDeleted ;
purchasereturnVM.AddBy = purchasereturnItems.AddBy ;
purchasereturnVM.AddDate = purchasereturnItems.AddDate ;
purchasereturnVM.UpdateBy = purchasereturnItems.UpdateBy ;
purchasereturnVM.UpdateDate = purchasereturnItems.UpdateDate ;
purchasereturnVM.Remarks = purchasereturnItems.Remarks ;
purchasereturnVM.Status = purchasereturnItems.Status ;
    return purchasereturnVM;
    }
    
    public void Add(PurchaseReturn purchasereturn) {
      purchasereturn.AddDate = DateTime.Now;
       purchasereturn.AddBy = HttpContext.Current.User.Identity.Name;
      purchasereturn.Status = "A";
      unitOfWork.PurchaseReturnRepository.Add(entity: purchasereturn);
     unitOfWork.Save();
    }
    
    public void Update(PurchaseReturn purchasereturn) {
     purchasereturn.UpdateDate = DateTime.Now;
      purchasereturn.UpdateBy = HttpContext.Current.User.Identity.Name;
      purchasereturn.Status = "A";
      unitOfWork.PurchaseReturnRepository.Update(entity: purchasereturn);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(PurchaseReturn purchasereturn) {
      purchasereturn.UpdateDate = DateTime.Now;
       purchasereturn.UpdateBy = HttpContext.Current.User.Identity.Name;
       purchasereturn.Status = purchasereturn.Status== "A" ? "D" : "A";
      unitOfWork.PurchaseReturnRepository.Update(entity: purchasereturn);
     unitOfWork.Save();
    }
    
    public void Delete(PurchaseReturn purchasereturn) {
      unitOfWork.PurchaseReturnRepository.Delete(Id: purchasereturn.Id);
      unitOfWork.Save();
    }
    
    }
}
