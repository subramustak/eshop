       private Repository<PurchaseReturn> purchasereturnRepository;
       public Repository<PurchaseReturn> PurchaseReturnRepository
       {
        get
         {
          if (this.purchasereturnRepository == null)
          {
           this.purchasereturnRepository = new Repository<PurchaseReturn>(cRMContext);
          }
       return purchasereturnRepository;
       }
      }
