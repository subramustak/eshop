using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class PurchaseReturnVM
    {
      public int Id { get; set; }
      public string InvoiecNo { get; set; }
      public int SupplierId { get; set; }
      public int BranchId { get; set; }
      public int BankId { get; set; }
      public string ChequeNo { get; set; }
      public int PaymentType { get; set; }
      public int EmployeeId { get; set; }
      public string Date { get; set; }
      public decimal Due { get; set; }
      public decimal Total { get; set; }
      public decimal NetTotal { get; set; }
      public decimal Discount { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
    }
}
