using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
 
namespace eshop.ApplicationService.MasterSetUp
{
 public class EventService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<EventVM> GetAllEvent()
   {
    var eventItems = (from rItem in unitOfWork.EventRepository.GetAll().OrderBy(r => r.Id)
                                  select new EventVM
                                 {
      Id = rItem.Id,
      EventName = rItem.EventName,
      DsicountAmount = rItem.DsicountAmount,
      DsicountPercent = rItem.DsicountPercent,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return eventItems;
    }
    
    
    public EventVM Get(long Id)
    {
     EventVM eventVM = new EventVM();
    var eventItems = unitOfWork.EventRepository.Get(Id);
eventVM.Id = eventItems.Id ;
eventVM.EventName = eventItems.EventName ;
eventVM.DsicountAmount = eventItems.DsicountAmount ;
eventVM.DsicountPercent = eventItems.DsicountPercent ;
eventVM.IsArchived = eventItems.IsArchived ;
eventVM.IsDeleted = eventItems.IsDeleted ;
eventVM.AddBy = eventItems.AddBy ;
eventVM.AddDate = eventItems.AddDate ;
eventVM.UpdateBy = eventItems.UpdateBy ;
eventVM.UpdateDate = eventItems.UpdateDate ;
eventVM.Remarks = eventItems.Remarks ;
eventVM.Status = eventItems.Status ;
    return eventVM;
    }
    
    public void Add(Event event) {
      event.AddDate = DateTime.Now;
       event.AddBy = HttpContext.Current.User.Identity.Name;
      event.Status = "A";
      unitOfWork.EventRepository.Add(entity: event);
     unitOfWork.Save();
    }
    
    public void Update(Event event) {
     event.UpdateDate = DateTime.Now;
      event.UpdateBy = HttpContext.Current.User.Identity.Name;
      event.Status = "A";
      unitOfWork.EventRepository.Update(entity: event);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Event event) {
      event.UpdateDate = DateTime.Now;
       event.UpdateBy = HttpContext.Current.User.Identity.Name;
       event.Status = event.Status== "A" ? "D" : "A";
      unitOfWork.EventRepository.Update(entity: event);
     unitOfWork.Save();
    }
    
    public void Delete(Event event) {
      unitOfWork.EventRepository.Delete(Id: event.Id);
      unitOfWork.Save();
    }
    
    }
}
