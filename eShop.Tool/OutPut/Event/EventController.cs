using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.MasterSetUp
 {
  public class EventController : ApiController
   {
    EventService eventService;
    public EventController()
     {
       this.eventService = new EventService();
     }
    #region Event
   [Route("Event/AddEvent/")]
   [HttpPost]
   public IHttpActionResult AddEvent(Event event)
   {
    CommonResponse cr = new CommonResponse();
     Event oEvent = new Event();
     try
      {
      if (event.Id == 0)
      {
       eventService.Add(event);
        cr.message = Message.SAVED;
      }
     else
     {
      eventService.Update(event);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Event/GetAllEvent/")]
   [HttpGet]
   public IHttpActionResult GetAllEvent()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = eventService.GetAllEvent();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Event/GetEvent/")]
   [HttpGet]
   public IHttpActionResult GetEvent(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.eventService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Event/DeleteEvent/")]
   [HttpGet]
   public IHttpActionResult DeleteEvent(Event event)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       eventService.Delete(event);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Event/StatusEvent/")]
   [HttpPost]
   public IHttpActionResult StatusEvent(Event event)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.eventService.ChangeStatus(event);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Event
   }
 }
