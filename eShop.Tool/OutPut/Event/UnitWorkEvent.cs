       private Repository<Event> eventRepository;
       public Repository<Event> EventRepository
       {
        get
         {
          if (this.eventRepository == null)
          {
           this.eventRepository = new Repository<Event>(cRMContext);
          }
       return eventRepository;
       }
      }
