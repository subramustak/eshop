using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class PurchaseService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<PurchaseVM> GetAllPurchase()
   {
    var purchaseItems = (from rItem in unitOfWork.PurchaseRepository.GetAll().OrderBy(r => r.Id)
                                  select new PurchaseVM
                                 {
      Id = rItem.Id,
      InvoiecNo = rItem.InvoiecNo,
      LCNo = rItem.LCNo,
      SupplierId = rItem.SupplierId,
      BranchId = rItem.BranchId,
      BankId = rItem.BankId,
      ChequeNo = rItem.ChequeNo,
      PaymentType = rItem.PaymentType,
      EmployeeId = rItem.EmployeeId,
      Date = rItem.Date,
      Due = rItem.Due,
      TotalPayment = rItem.TotalPayment,
      Discount = rItem.Discount,
      CouponName = rItem.CouponName,
      CouponAmunt = rItem.CouponAmunt,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return purchaseItems;
    }
    
    
    public PurchaseVM Get(long Id)
    {
     PurchaseVM purchaseVM = new PurchaseVM();
    var purchaseItems = unitOfWork.PurchaseRepository.Get(Id);
purchaseVM.Id = purchaseItems.Id ;
purchaseVM.InvoiecNo = purchaseItems.InvoiecNo ;
purchaseVM.LCNo = purchaseItems.LCNo ;
purchaseVM.SupplierId = purchaseItems.SupplierId ;
purchaseVM.BranchId = purchaseItems.BranchId ;
purchaseVM.BankId = purchaseItems.BankId ;
purchaseVM.ChequeNo = purchaseItems.ChequeNo ;
purchaseVM.PaymentType = purchaseItems.PaymentType ;
purchaseVM.EmployeeId = purchaseItems.EmployeeId ;
purchaseVM.Date = purchaseItems.Date ;
purchaseVM.Due = purchaseItems.Due ;
purchaseVM.TotalPayment = purchaseItems.TotalPayment ;
purchaseVM.Discount = purchaseItems.Discount ;
purchaseVM.CouponName = purchaseItems.CouponName ;
purchaseVM.CouponAmunt = purchaseItems.CouponAmunt ;
purchaseVM.IsArchived = purchaseItems.IsArchived ;
purchaseVM.IsDeleted = purchaseItems.IsDeleted ;
purchaseVM.AddBy = purchaseItems.AddBy ;
purchaseVM.AddDate = purchaseItems.AddDate ;
purchaseVM.UpdateBy = purchaseItems.UpdateBy ;
purchaseVM.UpdateDate = purchaseItems.UpdateDate ;
purchaseVM.Remarks = purchaseItems.Remarks ;
purchaseVM.Status = purchaseItems.Status ;
    return purchaseVM;
    }
    
    public void Add(Purchase purchase) {
      purchase.AddDate = DateTime.Now;
       purchase.AddBy = HttpContext.Current.User.Identity.Name;
      purchase.Status = "A";
      unitOfWork.PurchaseRepository.Add(entity: purchase);
     unitOfWork.Save();
    }
    
    public void Update(Purchase purchase) {
     purchase.UpdateDate = DateTime.Now;
      purchase.UpdateBy = HttpContext.Current.User.Identity.Name;
      purchase.Status = "A";
      unitOfWork.PurchaseRepository.Update(entity: purchase);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Purchase purchase) {
      purchase.UpdateDate = DateTime.Now;
       purchase.UpdateBy = HttpContext.Current.User.Identity.Name;
       purchase.Status = purchase.Status== "A" ? "D" : "A";
      unitOfWork.PurchaseRepository.Update(entity: purchase);
     unitOfWork.Save();
    }
    
    public void Delete(Purchase purchase) {
      unitOfWork.PurchaseRepository.Delete(Id: purchase.Id);
      unitOfWork.Save();
    }
    
    }
}
