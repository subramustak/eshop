using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class PurchaseController : ApiController
   {
    PurchaseService purchaseService;
    public PurchaseController()
     {
       this.purchaseService = new PurchaseService();
     }
    #region Purchase
   [Route("Purchase/AddPurchase/")]
   [HttpPost]
   public IHttpActionResult AddPurchase(Purchase purchase)
   {
    CommonResponse cr = new CommonResponse();
     Purchase oPurchase = new Purchase();
     try
      {
      if (purchase.Id == 0)
      {
       purchaseService.Add(purchase);
        cr.message = Message.SAVED;
      }
     else
     {
      purchaseService.Update(purchase);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Purchase/GetAllPurchase/")]
   [HttpGet]
   public IHttpActionResult GetAllPurchase()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = purchaseService.GetAllPurchase();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Purchase/GetPurchase/")]
   [HttpGet]
   public IHttpActionResult GetPurchase(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.purchaseService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Purchase/DeletePurchase/")]
   [HttpGet]
   public IHttpActionResult DeletePurchase(Purchase purchase)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       purchaseService.Delete(purchase);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Purchase/StatusPurchase/")]
   [HttpPost]
   public IHttpActionResult StatusPurchase(Purchase purchase)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.purchaseService.ChangeStatus(purchase);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Purchase
   }
 }
