using System;
namespace eShop.BusinessDomain.MasterSetUp.ViewModels
{
    public class EmployeeVM
    {
      public int Id { get; set; }
      public string FullName { get; set; }
      public int DesignationID { get; set; }
      public int TypeID { get; set; }
      public int BranchID { get; set; }
      public int DepartmentID { get; set; }
      public DateTime JoiningDate { get; set; }
      public int StatusID { get; set; }
      public  Image { get; set; }
      public string FatherName { get; set; }
      public string MotherName { get; set; }
      public string Nationality { get; set; }
      public string Religion { get; set; }
      public string BloodGroup { get; set; }
      public string NationalID { get; set; }
      public DateTime DateOfBirth { get; set; }
      public string Contact { get; set; }
      public string PresentAddress { get; set; }
      public string PermanentAddress { get; set; }
      public string Gender { get; set; }
      public string MaritalStatus { get; set; }
      public string EmergencyContactAddress { get; set; }
      public string EmergencyTandTNo { get; set; }
      public string MobileNo { get; set; }
      public string Email { get; set; }
      public string DeviceUserID { get; set; }
      public string CardNo { get; set; }
      public bool IsPermanent { get; set; }
      public DateTime ProbationPeriodEndDate { get; set; }
      public DateTime ConfirmationDate { get; set; }
      public string ShortName { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
    }
}
