       private Repository<Employee> employeeRepository;
       public Repository<Employee> EmployeeRepository
       {
        get
         {
          if (this.employeeRepository == null)
          {
           this.employeeRepository = new Repository<Employee>(cRMContext);
          }
       return employeeRepository;
       }
      }
