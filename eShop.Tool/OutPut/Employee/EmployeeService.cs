using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using eShop.BusinessDomain.MasterSetUp.DomainObject;
 
namespace eshop.ApplicationService.MasterSetUp
{
 public class EmployeeService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<EmployeeVM> GetAllEmployee()
   {
    var employeeItems = (from rItem in unitOfWork.EmployeeRepository.GetAll().OrderBy(r => r.Id)
                                  select new EmployeeVM
                                 {
      Id = rItem.Id,
      FullName = rItem.FullName,
      DesignationID = rItem.DesignationID,
      TypeID = rItem.TypeID,
      BranchID = rItem.BranchID,
      DepartmentID = rItem.DepartmentID,
      JoiningDate = rItem.JoiningDate,
      StatusID = rItem.StatusID,
      Image = rItem.Image,
      FatherName = rItem.FatherName,
      MotherName = rItem.MotherName,
      Nationality = rItem.Nationality,
      Religion = rItem.Religion,
      BloodGroup = rItem.BloodGroup,
      NationalID = rItem.NationalID,
      DateOfBirth = rItem.DateOfBirth,
      Contact = rItem.Contact,
      PresentAddress = rItem.PresentAddress,
      PermanentAddress = rItem.PermanentAddress,
      Gender = rItem.Gender,
      MaritalStatus = rItem.MaritalStatus,
      EmergencyContactAddress = rItem.EmergencyContactAddress,
      EmergencyTandTNo = rItem.EmergencyTandTNo,
      MobileNo = rItem.MobileNo,
      Email = rItem.Email,
      DeviceUserID = rItem.DeviceUserID,
      CardNo = rItem.CardNo,
      IsPermanent = rItem.IsPermanent,
      ProbationPeriodEndDate = rItem.ProbationPeriodEndDate,
      ConfirmationDate = rItem.ConfirmationDate,
      ShortName = rItem.ShortName,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return employeeItems;
    }
    
    
    public EmployeeVM Get(long Id)
    {
     EmployeeVM employeeVM = new EmployeeVM();
    var employeeItems = unitOfWork.EmployeeRepository.Get(Id);
employeeVM.Id = employeeItems.Id ;
employeeVM.FullName = employeeItems.FullName ;
employeeVM.DesignationID = employeeItems.DesignationID ;
employeeVM.TypeID = employeeItems.TypeID ;
employeeVM.BranchID = employeeItems.BranchID ;
employeeVM.DepartmentID = employeeItems.DepartmentID ;
employeeVM.JoiningDate = employeeItems.JoiningDate ;
employeeVM.StatusID = employeeItems.StatusID ;
employeeVM.Image = employeeItems.Image ;
employeeVM.FatherName = employeeItems.FatherName ;
employeeVM.MotherName = employeeItems.MotherName ;
employeeVM.Nationality = employeeItems.Nationality ;
employeeVM.Religion = employeeItems.Religion ;
employeeVM.BloodGroup = employeeItems.BloodGroup ;
employeeVM.NationalID = employeeItems.NationalID ;
employeeVM.DateOfBirth = employeeItems.DateOfBirth ;
employeeVM.Contact = employeeItems.Contact ;
employeeVM.PresentAddress = employeeItems.PresentAddress ;
employeeVM.PermanentAddress = employeeItems.PermanentAddress ;
employeeVM.Gender = employeeItems.Gender ;
employeeVM.MaritalStatus = employeeItems.MaritalStatus ;
employeeVM.EmergencyContactAddress = employeeItems.EmergencyContactAddress ;
employeeVM.EmergencyTandTNo = employeeItems.EmergencyTandTNo ;
employeeVM.MobileNo = employeeItems.MobileNo ;
employeeVM.Email = employeeItems.Email ;
employeeVM.DeviceUserID = employeeItems.DeviceUserID ;
employeeVM.CardNo = employeeItems.CardNo ;
employeeVM.IsPermanent = employeeItems.IsPermanent ;
employeeVM.ProbationPeriodEndDate = employeeItems.ProbationPeriodEndDate ;
employeeVM.ConfirmationDate = employeeItems.ConfirmationDate ;
employeeVM.ShortName = employeeItems.ShortName ;
employeeVM.IsArchived = employeeItems.IsArchived ;
employeeVM.IsDeleted = employeeItems.IsDeleted ;
employeeVM.AddBy = employeeItems.AddBy ;
employeeVM.AddDate = employeeItems.AddDate ;
employeeVM.UpdateBy = employeeItems.UpdateBy ;
employeeVM.UpdateDate = employeeItems.UpdateDate ;
employeeVM.Remarks = employeeItems.Remarks ;
employeeVM.Status = employeeItems.Status ;
    return employeeVM;
    }
    
    public void Add(Employee employee) {
      employee.AddDate = DateTime.Now;
       employee.AddBy = HttpContext.Current.User.Identity.Name;
      employee.Status = "A";
      unitOfWork.EmployeeRepository.Add(entity: employee);
     unitOfWork.Save();
    }
    
    public void Update(Employee employee) {
     employee.UpdateDate = DateTime.Now;
      employee.UpdateBy = HttpContext.Current.User.Identity.Name;
      employee.Status = "A";
      unitOfWork.EmployeeRepository.Update(entity: employee);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Employee employee) {
      employee.UpdateDate = DateTime.Now;
       employee.UpdateBy = HttpContext.Current.User.Identity.Name;
       employee.Status = employee.Status== "A" ? "D" : "A";
      unitOfWork.EmployeeRepository.Update(entity: employee);
     unitOfWork.Save();
    }
    
    public void Delete(Employee employee) {
      unitOfWork.EmployeeRepository.Delete(Id: employee.Id);
      unitOfWork.Save();
    }
    
    }
}
