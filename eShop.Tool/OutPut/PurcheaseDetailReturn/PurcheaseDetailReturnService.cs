using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class PurcheaseDetailReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<PurcheaseDetailReturnVM> GetAllPurcheaseDetailReturn()
   {
    var purcheasedetailreturnItems = (from rItem in unitOfWork.PurcheaseDetailReturnRepository.GetAll().OrderBy(r => r.Id)
                                  select new PurcheaseDetailReturnVM
                                 {
      Id = rItem.Id,
      PurchaseId = rItem.PurchaseId,
      ProductId = rItem.ProductId,
      UnitePrice = rItem.UnitePrice,
      Date = rItem.Date,
      Quantity = rItem.Quantity,
      Discount = rItem.Discount,
      TotalAmount = rItem.TotalAmount,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
      purchaseReturns_Id = rItem.purchaseReturns_Id,
                                    }).ToList();
    return purcheasedetailreturnItems;
    }
    
    
    public PurcheaseDetailReturnVM Get(long Id)
    {
     PurcheaseDetailReturnVM purcheasedetailreturnVM = new PurcheaseDetailReturnVM();
    var purcheasedetailreturnItems = unitOfWork.PurcheaseDetailReturnRepository.Get(Id);
purcheasedetailreturnVM.Id = purcheasedetailreturnItems.Id ;
purcheasedetailreturnVM.PurchaseId = purcheasedetailreturnItems.PurchaseId ;
purcheasedetailreturnVM.ProductId = purcheasedetailreturnItems.ProductId ;
purcheasedetailreturnVM.UnitePrice = purcheasedetailreturnItems.UnitePrice ;
purcheasedetailreturnVM.Date = purcheasedetailreturnItems.Date ;
purcheasedetailreturnVM.Quantity = purcheasedetailreturnItems.Quantity ;
purcheasedetailreturnVM.Discount = purcheasedetailreturnItems.Discount ;
purcheasedetailreturnVM.TotalAmount = purcheasedetailreturnItems.TotalAmount ;
purcheasedetailreturnVM.IsArchived = purcheasedetailreturnItems.IsArchived ;
purcheasedetailreturnVM.IsDeleted = purcheasedetailreturnItems.IsDeleted ;
purcheasedetailreturnVM.AddBy = purcheasedetailreturnItems.AddBy ;
purcheasedetailreturnVM.AddDate = purcheasedetailreturnItems.AddDate ;
purcheasedetailreturnVM.UpdateBy = purcheasedetailreturnItems.UpdateBy ;
purcheasedetailreturnVM.UpdateDate = purcheasedetailreturnItems.UpdateDate ;
purcheasedetailreturnVM.Remarks = purcheasedetailreturnItems.Remarks ;
purcheasedetailreturnVM.Status = purcheasedetailreturnItems.Status ;
purcheasedetailreturnVM.purchaseReturns_Id = purcheasedetailreturnItems.purchaseReturns_Id ;
    return purcheasedetailreturnVM;
    }
    
    public void Add(PurcheaseDetailReturn purcheasedetailreturn) {
      purcheasedetailreturn.AddDate = DateTime.Now;
       purcheasedetailreturn.AddBy = HttpContext.Current.User.Identity.Name;
      purcheasedetailreturn.Status = "A";
      unitOfWork.PurcheaseDetailReturnRepository.Add(entity: purcheasedetailreturn);
     unitOfWork.Save();
    }
    
    public void Update(PurcheaseDetailReturn purcheasedetailreturn) {
     purcheasedetailreturn.UpdateDate = DateTime.Now;
      purcheasedetailreturn.UpdateBy = HttpContext.Current.User.Identity.Name;
      purcheasedetailreturn.Status = "A";
      unitOfWork.PurcheaseDetailReturnRepository.Update(entity: purcheasedetailreturn);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(PurcheaseDetailReturn purcheasedetailreturn) {
      purcheasedetailreturn.UpdateDate = DateTime.Now;
       purcheasedetailreturn.UpdateBy = HttpContext.Current.User.Identity.Name;
       purcheasedetailreturn.Status = purcheasedetailreturn.Status== "A" ? "D" : "A";
      unitOfWork.PurcheaseDetailReturnRepository.Update(entity: purcheasedetailreturn);
     unitOfWork.Save();
    }
    
    public void Delete(PurcheaseDetailReturn purcheasedetailreturn) {
      unitOfWork.PurcheaseDetailReturnRepository.Delete(Id: purcheasedetailreturn.Id);
      unitOfWork.Save();
    }
    
    }
}
