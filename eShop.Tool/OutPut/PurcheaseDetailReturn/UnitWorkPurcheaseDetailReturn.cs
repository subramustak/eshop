       private Repository<PurcheaseDetailReturn> purcheasedetailreturnRepository;
       public Repository<PurcheaseDetailReturn> PurcheaseDetailReturnRepository
       {
        get
         {
          if (this.purcheasedetailreturnRepository == null)
          {
           this.purcheasedetailreturnRepository = new Repository<PurcheaseDetailReturn>(cRMContext);
          }
       return purcheasedetailreturnRepository;
       }
      }
