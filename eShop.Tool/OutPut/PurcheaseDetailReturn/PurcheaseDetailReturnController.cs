using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class PurcheaseDetailReturnController : ApiController
   {
    PurcheaseDetailReturnService purcheasedetailreturnService;
    public PurcheaseDetailReturnController()
     {
       this.purcheasedetailreturnService = new PurcheaseDetailReturnService();
     }
    #region PurcheaseDetailReturn
   [Route("PurcheaseDetailReturn/AddPurcheaseDetailReturn/")]
   [HttpPost]
   public IHttpActionResult AddPurcheaseDetailReturn(PurcheaseDetailReturn purcheasedetailreturn)
   {
    CommonResponse cr = new CommonResponse();
     PurcheaseDetailReturn oPurcheaseDetailReturn = new PurcheaseDetailReturn();
     try
      {
      if (purcheasedetailreturn.Id == 0)
      {
       purcheasedetailreturnService.Add(purcheasedetailreturn);
        cr.message = Message.SAVED;
      }
     else
     {
      purcheasedetailreturnService.Update(purcheasedetailreturn);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("PurcheaseDetailReturn/GetAllPurcheaseDetailReturn/")]
   [HttpGet]
   public IHttpActionResult GetAllPurcheaseDetailReturn()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = purcheasedetailreturnService.GetAllPurcheaseDetailReturn();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("PurcheaseDetailReturn/GetPurcheaseDetailReturn/")]
   [HttpGet]
   public IHttpActionResult GetPurcheaseDetailReturn(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.purcheasedetailreturnService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurcheaseDetailReturn/DeletePurcheaseDetailReturn/")]
   [HttpGet]
   public IHttpActionResult DeletePurcheaseDetailReturn(PurcheaseDetailReturn purcheasedetailreturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       purcheasedetailreturnService.Delete(purcheasedetailreturn);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("PurcheaseDetailReturn/StatusPurcheaseDetailReturn/")]
   [HttpPost]
   public IHttpActionResult StatusPurcheaseDetailReturn(PurcheaseDetailReturn purcheasedetailreturn)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.purcheasedetailreturnService.ChangeStatus(purcheasedetailreturn);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion PurcheaseDetailReturn
   }
 }
