using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class SaleService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<SaleVM> GetAllSale()
   {
    var saleItems = (from rItem in unitOfWork.SaleRepository.GetAll().OrderBy(r => r.Id)
                                  select new SaleVM
                                 {
      Id = rItem.Id,
      InvoiecNo = rItem.InvoiecNo,
      CustomerId = rItem.CustomerId,
      EmployeeId = rItem.EmployeeId,
      BankId = rItem.BankId,
      BranchId = rItem.BranchId,
      PaymentType = rItem.PaymentType,
      DiscountRemarks = rItem.DiscountRemarks,
      Discount = rItem.Discount,
      TotalDiscount = rItem.TotalDiscount,
      Due = rItem.Due,
      TotalPaid = rItem.TotalPaid,
      GrandTotal = rItem.GrandTotal,
      Date = rItem.Date,
      ChequeNo = rItem.ChequeNo,
      PackUnitPrice = rItem.PackUnitPrice,
      PackQuantity = rItem.PackQuantity,
      EventName = rItem.EventName,
      EventAamount = rItem.EventAamount,
      IsArchived = rItem.IsArchived,
      IsDeleted = rItem.IsDeleted,
      AddBy = rItem.AddBy,
      AddDate = rItem.AddDate,
      UpdateBy = rItem.UpdateBy,
      UpdateDate = rItem.UpdateDate,
      Remarks = rItem.Remarks,
      Status = rItem.Status,
                                    }).ToList();
    return saleItems;
    }
    
    
    public SaleVM Get(long Id)
    {
     SaleVM saleVM = new SaleVM();
    var saleItems = unitOfWork.SaleRepository.Get(Id);
saleVM.Id = saleItems.Id ;
saleVM.InvoiecNo = saleItems.InvoiecNo ;
saleVM.CustomerId = saleItems.CustomerId ;
saleVM.EmployeeId = saleItems.EmployeeId ;
saleVM.BankId = saleItems.BankId ;
saleVM.BranchId = saleItems.BranchId ;
saleVM.PaymentType = saleItems.PaymentType ;
saleVM.DiscountRemarks = saleItems.DiscountRemarks ;
saleVM.Discount = saleItems.Discount ;
saleVM.TotalDiscount = saleItems.TotalDiscount ;
saleVM.Due = saleItems.Due ;
saleVM.TotalPaid = saleItems.TotalPaid ;
saleVM.GrandTotal = saleItems.GrandTotal ;
saleVM.Date = saleItems.Date ;
saleVM.ChequeNo = saleItems.ChequeNo ;
saleVM.PackUnitPrice = saleItems.PackUnitPrice ;
saleVM.PackQuantity = saleItems.PackQuantity ;
saleVM.EventName = saleItems.EventName ;
saleVM.EventAamount = saleItems.EventAamount ;
saleVM.IsArchived = saleItems.IsArchived ;
saleVM.IsDeleted = saleItems.IsDeleted ;
saleVM.AddBy = saleItems.AddBy ;
saleVM.AddDate = saleItems.AddDate ;
saleVM.UpdateBy = saleItems.UpdateBy ;
saleVM.UpdateDate = saleItems.UpdateDate ;
saleVM.Remarks = saleItems.Remarks ;
saleVM.Status = saleItems.Status ;
    return saleVM;
    }
    
    public void Add(Sale sale) {
      sale.AddDate = DateTime.Now;
       sale.AddBy = HttpContext.Current.User.Identity.Name;
      sale.Status = "A";
      unitOfWork.SaleRepository.Add(entity: sale);
     unitOfWork.Save();
    }
    
    public void Update(Sale sale) {
     sale.UpdateDate = DateTime.Now;
      sale.UpdateBy = HttpContext.Current.User.Identity.Name;
      sale.Status = "A";
      unitOfWork.SaleRepository.Update(entity: sale);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(Sale sale) {
      sale.UpdateDate = DateTime.Now;
       sale.UpdateBy = HttpContext.Current.User.Identity.Name;
       sale.Status = sale.Status== "A" ? "D" : "A";
      unitOfWork.SaleRepository.Update(entity: sale);
     unitOfWork.Save();
    }
    
    public void Delete(Sale sale) {
      unitOfWork.SaleRepository.Delete(Id: sale.Id);
      unitOfWork.Save();
    }
    
    }
}
