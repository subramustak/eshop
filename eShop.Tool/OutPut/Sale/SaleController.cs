using eshop.ApplicationService.CRM;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.CRM
 {
  public class SaleController : ApiController
   {
    SaleService saleService;
    public SaleController()
     {
       this.saleService = new SaleService();
     }
    #region Sale
   [Route("Sale/AddSale/")]
   [HttpPost]
   public IHttpActionResult AddSale(Sale sale)
   {
    CommonResponse cr = new CommonResponse();
     Sale oSale = new Sale();
     try
      {
      if (sale.Id == 0)
      {
       saleService.Add(sale);
        cr.message = Message.SAVED;
      }
     else
     {
      saleService.Update(sale);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("Sale/GetAllSale/")]
   [HttpGet]
   public IHttpActionResult GetAllSale()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = saleService.GetAllSale();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("Sale/GetSale/")]
   [HttpGet]
   public IHttpActionResult GetSale(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.saleService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Sale/DeleteSale/")]
   [HttpGet]
   public IHttpActionResult DeleteSale(Sale sale)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       saleService.Delete(sale);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("Sale/StatusSale/")]
   [HttpPost]
   public IHttpActionResult StatusSale(Sale sale)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.saleService.ChangeStatus(sale);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion Sale
   }
 }
