using System;
namespace eShop.BusinessDomain.CRM.ViewModels
{
    public class SaleVM
    {
      public int Id { get; set; }
      public string InvoiecNo { get; set; }
      public int CustomerId { get; set; }
      public int EmployeeId { get; set; }
      public int BankId { get; set; }
      public int BranchId { get; set; }
      public int PaymentType { get; set; }
      public string DiscountRemarks { get; set; }
      public decimal Discount { get; set; }
      public decimal TotalDiscount { get; set; }
      public decimal Due { get; set; }
      public decimal TotalPaid { get; set; }
      public decimal GrandTotal { get; set; }
      public string Date { get; set; }
      public string ChequeNo { get; set; }
      public decimal PackUnitPrice { get; set; }
      public decimal PackQuantity { get; set; }
      public string EventName { get; set; }
      public decimal EventAamount { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
    }
}
