using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Admin
 {
  public class AspNetPageController : ApiController
   {
    AspNetPageService aspnetpageService;
    public AspNetPageController()
     {
       this.aspnetpageService = new AspNetPageService();
     }
    #region AspNetPage
   [Route("AspNetPage/AddAspNetPage/")]
   [HttpPost]
   public IHttpActionResult AddAspNetPage(AspNetPage aspnetpage)
   {
    CommonResponse cr = new CommonResponse();
     AspNetPage oAspNetPage = new AspNetPage();
     try
      {
      if (aspnetpage.Id == 0)
      {
       aspnetpageService.Add(aspnetpage);
        cr.message = Message.SAVED;
      }
     else
     {
      aspnetpageService.Update(aspnetpage);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AspNetPage/GetAllAspNetPage/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetPage()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetpageService.GetAllAspNetPage();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("AspNetPage/GetAspNetPage/")]
   [HttpGet]
   public IHttpActionResult GetAspNetPage(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetpageService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetPage/DeleteAspNetPage/")]
   [HttpGet]
   public IHttpActionResult DeleteAspNetPage(AspNetPage aspnetpage)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetpageService.Delete(aspnetpage);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetPage/StatusAspNetPage/")]
   [HttpPost]
   public IHttpActionResult StatusAspNetPage(AspNetPage aspnetpage)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.aspnetpageService.ChangeStatus(aspnetpage);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AspNetPage
   }
 }
