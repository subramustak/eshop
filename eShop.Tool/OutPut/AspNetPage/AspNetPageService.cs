using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
 
namespace eshop.ApplicationService.Admin
{
 public class AspNetPageService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<AspNetPageVM> GetAllAspNetPage()
   {
    var aspnetpageItems = (from rItem in unitOfWork.AspNetPageRepository.GetAll().OrderBy(r => r.Id)
                                  select new AspNetPageVM
                                 {
      PageID = rItem.PageID,
      ParentID = rItem.ParentID,
      NameOption_En = rItem.NameOption_En,
      NameOption_Bn = rItem.NameOption_Bn,
      Controller = rItem.Controller,
      Action = rItem.Action,
      Area = rItem.Area,
      IconClass = rItem.IconClass,
      ActiveLi = rItem.ActiveLi,
      Status = rItem.Status,
      Displayorder = rItem.Displayorder,
      IsParent = rItem.IsParent,
                                    }).ToList();
    return aspnetpageItems;
    }
    
    
    public AspNetPageVM Get(long Id)
    {
     AspNetPageVM aspnetpageVM = new AspNetPageVM();
    var aspnetpageItems = unitOfWork.AspNetPageRepository.Get(Id);
aspnetpageVM.PageID = aspnetpageItems.PageID ;
aspnetpageVM.ParentID = aspnetpageItems.ParentID ;
aspnetpageVM.NameOption_En = aspnetpageItems.NameOption_En ;
aspnetpageVM.NameOption_Bn = aspnetpageItems.NameOption_Bn ;
aspnetpageVM.Controller = aspnetpageItems.Controller ;
aspnetpageVM.Action = aspnetpageItems.Action ;
aspnetpageVM.Area = aspnetpageItems.Area ;
aspnetpageVM.IconClass = aspnetpageItems.IconClass ;
aspnetpageVM.ActiveLi = aspnetpageItems.ActiveLi ;
aspnetpageVM.Status = aspnetpageItems.Status ;
aspnetpageVM.Displayorder = aspnetpageItems.Displayorder ;
aspnetpageVM.IsParent = aspnetpageItems.IsParent ;
    return aspnetpageVM;
    }
    
    public void Add(AspNetPage aspnetpage) {
      aspnetpage.AddDate = DateTime.Now;
       aspnetpage.AddBy = HttpContext.Current.User.Identity.Name;
      aspnetpage.Status = "A";
      unitOfWork.AspNetPageRepository.Add(entity: aspnetpage);
     unitOfWork.Save();
    }
    
    public void Update(AspNetPage aspnetpage) {
     aspnetpage.UpdateDate = DateTime.Now;
      aspnetpage.UpdateBy = HttpContext.Current.User.Identity.Name;
      aspnetpage.Status = "A";
      unitOfWork.AspNetPageRepository.Update(entity: aspnetpage);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(AspNetPage aspnetpage) {
      aspnetpage.UpdateDate = DateTime.Now;
       aspnetpage.UpdateBy = HttpContext.Current.User.Identity.Name;
       aspnetpage.Status = aspnetpage.Status== "A" ? "D" : "A";
      unitOfWork.AspNetPageRepository.Update(entity: aspnetpage);
     unitOfWork.Save();
    }
    
    public void Delete(AspNetPage aspnetpage) {
      unitOfWork.AspNetPageRepository.Delete(Id: aspnetpage.Id);
      unitOfWork.Save();
    }
    
    }
}
