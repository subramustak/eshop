using System;
namespace eShop.BusinessDomain.Admin.ViewModels
{
    public class AspNetPageVM
    {
      public int PageID { get; set; }
      public int ParentID { get; set; }
      public string NameOption_En { get; set; }
      public string NameOption_Bn { get; set; }
      public string Controller { get; set; }
      public string Action { get; set; }
      public string Area { get; set; }
      public string IconClass { get; set; }
      public string ActiveLi { get; set; }
      public string Status { get; set; }
      public int Displayorder { get; set; }
      public bool IsParent { get; set; }
    }
}
