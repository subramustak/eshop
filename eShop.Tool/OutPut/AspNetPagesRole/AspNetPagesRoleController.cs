using eshop.ApplicationService.Admin;
using eShop.BusinessDomain.Core;
using eShop.BusinessDomain.Admin.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System;
using System.Net;
using System.Web.Http;
 
namespace eshop.ApplicationService.Admin
 {
  public class AspNetPagesRoleController : ApiController
   {
    AspNetPagesRoleService aspnetpagesroleService;
    public AspNetPagesRoleController()
     {
       this.aspnetpagesroleService = new AspNetPagesRoleService();
     }
    #region AspNetPagesRole
   [Route("AspNetPagesRole/AddAspNetPagesRole/")]
   [HttpPost]
   public IHttpActionResult AddAspNetPagesRole(AspNetPagesRole aspnetpagesrole)
   {
    CommonResponse cr = new CommonResponse();
     AspNetPagesRole oAspNetPagesRole = new AspNetPagesRole();
     try
      {
      if (aspnetpagesrole.Id == 0)
      {
       aspnetpagesroleService.Add(aspnetpagesrole);
        cr.message = Message.SAVED;
      }
     else
     {
      aspnetpagesroleService.Update(aspnetpagesrole);
     cr.message =  Message.UPDATED; 
     }
    }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
    }
     return Json(cr);
   }
   [Route("AspNetPagesRole/GetAllAspNetPagesRole/")]
   [HttpGet]
   public IHttpActionResult GetAllAspNetPagesRole()
   {
    CommonResponse cr = new CommonResponse();
     try
      {
       cr.results = aspnetpagesroleService.GetAllAspNetPagesRole();
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
       }
     catch (Exception ex)
      {
       return BadRequest(ex.Message);
      }
      return Json(cr);
    }
   [Route("AspNetPagesRole/GetAspNetPagesRole/")]
   [HttpGet]
   public IHttpActionResult GetAspNetPagesRole(int Id)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
      cr.results = this.aspnetpagesroleService.Get(Id);
       cr.message = cr.results != null ? "Data Found" : Message.NOTFOUND;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetPagesRole/DeleteAspNetPagesRole/")]
   [HttpGet]
   public IHttpActionResult DeleteAspNetPagesRole(AspNetPagesRole aspnetpagesrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       aspnetpagesroleService.Delete(aspnetpagesrole);
        cr.message = Message.DELETED;
     }
    catch (Exception ex)
    {
     return BadRequest(ex.Message);
    }
    return Json(cr);
  }
   [Route("AspNetPagesRole/StatusAspNetPagesRole/")]
   [HttpPost]
   public IHttpActionResult StatusAspNetPagesRole(AspNetPagesRole aspnetpagesrole)
   {
   CommonResponse cr = new CommonResponse();
   try
    {
       this.aspnetpagesroleService.ChangeStatus(aspnetpagesrole);
        cr.message = Message.UPDATED;
     }
    catch (Exception ex)
    {
      return BadRequest(ex.Message);
     }
    return Json(cr);
   }
    #endregion AspNetPagesRole
   }
 }
