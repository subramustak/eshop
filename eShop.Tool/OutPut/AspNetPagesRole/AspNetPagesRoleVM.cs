using System;
namespace eShop.BusinessDomain.Admin.ViewModels
{
    public class AspNetPagesRoleVM
    {
      public int PageRoleId { get; set; }
      public int PageId { get; set; }
      public string RoleId { get; set; }
      public bool CanAdd { get; set; }
      public bool CanEdit { get; set; }
      public bool CanView { get; set; }
      public bool CanDelete { get; set; }
      public bool CanApprove { get; set; }
    }
}
