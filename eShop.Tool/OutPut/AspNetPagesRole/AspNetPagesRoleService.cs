using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Admin.ViewModels;
using eShop.BusinessDomain.Admin.DomainObject;
 
namespace eshop.ApplicationService.Admin
{
 public class AspNetPagesRoleService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<AspNetPagesRoleVM> GetAllAspNetPagesRole()
   {
    var aspnetpagesroleItems = (from rItem in unitOfWork.AspNetPagesRoleRepository.GetAll().OrderBy(r => r.Id)
                                  select new AspNetPagesRoleVM
                                 {
      PageRoleId = rItem.PageRoleId,
      PageId = rItem.PageId,
      RoleId = rItem.RoleId,
      CanAdd = rItem.CanAdd,
      CanEdit = rItem.CanEdit,
      CanView = rItem.CanView,
      CanDelete = rItem.CanDelete,
      CanApprove = rItem.CanApprove,
                                    }).ToList();
    return aspnetpagesroleItems;
    }
    
    
    public AspNetPagesRoleVM Get(long Id)
    {
     AspNetPagesRoleVM aspnetpagesroleVM = new AspNetPagesRoleVM();
    var aspnetpagesroleItems = unitOfWork.AspNetPagesRoleRepository.Get(Id);
aspnetpagesroleVM.PageRoleId = aspnetpagesroleItems.PageRoleId ;
aspnetpagesroleVM.PageId = aspnetpagesroleItems.PageId ;
aspnetpagesroleVM.RoleId = aspnetpagesroleItems.RoleId ;
aspnetpagesroleVM.CanAdd = aspnetpagesroleItems.CanAdd ;
aspnetpagesroleVM.CanEdit = aspnetpagesroleItems.CanEdit ;
aspnetpagesroleVM.CanView = aspnetpagesroleItems.CanView ;
aspnetpagesroleVM.CanDelete = aspnetpagesroleItems.CanDelete ;
aspnetpagesroleVM.CanApprove = aspnetpagesroleItems.CanApprove ;
    return aspnetpagesroleVM;
    }
    
    public void Add(AspNetPagesRole aspnetpagesrole) {
      aspnetpagesrole.AddDate = DateTime.Now;
       aspnetpagesrole.AddBy = HttpContext.Current.User.Identity.Name;
      aspnetpagesrole.Status = "A";
      unitOfWork.AspNetPagesRoleRepository.Add(entity: aspnetpagesrole);
     unitOfWork.Save();
    }
    
    public void Update(AspNetPagesRole aspnetpagesrole) {
     aspnetpagesrole.UpdateDate = DateTime.Now;
      aspnetpagesrole.UpdateBy = HttpContext.Current.User.Identity.Name;
      aspnetpagesrole.Status = "A";
      unitOfWork.AspNetPagesRoleRepository.Update(entity: aspnetpagesrole);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(AspNetPagesRole aspnetpagesrole) {
      aspnetpagesrole.UpdateDate = DateTime.Now;
       aspnetpagesrole.UpdateBy = HttpContext.Current.User.Identity.Name;
       aspnetpagesrole.Status = aspnetpagesrole.Status== "A" ? "D" : "A";
      unitOfWork.AspNetPagesRoleRepository.Update(entity: aspnetpagesrole);
     unitOfWork.Save();
    }
    
    public void Delete(AspNetPagesRole aspnetpagesrole) {
      unitOfWork.AspNetPagesRoleRepository.Delete(Id: aspnetpagesrole.Id);
      unitOfWork.Save();
    }
    
    }
}
