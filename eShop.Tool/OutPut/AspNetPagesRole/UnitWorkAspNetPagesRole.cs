       private Repository<AspNetPagesRole> aspnetpagesroleRepository;
       public Repository<AspNetPagesRole> AspNetPagesRoleRepository
       {
        get
         {
          if (this.aspnetpagesroleRepository == null)
          {
           this.aspnetpagesroleRepository = new Repository<AspNetPagesRole>(cRMContext);
          }
       return aspnetpagesroleRepository;
       }
      }
