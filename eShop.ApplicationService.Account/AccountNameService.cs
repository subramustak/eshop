using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Account.ViewModel;
using eShop.BusinessDomain.Account.DomainObject;

namespace eShop.ApplicationService.Account
{
 public class AccountNameService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<AccountNameVM> GetAllAccountName()
   {
    var AccountNameItems = (from rItem in unitOfWork.AccountNameRepository.GetAll().OrderBy(r => r.Id)
                            join lItemRoot in unitOfWork.RootAccountRepository.GetAll() on rItem.RootId equals lItemRoot.Id into ps
                            from lItemRoot in ps.DefaultIfEmpty()
                            select new AccountNameVM
                                 {
                                Id = rItem.Id,
                                Name = rItem.Name,
                                RootId = rItem.RootId,
                                RootName = lItemRoot.Name,
                                Status = lItemRoot.Status,
                            }).GroupBy(e=>e.Name).Select(e=>e.First()).ToList();
    return AccountNameItems;
    }
    
    
    public AccountNameVM Get(long Id)
    {
            AccountNameVM AccountNameVM = new AccountNameVM();
    var AccountNameItems = unitOfWork.AccountNameRepository.Get(Id);
AccountNameVM.Id = AccountNameItems.Id ;
AccountNameVM.Name = AccountNameItems.Name;
    return AccountNameVM;
    }
    
    public void Add(AccountName AccountName) {
      AccountName.AddDate = DateTime.Now;
       AccountName.AddBy = HttpContext.Current.User.Identity.Name;
      AccountName.Status = "A";
      unitOfWork.AccountNameRepository.Add(entity: AccountName);
     unitOfWork.Save();
    }
        public void AddDouble(AccountNameVM accountNameVM)
        {
           var acc= unitOfWork.AccountNameRepository.GetAny(e=>e.Id== accountNameVM.PayNreciveId);
            if (accountNameVM.PR)
            {
                acc.ClossingBalance = (acc.ClossingBalance==null ?0 : acc.ClossingBalance) + accountNameVM.ClossingBalance;
            }
            else {
                acc.ClossingBalance = acc.ClossingBalance - accountNameVM.ClossingBalance;
            }
            acc.UpdateDate = DateTime.Now;
            acc.UpdateBy = HttpContext.Current.User.Identity.Name;
            unitOfWork.AccountNameRepository.Update(entity: acc);
            if (accountNameVM.PaymentType == 1) //cash
            {
                if (accountNameVM.PR)
                {
                    acc = unitOfWork.AccountNameRepository.Get(e=>e.Id ==2 ).FirstOrDefault();
                    acc.ClossingBalance = acc.ClossingBalance ?? 0 - accountNameVM.ClossingBalance;
                    acc.UpdateDate = DateTime.Now;
                    acc.UpdateBy = HttpContext.Current.User.Identity.Name;
                    unitOfWork.AccountNameRepository.Update(entity: acc);
                }
                else {
                    acc = unitOfWork.AccountNameRepository.GetAny(e => e.Id == 1);
                    acc.ClossingBalance = acc.ClossingBalance + accountNameVM.ClossingBalance;
                    acc.UpdateDate = DateTime.Now;
                    acc.UpdateBy = HttpContext.Current.User.Identity.Name;
                    unitOfWork.AccountNameRepository.Update(entity: acc);
                }
            }
            else // bank
            {
                if (accountNameVM.PR)
                {
                    acc = unitOfWork.AccountNameRepository.GetAny(e => e.Id == 2);
                    acc.ClossingBalance = acc.ClossingBalance - accountNameVM.ClossingBalance;
                    acc.UpdateDate = DateTime.Now;
                    acc.UpdateBy = HttpContext.Current.User.Identity.Name;
                    unitOfWork.AccountNameRepository.Update(entity: acc);
                }
                else
                {
                    acc = unitOfWork.AccountNameRepository.GetAny(e => e.Id == 2);
                    acc.ClossingBalance = acc.ClossingBalance + accountNameVM.ClossingBalance;
                    acc.UpdateDate = DateTime.Now;
                    acc.UpdateBy = HttpContext.Current.User.Identity.Name;
                    unitOfWork.AccountNameRepository.Update(entity: acc);
                }
            }
            unitOfWork.Save();
        }

        public void Update(AccountName AccountName) {
            AccountName accountName = new AccountName();
            accountName = unitOfWork.AccountNameRepository.Get(AccountName.Id);
            accountName.ClossingBalance = AccountName.ClossingBalance;
            accountName.UpdateDate = DateTime.Now;
            accountName.UpdateBy = HttpContext.Current.User.Identity.Name;
            unitOfWork.AccountNameRepository.Update(entity: accountName);
            unitOfWork.Save();
        }
       

        public void UpdateByRoot(AccountName AccountName)
        {
            var data = unitOfWork.AccountNameRepository.GetAll(e=>e.RootId==AccountName.RootId && e.PayNreciveId==AccountName.PayNreciveId).FirstOrDefault();
            data.Name = AccountName.Name;
            data.RootId = AccountName.RootId;
            data.PayNreciveId = AccountName.PayNreciveId;
            data.Status = "A";
            AccountName.UpdateDate = DateTime.Now;
            AccountName.UpdateBy = HttpContext.Current.User.Identity.Name;
            unitOfWork.AccountNameRepository.Update(entity: AccountName);
            unitOfWork.Save();
        }

        public dynamic GetAllAccountNameById(int Id)
        {
            var AccountNameItems = (from rItem in unitOfWork.AccountNameRepository.GetAll(e=>e.RootId==Id).OrderBy(r => r.Id)
                                    join lItemRoot in unitOfWork.RootAccountRepository.GetAll() on rItem.RootId equals lItemRoot.Id into ps
                                    from lItemRoot in ps.DefaultIfEmpty()
                                    select new AccountNameVM
                                    {
                                        Id = rItem.Id,
                                        Name = rItem.Name,
                                        RootId = rItem.RootId,
                                        RootName = lItemRoot.Name,
                                        Status = lItemRoot.Status,
                                    }).ToList();
            return AccountNameItems;
        }

        public void ChangeStatus(AccountName AccountName) {
      AccountName.UpdateDate = DateTime.Now;
       AccountName.UpdateBy = HttpContext.Current.User.Identity.Name;
       AccountName.Status = AccountName.Status== "A" ? "D" : "A";
      unitOfWork.AccountNameRepository.Update(entity: AccountName);
     unitOfWork.Save();
    }
    
    public void Delete(AccountName AccountName) {
      unitOfWork.AccountNameRepository.Delete(Id: AccountName.Id);
      unitOfWork.Save();
    }
    
    }
}
