using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Account.ViewModel;
using eShop.BusinessDomain.Account.DomainObject;
using Infrastructure.Crosscutting.Utility;
using System.Transactions;

namespace eShop.ApplicationService.Account
{
    public class TransectionHistoryService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        CommonRepository commonRepository = new CommonRepository();
        AccountNameService accountNameService = new AccountNameService();
        public ICollection<TransectionHistoryVM> GetAllTransectionHistory()
        {
            var TransectionHistoryItems = (from rItem in unitOfWork.TransectionHistoryRepository.GetAll().OrderBy(r => r.Id)
                                           join lItemAccount in unitOfWork.AccountNameRepository.GetAll() on rItem.AccountId equals lItemAccount.Id into ps
                                           from lItemAccount in ps.DefaultIfEmpty()
                                           join lItemRoot in unitOfWork.RootAccountRepository.GetAll() on lItemAccount.RootId equals lItemRoot.Id into ar
                                           from lItemRoot in ar.DefaultIfEmpty()
                                           join lItemBank in unitOfWork.BankRepository.GetAll() on rItem.BankId equals lItemBank.Id into bk
                                           from lItemBank in bk.DefaultIfEmpty()
                                           select new TransectionHistoryVM
                                           {
                                               Id = rItem.Id,
                                               AccountId = rItem.AccountId,
                                               BankId = rItem.BankId,
                                               IsPayment = rItem.IsPayment,
                                               PaymentAmount = rItem.PaymentAmount,
                                               PaymentType = rItem.PaymentType,
                                               ReciveAmount = rItem.ReciveAmount,
                                               AccountName = lItemAccount?.Name ?? "",
                                               RootName = lItemRoot?.Name ?? "",
                                               BankName = lItemBank?.BankName ?? "",
                                               PaymentTypeName = rItem.PaymentType == 1 ? "Cash" : "Bank",
                                           }).ToList();
            return TransectionHistoryItems;
        }


        public TransectionHistoryVM Get(long Id)
        {
            TransectionHistoryVM TransectionHistoryVM = new TransectionHistoryVM();
            var TransectionHistoryItems = unitOfWork.TransectionHistoryRepository.Get(Id);
            TransectionHistoryVM.Id = TransectionHistoryItems.Id;
            TransectionHistoryVM.AccountId = TransectionHistoryItems.AccountId;
            TransectionHistoryVM.BankId = TransectionHistoryItems.BankId;
            TransectionHistoryVM.PaymentAmount = TransectionHistoryItems.PaymentAmount;
            TransectionHistoryVM.IsPayment = TransectionHistoryItems.IsPayment;
            TransectionHistoryVM.PaymentType = TransectionHistoryItems.PaymentType;
            TransectionHistoryVM.ReciveAmount = TransectionHistoryItems.ReciveAmount;
            return TransectionHistoryVM;
        }
        public string GenerateInvoice()
        {
            string invoice = string.Empty;
            var data = unitOfWork.TransectionHistoryRepository.GetAll().ToList();
            if (data.Where(e => e.InvoiceNo.Length > 10).Any())
            {
                var lastinvoice = data.Where(e => e.InvoiceNo.Length > 10).FirstOrDefault().InvoiceNo;
                invoice = DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("00") + (Convert.ToInt32(lastinvoice.Substring(6, 4)) + 1).ToString("0000");
            }
            else
            {
                invoice = DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("00") + (1).ToString("0000");
            }
            return invoice;
        }
        public void AddP(TransectionHistory transectionHistory)
        {
            transectionHistory.IsPayment = true;
            transectionHistory.AddDate = DateTime.Now;
            transectionHistory.AddBy = HttpContext.Current.User.Identity.Name;
            transectionHistory.Status = "A";
            var upatepur = unitOfWork.PurchaseRepository.GetAll(e => e.InvoiecNo == transectionHistory.InvoiceNo).FirstOrDefault();
            transectionHistory.AccountId = upatepur.SupplierId;
            transectionHistory.Remarks = "Due Payable";
            unitOfWork.TransectionHistoryRepository.Add(entity: transectionHistory);

            if (transectionHistory.PaymentAmount < upatepur.Due)
            {
                upatepur.Due = upatepur.Due - transectionHistory.PaymentAmount ?? 0;
            }
            if (transectionHistory.PaymentAmount == upatepur.Due)
            {
                upatepur.Due = 0;
            }
            upatepur.TotalPaid = upatepur.TotalPaid + transectionHistory.PaymentAmount ?? 0;
            upatepur.UpdateDate = DateTime.Now;
            upatepur.UpdateBy = HttpContext.Current.User.Identity.Name;
            unitOfWork.PurchaseRepository.Update(upatepur);
            unitOfWork.Save();
        }
        public void AddR(TransectionHistory transectionHistory)
        {

            transectionHistory.AddDate = DateTime.Now;
            transectionHistory.AddBy = HttpContext.Current.User.Identity.Name;
            transectionHistory.Status = "A";
            var upatepur = unitOfWork.SaleRepository.GetAll(e => e.InvoiecNo == transectionHistory.InvoiceNo).FirstOrDefault();
            transectionHistory.AccountId = upatepur.CustomerId;
            transectionHistory.Remarks = "Due Receivable";
            unitOfWork.TransectionHistoryRepository.Add(entity: transectionHistory);

            if (transectionHistory.ReciveAmount < upatepur.Due)
            {
                upatepur.Due = upatepur.Due - transectionHistory.PaymentAmount ?? 0;
            }
            if (transectionHistory.ReciveAmount == upatepur.Due)
            {
                upatepur.Due = 0;
                upatepur.NetTotal = upatepur.NetTotal + transectionHistory.ReciveAmount ?? 0;
            }
            upatepur.TotalPaid = upatepur.TotalPaid + transectionHistory.ReciveAmount ?? 0;
            upatepur.UpdateDate = DateTime.Now;
            upatepur.UpdateBy = HttpContext.Current.User.Identity.Name;
            unitOfWork.SaleRepository.Update(upatepur);
            unitOfWork.Save();
        }
        public void AddUpdate(TransectionHistory transectionHistory)
        {
            //var data = unitOfWork.TransectionHistoryRepository.GetAll(e=>e.);
            transectionHistory.AddDate = DateTime.Now;
            transectionHistory.AddBy = HttpContext.Current.User.Identity.Name;
            transectionHistory.Status = "A";
            unitOfWork.TransectionHistoryRepository.Add(entity: transectionHistory);
            var upatepur = unitOfWork.PurchaseRepository.GetAll(e => e.InvoiecNo == transectionHistory.InvoiceNo).FirstOrDefault();
            if (transectionHistory.PaymentAmount < upatepur.Due)
            {
                upatepur.Due = upatepur.Due - transectionHistory.PaymentAmount ?? 0;
            }
            if (transectionHistory.PaymentAmount == upatepur.Due)
            {
                upatepur.Due = 0;
            }
            upatepur.UpdateDate = DateTime.Now;
            upatepur.UpdateBy = HttpContext.Current.User.Identity.Name;
            unitOfWork.PurchaseRepository.Update(upatepur);
            unitOfWork.Save();
        }
        public void Add(TransectionHistoryVM transectionHistoryVM)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    AccountNameVM accountNameVM = new AccountNameVM();
                    TransectionHistory transectionHistory = new TransectionHistory();
                    transectionHistory.AccountId = unitOfWork.AccountNameRepository.GetAny(e => e.PayNreciveId == transectionHistoryVM.AccountId).Id;
                    transectionHistory.BankId = transectionHistoryVM.BankId;
                    transectionHistory.BranchId = transectionHistoryVM.BranchId;
                    transectionHistory.ClossingBalance = transectionHistoryVM.ClossingBalance;
                    transectionHistory.Date = DateTime.Now;
                    transectionHistory.InvoiceNo = transectionHistoryVM.InvoiceNo;
                    transectionHistory.IsPayment = transectionHistoryVM.IsPayment;
                    transectionHistory.IsRecive = transectionHistoryVM.IsRecive;
                    transectionHistory.OpeningBalance = transectionHistoryVM.OpeningBalance;
                    transectionHistory.PaymentAmount = transectionHistoryVM.PaymentAmount;
                    transectionHistory.PaymentType = transectionHistoryVM.PaymentType;
                    transectionHistory.ReciveAmount = transectionHistoryVM.ReciveAmount;
                    transectionHistory.Remarks = transectionHistoryVM.Remarks;
                    transectionHistory.Status = transectionHistoryVM.Status;
                    transectionHistory.AddDate = DateTime.Now;
                    transectionHistory.AddBy = HttpContext.Current.User.Identity.Name;
                    transectionHistory.Status = "A";
                    unitOfWork.TransectionHistoryRepository.Add(entity: transectionHistory);
                    accountNameVM.ClossingBalance = transectionHistory.PaymentAmount == 0 ?  (transectionHistory.ReciveAmount ?? 0) : transectionHistory.PaymentAmount;
                    if (transectionHistory.IsPayment == true)
                    {
                        accountNameVM.ClossingBalance = transectionHistory.PaymentAmount;
                    }
                    else {
                        accountNameVM.ClossingBalance = transectionHistory.ReciveAmount;
                    }
                    accountNameVM.PayNreciveId = transectionHistory.AccountId;
                    accountNameVM.PaymentType = transectionHistory.PaymentType;
                    accountNameVM.PR = transectionHistory.IsPayment == true ? true : false;
                    accountNameService.AddDouble(accountNameVM);
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(TransectionHistory TransectionHistory)
        {
            var t = unitOfWork.TransectionHistoryRepository.GetAll().ToList();
           var th= unitOfWork.TransectionHistoryRepository.GetAll(e => e.InvoiceNo==TransectionHistory.InvoiceNo).FirstOrDefault();
            th.InvoiceNo = TransectionHistory.InvoiceNo;
            th.Date = TransectionHistory.Date;
            th.PaymentAmount = TransectionHistory.PaymentAmount;
            th.PaymentType = TransectionHistory.PaymentType;
            th.Remarks = TransectionHistory.Remarks;
            th.BankId = TransectionHistory.BankId;
            //th.ChequeNo=TransectionHistory.ch
            th.UpdateDate = DateTime.Now;
            th.UpdateBy = HttpContext.Current.User.Identity.Name;
            th.Status = "A";
            unitOfWork.TransectionHistoryRepository.Update(th);
            unitOfWork.Save();
        }
        public void ChangeStatus(TransectionHistory TransectionHistory)
        {
            TransectionHistory.UpdateDate = DateTime.Now;
            TransectionHistory.UpdateBy = HttpContext.Current.User.Identity.Name;
            TransectionHistory.Status = TransectionHistory.Status == "A" ? "D" : "A";
            unitOfWork.TransectionHistoryRepository.Update(entity: TransectionHistory);
            unitOfWork.Save();
        }
        public dynamic TransactionReceipt(int? ACId, DateTime? startDate, DateTime? endDate)
        {
            var TransactionList = commonRepository.GetBySpWithParam("[dbo].[SP_Transaction]", new object[] { 3, ACId, startDate, endDate });
            var TransactionVMs = Converters.ConvertDataTable<TransectionHistoryVM>(TransactionList);
            return TransactionVMs;
        }
        public dynamic TransactionReceiptDetail(int? ACId, DateTime? startDate, DateTime? endDate)
        {
            var TransactionList = commonRepository.GetBySpWithParam("[dbo].[SP_Transaction]", new object[] { 5, ACId, startDate, endDate });
            var TransactionVMs = Converters.ConvertDataTable<TransectionHistoryVM>(TransactionList);
            return TransactionVMs;
        }
        public void Delete(TransectionHistory TransectionHistory)
        {
            unitOfWork.TransectionHistoryRepository.Delete(Id: TransectionHistory.Id);
            unitOfWork.Save();
        }
        public dynamic TransactionPayemnt(int? ACId, DateTime? startDate, DateTime? endDate)
        {
            var TransactionList = commonRepository.GetBySpWithParam("[dbo].[SP_Transaction]", new object[] { 4, ACId, startDate, endDate });
            var TransactionVMs = Converters.ConvertDataTable<TransectionHistoryVM>(TransactionList);
            return TransactionVMs;
        }
        public dynamic TransactionPayemntDetail(int? ACId, DateTime? startDate, DateTime? endDate)
        {
            var TransactionList = commonRepository.GetBySpWithParam("[dbo].[SP_Transaction]", new object[] { 5, ACId, startDate, endDate });
            var TransactionVMs = Converters.ConvertDataTable<TransectionHistoryVM>(TransactionList);
            return TransactionVMs;
        }
        #region Customer Receiveable
        public void CusotmerWiseDuePay(CusotmerWiseDuePayVM cusotmerWiseDuePay)
        {
            decimal paidAmount = cusotmerWiseDuePay.PaidAmount;
            var dueInvoiceList = unitOfWork.SaleRepository.GetAll(e => e.CustomerId == cusotmerWiseDuePay.CustomerId && e.Due > 0).OrderBy(e => e.InvoiecNo).ToList();
            foreach (var item in dueInvoiceList)
            {
                if (paidAmount > 0)
                {
                    TransectionHistory transectionHistory = new TransectionHistory();
                    if (paidAmount >= item.Due)
                    {
                        transectionHistory.ReciveAmount = item.Due;
                        paidAmount -= item.Due;
                        item.TotalPaid += item.Due;
                        item.Due = 0;
                    }
                    else
                    {
                        transectionHistory.ReciveAmount = paidAmount;
                        item.TotalPaid += paidAmount;
                        item.Due -= paidAmount;
                        paidAmount -= item.Due;
                    }
                    item.UpdateBy = HttpContext.Current.User.Identity.Name;
                    item.UpdateDate = DateTime.Now;
                    unitOfWork.SaleRepository.Update(item);
                    transectionHistory.AccountId = item.CustomerId;
                    transectionHistory.PaymentType = cusotmerWiseDuePay.PaymentType;
                    transectionHistory.IsRecive = true;
                    transectionHistory.InvoiceNo = item.InvoiecNo;
                    transectionHistory.BranchId = item.BranchId == null ? 0 : (int)item.BranchId;
                    transectionHistory.BankId = cusotmerWiseDuePay.BankId;
                    transectionHistory.AddDate = DateTime.Now;
                    transectionHistory.AddBy = HttpContext.Current.User.Identity.Name;
                    transectionHistory.Status = "A";
                    unitOfWork.TransectionHistoryRepository.Add(entity: transectionHistory);
                    unitOfWork.Save();
                }
                else
                {
                    break;
                }
            }
        }
        #endregion
    }
}
