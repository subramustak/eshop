using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Account.ViewModel;
using eShop.BusinessDomain.Account.DomainObject;

namespace eShop.ApplicationService.Account
{
 public class RootAccountService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<RootAccountVM> GetAllRootAccount()
   {
    var RootAccountItems = (from rItem in unitOfWork.RootAccountRepository.GetAll().OrderBy(r => r.Id)
                                  select new RootAccountVM
                                 {
      Id = rItem.Id,
                                      Name = rItem.Name,
                                    }).ToList();
            if (RootAccountItems.Count ==0)
            {
                RootAccount rootAccount = new RootAccount();
                rootAccount.Name = "Customer";
                Add(rootAccount);
                 rootAccount = new RootAccount();
                rootAccount.Name = "Supplier";
                Add(rootAccount);
                rootAccount = new RootAccount();
                rootAccount.Name = "OfficeExpense";
                Add(rootAccount);
                rootAccount = new RootAccount();
                rootAccount.Name = "OfficeLoan";
                Add(rootAccount);
            }
    return RootAccountItems;
    }
    
    
    public RootAccountVM Get(long Id)
    {
            RootAccountVM RootAccountVM = new RootAccountVM();
    var RootAccountItems = unitOfWork.RootAccountRepository.Get(Id);
RootAccountVM.Id = RootAccountItems.Id ;
RootAccountVM.Name = RootAccountItems.Name;
    return RootAccountVM;
    }
    
    public void Add(RootAccount RootAccount) {
      RootAccount.AddDate = DateTime.Now;
       RootAccount.AddBy = HttpContext.Current.User.Identity.Name;
      RootAccount.Status = "A";
      unitOfWork.RootAccountRepository.Add(entity: RootAccount);
     unitOfWork.Save();
    }
    
    public void Update(RootAccount RootAccount) {
     RootAccount.UpdateDate = DateTime.Now;
      RootAccount.UpdateBy = HttpContext.Current.User.Identity.Name;
      RootAccount.Status = "A";
      unitOfWork.RootAccountRepository.Update(entity: RootAccount);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(RootAccount RootAccount) {
      RootAccount.UpdateDate = DateTime.Now;
       RootAccount.UpdateBy = HttpContext.Current.User.Identity.Name;
       RootAccount.Status = RootAccount.Status== "A" ? "D" : "A";
      unitOfWork.RootAccountRepository.Update(entity: RootAccount);
     unitOfWork.Save();
    }
    
    public void Delete(RootAccount RootAccount) {
      unitOfWork.RootAccountRepository.Delete(Id: RootAccount.Id);
      unitOfWork.Save();
    }
    
    }
}
