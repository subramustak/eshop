﻿using eShop.BusinessDomain.Core;

namespace eShop.BusinessDomain.Account.ViewModel
{
    public class RootAccountVM: Entity
    {
        public string Name { get; set; }
    }
}
