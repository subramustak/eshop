﻿using eShop.BusinessDomain.Core;
using System;

namespace eShop.BusinessDomain.Account.ViewModel
{
    public class TransectionHistoryVM : Entity
    {
        public int AccountId { get; set; }
        public int BranchId { get; set; }
        public string InvoiceNo { get; set; }
        public string AccountName { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? ReciveAmount { get; set; }
        public decimal? Amount { get; set; }
        public int PaymentType { get; set; }
        public string PaymentTypeName { get; set; }
        public bool? IsPayment { get; set; }
        public bool? IsRecive { get; set; }
        public int? BankId { get; set; }
        public string RootName { get; set; }
        public string BankName { get; set; }
        public DateTime Date { get; set; }
        public decimal? OpeningBalance { get; set; }
        public decimal? ClossingBalance { get; set; }
        public int? PayNreciveId { get; set; }
    }
}
