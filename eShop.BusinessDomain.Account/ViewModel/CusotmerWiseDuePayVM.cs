﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.Account.ViewModel
{
    public class CusotmerWiseDuePayVM
    {
        public int CustomerId { get; set; }
        public decimal TotalDueAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public int PaymentType { get; set; }
        public int? BankId { get; set; }
        public int? ChequeNo { get; set; }
    }
}
