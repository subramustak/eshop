﻿using eShop.BusinessDomain.Core;

namespace eShop.BusinessDomain.Account.ViewModel
{
   public class AccountNameVM : Entity
    {
        
        public string RootName { get; set; }
        public string Name { get; set; }
        public int RootId { get; set; }
        public int? PayNreciveId { get; set; }
        public decimal? OpeningBalance { get; set; }
        public decimal? ClossingBalance { get; set; }
        public int PaymentType { get; set; } // 1=Cash ,2=Bank
        public bool PR { get; set; } // Payment or Receive
    }
}
