﻿using eShop.BusinessDomain.Core;
using System;
using System.Data;

namespace eShop.BusinessDomain.Account.DomainObject
{
    public class TransectionHistory: Entity
    {
        public int AccountId { get; set; }
        public int BranchId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? ReciveAmount { get; set; }
        public int PaymentType { get; set; }
        public bool? IsPayment { get; set; }
        public bool? IsRecive { get; set; }
        public int? BankId { get; set; }
        public decimal? OpeningBalance { get; set; }
        public decimal? ClossingBalance { get; set; }
        public DateTime Date { get; set; }
        public TransectionHistory() {
            Date = DateTime.Now;

        }
    }
}
