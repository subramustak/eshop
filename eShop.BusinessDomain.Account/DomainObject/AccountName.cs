﻿using eShop.BusinessDomain.Core;

namespace eShop.BusinessDomain.Account.DomainObject
{
   public class AccountName : Entity
    {
        public string Name { get; set; }
        public int RootId { get; set; }
        public int? PayNreciveId { get; set; }
        public decimal? OpeningBalance { get; set; }
        public decimal? ClossingBalance { get; set; }

    }
}
