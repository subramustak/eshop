﻿using eShop.BusinessDomain.Core;
using System.ComponentModel.DataAnnotations.Schema;

namespace eShop.BusinessDomain.Account.DomainObject
{
    public class RootAccount: Entity
    {
        public string Name { get; set; }
        public bool DC { get; set; }
        public bool IsDisplay { get; set; }
    }
}
