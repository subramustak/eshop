using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
using System.Transactions;
using eShop.BusinessDomain.Account.DomainObject;
using eShop.BusinessDomain.Core.Helpers;

namespace eshop.ApplicationService.CRM
{
 public class PurchaseReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        StockService StockService = new StockService();
        PurcheaseDetailReturnService purchaseDetialReturn = new PurcheaseDetailReturnService();
        CommonRepository commonRepository = new CommonRepository();
        public ICollection<PurchaseReturnVM> GetAllPurchaseReturn()
   {
    var purchasereturnItems = (from rItem in unitOfWork.PurchaseReturnRepository.GetAll().OrderBy(r => r.Id)
                               join lItemS in unitOfWork.SupplierRepository.GetAll() on rItem.SupplierId equals lItemS.Id into ps
                               from lItemS in ps.DefaultIfEmpty()
                               select new PurchaseReturnVM
                                 {
      Id = rItem.Id,
      InvoiecNo = rItem.InvoiecNo,
      SupplierId = rItem.SupplierId,
      BranchId = rItem.BranchId,
      BankId = rItem.BankId,
      ChequeNo = rItem.ChequeNo,
                                   SupplierName = lItemS.Name,
                                   PaymentType = rItem.PaymentType,
                                   ReceiverName = rItem.ReceiverName,
                                   ReceiverNumber = rItem.ReceiverNumber,
                                   EmployeeId = rItem.EmployeeId,
      Date = rItem.Date,
      ReturnDue = rItem.Due,
      ReturnTotal = rItem.Total,
      ReturnTotalPayment = rItem.NetTotal,
      Discount = rItem.Discount,
                                    }).ToList();
    return purchasereturnItems;
    }
    
    
    public PurchaseReturnVM Get(long Id)
    {
     PurchaseReturnVM purchasereturnVM = new PurchaseReturnVM();
    var purchasereturnItems = unitOfWork.PurchaseReturnRepository.Get(Id);
purchasereturnVM.Id = purchasereturnItems.Id ;
purchasereturnVM.InvoiecNo = purchasereturnItems.InvoiecNo ;
purchasereturnVM.SupplierId = purchasereturnItems.SupplierId ;
purchasereturnVM.BranchId = purchasereturnItems.BranchId ;
purchasereturnVM.BankId = purchasereturnItems.BankId ;
purchasereturnVM.ChequeNo = purchasereturnItems.ChequeNo ;
purchasereturnVM.PaymentType = purchasereturnItems.PaymentType ;
purchasereturnVM.EmployeeId = purchasereturnItems.EmployeeId ;
purchasereturnVM.Date = purchasereturnItems.Date ;
purchasereturnVM.ReturnDue = purchasereturnItems.Due ;
purchasereturnVM.ReturnTotal = purchasereturnItems.Total ;
purchasereturnVM.ReturnTotalPayment = purchasereturnItems.NetTotal ;
purchasereturnVM.Discount = purchasereturnItems.Discount ;
            purchasereturnVM.ReceiverName = purchasereturnItems.ReceiverName;
            purchasereturnVM.ReceiverNumber = purchasereturnItems.ReceiverNumber;
purchasereturnVM.PurcheaseDetails = purchaseDetialReturn.GetRetrun(Id).ToList();
    return purchasereturnVM;
    }
    
    public void Add(PurchaseReturnVM purchasereturnVM) {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    TransectionHistory transectionHistory = new TransectionHistory();

                    PurchaseReturn purReturn = new PurchaseReturn();
            purReturn.InvoiecNo = purchasereturnVM.InvoiecNo;
            purReturn.SupplierId = purchasereturnVM.SupplierId;
            purReturn.BranchId = purchasereturnVM.BranchId;
            purReturn.BankId = purchasereturnVM.BankId;
            purReturn.ChequeNo = purchasereturnVM.ChequeNo;
            purReturn.PaymentType = purchasereturnVM.PaymentType;
            purReturn.EmployeeId = purchasereturnVM.EmployeeId;
            purReturn.ReceiverName = purchasereturnVM.ReceiverName;
            purReturn.ReceiverNumber = purchasereturnVM.ReceiverNumber;
                    purReturn.Date = DateTime.Now;
                    purReturn.Total = purchasereturnVM.PurcheaseDetails.Sum(e => e.ReturnQuantity * e.UnitePrice);
                    purReturn.NetTotal = purReturn.Total - purchasereturnVM.PurcheaseDetails.Sum(e => e.ReturnDiscount);
                    if (purReturn.NetTotal > purchasereturnVM.ReturnTotalPayment)
                    {
                        purReturn.Due = purReturn.NetTotal - purchasereturnVM.ReturnTotalPayment;
                    }
                    else {
                    }
                    
            purReturn.Discount = purchasereturnVM.Discount;
            purReturn.IsArchived = purchasereturnVM.IsArchived;
            purReturn.IsDeleted = purchasereturnVM.IsDeleted;
            purReturn.AddDate = DateTime.Now;
            purReturn.AddBy = HttpContext.Current.User.Identity.Name;
            purReturn.Status = "A";
      unitOfWork.PurchaseReturnRepository.Add(entity: purReturn);
            foreach (var item in purchasereturnVM.PurcheaseDetails.Where(e=>e.Checked==true))
            {
                PurcheaseDetailReturn purDReturn = new PurcheaseDetailReturn();
                purDReturn.PurchaseReturnId = purReturn.Id;
                purDReturn.ProductId = item.ProductId;
                purDReturn.Discount = item.ReturnDiscount;
                purDReturn.Quantity = item.ReturnQuantity;
                purDReturn.TotalAmount = (item.ReturnQuantity* item.UnitePrice)- item.ReturnDiscount;
                purDReturn.UnitePrice = item.UnitePrice;
                        unitOfWork.PurcheaseDetailReturnRepository.Add(purDReturn);
                StockVM stocks = new StockVM();
                stocks.BranchId = purReturn.BranchId;
                stocks.SupplierId = purReturn.SupplierId;
                stocks.Date = purReturn.Date;
                stocks.UnitPrice = item.UnitePrice;
                stocks.PurcheaseReturnId = purReturn.Id;
                stocks.ProductId = item.ProductId;
                stocks.Quantity = item.ReturnQuantity;
                stocks.Discount = item.ReturnDiscount;
                StockService.AddStockProduct(stocks, false);

            }
                    transectionHistory.AccountId = unitOfWork.AccountNameRepository.GetAll(e => e.RootId == 2 && e.PayNreciveId == purchasereturnVM.SupplierId).FirstOrDefault().PayNreciveId ?? 0;
                    transectionHistory.BankId = purchasereturnVM.BankId;
                    transectionHistory.InvoiceNo = purReturn.InvoiecNo;
                    transectionHistory.Date = DateTime.Now;
                    transectionHistory.IsRecive = true;
                    transectionHistory.ReciveAmount = purchasereturnVM.ReturnTotalPayment;
                    transectionHistory.PaymentType = purReturn.PaymentType;
                    transectionHistory.Remarks = "Purchease Return Add";
                    transectionHistory.AddDate = DateTime.Now;
                    unitOfWork.TransectionHistoryRepository.Add(transectionHistory);
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    public void Update(PurchaseReturnVM purchasereturnVM) {
            PurchaseReturn purchaseReturn = new PurchaseReturn();
          
            StockVM stockvm = new StockVM();
            TransectionHistory transectionHistory = new TransectionHistory();
            Purchases pur = new Purchases();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    purchaseReturn = unitOfWork.PurchaseReturnRepository.Get(purchasereturnVM.Id);
                    purchaseReturn.InvoiecNo = purchasereturnVM.InvoiecNo;
                    purchaseReturn.SupplierId = purchasereturnVM.SupplierId;
                    purchaseReturn.BranchId = purchasereturnVM.BranchId;
                    purchaseReturn.BankId = purchasereturnVM.BankId;
                    purchaseReturn.ChequeNo = purchasereturnVM.ChequeNo;
                    purchaseReturn.PaymentType = purchasereturnVM.PaymentType;
                    purchaseReturn.EmployeeId = purchasereturnVM.EmployeeId;
                    purchaseReturn.ReceiverName = purchasereturnVM.ReceiverName;
                    purchaseReturn.ReceiverNumber = purchasereturnVM.ReceiverNumber;
                    purchaseReturn.Date = DateTime.Now;
                    purchaseReturn.Total = purchasereturnVM.PurcheaseDetails.Sum(e => e.ReturnQuantity * e.UnitePrice);
                    purchaseReturn.NetTotal = purchaseReturn.Total - purchasereturnVM.PurcheaseDetails.Sum(e => e.ReturnDiscount);
                    if (purchaseReturn.NetTotal > purchasereturnVM.ReturnTotalPayment)
                    {
                        purchaseReturn.Due = purchaseReturn.NetTotal - purchasereturnVM.ReturnTotalPayment;
                    }
                    else
                    {
                    }
                   purchaseReturn.Discount = purchasereturnVM.Discount;
                   purchaseReturn.IsArchived = purchasereturnVM.IsArchived;
                   purchaseReturn.IsDeleted = purchasereturnVM.IsDeleted;
                   purchaseReturn.AddDate = DateTime.Now;
                   purchaseReturn.AddBy = HttpContext.Current.User.Identity.Name;
                    purchaseReturn.Status = "A";
                    unitOfWork.PurchaseReturnRepository.Update(entity: purchaseReturn);
                    foreach (var item in purchasereturnVM.PurcheaseDetails)
                    {
                        if (unitOfWork.PurcheaseDetailReturnRepository.GetAll(e => e.Id == item.Id).Any())
                        {
                            unitOfWork.PurcheaseDetailReturnRepository.Delete(item.Id);
                            stockvm = new StockVM
                            {
                                BranchId = purchaseReturn.BranchId,
                                Date = purchaseReturn.Date,
                                ProductId = item.ProductId,
                                PurcheaseReturnId = purchaseReturn.Id,
                                Quantity = item.ReturnQuantity,
                                UnitPrice = item.UnitePrice,
                                SupplierId = purchaseReturn.SupplierId
                            };
                            StockService.AddStockProduct(stockvm, true);
                        }
                    }
                    foreach (var item in purchasereturnVM.PurcheaseDetails)
                    {
                        PurcheaseDetailReturn purDReturn = new PurcheaseDetailReturn();
                        purDReturn.PurchaseReturnId = purchaseReturn.Id;
                        purDReturn.ProductId = item.ProductId;
                        purDReturn.Discount = item.ReturnDiscount;
                        purDReturn.Quantity = item.ReturnQuantity;
                        purDReturn.TotalAmount = (item.ReturnQuantity * item.UnitePrice) - item.ReturnDiscount;
                        purDReturn.UnitePrice = item.UnitePrice;
                        unitOfWork.PurcheaseDetailReturnRepository.Add(purDReturn);
                        stockvm = new StockVM
                        {
                            BranchId = purchaseReturn.BranchId,
                            Date = purchaseReturn.Date,
                            ProductId = item.ProductId,
                            PurcheaseReturnId = purchaseReturn.Id,
                            Quantity = item.ReturnQuantity,
                            UnitPrice = item.UnitePrice,
                            SupplierId = purchaseReturn.SupplierId
                        };
                        StockService.AddStockProduct(stockvm, true);
                        
                    }
                    var date = Convert.ToDateTime(purchaseReturn.Date);
                    transectionHistory = unitOfWork.TransectionHistoryRepository.Get(e => e.AccountId == purchaseReturn.SupplierId && e.Date == date).FirstOrDefault();
                    transectionHistory.AccountId = purchaseReturn.SupplierId  ??0;
                    transectionHistory.BankId = purchaseReturn.BankId;
                    transectionHistory.Date = Convert.ToDateTime(purchaseReturn.Date);
                    transectionHistory.IsRecive = false;
                    transectionHistory.IsPayment = true;
                    transectionHistory.PaymentAmount = pur.NetTotal;
                    transectionHistory.PaymentType = purchaseReturn.PaymentType;
                    transectionHistory.UpdateDate = DateTime.Now;
                    transectionHistory.UpdateBy = HttpContext.Current.User.Identity.Name; 
                    transectionHistory.Remarks = "Purchease Return Updated";

                    unitOfWork.TransectionHistoryRepository.Update(transectionHistory);
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
    }
        public dynamic GetProductDetail(long Id)
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var data = commonRepository.GetBySpWithParam("SP_Purchease", new object[] { 4, (int)Id, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value });
            return data;
        }
        public void ChangeStatus(PurchaseReturn purchasereturn) {
      purchasereturn.UpdateDate = DateTime.Now;
       purchasereturn.UpdateBy = HttpContext.Current.User.Identity.Name;
       purchasereturn.Status = purchasereturn.Status== "A" ? "D" : "A";
      unitOfWork.PurchaseReturnRepository.Update(entity: purchasereturn);
     unitOfWork.Save();
    }
    
    public void Delete(PurchaseReturn purchasereturn) {
            StockVM stockvm = new StockVM();
            TransectionHistory transectionHistory = new TransectionHistory();

            var pur = Get((long)purchasereturn.Id);
          
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    unitOfWork.PurchaseReturnRepository.Delete(pur.Id);
                    foreach (var item in pur.PurcheaseDetails)
                    {
                        if (unitOfWork.PurcheaseDetailReturnRepository.GetAll(e => e.Id == item.Id).Any())
                        {
                            unitOfWork.PurcheaseDetailReturnRepository.Delete(item.Id);
                            stockvm = new StockVM
                            {
                                BranchId = pur.BranchId,
                                Date = pur.Date,
                                ProductId = item.ProductId,
                                PurcheaseId = pur.Id,
                                Quantity = item.ReturnQuantity,
                                UnitPrice = item.UnitePrice,
                                SupplierId = pur.SupplierId
                            };
                            StockService.AddStockProduct(stockvm, true);
                        }
                    }
                    var date = Convert.ToDateTime(pur.Date);
                    transectionHistory = unitOfWork.TransectionHistoryRepository.Get(e => e.AccountId == pur.SupplierId && e.Date == date).FirstOrDefault();
                    unitOfWork.TransectionHistoryRepository.Delete(transectionHistory.Id);
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    }
}
