using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.Sales.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;

namespace eshop.ApplicationService.CRM
{
    public class PurcheaseDetailService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        public List<PurcheaseDetailVM> GetAllPurcheaseDetail()
        {
            var purcheasedetailItems = (from rItem in unitOfWork.PurcheaseDetailRepository.GetAll().OrderBy(r => r.Id)
                                        select new PurcheaseDetailVM
                                        {
                                            Id = rItem.Id,
                                            PurchaseId = rItem.PurchaseId,
                                            ProductId = rItem.ProductId,
                                            UnitePrice = rItem.UnitePrice,
                                            Quantity = rItem.Quantity,
                                            Discount = rItem.Discount,
                                            Amount = rItem.Amount,
                                        }).ToList();
            return purcheasedetailItems;
        }
        public List<PurcheaseDetailVM> GetAllPurcheaseDetailByPurcheasId(int PurcheasId)
        {
            var purcheasedetailItems = (from rItem in unitOfWork.PurcheaseDetailRepository.GetAll(e=>e.PurchaseId==PurcheasId).OrderBy(r => r.Id)
                                        join lItemProduct in unitOfWork.ProductRepository.GetAll() on rItem.ProductId equals lItemProduct.Id into ps
                                        from lItemProduct in ps.DefaultIfEmpty()
                                        select new PurcheaseDetailVM
                                        {
                                            Id = rItem.Id,
                                            PurchaseId = rItem.PurchaseId,
                                            ProductId = rItem.ProductId,
                                            ProductName = lItemProduct.Name,
                                            ProductCode = lItemProduct.Code,
                                            UnitePrice = rItem.UnitePrice,
                                            Quantity = rItem.Quantity,
                                            Discount = rItem.Discount,
                                            Amount = rItem.Amount,
                                            NetAmount=rItem.Amount,
                                            Sales= lItemProduct.SalsPrice
                                        }).ToList();
            return purcheasedetailItems;
        }

        public PurcheaseDetailVM Get(long Id)
        {
            PurcheaseDetailVM purcheasedetailVM = new PurcheaseDetailVM();
            var purcheasedetailItems = unitOfWork.PurcheaseDetailRepository.Get(Id);
            purcheasedetailVM.Id = purcheasedetailItems.Id;
            purcheasedetailVM.PurchaseId = purcheasedetailItems.PurchaseId;
            purcheasedetailVM.ProductId = purcheasedetailItems.ProductId;
            purcheasedetailVM.UnitePrice = purcheasedetailItems.UnitePrice;
            purcheasedetailVM.Quantity = purcheasedetailItems.Quantity;
            purcheasedetailVM.Discount = purcheasedetailItems.Discount;
            purcheasedetailVM.Amount = purcheasedetailItems.Amount;
            return purcheasedetailVM;
        }

        public void Add(PurcheaseDetails purcheasedetail)
        {
            unitOfWork.PurcheaseDetailRepository.Add(entity: purcheasedetail);
            unitOfWork.Save();
        }

        public void Update(PurcheaseDetails purcheasedetail)
        {
            unitOfWork.PurcheaseDetailRepository.Update(entity: purcheasedetail);
            unitOfWork.Save();
        }

        public void ChangeStatus(PurcheaseDetails purcheasedetail)
        {
            unitOfWork.PurcheaseDetailRepository.Update(entity: purcheasedetail);
            unitOfWork.Save();
        }

        public void Delete(PurcheaseDetails purcheasedetail)
        {
            unitOfWork.PurcheaseDetailRepository.Delete(Id: purcheasedetail.Id);
            unitOfWork.Save();
        }

    }
}
