using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class PurcheaseDetailReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<PurcheaseDetailReturnVM> GetAllPurcheaseDetailReturn()
   {
    var purcheasedetailreturnItems = (from rItem in unitOfWork.PurcheaseDetailReturnRepository.GetAll().OrderBy(r => r.Id)
                                  select new PurcheaseDetailReturnVM
                                 {
      Id = rItem.Id,
      PurchaseId = rItem.PurchaseReturnId,
      ProductId = rItem.ProductId,
      UnitePrice = rItem.UnitePrice,
      ReturnQuantity = rItem.Quantity,
      ReturnDiscount = rItem.Discount,
      ReturnTotalAmount = rItem.TotalAmount,
                                    }).ToList();
    return purcheasedetailreturnItems;
    }
    
    
    public PurcheaseDetailReturnVM Get(long Id)
    {
     PurcheaseDetailReturnVM purcheasedetailreturnVM = new PurcheaseDetailReturnVM();
    var purcheasedetailreturnItems = unitOfWork.PurcheaseDetailReturnRepository.Get(Id);
purcheasedetailreturnVM.Id = purcheasedetailreturnItems.Id ;
purcheasedetailreturnVM.PurchaseId = purcheasedetailreturnItems.PurchaseReturnId ;
purcheasedetailreturnVM.ProductId = purcheasedetailreturnItems.ProductId ;
purcheasedetailreturnVM.UnitePrice = purcheasedetailreturnItems.UnitePrice ;
purcheasedetailreturnVM.ReturnQuantity = purcheasedetailreturnItems.Quantity ;
purcheasedetailreturnVM.ReturnDiscount = purcheasedetailreturnItems.Discount ;
purcheasedetailreturnVM.ReturnTotalAmount = purcheasedetailreturnItems.TotalAmount ;
    return purcheasedetailreturnVM;
    }
        public ICollection<PurcheaseDetailReturnVM> GetRetrun(long Id)
        {
            PurcheaseDetailReturnVM purcheasedetailreturnVM = new PurcheaseDetailReturnVM();
            var purcheasedetailreturnItems = (from rItem in unitOfWork.PurcheaseDetailReturnRepository.GetAll(e=>e.PurchaseReturnId == Id).OrderBy(r => r.Id)
                                              select new PurcheaseDetailReturnVM
                                              {
                                                  Id = rItem.Id,
                                                  PurchaseId = rItem.PurchaseReturnId,
                                                  ProductId = rItem.ProductId,
                                                  UnitePrice = rItem.UnitePrice,
                                                  ReturnQuantity = rItem.Quantity,
                                                  ReturnDiscount = rItem.Discount,
                                                  ReturnTotalAmount = rItem.TotalAmount,
                                              }).ToList();
            return purcheasedetailreturnItems;
        }
        public void Add(PurcheaseDetailReturn purcheasedetailreturn) {
      unitOfWork.PurcheaseDetailReturnRepository.Add(entity: purcheasedetailreturn);
     unitOfWork.Save();
    }
    
    public void Update(PurcheaseDetailReturn purcheasedetailreturn) {
      unitOfWork.PurcheaseDetailReturnRepository.Update(entity: purcheasedetailreturn);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(PurcheaseDetailReturn purcheasedetailreturn) {
      unitOfWork.PurcheaseDetailReturnRepository.Update(entity: purcheasedetailreturn);
     unitOfWork.Save();
    }
    
    public void Delete(PurcheaseDetailReturn purcheasedetailreturn) {
      unitOfWork.PurcheaseDetailReturnRepository.Delete(Id: purcheasedetailreturn.Id);
      unitOfWork.Save();
    }
    
    }
}
