using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.ApplicationService.Account;
using eShop.BusinessDomain.Account.DomainObject;
using Infrastructure.Crosscutting.Utility;
using Microsoft.Web.Administration;
using System.Transactions;
using eShop.BusinessDomain.Account.ViewModel;

namespace eshop.ApplicationService.CRM
{
    public class PurchaseService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        StockService StockService = new StockService();
        TransectionHistoryService transectionHistoryService = new TransectionHistoryService();
        PurcheaseDetailService purcheaseDetailService = new PurcheaseDetailService();
        CommonRepository commonRepository = new CommonRepository();
        public ICollection<PurchaseVM> GetAllPurchase()
        {
            //try
            //{

          
            //ServerManager iisManager = new ServerManager();
            //iisManager.Sites.Add("saulhamid001", "http", "*:9999:", "d:\\saul");
            //iisManager.CommitChanges();
            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}


            var purchaseItems = (from rItem in unitOfWork.PurchaseRepository.GetAll().OrderBy(r => r.Id)
                                 join lItemS in unitOfWork.SupplierRepository.GetAll() on rItem.SupplierId equals lItemS.Id into ps
                                 from lItemS in ps.DefaultIfEmpty()
                                 //join lItemB in unitOfWork.BranchRepository.GetAll() on rItem.BranchId equals lItemB.Id into bs
                                 //from lItemB in bs.DefaultIfEmpty()
                                     //join lItemBB in unitOfWork.BankRepository.GetAll() on rItem.BankId equals lItemBB.Id into bbs
                                     //from lItemBB in bbs.DefaultIfEmpty()
                                 select new PurchaseVM
                                 {
                                     Id = rItem.Id,
                                     InvoiecNo = rItem.InvoiecNo,
                                     SupplierId = rItem.SupplierId,
                                     BranchId = rItem.BranchId,
                                     BankId = rItem.BankId,
                                     //BranchName = lItemB.BranchName,
                                     //BankName = lItemBB.BankName,
                                     SupplierName = lItemS.Name,
                                     ChequeNo = rItem.ChequeNo,
                                     PaymentType = rItem.PaymentType,
                                     EmployeeId = rItem.EmployeeId,
                                     Date = rItem.Date,
                                     Due = rItem.Due,
                                     NetTotal = rItem.NetTotal,
                                     Discount = rItem.Discount,
                                 }).OrderByDescending(e => e.Id).ToList();
            return purchaseItems;
        }
        public ICollection<PurchaseVM> GetAllPurchaseDue()
        {
            var purchaseItems = (from rItem in unitOfWork.PurchaseRepository.GetAll(e=>e.Due !=0).OrderBy(r => r.Id)
                                 join lItemS in unitOfWork.SupplierRepository.GetAll() on rItem.SupplierId equals lItemS.Id into ps
                                 from lItemS in ps.DefaultIfEmpty()
                                     //join lItemB in unitOfWork.BranchRepository.GetAll() on rItem.BranchId equals lItemB.Id into bs
                                     //from lItemB in bs.DefaultIfEmpty()
                                     //join lItemBB in unitOfWork.BankRepository.GetAll() on rItem.BankId equals lItemBB.Id into bbs
                                     //from lItemBB in bbs.DefaultIfEmpty()
                                 select new PurchaseVM
                                 {
                                     Id = rItem.Id,
                                     InvoiecNo = rItem.InvoiecNo,
                                     SupplierId = rItem.SupplierId,
                                     BranchId = rItem.BranchId,
                                     BankId = rItem.BankId,
                                     //BranchName = lItemB.BranchName,
                                     //BankName = lItemBB.BankName,
                                     SupplierName = lItemS.Name,
                                     ChequeNo = rItem.ChequeNo,
                                     PaymentType = rItem.PaymentType,
                                     EmployeeId = rItem.EmployeeId,
                                     Date = rItem.Date,
                                     Due = rItem.Due,
                                     NetTotal = rItem.NetTotal,
                                     Discount = rItem.Discount,
                                 }).ToList();
            return purchaseItems;
        }


        public PurchaseVM Get(long Id)
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var purchaseItems = unitOfWork.PurchaseRepository.Get(Id);
            var suplierdata = unitOfWork.SupplierRepository.Get((int)purchaseItems.SupplierId);
            purchaseVM.Id = purchaseItems.Id;
            purchaseVM.InvoiecNo = purchaseItems.InvoiecNo;
            purchaseVM.SupplierId = purchaseItems.SupplierId;
            purchaseVM.SupplierName = suplierdata.Name;
            purchaseVM.PresentAddress = suplierdata.PresentAddress;
            purchaseVM.Mobile = suplierdata.Mobile;
            purchaseVM.Email = suplierdata.Email;
            purchaseVM.BranchId = purchaseItems.BranchId;
            purchaseVM.BankId = purchaseItems.BankId;
            purchaseVM.ChequeNo = purchaseItems.ChequeNo;
            purchaseVM.PaymentType = purchaseItems.PaymentType;
            purchaseVM.EmployeeId = purchaseItems.EmployeeId;
            purchaseVM.Date = purchaseItems.Date;
            purchaseVM.Due = purchaseItems.Due;
            purchaseVM.NetTotal = purchaseItems.NetTotal;
            purchaseVM.Discount = purchaseItems.Discount;
            purchaseVM.TotalPaid = purchaseItems.TotalPaid;
            purchaseVM.PurcheaseDetails = purcheaseDetailService.GetAllPurcheaseDetailByPurcheasId((int)Id);
            return purchaseVM;
        }
        public dynamic GetProductDetail(long Id)
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var data = commonRepository.GetBySpWithParam("SP_Purchease", new object[] { 1, (int)Id, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value });
            return data;
        }
        public PurchaseVM GetProductDetailByInvoice(string InvoiceNumber)
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var data = commonRepository.GetBySpWithParam("SP_Purchease", new object[] { 4, DBNull.Value, InvoiceNumber, DBNull.Value, DBNull.Value, DBNull.Value });
            purchaseVM =  Converters.ConvertDataTable<PurchaseVM>(data).FirstOrDefault();
            purchaseVM.PurcheaseDetails = purcheaseDetailService.GetAllPurcheaseDetailByPurcheasId(purchaseVM.Id);
            return purchaseVM;
        }
        public dynamic GetInvoice(string InvoiceNumber)
        {
            var data = unitOfWork.PurchaseRepository.GetAll(e => e.InvoiecNo.Contains(InvoiceNumber)).Select(e => new { Text = e.InvoiecNo, Value = e.Id }).ToList();
            return data;
        }
        public dynamic GetProductDue(long Id)
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var data = commonRepository.GetBySpWithParam("SP_Purchease", new object[] { 3, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value });
            return data;
        }
        public void Add(Purchases purchase)
        {
            StockVM stockvm = new StockVM();
            TransectionHistoryVM transectionHistory = new TransectionHistoryVM();
            purchase.Total = purchase.PurcheaseDetails.Sum(e => e.Quantity * e.UnitePrice);
            purchase.NetTotal = purchase.Total - purchase.PurcheaseDetails.Sum(e => e.Discount);
            if (purchase.NetTotal > purchase.TotalPaid)
            {
                purchase.Due = purchase.NetTotal - purchase.TotalPaid;
            }
            if (purchase.NetTotal < purchase.TotalPaid)
            {
                purchase.Return = purchase.TotalPaid  - purchase.NetTotal;
            }
            purchase.InvoiecNo = GenerateInvoices();
            purchase.AddDate = DateTime.Now;
            purchase.AddBy = HttpContext.Current.User.Identity.Name;
            purchase.Status = "A";
            purchase.Date = purchase.Date;
            unitOfWork.PurchaseRepository.Add(entity: purchase);
            foreach (var item in purchase.PurcheaseDetails)
            {
                item.PurchaseId = purchase.Id;
                item.Amount = item.Quantity * item.UnitePrice - item.Discount;
                unitOfWork.PurcheaseDetailRepository.Add(entity: item);
                stockvm = new StockVM
                {
                    BranchId = purchase.BranchId,
                    Date = purchase.Date,
                    ProductId = item.ProductId,
                    PurcheaseId = purchase.Id,
                    Quantity = item.Quantity,
                    UnitPrice = item.UnitePrice,
                    SupplierId = purchase.SupplierId
                };
                StockService.AddStockProduct(stockvm,true);
                if (item.Sales > 0)
                {
                    var product= unitOfWork.ProductRepository.Get(e => e.Id == item.ProductId).FirstOrDefault();
                    product.SalsPrice = item.Sales;
                    unitOfWork.ProductRepository.Update(product);
                }
            }
            transectionHistory.AccountId = unitOfWork.AccountNameRepository.GetAll(e=>e.RootId==2 && e.PayNreciveId == purchase.SupplierId ).FirstOrDefault().PayNreciveId ?? 0;
            transectionHistory.InvoiceNo = purchase.InvoiecNo;
            transectionHistory.BankId = purchase.BankId;
            transectionHistory.Date =Convert.ToDateTime(purchase.Date);
            transectionHistory.IsRecive =false;
            transectionHistory.IsPayment =true;
            transectionHistory.PaymentAmount = purchase.TotalPaid;
            transectionHistory.PaymentType = purchase.PaymentType;
            transectionHistory.AddDate = DateTime.Now;
            transectionHistory.AddBy = HttpContext.Current.User.Identity.Name;
            transectionHistory.Remarks = "Purchease Add";
            transectionHistoryService.Add(transectionHistory);
            unitOfWork.Save();
        }

        public void Update(Purchases purchase)
        {
            StockVM stockvm = new StockVM();
            TransectionHistory transectionHistory = new TransectionHistory();
            Purchases pur = new Purchases();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    pur = unitOfWork.PurchaseRepository.Get(purchase.Id);
            pur.Total = purchase.PurcheaseDetails.Sum(e => e.Quantity * e.UnitePrice);
            pur.NetTotal = pur.Total - purchase.PurcheaseDetails.Sum(e => e.Discount);
                    if (pur.NetTotal > purchase.TotalPaid)
                    {
                        pur.Due = pur.NetTotal - purchase.TotalPaid;
                    }
                    else {
                        pur.TotalPaid = pur.NetTotal;
                    }
            if (pur.NetTotal < purchase.TotalPaid)
            {
                pur.Return = purchase.TotalPaid - purchase.NetTotal;
            }
            pur.UpdateDate = DateTime.Now;
            pur.UpdateBy = HttpContext.Current.User.Identity.Name;
            pur.Status = "A";
            pur.Date = Convert.ToDateTime(purchase.Date);
            unitOfWork.PurchaseRepository.Update(entity: pur);
            foreach (var item in purchase.PurcheaseDetails)
            {
                if (unitOfWork.PurcheaseDetailRepository.GetAll(e=>e.Id==item.Id).Any())
                {
                unitOfWork.PurcheaseDetailRepository.Delete(item.Id);
                stockvm = new StockVM
                {
                    BranchId = purchase.BranchId,
                    Date = purchase.Date,
                    ProductId = item.ProductId,
                    PurcheaseReturnId = purchase.Id,
                    Quantity = item.Quantity,
                    UnitPrice = item.UnitePrice,
                    SupplierId = purchase.SupplierId
                };
                StockService.AddStockProduct(stockvm, false);
                }
            }
            foreach (var item in purchase.PurcheaseDetails)
            {
                item.PurchaseId = purchase.Id;
                item.Amount = item.Quantity * item.UnitePrice - item.Discount;
                unitOfWork.PurcheaseDetailRepository.Add(entity: item);
                stockvm = new StockVM
                {
                    BranchId = purchase.BranchId,
                    Date = purchase.Date,
                    ProductId = item.ProductId,
                    PurcheaseReturnId = purchase.Id,
                    Quantity = item.Quantity,
                    UnitPrice = item.UnitePrice,
                    SupplierId = purchase.SupplierId
                };
                StockService.AddStockProduct(stockvm, true);
                if (item.Sales > 0)
                {
                    var product = unitOfWork.ProductRepository.Get(e => e.Id == item.ProductId).FirstOrDefault();
                    product.SalsPrice = item.Sales;
                    unitOfWork.ProductRepository.Update(product);
                }
            }
            var date = Convert.ToDateTime(purchase.Date);
            transectionHistory = unitOfWork.TransectionHistoryRepository.Get(e=>e.AccountId == purchase.SupplierId && e.Date == date ).FirstOrDefault();
            transectionHistory.AccountId = purchase.SupplierId;
                    transectionHistory.InvoiceNo = purchase.InvoiecNo;
                    transectionHistory.BankId = purchase.BankId;
            transectionHistory.Date = Convert.ToDateTime(purchase.Date);
            transectionHistory.IsRecive = false;
            transectionHistory.IsPayment = true;
            transectionHistory.PaymentAmount = pur.NetTotal;
            transectionHistory.PaymentType = purchase.PaymentType;
            transectionHistory.UpdateDate = DateTime.Now;
            transectionHistory.UpdateBy = HttpContext.Current.User.Identity.Name;
                    transectionHistory.Remarks = "Purchease Return Updated";
                    unitOfWork.TransectionHistoryRepository.Update(transectionHistory);

            unitOfWork.Save();
                scope.Complete();
            }
          }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GenerateInvoice()
        {
            var data = unitOfWork.PurchaseRepository.GetAll().LastOrDefault() ?? new Purchases();

            if (String.IsNullOrWhiteSpace(data.InvoiecNo))
            {
                data.InvoiecNo = "0001";
            }
            data.InvoiecNo = (Convert.ToInt32(data.InvoiecNo) + 1).ToString("0000");
            return data.InvoiecNo.ToString();
        }
        public void ChangeStatus(Purchases purchase)
        {
            purchase.UpdateDate = DateTime.Now;
            purchase.UpdateBy = HttpContext.Current.User.Identity.Name;
            purchase.Status = purchase.Status == "A" ? "D" : "A";
            unitOfWork.PurchaseRepository.Update(entity: purchase);
            unitOfWork.Save();
        }

        public void Delete(Purchases purchase)
        {
            StockVM stockvm = new StockVM();
            TransectionHistory transectionHistory = new TransectionHistory();
            
           var pur = Get((long)purchase.Id);
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                   unitOfWork.PurchaseRepository.Delete(pur.Id);
                    foreach (var item in pur.PurcheaseDetails)
                    {
                        if (unitOfWork.PurcheaseDetailRepository.GetAll(e => e.Id == item.Id).Any())
                        {
                            unitOfWork.PurcheaseDetailRepository.Delete(item.Id);
                            stockvm = new StockVM
                            {
                                BranchId = pur.BranchId,
                                Date = pur.Date,
                                ProductId = item.ProductId,
                                PurcheaseId = pur.Id,
                                Quantity = item.Quantity,
                                UnitPrice = item.UnitePrice,
                                SupplierId = pur.SupplierId
                            };
                            StockService.AddStockProduct(stockvm, false);
                        }
                    }
                  
                    var date = Convert.ToDateTime(pur.Date);
                    transectionHistory = unitOfWork.TransectionHistoryRepository.Get(e => e.AccountId == pur.SupplierId && e.Date == date).FirstOrDefault();
                    unitOfWork.TransectionHistoryRepository.Delete(transectionHistory.Id);
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GenerateInvoices()
        {
            //var setting = unitOfWork.SettingRepository.GetAll().FirstOrDefault();
            //var Prefix = setting.PurcheasePrefix;
            //var Postfix = setting.PurcheasePostfix;
            //var Number = setting.PurcheaseStart;
            //var data = unitOfWork.PurchaseRepository.GetAll().LastOrDefault() ?? new Purchases();
            //if (String.IsNullOrWhiteSpace(data.InvoiecNo))
            //{
            //    data.InvoiecNo = Prefix + (1).ToString("00000").PadLeft(5) + Postfix;
            //}
            //else
            //{
            //    if (!String.IsNullOrWhiteSpace(Prefix))
            //    {
            //        data.InvoiecNo = data.InvoiecNo.Substring(data.InvoiecNo.Length, data.InvoiecNo.Length - Prefix.Length);
            //    }
            //    if (!String.IsNullOrWhiteSpace(Postfix))
            //    {
            //        data.InvoiecNo = data.InvoiecNo.Substring(0, data.InvoiecNo.Length - Postfix.Length);
            //    }
            //    if (!String.IsNullOrWhiteSpace(data.InvoiecNo))
            //    {
            //        data.InvoiecNo = (Convert.ToInt32(data.InvoiecNo) + 1).ToString("00000");
            //        data.InvoiecNo = Prefix + data.InvoiecNo + Postfix;
            //    }
            //}
            var data = unitOfWork.PurchaseRepository.GetAll(e=>e.InvoiecNo.Length == 12).LastOrDefault() ?? new Purchases();
            var InvoiecNo = data.InvoiecNo;
            if (String.IsNullOrWhiteSpace(InvoiecNo))
            {
                InvoiecNo = (10).ToString()+ DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + (1).ToString("0000");
            }
            else
            {
                InvoiecNo = (10).ToString()+ DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + (Convert.ToInt32(InvoiecNo.Substring(8, 4)) + 1).ToString("0000");
            }
            return InvoiecNo.ToString();
        }
        public dynamic PerchesReport(PurchaseReportVM purchase)
        {
             object[] param = new object[8];
            param[0] = 1;
            param[1] = purchase.InvoiecNo;
            param[2] = purchase.SupplierId;
            param[3] = purchase.BranchId;
            param[4] = purchase.MinAmunt;
            param[5] = purchase.MaxAmount;
            param[6] = null;
            param[7] = null;
            if (purchase.DateFrom != null)
                param[6] = Convert.ToDateTime(purchase.DateFrom);
            if (purchase.DateTo != null)
                param[7] = Convert.ToDateTime(purchase.DateTo);
            var data = commonRepository.GetBySpWithParam("SP_PurchaseReport", new object[] { 1, purchase.InvoiecNo, purchase.SupplierId, purchase.BranchId, purchase.MinAmunt, purchase.MaxAmount,purchase.DateFrom,purchase.DateTo});
            return data;
        }
    }
}
