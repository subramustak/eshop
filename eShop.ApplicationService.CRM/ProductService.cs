using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
using System.Web;
using Infrastructure.Crosscutting.Utility;
using System.Data;
using eShop.BusinessDomain.Sales.ViewModels;

namespace eshop.ApplicationService.CRM
{
    public class ProductService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        CommonRepository commonRepository = new CommonRepository();

        public ICollection<ProductVM> GetAllProduct()
        {
            List<ProductVM> productVMs = new List<ProductVM>();
            productVMs = (from rItem in unitOfWork.ProductRepository.GetAll().OrderByDescending(r => r.Id)
                          join lItemSize in unitOfWork.SizeRepository.GetAll() on rItem.SizeId equals lItemSize.Id into ps
                          from lItemSize in ps.DefaultIfEmpty()
                          join lItemColor in unitOfWork.ColorRepository.GetAll() on rItem.ColorId equals lItemColor.Id into pColor
                          from lItemColor in pColor.DefaultIfEmpty()
                          join lItemBrand in unitOfWork.BrandRepository.GetAll() on rItem.SizeId equals lItemBrand.Id into pBrand
                          from lItemBrand in pBrand.DefaultIfEmpty()
                          join lItemUOM in unitOfWork.UOMRepository.GetAll() on rItem.UOMId equals lItemUOM.Id into pUOM
                          from lItemUOM in pUOM.DefaultIfEmpty()
                          join lItemCategory in unitOfWork.CategoryRepository.GetAll() on rItem.SizeId equals lItemCategory.Id into pCategory
                          from lItemCategory in pCategory.DefaultIfEmpty()
                          select new ProductVM
                          {
                              Id = rItem.Id,
                              Name = rItem.Name,
                              Code = rItem.Code,
                              MinimumStock = rItem.MinimumStock,
                              OtherCost = rItem.OtherCost,
                              Discount = rItem.Discount,
                              UnitePrice = rItem.UnitePrice,
                              Quantity = rItem.Quantity,
                              OpeningQuantity = rItem.OpeningQuantity,
                              SalsPrice = rItem.SalsPrice,
                              IsPercentage = rItem.IsPercentage,
                              IsFixed = rItem.IsFixed,
                              ColorId = rItem.ColorId,
                              SizeId = rItem.SizeId,
                              MaterialId = rItem.MaterialId,
                              SubCatagoryId = rItem.SubCatagoryId,
                              UOMId = rItem.UOMId,
                              ProductBrandId = rItem.ProductBrandId,
                              IsArchived = rItem.IsArchived,
                              IsDeleted = rItem.IsDeleted,
                              AddBy = rItem.AddBy,
                              AddDate = rItem.AddDate,
                              UpdateBy = rItem.UpdateBy,
                              UpdateDate = rItem.UpdateDate,
                              Remarks = rItem.Remarks,
                              Status = rItem.Status,
                              Image = rItem.Image,
                              Barcode = rItem.Barcode,
                              VATPercent = rItem.VATPercent,
                              ProductBrandName = lItemBrand?.BrandName ?? "",
                              ProductColorName = lItemColor?.ColorName ?? "",
                              ProductSizeName = lItemSize?.SizeName ?? "",
                              UOMName = lItemUOM?.UOMName ?? "",
                              CatagoryName = lItemCategory?.CatagoryName ?? "",
                          }).ToList();

            return productVMs;
        }
        public ICollection<ProductVM> GetAllProductByFilter(ProductVM pVM)
        {
            var paramstring = string.Empty;
            if (pVM.Id != 0)
            {
                paramstring += " and  p.Id=" + pVM.Id.ToString();
            }
            if (pVM.ColorId != 0 && pVM.ColorId != null)
            {
                paramstring += " and  p.ColorId=" + pVM.ColorId.ToString();
            }
            if (pVM.SizeId != 0 && pVM.SizeId != null)
            {
                paramstring += " and  p.SizeId=" + pVM.SizeId.ToString();
            }
            if (pVM.CatagoryId != 0 && pVM.CatagoryId != null)
            {
                paramstring += " and  p.CatagoryId=" + pVM.CatagoryId.ToString();
            }
            if (pVM.SubCatagoryId != 0 && pVM.SubCatagoryId != null)
            {
                paramstring += " and  p.SubCatagoryId=" + pVM.SubCatagoryId.ToString();
            }
            if (pVM.UOMId != 0 && pVM.UOMId != null)
            {
                paramstring += " and  p.UOMId=" + pVM.UOMId.ToString();
            }
            if (pVM.ProductBrandId != 0 && pVM.ProductBrandId != null)
            {
                paramstring += " and  p.ProductBrandId=" + pVM.ProductBrandId.ToString();
            }
            var data = commonRepository.GetBySpWithParam("[SP_Product]", new object[] { 1, paramstring });
            var Items = Converters.ConvertDataTable<ProductVM>(data);
            return Items;
        }
        public ICollection<ProductVM> GetAllProductByFilter(int? ProId, int? CategoryId)
        {
            List<ProductVM> productVMs = new List<ProductVM>();
            productVMs = (from rItem in unitOfWork.ProductRepository.GetAll(e => e.Id == ProId || e.CatagoryId == CategoryId).OrderBy(r => r.Id)
                          select new ProductVM
                          {
                              Id = rItem.Id,
                              Name = rItem.Name,
                              Code = rItem.Code,
                              Barcode = rItem.Barcode,
                              MinimumStock = rItem.MinimumStock,
                              OtherCost = rItem.OtherCost,
                              Discount = rItem.Discount,
                              UnitePrice = rItem.UnitePrice,
                              Quantity = rItem.Quantity,
                              OpeningQuantity = rItem.OpeningQuantity,
                              SalsPrice = rItem.SalsPrice,
                              IsPercentage = rItem.IsPercentage,
                              IsFixed = rItem.IsFixed,
                              ColorId = rItem.ColorId,
                              SizeId = rItem.SizeId,
                              MaterialId = rItem.MaterialId,
                              Image = rItem.Image,
                              UOMId = rItem.UOMId,
                              ProductBrandId = rItem.ProductBrandId,
                              IsArchived = rItem.IsArchived,
                              IsDeleted = rItem.IsDeleted,
                              AddBy = rItem.AddBy,
                              AddDate = rItem.AddDate,
                              UpdateBy = rItem.UpdateBy,
                              UpdateDate = rItem.UpdateDate,
                              Remarks = rItem.Remarks,
                              Status = rItem.Status,
                              VATPercent = rItem.VATPercent,

                          }).ToList();
            return productVMs;
        }

        public ProductVM Get(long Id)
    {
     ProductVM productVM = new ProductVM();
           
                string name = string.Empty;
                DataTable data = commonRepository.GetDatatableBySQL("select * from VMProduct  where Id=" + Id);
                if (data.Rows.Count > 0)
                {
                productVM = Converters.ConvertDataTable<ProductVM>(data).ToList().FirstOrDefault();
            }

            return productVM;
        }

        public void Add(Product product)
        {
            var AllProduct = unitOfWork.ProductRepository.GetAll();
            // if (AllProduct.Where(e => e.Name == product.Name).Any()) throw new Exception("Product Name Already Exist");
            if (AllProduct.Where(e => e.Code == product.Code).Any()) throw new Exception("Product Code Already Exist");
            if (AllProduct.Any())
            {
                var lastcode = AllProduct.Last().Barcode;
                if (lastcode == null)
                {
                    product.Barcode = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + (1).ToString("00000");
                }
                else
                {
                    int laststing = Convert.ToInt32(Converters.GetLast(lastcode, 5));
                    product.Barcode = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + (laststing + 1).ToString("00000");
                }
            }
            else
            {
                product.Barcode = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + (1).ToString("00000");
            }
            product.AddDate = DateTime.Now;
            product.AddBy = HttpContext.Current.User.Identity.Name;
            product.Status = "A";
            unitOfWork.ProductRepository.Add(entity: product);
            unitOfWork.Save();
        }

        public void Update(Product product)
        {
            var AllProduct = unitOfWork.ProductRepository.GetAll();
            if (AllProduct.Where(e => e.Id != product.Id && e.Name == product.Name).Any()) throw new Exception("Product Name Already Exist");
            var products = unitOfWork.ProductRepository.Get(product.Id);
            products.IsArchived = product.IsArchived;
            products.ProductBrandId = product.ProductBrandId;
            products.UOMId = product.UOMId;
            products.SubCatagoryId = product.SubCatagoryId;
            products.MaterialId = product.MaterialId;
            products.SizeId = product.SizeId;
            products.ColorId = product.ColorId;
            products.IsFixed = product.IsFixed;
            products.IsPercentage = product.IsPercentage;
            products.SalsPrice = product.SalsPrice;
            products.OpeningQuantity = product.OpeningQuantity;
            products.Quantity = product.Quantity;
            products.UnitePrice = product.UnitePrice;
            products.Discount = product.Discount;
            products.OtherCost = product.OtherCost;
            products.Name = product.Name;
            products.Code = product.Code;
            products.VATPercent = product.VATPercent;
            products.MinimumStock = product.MinimumStock;
            products.UpdateDate = DateTime.Now;
            products.UpdateBy = HttpContext.Current.User.Identity.Name;
            product.Status = "A";
            unitOfWork.ProductRepository.Update(entity: product);
            unitOfWork.Save();
        }

        public void ChangeStatus(Product product)
        {
            product.UpdateDate = DateTime.Now;
            product.UpdateBy = HttpContext.Current.User.Identity.Name;
            product.Status = product.Status == "A" ? "D" : "A";
            unitOfWork.ProductRepository.Update(entity: product);
            unitOfWork.Save();
        }

        public void Delete(Product product)
        {
            try
            {

           
            if (unitOfWork.StockRepository.GetAll(e=>e.ProductId==product.Id).Any())
            {
                throw new Exception("Stock Exist In stock");
            }
            unitOfWork.ProductRepository.Delete(Id: product.Id);
            unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic GetProductName(string Name)
        {
            var product = unitOfWork.ProductRepository.GetAll(e => e.Name.ToLower().StartsWith(Name.ToLower()) || e.Code.ToLower().StartsWith(Name.ToLower())).ToList();
            var data = (from p in product

                        select new { Text = p.Code + " " + p.Name, Value = p.Id }).ToList();
            return data;
        }
        public ICollection<ProductVM> ScarchProduct(string ScarchString)
        {
            List<ProductVM> productVMs = new List<ProductVM>();
            productVMs = (from rItem in unitOfWork.ProductRepository.GetAll(e => e.Name.Contains(ScarchString)).OrderBy(r => r.Id)
                          join lItemSize in unitOfWork.SizeRepository.GetAll() on rItem.SizeId equals lItemSize.Id into ps
                          from lItemSize in ps.DefaultIfEmpty()
                          join lItemColor in unitOfWork.ColorRepository.GetAll() on rItem.ColorId equals lItemColor.Id into pColor
                          from lItemColor in pColor.DefaultIfEmpty()
                          join lItemBrand in unitOfWork.BrandRepository.GetAll() on rItem.SizeId equals lItemBrand.Id into pBrand
                          from lItemBrand in pBrand.DefaultIfEmpty()
                          join lItemUOM in unitOfWork.UOMRepository.GetAll() on rItem.SizeId equals lItemUOM.Id into pUOM
                          from lItemUOM in pUOM.DefaultIfEmpty()
                          join lItemCategory in unitOfWork.CategoryRepository.GetAll() on rItem.SizeId equals lItemCategory.Id into pCategory
                          from lItemCategory in pCategory.DefaultIfEmpty()
                          select new ProductVM
                          {
                              Id = rItem.Id,
                              Name = rItem.Name,
                              Code = rItem.Code,
                              MinimumStock = rItem.MinimumStock,
                              OtherCost = rItem.OtherCost,
                              Discount = rItem.Discount,
                              UnitePrice = rItem.UnitePrice,
                              Quantity = rItem.Quantity,
                              OpeningQuantity = rItem.OpeningQuantity,
                              SalsPrice = rItem.SalsPrice,
                              IsPercentage = rItem.IsPercentage,
                              IsFixed = rItem.IsFixed,
                              ColorId = rItem.ColorId,
                              SizeId = rItem.SizeId,
                              MaterialId = rItem.MaterialId,
                              SubCatagoryId = rItem.SubCatagoryId,
                              UOMId = rItem.UOMId,
                              ProductBrandId = rItem.ProductBrandId,
                              IsArchived = rItem.IsArchived,
                              IsDeleted = rItem.IsDeleted,
                              AddBy = rItem.AddBy,
                              AddDate = rItem.AddDate,
                              UpdateBy = rItem.UpdateBy,
                              UpdateDate = rItem.UpdateDate,
                              Remarks = rItem.Remarks,
                              VATPercent = rItem.VATPercent,
                              Status = rItem.Status,
                              ProductBrandName = lItemBrand?.BrandName ?? "",
                              ProductColorName = lItemColor?.ColorName ?? "",
                              ProductSizeName = lItemSize?.SizeName ?? "",
                              UOMName = lItemUOM?.UOMName ?? "",
                              CatagoryName = lItemCategory?.CatagoryName ?? "",
                          }).ToList();

            return productVMs;
        }
        public ICollection<ProductVM> ProdutReport(ProductSearchVM product)
        {
            var productList = commonRepository.GetBySpWithParam("SP_ProductReoort", new object[] { 1, product.ProductId, product.StockFrom,product.StockTo, product.CatagoryId,product.SubCatagoryId,product.BrandId
            ,product.UOMId,product.SizeId,product.PriceFrom,product.PriceTo,product.IsImage});
            var productVMs = Converters.ConvertDataTable<ProductVM>(productList);
            return productVMs;
        }
        public List<ProductVM> SelectedProdutBarcode(string productIds)
        {
           
            var productList = commonRepository.GetBySpWithParam("SP_ProductforBarcode", new object[] { 1,productIds});
            var productVMs = Converters.ConvertDataTable<ProductVM>(productList);
            return productVMs;
        }
        public dynamic ProdutPurcheas(int Id)
        {
            var productList = commonRepository.GetBySpWithParam("SP_Product", new object[] { 3, Id });
            var productVMs = Converters.ConvertDataTable<PurcheaseDetails>(productList);
            return productVMs;
        }
        public dynamic ProdutSales(int Id)
        {
            var productList = commonRepository.GetBySpWithParam("SP_Product", new object[] { 4, Id });
            var productVMs = Converters.ConvertDataTable<PurcheaseDetails>(productList);
            return productVMs;
        }
        public dynamic ProdutStock(int Id)
        {
            var productList = commonRepository.GetBySpWithParam("SP_Product", new object[] { 5, Id });
            var productVMs = Converters.ConvertDataTable<PurcheaseDetailVM>(productList);
            return productVMs;
        }
    }
}
