using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
using Infrastructure.Crosscutting.Utility;
using eshop.ApplicationService.MasterSetUp;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using System.Data;
using System.Globalization;

namespace eshop.ApplicationService.CRM
{
    public class StockService
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        CommonRepository commonRepository = new CommonRepository();
        ProductService ProductService = new ProductService();
        BranchService branchService = new BranchService();
        #region Stock

        public ICollection<StockVM> GetAllStock(int BranchId)
        {
            var data = commonRepository.GetBySpWithParam("GetStockAll",new object[] {BranchId });
            var stockItems = Converters.ConvertDataTable<StockVM>(data);
            return stockItems;
        }
        public ICollection<StockVM> GetStockByProductId(int Id)
        {
            var data = commonRepository.GetBySpWithParam("GetStockAll", new object[] { Id });
            var stockItems = Converters.ConvertDataTable<StockVM>(data);
            return stockItems;
        }

        public StockVM Get(long Id)
        {
            StockVM stockVM = new StockVM();
            var stockItems = unitOfWork.StockRepository.Get(Id);
            stockVM.Id = stockItems.Id;
            stockVM.ProductId = stockItems.ProductId;
            stockVM.BranchId = stockItems.BranchId;
            stockVM.TotalPaid = stockItems.TotalPaid;
            stockVM.TotalPrice = stockItems.TotalPrice;
            stockVM.GrandTotal = stockItems.GrandTotal;
            stockVM.Date = stockItems.Date;
            stockVM.UnitPrice = stockItems.UnitPrice;
            stockVM.OpeningQuantity = stockItems.OpeningQuantity;
            stockVM.Quantity = stockItems.Quantity;
            stockVM.IsArchived = stockItems.IsArchived;
            stockVM.IsDeleted = stockItems.IsDeleted;
            stockVM.AddBy = stockItems.AddBy;
            stockVM.AddDate = stockItems.AddDate;
            stockVM.UpdateBy = stockItems.UpdateBy;
            stockVM.UpdateDate = stockItems.UpdateDate;
            stockVM.Remarks = stockItems.Remarks;
            stockVM.Status = stockItems.Status;
            return stockVM;
        }

        public static object GetProductDetail(int id)
        {
            throw new NotImplementedException();
        }

        public void Add(Stocks stock)
        {
            stock.AddDate = DateTime.Now;
            stock.AddBy = HttpContext.Current.User.Identity.Name;
            stock.Status = "A";
            unitOfWork.StockRepository.Add(entity: stock);
            unitOfWork.Save();
        }
        public void AddDetail(StockVM stock)
        {
            Stocks stocks = new Stocks();
            stock.AddDate = DateTime.Now;
            stock.AddBy = HttpContext.Current.User.Identity.Name;
            stock.Status = "A";
            unitOfWork.StockRepository.Add(entity: stocks);
            unitOfWork.Save();
        }
        public void AddStockProduct(StockVM stockVM, bool PM)
        {
            try
            {
                Stocks stocks = new Stocks();
                var stockdetail = unitOfWork.StockRepository.GetAll(e => e.ProductId == stockVM.ProductId && e.BranchId==stockVM.BranchId).FirstOrDefault();
                if (stockdetail != null)
                {
                    stocks = stockdetail;
                }
                if (stockVM.UnitPrice != null)
                {
                    stocks.UnitPrice = stockVM.UnitPrice;
                }
                stocks.Date = stockVM.Date;
                stocks.ProductId = stockVM.ProductId;
                stocks.TotalPrice = stockVM.Quantity * stockVM.UnitPrice;
                stocks.BranchId = stockVM.BranchId;
                stocks.Date = DateTime.Now;

                if (PM == false && stocks.Quantity < stockVM.Quantity)
                {
                    throw new Exception("Stock Limit Cross");
                }
                if (stockVM.UnitPrice != null)
                {
                    stocks.UnitPrice = stockVM.UnitPrice;
                }
                if (stockdetail == null)
                {
                    stocks.Quantity = stockVM.Quantity;
                    stocks.AddDate = DateTime.Now;
                    stocks.AddBy = HttpContext.Current.User.Identity.Name;
                    stocks.Status = "A";
                    unitOfWork.StockRepository.Add(entity: stocks);
                }
                else
                {
                    stocks.Quantity = PM == true ? (stocks.Quantity + stockVM.Quantity) : (stocks.Quantity - stockVM.Quantity);
                    stocks.UpdateDate = DateTime.Now;
                    stocks.UpdateBy = HttpContext.Current.User.Identity.Name;
                    stocks.Status = "A";
                    unitOfWork.StockRepository.Update(entity: stocks);
                }
                StockDetail stockDetail = new StockDetail();
                stockDetail.CustomerId = stockVM.CustomerId;
                stockDetail.ProductId = stockVM.ProductId;
                stockDetail.PurcheaseId = stockVM.PurcheaseId;
                stockDetail.PurcheaseReturnId = stockVM.PurcheaseReturnId;
                stockDetail.SalesId = stockVM.SalesId;
                stockDetail.SalesReturnId = stockVM.SalesReturnId;
                stockDetail.EmployeeId = stockVM.EmployeeId;
                stockDetail.StockDiscount = stockVM.Discount;
                stockDetail.StockQuantity = stockVM.Quantity;
                stockDetail.StockPrice = stockVM.UnitPrice;
                stockDetail.AddDate = DateTime.Now;
                stockDetail.AddBy = HttpContext.Current.User.Identity.Name;
                stockDetail.Status = "A";
                unitOfWork.StockdetailRepository.Add(stockDetail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public dynamic GetProductName(string Name,int BranchId)
        {
            try
            {
                var product = commonRepository.GetBySpWithParam("[SP_GetProduct]", new object[] { 2, Name,BranchId });
                return product;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public dynamic GetStockByProduct(string Id ,int BranchId)
        {
            var product = commonRepository.GetDatatableBySQL("select (p.Code+ ' '+p.Name) as ProductName ,p.EventId,p.Id as ProductId, p.UnitePrice,p.Discount,p.SalsPrice,p.DsicountPercent,p.DsicountAmount,p.DsicountCount,p.VATPercent,p.EVstartDate,p.EVendDate,p.stockQuantity as Quantity, p.BranchId from dbo.[VMProductWithStock] p where (Id =" + Id+ " or p.Barcode  = '" + Convert.ToInt64(Id) + "') and p.BranchId=" + BranchId);
            var products = Converters.ConvertDataTable<SalesDetailVM>(product).FirstOrDefault();
            if (products.Quantity == 00)
            {
                throw new Exception("Out Of Stock this Product");
            }
            var EVstartDate = Convert.ToDateTime(products.EVstartDate);
            var EVendDate = Convert.ToDateTime(products.EVendDate);
            if (EVstartDate <= DateTime.Now && EVendDate >= DateTime.Now)
            {
                
            }
            else {
                products.DsicountAmount = 0;
                products.DsicountCount = 0;
                products.DsicountPercent = false;
            }
            products.Quantity = 1;
            return products;
        }
        public dynamic GetStockByProductByBarCode(string Barcode, int BranchId)
        {
            var product = commonRepository.GetDatatableBySQL("select (p.Code+ ' '+p.Name) as ProductName ,p.EventId,p.UnitePrice,p.Discount,p.SalsPrice,p.DsicountPercent,p.DsicountAmount,p.DsicountCount,p.VATPercent,p.EVstartDate,p.EVendDate,p.stockQuantity as Quantity, p.BranchId from dbo.[VMProductWithStock] p where  p.Barcode  = '" +Convert.ToInt64(Barcode) + "' and p.BranchId=" + BranchId);
            var products = Converters.ConvertDataTable<SalesDetailVM>(product).FirstOrDefault();
            if (products.Quantity == 00)
            {
                throw new Exception("Out Of Stock this Product");
            }
            var EVstartDate = Convert.ToDateTime(products.EVstartDate);
            var EVendDate = Convert.ToDateTime(products.EVendDate);
            if (EVstartDate <= DateTime.Now && EVendDate >= DateTime.Now)
            {

            }
            else
            {
                products.DsicountAmount = 0;
                products.DsicountCount = 0;
                products.DsicountPercent = false;
            }
            products.Quantity = 1;
            return products;
        }

        public void Update(Stocks stock)
        {
            stock.UpdateDate = DateTime.Now;
            stock.UpdateBy = HttpContext.Current.User.Identity.Name;
            stock.Status = "A";
            unitOfWork.StockRepository.Update(entity: stock);
            unitOfWork.Save();
        }

        public void ChangeStatus(Stocks stock)
        {
            stock.UpdateDate = DateTime.Now;
            stock.UpdateBy = HttpContext.Current.User.Identity.Name;
            stock.Status = stock.Status == "A" ? "D" : "A";
            unitOfWork.StockRepository.Update(entity: stock);
            unitOfWork.Save();
        }

        public void Delete(Stocks stock)
        {
            unitOfWork.StockRepository.Delete(Id: stock.Id);
            unitOfWork.Save();
        }
        #endregion
        #region Stock Report
        public dynamic GetStockReportData(StockReportVM selseReportVM)
        {
            object[] param = new object[7];
            param[0] = 1;
            param[1] = selseReportVM.ProductId;
            param[2] = selseReportVM.BranchId;
            param[3] = selseReportVM.StockMin;
            param[4] = selseReportVM.StockMax;
            param[5] = selseReportVM.PriceMin;
            param[6] = selseReportVM.PriceMax;
            DataTable dt = new DataTable();
            dt = commonRepository.GetBySpWithParam("SP_StockReport", param);
            return dt;
        }
        #endregion
        #region StockTransfer
        public List<BranchVM> getAllActiveBranches()
        {
            return branchService.GetAllItems().Where(e => e.IsDeleted == false && e.Status == "A").ToList();
        }
        public dynamic GetProductsbyBranchId(string Name, int BranchId)
        {
            var product = commonRepository.GetBySpWithParam("[SP_GetProductbyBranchId]", new object[] { 1, BranchId, Name });
            return product;
        }
        public dynamic TransfarProducts(StockTransfarVM stockTransfarVM)
        {
            try
            {
                int roweffect = 0;
                foreach (var item in stockTransfarVM.SalesDetails)
                {
                        StockTransfars stockTransfars = new StockTransfars();
                        stockTransfars.ProductId = item.ProdutId;
                        stockTransfars.FromBranchId = stockTransfarVM.FromBranchId;
                        stockTransfars.ToBranchId = stockTransfarVM.ToBranchId;
                        stockTransfars.Quantity = item.Quantity;
                        stockTransfars.Date = DateTime.Now.ToString("MM/dd/yyyy");
                        stockTransfars.IsApprove = false;
                        stockTransfars.AddDate = DateTime.Now;
                        stockTransfars.AddBy = HttpContext.Current.User.Identity.Name;
                        stockTransfars.Status = "A";
                        unitOfWork.StockTransfarRepository.Add(entity: stockTransfars);
                        
                    StockVM stockvm = new StockVM
                    {
                        BranchId = stockTransfarVM.FromBranchId,
                        ProductId = item.ProdutId,
                        Quantity = item.Quantity,
                    };
                    AddStockProduct(stockvm, false);
                     stockvm = new StockVM
                    {
                         BranchId = stockTransfarVM.ToBranchId,
                         ProductId = item.ProdutId,
                         Quantity = item.Quantity
                     };
                    AddStockProduct(stockvm, true);
                    unitOfWork.Save();
                }
                return roweffect;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public dynamic TransfarProductList(StockTransfarReportVM stockTransfarReportVM)
        {
            object[] param = new object[5];
            param[0] = 1;
            param[1] = stockTransfarReportVM.FromBranchId;
            param[2] = stockTransfarReportVM.ToBranchId;
            param[3] = null;
            param[4] = null;
            if (stockTransfarReportVM.FromDate != null)
                param[3] = Convert.ToDateTime(stockTransfarReportVM.FromDate);
            if (stockTransfarReportVM.ToDate != null)
                param[4] = Convert.ToDateTime(stockTransfarReportVM.ToDate);

            var product = commonRepository.GetBySpWithParam("[SP_StockTransfarReport]", param);
            return product;
        }
        #endregion
    }
}
