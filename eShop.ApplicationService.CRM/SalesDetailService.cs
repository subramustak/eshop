using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
using System.Web.Configuration;

namespace eshop.ApplicationService.CRM
{
 public class SalesDetailService
  {
        StockService stockService = new StockService();
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<SalesDetailVM> GetAllSalesDetail()
   {
    var salesdetailItems = (from rItem in unitOfWork.SalesDetailRepository.GetAll().OrderBy(r => r.Id)
                                  select new SalesDetailVM
                                 {
      Id = rItem.Id,
      SalesId = rItem.SalesId,
      ProductId = rItem.ProductId,
      Discount = rItem.Discount,
      UnitePrice = rItem.UnitePrice,
      Quantity = rItem.Quantity,
      Return = rItem.Return,
                                      Amount = rItem.Amount,
                                    }).ToList();
    return salesdetailItems;
    }
        public ICollection<SalesDetailVM> GetAllSalesDetailBySalesId(int SalesId)
        {
            var salesdetailItems = (from rItem in unitOfWork.SalesDetailRepository.GetAll(e=>e.SalesId== SalesId).OrderBy(r => r.Id)
                                    join lItemproduct in unitOfWork.ProductRepository.GetAll() on rItem.ProductId equals lItemproduct.Id into pProduct
                                    from lItemproduct in pProduct.DefaultIfEmpty()
                                    join lItemEvent in unitOfWork.EventRepository.GetAll() on lItemproduct.EventId equals lItemEvent.Id into pEvent
                                    from lItemEvent in pEvent.DefaultIfEmpty()
                                    select new SalesDetailVM
                                    {
                                        Id = rItem.Id,
                                        SalesId = rItem.SalesId,
                                        ProductId = rItem.ProductId,
                                        ProductName = lItemproduct.Name,
                                        EventId = lItemEvent?.Id ?? 0,
                                        EventName = lItemEvent?.EventName ?? "",
                                        Discount = rItem.Discount,
                                        UnitePrice = rItem.UnitePrice,
                                        Quantity = rItem.Quantity,
                                        Return = rItem.Return,
                                        Amount = rItem.Amount,
                                    }).ToList();
            return salesdetailItems;
        }

        public SalesDetailVM Get(long Id)
    {
     SalesDetailVM salesdetailVM = new SalesDetailVM();
    var salesdetailItems = unitOfWork.SalesDetailRepository.Get(Id);
salesdetailVM.Id = salesdetailItems.Id ;
salesdetailVM.SalesId = salesdetailItems.SalesId ;
salesdetailVM.ProductId = salesdetailItems.ProductId ;
salesdetailVM.Discount = salesdetailItems.Discount ;
salesdetailVM.UnitePrice = salesdetailItems.UnitePrice ;
salesdetailVM.Quantity = salesdetailItems.Quantity ;
salesdetailVM.Return = salesdetailItems.Return ;
salesdetailVM.Amount = salesdetailItems.Amount ;
    return salesdetailVM;
    }
    
    public void Add(SalesDetails salesdetail) {

      unitOfWork.SalesDetailRepository.Add(entity: salesdetail);
           
     unitOfWork.Save();
    }
    
    public void Update(SalesDetails salesdetail) {
      unitOfWork.SalesDetailRepository.Update(entity: salesdetail);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(SalesDetails salesdetail) {
      unitOfWork.SalesDetailRepository.Update(entity: salesdetail);
     unitOfWork.Save();
    }
    
    public void Delete(SalesDetails salesdetail) {
      unitOfWork.SalesDetailRepository.Delete(Id: salesdetail.Id);
      unitOfWork.Save();
    }
        
    }
}
