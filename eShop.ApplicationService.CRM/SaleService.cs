using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.MasterSetUp.ViewModels;
using Infrastructure.Crosscutting.Utility;
using eShop.BusinessDomain.Account.DomainObject;
using System.Transactions;
using System.Data;
using System.Configuration;
using System.IO;
using Microsoft.Web.Administration;
using eShop.BusinessDomain.Account.ViewModel;
using eShop.ApplicationService.Account;

namespace eshop.ApplicationService.CRM
{
    public class SaleService
    {
        SalesDetailService SalesDetailService = new SalesDetailService();
        StockService StockService = new StockService();
        CommonRepository commonRepository = new CommonRepository();
        TransectionHistoryService transectionHistoryService = new TransectionHistoryService();
        UnitOfWork unitOfWork = new UnitOfWork();

        public ICollection<SaleVM> GetAllSale()
        {
            var saleItems = (from rItem in unitOfWork.SaleRepository.GetAll().OrderBy(r => r.Id)
                             join lItemCustomer in unitOfWork.CustomerRepository.GetAll() on rItem.CustomerId equals lItemCustomer.Id into cus
                             from lItemCustomer in cus.DefaultIfEmpty()
                             join lItemEmployee in unitOfWork.EmployeeRepository.GetAll() on rItem.EmployeeId equals lItemEmployee.Id into emp
                             from lItemEmployee in emp.DefaultIfEmpty()
                             join lItemBank in unitOfWork.BankRepository.GetAll() on rItem.EmployeeId equals lItemBank.Id into bk
                             from lItemBank in bk.DefaultIfEmpty()
                             select new SaleVM
                             {
                                 Id = rItem.Id,
                                 InvoiecNo = rItem.InvoiecNo,
                                 BranchId = rItem.BranchId,
                                 Discount = rItem.Discount,
                                 TotalDiscount = rItem.TotalDiscount,
                                 Due = rItem.Due,
                                 TotalPaid = rItem.TotalPaid,
                                 NetTotal = rItem.NetTotal,
                                 Date = rItem.Date,
                                 VAT = rItem.VAT,
                                 CustomerName = lItemCustomer?.Name ?? "",
                                 Customer = lItemCustomer,
                                 Employee = lItemEmployee,
                                 BankName = lItemBank?.BankName ?? "",
                             }).OrderByDescending(e => e.Id).ToList();
            return saleItems;
        }
        public dynamic GetProductDetail(int id)
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var data = commonRepository.GetBySpWithParam("SP_Sales", new object[] { 1, (int)id, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value });
            return data;
        }
        public dynamic GetSaleDetail(int id)
        {
            var data = commonRepository.GetBySpWithParam("SP_Sales", new object[] { 4, (int)id, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value });
            SaleVM Sale = Converters.ConvertDataTable<SaleVM>(data).ToList().FirstOrDefault();
            var datadetail = commonRepository.GetBySpWithParam("[dbo].[SP_SaleDetail]", new object[] { 3, DBNull.Value, (int)id, DBNull.Value, DBNull.Value });
            Sale.SalesDetails = Converters.ConvertDataTable<SalesDetailVM>(datadetail);
            return Sale;
        }
        public dynamic GetSalesDue()
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var data = commonRepository.GetBySpWithParam("SP_Sales", new object[] { 3, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value });
            return data;
        }
        public dynamic GetSalesDueCustomerWise()
        {
            PurchaseVM purchaseVM = new PurchaseVM();
            var data = commonRepository.GetBySpWithParam("SP_Sales", new object[] { 5, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value });
            return data;
        }
        public dynamic GetInvoice(string InvoiceNo)
        {
            var data = unitOfWork.SaleRepository.Get(e => e.InvoiecNo.Contains(InvoiceNo)).Select(e => new { Value = e.Id, Text = e.InvoiecNo }).Take(10).ToList(); ;
            return data;
        }
        public SaleVM Get(long Id)
        {
            SaleVM saleVM = new SaleVM();
            var saleItems = unitOfWork.SaleRepository.Get(Id);

            saleVM.Id = saleItems.Id;
            saleVM.InvoiecNo = saleItems.InvoiecNo;
            saleVM.CustomerId = saleItems.CustomerId;
            saleVM.EmployeeId = saleItems.EmployeeId;
            saleVM.BankId = saleItems.BankId;
            saleVM.BranchId = saleItems.BranchId;
            saleVM.PaymentType = saleItems.PaymentType;
            saleVM.Discount = saleItems.Discount;
            saleVM.TotalDiscount = saleItems.TotalDiscount;
            saleVM.Due = saleItems.Due;
            saleVM.TotalPaid = saleItems.TotalPaid;
            saleVM.NetTotal = saleItems.NetTotal;
            saleVM.Return = saleItems.Return;
            saleVM.Date = saleItems.Date;
            saleVM.ChequeNo = saleItems.ChequeNo;
            saleVM.IsArchived = saleItems.IsArchived;
            saleVM.IsDeleted = saleItems.IsDeleted;
            saleVM.AddBy = saleItems.AddBy;
            saleVM.AddDate = saleItems.AddDate;
            saleVM.UpdateBy = saleItems.UpdateBy;
            saleVM.UpdateDate = saleItems.UpdateDate;
            saleVM.Remarks = saleItems.Remarks;
            saleVM.Status = saleItems.Status;
            saleVM.SalesDetails = SalesDetailService.GetAllSalesDetailBySalesId((int)Id);
            return saleVM;
        }
        public void Add(SaleVM sale)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    TransectionHistoryVM transectionHistory = new TransectionHistoryVM();
                    Sales sales = new Sales();
                    var AllSale = unitOfWork.SaleRepository.GetAll() ?? new List<Sales>();
                    var Org = unitOfWork.OrganizationRepository.GetAll().FirstOrDefault();
                    
                    sale.Discount = sale.SalesDetails.Sum(e => e.Discount) + sale.TotalDiscount;
                    sale.Total = sale.SalesDetails.Sum(e => e.Quantity * e.SalsPrice);

                    sale.NetTotal = (sale.Total + sale.VAT) - (sale.SalesDetails.Sum(e => e.Discount) + sale.TotalDiscount) ?? 0;
                    if (sale.NetTotal > sale.TotalPaid)
                    {
                        sale.Due = sale.NetTotal - sale.TotalPaid;
                    }
                    if (sale.NetTotal < sale.TotalPaid)
                    {
                        sale.Return = sale.TotalPaid - sale.NetTotal;
                    }
                    sale.InvoiecNo = GenerateInvoice(AllSale.ToList());
                    sales = ConvertVM(sale);

                    unitOfWork.SaleRepository.Add(entity: sales);
                    sale.Id = sales.Id;
                    foreach (var item in sale.SalesDetails)
                    {
                        SalesDetails salesDetails = new SalesDetails();
                        salesDetails.ProductId = item.ProductId;
                        salesDetails.Quantity = item.Quantity;
                        salesDetails.Amount = item.Quantity * item.SalsPrice;
                        salesDetails.NetAmount = item.Quantity * item.SalsPrice - salesDetails.Discount ?? 0;
                        salesDetails.Discount = item.Discount;
                        salesDetails.SalesId = sales.Id;
                        salesDetails.UnitePrice = item.SalsPrice;

                        unitOfWork.SalesDetailRepository.Add(salesDetails);
                        StockVM stocks = new StockVM();
                        stocks.BranchId = sale.BranchId;
                        stocks.CustomerId = sale.CustomerId;
                        stocks.Date = sale.Date;
                        stocks.SalesId = sales.Id;
                        stocks.ProductId = salesDetails.ProductId;
                        stocks.Quantity = salesDetails.Quantity;
                        stocks.Discount = item.Discount;
                        StockService.AddStockProduct(stocks, false);
                    }

                    transectionHistory.AccountId = unitOfWork.AccountNameRepository.GetAll(e => e.RootId == 1 && e.PayNreciveId == sale.CustomerId).FirstOrDefault().PayNreciveId ?? 0;
                    transectionHistory.InvoiceNo = sale.InvoiecNo;
                    transectionHistory.BankId = sale.BankId;
                    transectionHistory.Date = DateTime.Now;
                    transectionHistory.IsRecive = true;
                    transectionHistory.ReciveAmount = sale.NetTotal - sale.Due;
                    transectionHistory.PaymentType = sale.PaymentType;
                    transectionHistory.Remarks = "Sales Pos";

                    transectionHistory.AddDate = DateTime.Now;
                    transectionHistoryService.Add(transectionHistory);
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(SaleVM sale)
        {
            Sales sales = new Sales();
            var AllSale = unitOfWork.SaleRepository.GetAll(e => e.Id != sale.Id && e.InvoiecNo == sale.InvoiecNo);
            if (AllSale.Any())
            {
                throw new Exception("Sale Invoice Exist");
            }
            sales = ConvertVM(sale);

            unitOfWork.SaleRepository.Update(entity: sales);
            unitOfWork.Save();
        }
        public void ChangeStatus(Sales sale)
        {
            sale.UpdateDate = DateTime.Now;
            sale.UpdateBy = HttpContext.Current.User.Identity.Name;
            sale.Status = sale.Status == "A" ? "D" : "A";
            unitOfWork.SaleRepository.Update(entity: sale);
            unitOfWork.Save();
        }
        public void Delete(Sales sale)
        {
            StockVM stockvm = new StockVM();
            TransectionHistory transectionHistory = new TransectionHistory();
            var sal = Get((long)sale.Id);
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    unitOfWork.SaleRepository.Delete(sal.Id);
                    foreach (var item in sal.SalesDetails)
                    {
                        var aa = unitOfWork.SalesDetailRepository.GetAll(e => e.Id == item.Id).ToList();
                        var dd = SalesDetailService.Get(item.Id);
                        if (unitOfWork.SalesDetailRepository.GetAll(e => e.Id == item.Id).Any())
                        {
                            unitOfWork.SalesDetailRepository.Delete(item.Id);
                            stockvm = new StockVM
                            {
                                BranchId = sal.BranchId,
                                Date = sal.Date,
                                ProductId = item.ProductId,
                                PurcheaseId = sal.Id,
                                Quantity = item.Quantity,
                                UnitPrice = item.UnitePrice,
                                CustomerId = sal.CustomerId
                            };
                            StockService.AddStockProduct(stockvm, true);
                        }
                    }

                    var date = Convert.ToDateTime(sal.Date);
                    transectionHistory = unitOfWork.TransectionHistoryRepository.Get(e => e.AccountId == sal.CustomerId && e.Date == date).FirstOrDefault();
                    if (transectionHistory != null)
                    {
                        unitOfWork.TransectionHistoryRepository.Delete(transectionHistory.Id);
                    }
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GenerateInvoice(List<Sales> saleslist)
        {
            //var setting = unitOfWork.SettingRepository.GetAll().FirstOrDefault();
            var sales = new Sales();
            if (saleslist.Count == 0)
            {
                sales = new Sales();
            }
            else {
                sales.InvoiecNo = saleslist.Last().InvoiecNo;
            }
            Sales a = new Sales();
            var data = string.Empty;

            if (String.IsNullOrWhiteSpace(sales.InvoiecNo))
            {
                    data = (20).ToString() + DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("00") +
                        DateTime.Now.Day.ToString("00") + (1).ToString("0000");
            }
            else
            {
                if (sales.InvoiecNo.Length > 6)
                {
                    data = (20).ToString() + DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + (Convert.ToInt32(sales.InvoiecNo.Substring(8, 4)) + 1).ToString("0000");
                }
                else
                {
                    data = (Convert.ToInt32(sales.InvoiecNo) + 1).ToString("000000");
                }
            }
            return data.ToString();
        }
        public SalesDetailVM GetStockDetailByProduct(int BranchId, int ProductId, decimal Qty)
        {
            SalesDetailVM _SalesDetails = new SalesDetailVM();
            var ProductDetail = unitOfWork.SaleRepository.getResponseBySpWithParam("[dbo].[SP_Stock]", new object[] { BranchId, ProductId }).results;
            var pro = Converters.ConvertDataTable<StockVM>(ProductDetail).FirstOrDefault();
            _SalesDetails.ProductName = pro.Code + "~" + pro.Name;
            _SalesDetails.ProductId = ProductId;
            _SalesDetails.Quantity = Qty;
            _SalesDetails.UnitePrice = pro.SalsPrice ?? 00;
            _SalesDetails.Discount = (pro.IsPercentage == true ? (_SalesDetails.TotalPrice * pro.Discount) / 100 : pro.Discount) ?? 00;
            _SalesDetails.TotalPrice = (Qty * pro.SalsPrice) - _SalesDetails.Discount;
            return _SalesDetails;
        }
        private Sales ConvertVM(SaleVM sale)
        {
            Sales sales = new Sales();
            sales.EmployeeId = sale.EmployeeId;
            sales.CustomerId = sale.CustomerId;
            sales.InvoiecNo = sale.InvoiecNo;
            sales.BankId = sale.BankId;
            sales.BranchId = sale.BranchId;
            sales.PaymentType = sale.PaymentType;
            sales.Discount = sale.Discount;
            sales.TotalDiscount = sale.TotalDiscount;
            sales.Due = sale.Due;
            sales.TotalPaid = sale.TotalPaid;
            sales.NetTotal = sale.NetTotal;
            sales.Date = sale.Date;
            sales.VAT = sale.VAT;
            sales.Return = sale.Return;
            sales.ChequeNo = sale.ChequeNo;
            sales.IsArchived = sale.IsArchived;
            sales.IsDeleted = sale.IsDeleted;
            sales.Remarks = sale.Remarks;
            sales.Status = sale.Status;
            sales.BankId = sale.BankId;
            sales.AddDate = DateTime.Now;
            sales.AddBy = HttpContext.Current.User.Identity.Name;
            sales.Status = "A";
            return sales;
        }
        public dynamic GetSalesReportData(SelseReportVM selseReportVM)
        {
            object[] param = new object[6];
            param[0] = 1;
            param[1] = selseReportVM.InvoiceNo;
            param[2] = selseReportVM.CustomerId;
            param[3] = selseReportVM.BranchId;
            param[4] = null;
            param[5] = null;
            if (selseReportVM.From != null)
                param[4] = Convert.ToDateTime(selseReportVM.From);
            if (selseReportVM.To != null)
                param[5] = Convert.ToDateTime(selseReportVM.To);
            DataTable dt = new DataTable();
            dt = commonRepository.GetBySpWithParam("SP_SalesReport", param);
            return dt;
        }
    }

public partial class _Default : System.Web.UI.Page
{

    private const string SERVER_IP = "192.168.111.112";// put your ip address
    private const int PORT = 80;
    private const string WEB_DOMAIN_PATH = @"F:\\web\domains\{0}\";

    //Live server
    //private const string SERVER_IP = "192.168.111.111";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["user"]))
        {

            try
            {
                string username = Request.QueryString["user"];
                string status = CreateUserSite(username, "codeproject.com");//change your Domain id

                Response.Write(status);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        else
        {
            Response.Write("user parameter not supplied");
        }


    }


    private string CreateUserSite(string user, string domain)
    {

        string path = string.Format(WEB_DOMAIN_PATH, domain);

        string userpath = path + user;

        string userUrl = user + "." + domain;

        using (ServerManager serverManager = new ServerManager())
        {

            bool siteExists = false;
            int number = serverManager.Sites.Where(p => p.Name.ToLower().Equals(userUrl.ToLower())).Count();

            if (number == 0)
            {
                siteExists = false;
            }
            else
            {
                siteExists = true;
            }

            if (!siteExists)
            {

                //create user directory
                Directory.CreateDirectory(userpath);

                //copy every files from a-base to a new created folder
                FileInfo[] d = new DirectoryInfo(path + @"\a-base").GetFiles();
                foreach (FileInfo fi in d)
                {
                    File.Copy(fi.FullName, userpath + @"\" + fi.Name, true);
                }

                //create a directory
                Directory.CreateDirectory(userpath + @"\swfobject");

                FileInfo[] d1 = new DirectoryInfo(path + @"\a-base\swfobject").GetFiles();
                foreach (FileInfo fi in d1)
                {
                    File.Copy(fi.FullName, userpath + @"\swfobject\" + fi.Name, true);
                }



                //create site
                Site mySite = serverManager.Sites.Add(userUrl, path + user, PORT);
                mySite.ServerAutoStart = true;
                mySite.Applications[0].ApplicationPoolName = domain;

                //create bindings
                mySite.Bindings.Clear();
                mySite.Bindings.Add(string.Format("{0}:{2}:{1}", SERVER_IP, userUrl, PORT), "http");
                mySite.Bindings.Add(string.Format("{0}:{2}:www.{1}", SERVER_IP, userUrl, PORT), "http");


                    Microsoft.Web.Administration.Configuration config = serverManager.GetApplicationHostConfiguration();
                    Microsoft.Web.Administration.ConfigurationSection httpLoggingSection = config.GetSection("system.webServer/httpLogging", userUrl);
                httpLoggingSection["dontLog"] = true;

                serverManager.CommitChanges();

                // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "success", "alert('" + userUrl + " created');", true);

            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "error", "alert('user exists. Please use other name');", true);
                throw new Exception("user exists. Please use other name");
            }


            return userUrl + " has been successfully created";
        }
    }
}
}