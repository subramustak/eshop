using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
 
namespace eshop.ApplicationService.CRM
{
 public class SalesDetailsReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
   public ICollection<SalesDetailsReturnVM> GetAllSalesDetailsReturn()
   {
    var salesdetailsreturnItems = (from rItem in unitOfWork.SalesDetailsReturnRepository.GetAll().OrderBy(r => r.Id)
                                  select new SalesDetailsReturnVM
                                 {
      Id = rItem.Id,
      SalesId = rItem.SalesId,
      ProductId = rItem.ProductId,
      Discount = rItem.Discount,
      UnitePrice = rItem.UnitePrice,
      Quantity = rItem.Quantity,
      Amount = rItem.TotalAmount,
                                    }).ToList();
    return salesdetailsreturnItems;
    }
    
    
    public SalesDetailsReturnVM Get(long Id)
    {
     SalesDetailsReturnVM salesdetailsreturnVM = new SalesDetailsReturnVM();
    var salesdetailsreturnItems = unitOfWork.SalesDetailsReturnRepository.Get(Id);
salesdetailsreturnVM.Id = salesdetailsreturnItems.Id ;
salesdetailsreturnVM.SalesId = salesdetailsreturnItems.SalesId ;
salesdetailsreturnVM.ProductId = salesdetailsreturnItems.ProductId ;
salesdetailsreturnVM.Discount = salesdetailsreturnItems.Discount ;
salesdetailsreturnVM.UnitePrice = salesdetailsreturnItems.UnitePrice ;
salesdetailsreturnVM.Quantity = salesdetailsreturnItems.Quantity ;
salesdetailsreturnVM.Amount = salesdetailsreturnItems.TotalAmount ;
    return salesdetailsreturnVM;
    }
        public ICollection<SalesDetailsReturnVM> GetAllSalesReturnDetailBySalesId(int SalesId)
        {
            var salesdetailItems = (from rItem in unitOfWork.SalesDetailsReturnRepository.GetAll(e => e.SalesId == SalesId).OrderBy(r => r.Id)
                                    join lItemproduct in unitOfWork.ProductRepository.GetAll() on rItem.ProductId equals lItemproduct.Id into pProduct
                                    from lItemproduct in pProduct.DefaultIfEmpty()
                                    select new SalesDetailsReturnVM
                                    {
                                        Id = rItem.Id,
                                        SalesId = rItem.SalesId,
                                        ProductId = rItem.ProductId,
                                        ProductName = lItemproduct.Name,
                                        Discount = rItem.Discount,
                                        UnitePrice = rItem.UnitePrice,
                                        Quantity = rItem.Quantity,
                                        Amount = rItem.TotalAmount,
                                    }).ToList();
            return salesdetailItems;
        }
        public void Add(SalesDetailsReturn salesdetailsreturn) {
      unitOfWork.SalesDetailsReturnRepository.Add(entity: salesdetailsreturn);
     unitOfWork.Save();
    }
    
    public void Update(SalesDetailsReturn salesdetailsreturn) {
      unitOfWork.SalesDetailsReturnRepository.Update(entity: salesdetailsreturn);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(SalesDetailsReturn salesdetailsreturn) {
      unitOfWork.SalesDetailsReturnRepository.Update(entity: salesdetailsreturn);
     unitOfWork.Save();
    }
    
    public void Delete(SalesDetailsReturn salesdetailsreturn) {
      unitOfWork.SalesDetailsReturnRepository.Delete(Id: salesdetailsreturn.Id);
      unitOfWork.Save();
    }
    
    }
}
