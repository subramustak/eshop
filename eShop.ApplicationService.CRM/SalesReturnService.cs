using System;
using eShop.Infrastructure.Data.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eShop.BusinessDomain.CRM.ViewModels;
using eShop.BusinessDomain.CRM.DomainObject;
using eShop.BusinessDomain.Account.DomainObject;
using System.Transactions;

namespace eshop.ApplicationService.CRM
{
 public class SalesReturnService
  {
   UnitOfWork unitOfWork = new UnitOfWork();
        StockService StockService = new StockService();
        public ICollection<SalesReturnVM> GetAllSalesReturn()
   {
    var salesreturnItems = (from rItem in unitOfWork.SalesReturnRepository.GetAll().OrderBy(r => r.Id)
                            join lItemCustomer in unitOfWork.CustomerRepository.GetAll() on rItem.CustomerId equals lItemCustomer.Id into cus
                            from lItemCustomer in cus.DefaultIfEmpty()
                            select new SalesReturnVM
                                 {
      Id = rItem.Id,
      InvoiecNo = rItem.InvoiecNo,
      CustomerId = rItem.CustomerId,
      EmployeeId = rItem.EmployeeId,
      BankId = rItem.BankId,
      BranchId = rItem.BranchId,
      PaymentType = rItem.PaymentType,
      DiscountRemarks = rItem.DiscountRemarks,
      Discount = rItem.Discount,
      TotalDiscount = rItem.TotalDiscount,
      Due = rItem.Due,
      Total = rItem.Total,
      Return = rItem.Return,
      NetTotal = rItem.NetTotal,
      Date = rItem.Date,
      ChequeNo = rItem.ChequeNo,
                                    }).ToList();
    return salesreturnItems;
    }
    
    
    public SalesReturnVM Get(long Id)
    {
     SalesReturnVM salesreturnVM = new SalesReturnVM();
    var salesreturnItems = unitOfWork.SalesReturnRepository.Get(Id);
salesreturnVM.Id = salesreturnItems.Id ;
salesreturnVM.InvoiecNo = salesreturnItems.InvoiecNo ;
salesreturnVM.CustomerId = salesreturnItems.CustomerId ;
salesreturnVM.EmployeeId = salesreturnItems.EmployeeId ;
salesreturnVM.BankId = salesreturnItems.BankId ;
salesreturnVM.BranchId = salesreturnItems.BranchId ;
salesreturnVM.PaymentType = salesreturnItems.PaymentType ;
salesreturnVM.DiscountRemarks = salesreturnItems.DiscountRemarks ;
salesreturnVM.Discount = salesreturnItems.Discount ;
salesreturnVM.TotalDiscount = salesreturnItems.TotalDiscount ;
salesreturnVM.Due = salesreturnItems.Due ;
salesreturnVM.Total = salesreturnItems.Total ;
salesreturnVM.Return = salesreturnItems.Return ;
salesreturnVM.NetTotal = salesreturnItems.NetTotal ;
salesreturnVM.Date = salesreturnItems.Date ;
salesreturnVM.ChequeNo = salesreturnItems.ChequeNo ;
salesreturnVM.IsArchived = salesreturnItems.IsArchived ;
salesreturnVM.IsDeleted = salesreturnItems.IsDeleted ;
salesreturnVM.AddBy = salesreturnItems.AddBy ;
salesreturnVM.AddDate = salesreturnItems.AddDate ;
salesreturnVM.UpdateBy = salesreturnItems.UpdateBy ;
salesreturnVM.UpdateDate = salesreturnItems.UpdateDate ;
salesreturnVM.Remarks = salesreturnItems.Remarks ;
salesreturnVM.Status = salesreturnItems.Status ;
            
    return salesreturnVM;
    }
    
    public void Add(SalesReturnVM salesreturn) {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    StockVM stockvm = new StockVM();
                    TransectionHistory transectionHistory = new TransectionHistory();
                    SalesReturn salesReturn = new SalesReturn();
                    salesReturn.InvoiecNo = salesreturn.InvoiecNo;
                    salesReturn.CustomerId = salesreturn.CustomerId;
                    salesReturn.EmployeeId = salesreturn.EmployeeId;
                    salesReturn.BankId = salesreturn.BankId;
                    salesReturn.BranchId = salesreturn.BranchId;
                    salesReturn.PaymentType = salesreturn.PaymentType;
                    salesReturn.DiscountRemarks = salesreturn.DiscountRemarks;
                    salesReturn.Discount = 0;
                    salesReturn.Due = salesreturn.Due;
                    salesReturn.Total = salesreturn.Total;
                    salesReturn.Return = salesreturn.Return;
                    salesReturn.NetTotal = salesreturn.NetTotal;
                    salesReturn.Date = salesreturn.Date;
                    salesReturn.ChequeNo = salesreturn.ChequeNo;
                    salesReturn.AddDate = DateTime.Now;
                    salesReturn.AddBy = HttpContext.Current.User.Identity.Name;
                    salesReturn.Status = "A";
                    unitOfWork.SalesReturnRepository.Add(entity: salesReturn);
                    salesreturn.Id = salesReturn.Id;
                    foreach (var item in salesreturn.SalesDetails.Where(e => e.Checked == true))
                    {
                        SalesDetailsReturn salesDetailsReturn = new SalesDetailsReturn();
                        salesDetailsReturn.SalesId = salesReturn.Id;
                        salesDetailsReturn.ProductId = item.ProductId;
                        salesDetailsReturn.Quantity = item.Return ?? 0;
                        salesDetailsReturn.UnitePrice = item.SalsPrice;
                        salesDetailsReturn.TotalAmount = item.SalsPrice * item.Return;
                        unitOfWork.SalesDetailsReturnRepository.Add(entity: salesDetailsReturn);
                        stockvm = new StockVM();
                        stockvm.BranchId = salesReturn.BranchId;
                        stockvm.ProductId = item.ProductId;
                        stockvm.SalesReturnId = salesReturn.Id;
                        stockvm.Quantity = item.Return ?? 0;
                        stockvm.UnitPrice = item.SalsPrice;
                        stockvm.CustomerId = salesReturn.CustomerId;
                        StockService.AddStockProduct(stockvm, true);

                    }
                    transectionHistory.AccountId = unitOfWork.AccountNameRepository.GetAll(e => e.RootId == 1 && e.PayNreciveId == salesreturn.CustomerId).FirstOrDefault().PayNreciveId ?? 0;
                    transectionHistory.InvoiceNo = salesReturn.InvoiecNo;
                    transectionHistory.BankId = salesReturn.BankId;
                    transectionHistory.Date = Convert.ToDateTime(salesReturn.Date);
                    transectionHistory.IsRecive = false;
                    transectionHistory.IsPayment = true;
                    transectionHistory.PaymentAmount = salesReturn.NetTotal;
                    transectionHistory.PaymentType = salesReturn.PaymentType ?? 0;
                    transectionHistory.AddDate = DateTime.Now;
                    transectionHistory.Remarks = "Sales Return";
                    unitOfWork.TransectionHistoryRepository.Add(transectionHistory);
                    unitOfWork.Save();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    public void Update(SalesReturnVM salesreturn) {
            SalesReturn salesReturn = new SalesReturn();
            salesReturn.InvoiecNo = salesreturn.InvoiecNo;
            salesReturn.CustomerId = salesreturn.CustomerId;
            salesReturn.EmployeeId = salesreturn.EmployeeId;
            salesReturn.BankId = salesreturn.BankId;
            salesReturn.BranchId = salesreturn.BranchId;
            salesReturn.PaymentType = salesreturn.PaymentType;
            salesReturn.DiscountRemarks = salesreturn.DiscountRemarks;
            salesReturn.Discount = salesreturn.Discount;
            salesReturn.TotalDiscount = salesreturn.TotalDiscount;
            salesReturn.Due = salesreturn.Due;
            salesReturn.Total = salesreturn.Total;
            salesReturn.Return = salesreturn.Return;
            salesReturn.NetTotal = salesreturn.NetTotal;
            salesReturn.Date = salesreturn.Date;
            salesReturn.ChequeNo = salesreturn.ChequeNo;
            salesreturn.UpdateDate = DateTime.Now;
            salesReturn.UpdateBy = HttpContext.Current.User.Identity.Name;
            salesReturn.Status = "A";
      unitOfWork.SalesReturnRepository.Update(entity: salesReturn);
     unitOfWork.Save();
    }
    
    public void ChangeStatus(SalesReturn salesreturn) {
      salesreturn.UpdateDate = DateTime.Now;
       salesreturn.UpdateBy = HttpContext.Current.User.Identity.Name;
       salesreturn.Status = salesreturn.Status== "A" ? "D" : "A";
      unitOfWork.SalesReturnRepository.Update(entity: salesreturn);
     unitOfWork.Save();
    }
    
    public void Delete(SalesReturn salesreturn) {
      unitOfWork.SalesReturnRepository.Delete(Id: salesreturn.Id);
      unitOfWork.Save();
    }
    
    }
}
