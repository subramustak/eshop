﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.Core.Helpers
{
    public class CommonRepository 
    {
        public string ConStr
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["eShopDB"].ToString();
            }

        }
        public CommonResponse getResponseBySp(string SpName)
        {
            DataTable result = SqlHelper.ExecuteDataTable(ConStr, SpName, null);
            CommonResponse cr = new CommonResponse(result.Rows.Count > 0 ? HttpStatusCode.Accepted : HttpStatusCode.NoContent);
            cr.pageno = 0;
            cr.totalcount = 0;
            cr.totalSum = 0;
            cr.Message = string.Empty;

            List<object> _obj = new List<object>();
            //_obj.Add(result.Rows[0]);
            //foreach (DataRow row in result.Rows[0])
            //{
            ////    _obj.Add(row.Table); // = result.AsEnumerable().Select(e => e.Table).ToList();
            ////}

            //foreach (DataRow row in result.Rows)
            //{
            //    T item = GetItem<T>(row);
            //    data.Add(item);
            //}
            //cr.Results = ConvertDataTable<object> result.AsEnumerable().ToList();

            //cr.Results = result.Rows[0].Table..AsEnumerable().ToList();
            cr.results = result;
            return cr;
        }

        public CommonResponse getResponseBySpWithParam(string SpName, params object[] parameterValues)
        {
            try
            {
                //DataTable result = SqlHelper.ExecuteDataTable(ConStr, SpName, parameterValues);
                DataTable result = SqlHelper.ExecuteDataTable(ConStr, SpName, parameterValues);
                CommonResponse cr = new CommonResponse(result.Rows.Count > 0 ? HttpStatusCode.Accepted : HttpStatusCode.NoContent);
                cr.pageno = 0;
                cr.totalcount = 0;
                cr.totalSum = 0;
                cr.Message = string.Empty;


                cr.results = result;
                return cr;
            }
            catch (Exception ex)
            {

                CommonResponse cr = new CommonResponse(HttpStatusCode.BadRequest);
                cr.results = null;
                cr.HasError = true;
                cr.Message = ex.Message;
                return cr;
            }
           
        }

        public DataTable GetBySp(string SpName)
        {
            try
            {
                DataTable result = SqlHelper.ExecuteDataTable(ConStr, SpName);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }
         
        public DataTable GetBySpWithParam(string SpName, params object[] parameterValues)
        {
            try   
            {
                //SqlHelper.SqlDbType.Structured
                DataTable result = SqlHelper.ExecuteDataTable(ConStr, SpName, parameterValues);
                return result;
            }
            catch (Exception ex)
            {                    
                throw ex;
            }
          
        }  
        public CommonResponse getDatasetResponseBySp(string SpName, params object[] paramValues)
        {
            try
            {

                DataSet result = SqlHelper.ExecuteDataset(ConStr, SpName, paramValues);
                CommonResponse cr = new CommonResponse(result.Tables.Count > 0 ? HttpStatusCode.OK : HttpStatusCode.NoContent);
                cr.results = result;
                cr.pageno = 0;
                cr.totalcount = 0;
                cr.totalSum = 0;
                cr.Message = string.Empty;
                cr.results = result;
                return cr;
            }
            catch (Exception ex)
            {

                CommonResponse cr = new CommonResponse(HttpStatusCode.BadRequest);
                cr.results = null;
                cr.HasError = true;
                cr.Message = ex.Message;
                return cr;
            }

        }
        public object GetScalarValueBySP(string SPName)
        {
            object objValue;
            try
            {
                objValue = SqlHelper.ExecuteScalar(ConStr, CommandType.StoredProcedure, SPName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objValue;
        }

        public DataTable GetDatatableBySQL(string SQL)
        {
            DataTable objValue;
            try
            {
                objValue = SqlHelper.ExecuteDataTable(ConStr, CommandType.Text, SQL);
            }
            catch (Exception ex)
            {
                throw ;
            }
            return objValue;
        }
        public dynamic ExecuteSQL(string SQL)
        {
            dynamic objValue;
            try
            {
                objValue = SqlHelper.ExecuteNonQuery(ConStr, CommandType.Text, SQL);
            }
            catch (Exception ex)
            {
                throw;
            }
            return objValue;
        }

        public dynamic ExecuteSQL(string SQL, params SqlParameter[] commandParameters)
        {
            dynamic objValue;
            try
            {
                objValue = SqlHelper.ExecuteNonQuery(ConStr, CommandType.Text, SQL,commandParameters);
            }
            catch (Exception ex)
            {
                throw;
            }
            return objValue;
        }
        public bool IsExist(string TableName,string Col, string value)
        {
            bool isExist = true;
            DataTable dtResults = new DataTable();
            try
            {
                string strSQL = "SELECT TOP 1 "+ Col +" FROM " + TableName + " WHERE " + Col + " = " + "'"+value+"'";
                dtResults = SqlHelper.ExecuteDataTable(ConStr, CommandType.Text, strSQL);
                isExist = dtResults.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                isExist = true;
            }
            return isExist;
        }
        public bool IsExist(string TableName, string Col, int value)
        {
            bool isExist = true;
            DataTable dtResults = new DataTable();
            try
            {
                string strSQL = "SELECT TOP 1 " + Col + " FROM " + TableName + " WHERE " + Col + " = " + value ;
                dtResults = SqlHelper.ExecuteDataTable(ConStr, CommandType.Text, strSQL);
                isExist = dtResults.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                isExist = true;
            }
            return isExist;
        }
        public bool IsExist(string TableName, string WhereClause)
        {
            bool isExist = true;
            DataTable dtResults = new DataTable();
            try
            {
                string strSQL = "SELECT TOP 1 * FROM " + TableName + " WHERE " + WhereClause;
                dtResults = SqlHelper.ExecuteDataTable(ConStr, CommandType.Text, strSQL);
                isExist = dtResults.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                isExist = true;
            }
            return isExist;
        }
        public bool WriteLog(int Vid, int SId, int ShId, int ClassId, int GroupId, int MainExamId,string PID, string Msg, string AddBy)
        {
          
            string strSQL = @"INSERT INTO [dbo].[Res_ExamProccessLog]
           ([VersionID],[SessionId],[ShiftID],[ClassId],[GroupId],[MainExamId],[PId],[LogTime],[Msg],[AddBy])
     VALUES(" + Vid + "," + SId + "," + ShId + "," + ClassId + "," + GroupId + "," + MainExamId + ",'" + PID + "',GETDATE(),'" + Msg + "','" + AddBy + "' )";
            bool isExist = true;
            DataTable dtResults = new DataTable();
            try
            {
              
                dtResults = SqlHelper.ExecuteDataTable(ConStr, CommandType.Text, strSQL);
                isExist = dtResults.Rows.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isExist;
        }

       

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName]==DBNull.Value?null: dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
