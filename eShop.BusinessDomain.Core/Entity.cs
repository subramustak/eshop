﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace eShop.BusinessDomain.Core
{
    public  class Entity
    {
        int? RequestedHashCode;
        [Key]
        public int Id { get; set; }
        public bool IsArchived { get; set; }

        public bool IsTransient()
        {
            return this.Id > 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Entity))
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            Entity item = (Entity)obj;

            if (item.IsTransient() || this.IsTransient())
                return false;
            else
                return item.Id == this.Id;
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!RequestedHashCode.HasValue)
                {
                    RequestedHashCode = this.Id.GetHashCode() ^ 31; // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)
                }
                return RequestedHashCode.Value;
            }
            else
            {
                return base.GetHashCode();
            }
        }

        public static bool operator ==(Entity left, Entity right)
        {
            if (Object.Equals(left, null))
                return (Object.Equals(right, null)) ? true : false;
            else
                return left.Equals(right);
        }

        public static bool operator !=(Entity left, Entity right)
        {
            return !(left == right);
        }

        public static int GenerateId()
        {
            return Convert.ToInt32(Guid.NewGuid());
        }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        [StringLength(128)]
        public string AddBy { get; set; }

        public DateTime? AddDate { get; set; }

        [StringLength(128)]
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        [StringLength(100)]
        public string Remarks { get; set; }
        [StringLength(20)]
        public string Status { get; set; } = "A";

        public void SetDate()
        {
            if (AddDate == null)
            {
                AddDate = DateTime.Now;
            }
            if (!String.IsNullOrEmpty(UpdateBy) && UpdateDate == null)
            {
                UpdateDate = DateTime.Now;
            }
        }

        public virtual IEnumerable<ValidationResult> Validate()
        {
            return EntityValidator.ValidateEntity(this);
        }
    }

    public class EntityValidator
    {
        public static IEnumerable<ValidationResult> ValidateEntity<T>(T entity) where T : Entity
        {
            return new EntityValidation<T>().Validate(entity);
        }
    }
    public class EntityValidation<T> where T : Entity
    {
        public IEnumerable<ValidationResult> Validate(T entity)
        {
            var validationResults = new List<ValidationResult>();
            var validationContext = new ValidationContext(entity, null, null);
            Validator.TryValidateObject(entity, validationContext, validationResults, true);
            return validationResults;
        }
    }
}