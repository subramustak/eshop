﻿

namespace eShop.BusinessDomain.Core
{
    public class MenuSelectVM
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string type { get; set; }
        public  State state { get; set; }
        
    }

    public class State
    {
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
    }
}
