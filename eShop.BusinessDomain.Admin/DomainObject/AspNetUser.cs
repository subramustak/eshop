
namespace eShop.BusinessDomain.Admin.DomainObject
{
    using eShop.BusinessDomain.Core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class AspNetUser:Entity
    {
        
        public AspNetUser()
        {
            this.AspNetRoles = new HashSet<AspNetRole>();
        }
        public string FullName { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public byte[] Image { get; set; }
        public string Address { get; set; }
        public int? DatabaseId { get; set; }
        public virtual ICollection<AspNetRole> AspNetRoles { get; set; }
    }
}
