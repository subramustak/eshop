﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.Admin.DomainObject
{
   public class DatabaseDetail
    {
        [Key]
        public int Id { get; set; }
        public string CoCode { get; set; }
        public string DatabaseName { get; set; }
        public string DBName { get; set; }
        public string DBSource { get; set; }
        public string DBUserName { get; set; }
        public string DBPassword { get; set; }
    }
}
