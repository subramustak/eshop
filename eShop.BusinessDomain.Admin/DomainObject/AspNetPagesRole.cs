﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.Admin.DomainObject
{
  public  class AspNetPagesRole
    {
        [Key]
        public int PageRoleId { get; set; }
        public int PageId { get; set; }
        [ForeignKey("AspNetRole")]
        public string RoleId { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanView { get; set; }
        public bool CanDelete { get; set; }
        public bool CanApprove { get; set; }
        //public AspNetPage AspNetPage { get; set; }
        public AspNetRole AspNetRole { get; set; }
    }
}
