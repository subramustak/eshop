﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.BusinessDomain.Admin.DomainObject
{
  public  class AspNetPage
    {
        public AspNetPage()
        {
            this.AspNetPagesRoles = new HashSet<AspNetPagesRole>();
        }
        [Key]
        public int PageID { get; set; }
        public int ParentID { get; set; }
        public string NameOption_En { get; set; }
        public string NameOption_Bn { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Area { get; set; }
        public string IconClass { get; set; }
        public string ActiveLi { get; set; }
        public string Status { get; set; }
        public string ModuleName { get; set; }
        public int Displayorder { get; set; }
        public Nullable<bool> IsParent { get; set; }
        public ICollection<AspNetPagesRole> AspNetPagesRoles { get; set; }
    }
}
