using System;
namespace eShop.BusinessDomain.Admin.DomainObject
{
    public class AspNetUserRoleVM
    {
      public string UserId { get; set; }
      public string RoleId { get; set; }
    }
}
