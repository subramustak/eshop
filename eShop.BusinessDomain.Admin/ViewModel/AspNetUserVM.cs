using System;
namespace eShop.BusinessDomain.Admin.ViewModels
{
    public class AspNetUserVM
    {
      public string Id { get; set; }
      public string Email { get; set; }
      public bool EmailConfirmed { get; set; }
      public string PasswordHash { get; set; }
      public string SecurityStamp { get; set; }
      public string PhoneNumber { get; set; }
      public bool PhoneNumberConfirmed { get; set; }
      public bool TwoFactorEnabled { get; set; }
      public DateTime LockoutEndDateUtc { get; set; }
      public bool LockoutEnabled { get; set; }
      public int AccessFailedCount { get; set; }
      public string UserName { get; set; }
      public string RoleName { get; set; }

      public string Address { get; set; }
      public bool IsArchived { get; set; }
      public bool IsDeleted { get; set; }
      public string AddBy { get; set; }
      public DateTime AddDate { get; set; }
      public string UpdateBy { get; set; }
      public DateTime UpdateDate { get; set; }
      public string Remarks { get; set; }
      public string Status { get; set; }
      public string FullName { get; set; }
      public string MobileNo { get; set; }
    }
}
